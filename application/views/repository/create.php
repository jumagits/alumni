     <div id="page-wrapper">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-8">
              <div class="panel ">
               <h4>
                  Add New Document 
                  <a href='<?php echo URL;?>repository' class='btn pull-right btn-sm btn-danger'><i class='fa fa-angle-double-left'></i> Back </a>
               </h4>
               <hr>
           
               <div class="panel-body">
              <div class='row'>
                  <div class='col-sm-12'><?php if(isset($feedback)){echo $feedback;}?></div>
              </div>
              <form action="<?php echo URL;?>repository/add-document" method="post" enctype="multipart/form-data">
             
                     <div class="form-group">
                        <label for="email">Document Title :</label>
                        <input type="text" name='title' class="form-control" required autocomplete="off">
                      </div>
                      <div class="form-group">
                        <label for="pwd">Document Notes / Description / Extra Notes :</label>
                       <textarea id='trumbowyg' class='form-control' name='description'></textarea>
                      </div>
                      <div class="form-group">
                        <label for="pwd"><i class='fa fa-group'></i> Share With :</label>
                        <select class='form-control select2 select2-multiple' name='sharewith[]' id='shareWith' data-placeholder="Choose people to share with ...." multiple="multiple">                           
                           <?php echo !empty($userList) ? $userList : ""; ?>
                        </select>
                      </div>

                       <div class="form-group">
                       <label for="email"></label>
                        <input type="file" name="files[]" id="filer_input"required class="form-control">
                      </div>

                      <div class="form-group">                        
                        <button type="submit" class='btn btn-primary btn-block' name='upload' >Upload document(s)</button>
                      </div>
                  </div>
              </div>
            
              </form>
          </div>
        </div>
      </div>
    </div>       
 </div>
</div>


