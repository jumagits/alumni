  <div id="page-wrapper">
      <div class="container-fluid">
          <div class="row">
              <div class="col-lg-12">
                  <h4>Preview</h4>
              </div>
          </div>
        <div class="panel panel-default">
          <div class="panel-body">
             <div class="panel-heading">
                <h3 class="panel-title"><i class='fa fa-copy'></i> Document Preview - <strong> <?php if(isset($docid)){echo urldecode($docid);}?> </strong><span class='pull-right'><a href='<?php echo URL;?>repository/'><i class="fa fa-angle-double-left"></i> Back to Documents</a></span></h3>
              </div>
              <hr>
              <div class='row'>
                  <div class='col-sm-9'>
                     <div class="panel-body" style="width:300px !important;">
                        <?php if(isset($preview)){echo $preview;}?>
                     </div>
                  </div>
                  <div class='col-sm-3'>
                      <span class='text-warning'>Details &amp; Actions</span>
                      <hr>
                      <ul>
                          <li><p><strong>Name : </strong><?php echo $docDetails['title'];?></p></li>
                          <li><p><strong>Size : </strong></p></li>
                      </ul>
                      <p>&nbsp;</p>
                      <blockquote><?php echo $docDetails['description'];?></blockquote>
                      <hr>
                      <fieldset class='edits'>
                          <legend>Actions</legend>
                          <div class='text-center'>
                              <a href='<?php echo URL;?>repository/dl/<?php echo $docDetails['slug'];?>'><button class='btn btn-info'><i class='fa fa-cloud-download'></i></button></a>
                              <button class='btn btn-success'><i class='fa fa-share-alt'></i></button>
                              <a href='<?php echo URL;?>repository/trash/<?php echo $docDetails['slug'];?>' class='delete'><button class='btn btn-danger'><i class='fa fa-trash-o'></i></button></a>
                          </div>
                      </fieldset>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>