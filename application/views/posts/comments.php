  <div id="page-wrapper">
      <div class="container-fluid">        
          <div class="panel">
           <h4> <?php echo $pageTitle; ?> </h4><hr>
              <div class="panel-body " >
                 <div class="row">
                  <div class="col-lg-12">
                  <div class="table-responsive ">
                      <form action="" method="POST">
                          <table class="table table-hover table-bordered table-striped" id="datatable1" width="100%">
                              <thead>
                                  <tr>
                                      <th>#</th>
                                      <th>Post</th>
                                      <th>Comment</th>
                                      <th>Author</th>
                                      <th>Status </th>
                                      <th>Date</th>
                                      <th>Approve</th>
                                      <th>Unapprove</th>
                                  </tr>
                              </thead>
                              <tbody>
                                  <?php $count = 0;foreach ($all_comments as $key => $comment) {  $count++;?>
                                  <tr>
                                      <td><?php echo $count; ?></td>
                                      <td><?php  echo $comment->post_title;  ?></td>
                                      <td><?php  echo $comment->comment_content;  ?></td>
                                      <td><?php echo $comment->comment_author;  ?></td>
                                      <td>
                                          <div class="btn-group">
                                              <button class="btn btn-xs btn-warning"><?php echo $comment->comment_status; ?> </button>
                                          </div>
                                      </td>

                                      <td><?php echo f_date(  $comment->comment_date);  ?></td>
                                      <td>

                                          <a class="btn  btn-success"
                                              href="<?php echo URL; ?>post/approve/<?php echo base64_encode($comment->comment_id); ?>">Approve</a>

                                      </td>
                                      <td>
                                          <a class="btn  btn-danger"
                                              href="<?php echo URL; ?>post/unapprove/<?php echo base64_encode($comment->comment_id); ?>">Unapprove</a>

                                      </td>
                                  </tr>

                                  <?php } ?>
                              </tbody>
                          </table>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
  <?php 

function f_date($date){
   return date_format(date_create($date), ' l jS F Y'); 
}

 ?>