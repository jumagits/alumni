     <div id="page-wrapper">


            <div class="container-fluid">

		            	   <div class="row">
		            	         <div class="col-lg-12">
				                       <h4 class="page-header bg-primary">
				                        Hey <?php echo !isset($user_info) ? header('URL'): $user_info['names'];
				                                ?>, Welcome to <?php echo $pageTitle; ?> 
				                          </h4>  
				                  </div>

				               </div>

				             <?php foreach ($post as $key => $p) {} ?>

				                <div class="panel">
				                    	<div class="panel-heading bg-primary">

				                    		<h4>Update Post</h4>
				                    		
				                    	</div>


				                <div class="panel-body">
				                    		
				                <form id="edit-post"  action="<?php echo URL; ?>post/update" >

				                	<input type="hidden" name="id" value="<?php echo $p->post_id; ?>">
						
								<div  class="form-group">
								<label  for="post_title">Post Title</label>
								<input type="text" name="post_title" class="form-control" value="<?php echo $p->post_title; ?>" required>
								</div>	


								<div  class="form-group" >
								<label for="cat_title">Category</label>
								<select name="post_category_id" id="" class="form-control" >

									<option value="<?php echo $p->post_category_id; ?>" selected="" ><?php echo $category->cat_by_name($p->post_category_id); ?></option>
								<?php foreach ($all_cats as $key => $category) { ?> 

								    <option value='<?php echo $category->cat_id; ?>'><?php echo $category->cat_title; ?></option>

							    <?php } ?>

								</select>
								</div>

								<div  class="form-group">
								<label  for="post_author">Post Author</label>
								<input type="text" name="post_author" class="form-control"  value="<?php echo $p->post_author; ?>" required>
								</div>

								

								<div  class="form-group">
								<label  for="post_image">Post image</label>
								<input type="file" name="photo" class="form-control">
								</div>

								<div  class="form-group">
								<label  for="post_tags">Post Tags</label>
								<input type="text" name="post_tags" class="form-control"  value="<?php echo $p->post_tags; ?>" required>
								</div>



								<div  class="form-group">
								<label  for="post_content">Post Content</label><br>
								<textarea class="form-control text-jqte " name="post_content" id="" cols="30" rows="10"><?php echo $p->post_content; ?></textarea>
								</div>


								<div>
								<input type="submit" class="btn btn-primary" name="submit" value="Update Post">
								</div>
						</form>

				                    	</div>
				                    </div>

				  


						 </div>
                        </div>



                        <script>


                        	jQuery(document).ready(function($) {
                        		

                        	     $("form#edit-post").submit(function(e) {

										e.preventDefault();
										var formData = new FormData(this);
										var action = $('form#edit-post').attr('action');

										$.ajax({
										     url: action,
										     type: 'POST',
										     data: formData,                    
										     error: function(jqXHR, textStatus, errorThrown) {
										         console.log(jqXHR);
										     },
										     cache: false,
										     contentType: false,
										     processData: false
										 })
										 .done(function(res) {
										      alert(res.msg);

										         // swal({
										         //     title: "Good job!",
										         //     text: res.msg,
										         //     type: "success",
										         //     showCancelButton: !0,
										         //     confirmButtonClass: "btn btn-success",
										         //     cancelButtonClass: "btn btn-danger m-l-10"
										         // })

										         setTimeout(function() {

										             window.location.href="<?php echo URL ?>post";

										         }, 1000)
										 });

                                  });
                        	});	
    
  
                      </script>
                  

