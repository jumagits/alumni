  <div id="page-wrapper">


            <div class="container-fluid">

            <!-- Page Heading -->
                 
                   <!--    header end -->

                     <?php

                           global $config;
                          
                          @$connection = new mysqli($config['db_host'], $config['db_username'], $config['db_password'],$config['db_name']); 

                          $message = '';

                          if (isset($_POST['checkboxarray'])) {

                          foreach ($_POST['checkboxarray'] as $checkboxvalue) {

                          $bulkOptions = $_POST['bulkOptions'];

                          switch ($bulkOptions) {

                          case 'Published':

                                $query = "UPDATE posts SET post_status = '{$bulkOptions}' WHERE post_id = $checkboxvalue";
                                $Published_query = mysqli_query($connection,$query);
                                if ($Published_query) {

                                    echo "<script> 
                                     alert('Successifully Published');
                                     setTimeout(function(){
                                       window.location.href = '".URL."/post';
                                      },200);
                                     </script>";
                                }
                          break;

                          case 'Drafted':
                                $query = "UPDATE posts SET post_status = '{$bulkOptions}' WHERE post_id = $checkboxvalue";
                                $drafted_query = mysqli_query($connection,$query);
                                if ($drafted_query) {
                                   
                                    
                                     echo "<script> 
                                     alert('Successifully Drafted');
                                     setTimeout(function(){
                                       window.location.href = '".URL."/post';
                                      },200);
                                     </script>";
                                }
                                     
                          
                          break;

                          case 'Deleted':

                                $query = "DELETE FROM posts WHERE post_id = $checkboxvalue";
                                $delete_query = mysqli_query($connection,$query);
                                if ($delete_query) {
                                    
                                     echo "<script> 
                                     alert('Successifully Deleted');
                                     setTimeout(function(){
                                       window.location.href = '".URL."/post';
                                      },200);
                                     </script>";
                                     
                                }

                          break;



                          default:
                          # code...
                          break;
                          }}}


        ?>
                
                        <div class="panel">

                           <h4> POSTS  <a href="<?php echo URL ?>post/add-post" class="btn btn-danger pull-right">Add new</a>
                                </h4><hr>

                            <div class="panel-body ">

                                 <div class="row">

                                       <div class="col-lg-12">                   

                                           <p >  <?php  echo $message; ?></p>
                                              <div class="table-responsive ">

                                                <form action="" method="POST">
                                                  
                                                  <table class="table table-hover table-bordered table-striped" id="datatable1" width="100%">

                                                      <div id="bulkOptionscontainer" class="col-xs-4" style="padding: 0px">
                                                          <select class="form-control" name="bulkOptions" id="">
                                                              <option>Select Options</option>
                                                              <option value="Deleted">Delete</option>
                                                              <option value="Published">Publish</option>
                                                              <option value="Drafted">Draft</option>
                                                            
                                                          </select>
                                                    </div>

                                                    <div class="col-xs-4">
                                                      <input type="submit" name="submit" class="btn btn-success" value="Apply"> 
                                                    </div>

                                                
                                                      <thead>
                                                          <tr>
                                                           
                                                              <th><input type="checkbox"  id="selectAllcheckboxes" ></th>
                                                             
                                                              <th>Status</th>

                                                              <th>Title</th>

                                                              <th>Category</th>

                                                              <th>Tags</th>

                                                              <th>
                                                               (View/Comments)

                                                              </th>
                                                              <th>Author</th>
                                                              <th>Date</th>
                                                              <th><i class="fa fa-edit"></i> Edit</th>  
                                                                
                                                             
                                                          </tr>
                                                      </thead>
                                                      <tbody>


                                                          <?php 

                                                          $count = 0;

                                                  foreach ($all_posts as $key => $post) {  $count++;?>


                                                      <tr>
                                                          <td>
                                                              <input type='checkbox' class='checkboxes'  name='checkboxarray[]' value='<?php  echo $post->post_id; ?>'>
                                                          </td>
                                                       

                                                          <td><?php echo strtolower( $post->post_status); ?></td>

                                                           <td><?php echo strtolower( $post->post_title); ?></td>
                                                           <td><?php echo strtolower( $post->cat_title); ?></td>

                                                           <td><?php echo $post->post_tags; ?></td>

                                                           <td>
                                                             <div class="btn-group">
                                                               <button class="btn btn-xs btn-default"><?php echo $post->post_views_count;?></button>
                                                               <button class="btn btn-xs btn-default"><?php echo $post->post_comment_count;?></button>
                                                             </div>
                                                           </td>

                                                            <td><?php echo $post->post_author; ?></td>
                                                             <td><?php echo f_date($post->post_date); ?>
                                                                 
                                                             </td>
                                                             <td><a class="btn btn-xs btn-danger" href="<?php echo URL; ?>post/show/<?php echo base64_encode($post->post_id); ?>">Edit</a>
                                                                 
                                                             </td>
                                                            
                                                               
                                                      </tr>

                                                      <?php } ?>

                                                          
                                                      </tbody>
                                                  </table>
                                              </div>
                                              </form>
                                          </div>

                                           </div>
                                      </div>
                         
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


  




    


<?php 

function f_date($date){
   return date_format(date_create($date), ' l jS F Y'); 
}

 ?>