     <div id="page-wrapper">
         <div class="container">
           <div class="row">
             <div class="col-lg-8 col-md-8 m-auto">
                 <div class="panel">
                      <h4>Create Post</h4><hr>
                     <div class="panel-body">
                        
                         <form action="<?php echo URL; ?>post/create" id="add_post">

                             <div class="form-group">
                                 <label for="post_title">Post Title</label>
                                 <input type="text" name="post_title" class="form-control">
                             </div>
                             <div class="form-group">
                                 <label for="cat_title">Category</label>
                                 <select name="post_category_id" id="" class="form-control">

                                     <option value="#" selected="" disabled="">--Select Category --</option>
                                     <?php foreach ($all_cats as $key => $category) { ?>

                                     <option value='<?php echo $category->cat_id; ?>'><?php echo $category->cat_title; ?>
                                     </option>

                                     <?php } ?>

                                 </select>
                             </div>                       
                             <div class="form-group">
                                 <label for="post_author">Post Author</label>
                                 <input type="text" name="post_author" class="form-control">
                             </div>

                             <div class="form-group">
                                 <label for="post_image">Post image</label>
                                 <input type="file" name="photo" class="form-control">
                             </div>
                                

                             <div class="form-group">
                                 <label for="post_tags">Post Tags</label>
                                 <input type="text" name="post_tags" class="form-control">
                             </div>

                             <div class="form-group">
                                 <label for="post_content">Post Content</label><br>
                                 <textarea class="form-control text-jqte " name="post_content" id="" cols="30"
                                     rows="10"></textarea>
                             </div>

                             <div>
                                 <input type="submit" class="btn btn-primary" name="Create_Post" value="Publish Post">
                             </div>
                         </form>

                     </div>
                 </div>

             </div>
          </div>
       </div>
     </div>

