<!-- #364258 LEFT COLOR = #fff-->
<!-- #f5f6fa WRAPPER COLOR = #333-->

<style>
.fab {
    padding: 20px 20px 20px 20px;
    cursor: pointer;
    border-radius: 10px !important;
    background-color: navy;    
    text-align: center;
}
.fab .font-12{
    font-size: 20px;
     color: #fff;
    font-weight: bolder;
}
.fab .font-weight-400{
    font-size: 30px;
    padding-top: 20px;
    font-weight: bold;
    color: #fff;
}
.bg-purple{
    background-color: #6d1458;
}

.bg-primary{
    background-color: #000 !important;
}

.bg-success{
    background-color: #212e5a !important;
}
</style>

<div id="page-wrapper" >
    <div class="container-fluid">  
        <div class="row">
            <div class="col-md-4">
                <a href="<?php echo URL ?>register-clients/members">
                <div class="panel fab bg-purple">
                    <div class="panel-body">
                        <div class="d-flex align-items-center justify-content-between">                            
                                <span
                                    class="d-block font-12 font-weight-500 text-dark text-uppercase mb-5">ALUMNI
                                </span><br>
                                <span
                                    class="d-block display-6 font-weight-400 text-dark"><?php echo $count_clients; ?>
                                </span>  
                        </div>
                    </div>
                </div>
              </a>
            </div>

            <div class="col-md-4">
                 <a href="<?php echo URL ?>register-clients/companies">
                <div class="panel fab bg-primary">
                    <div class="panel-body">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span
                                    class="d-block font-12 font-weight-500 text-dark text-uppercase mb-5">WELL WISHERS</span><br>
                                <span class="d-block display-6 font-weight-400 text-dark"><?php echo isset($count_companies_all) ? $count_companies_all : '0' ?></span>
                            </div>
                           
                        </div>
                    </div>
                </div>
               </a>
            </div>
            <div class="col-md-4">
                 <a href="<?php echo URL ?>job">
                <div class="panel fab bg-info">
                    <div class="panel-body">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span
                                    class="d-block font-12 font-weight-500 text-dark text-uppercase mb-5">JOBS</span><br>
                                <span
                                    class="d-block display-6 font-weight-400 text-dark"><?php echo $jobs_count; ?></span>
                            </div>
                            
                        </div>
                    </div>
                </div>
               </a>
            </div>
            <div class="col-md-4">
                 <a href="<?php echo URL ?>event">
                <div class="panel fab bg-success">
                    <div class="panel-body">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span
                                    class="d-block font-12 font-weight-500 text-dark text-uppercase mb-5">EVENTS</span><br>
                                <span class="d-block display-6 font-weight-400 text-dark"><?php echo isset($count_events) ? $count_events :''; ?></span>
                            </div>
                           
                        </div>
                    </div>
                </div>
              </a>
            </div>
            <div class="col-md-4">
                <div class="panel fab bg-danger">
                    <div class="panel-body">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span
                                    class="d-block font-12 font-weight-500 text-dark text-uppercase mb-5">Categories</span><br>
                                <span class="d-block display-6 font-weight-400 text-dark"><?php echo isset($count_all_categories) ? $count_all_categories: '0' ?></span>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                 <a href="<?php echo URL ?>post">
                <div class="panel fab">
                    <div class="panel-body">
                        <div class="d-flex align-items-center justify-content-between">
                            <div>
                                <span
                                    class="d-block font-12 font-weight-500 text-dark text-uppercase mb-5">POSTS</span><br>
                                <span
                                    class="d-block display-6 font-weight-400 text-dark"><?php echo $count_posts; ?></span>
                            </div>
                           
                        </div>
                    </div>
                </div>
               </a>
            </div>

        </div>


        <hr style="border:1px solid #6d1458;padding: 0px;margin: 0px;"><br>
       
            <div class="row">   
                <div class="col-lg-6">
                    <div class="panel  bord">                       
                        <div class="panel-body ">
                             <div id="bargraph" style="height: 365px !important;width:100% !important;"></div>                            
                        </div>
                    </div>
                </div>
                
                 <div class="col-lg-6">

                      <div class="panel bord">
            
                        <div class="panel-body ">
                            <table class="table">
                            <thead >
                                <tr>
                                    <th>CAMPUS</th>
                                    <th>MALE</th>
                                    <th>FEMALE</th>
                                     <th>TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                            if (isset($all_clinics)) {                                              
                              foreach ($all_clinics as $key => $clinic) {                                               
                              ?>
                                <tr>
                                    <td><?php echo substr($clinic->clinic_name, 0,10); ?></td>
                                  
                                    <td> <?php echo  $administrativeModel->get_male_gender_per_clinic($clinic->id);  ?>
                                    </td>
                                     <td> <?php echo  $administrativeModel->get_female_gender_per_clinic($clinic->id);  ?>
                                    </td>
                                     <td><?php echo  $administrativeModel->get_members_per_clinic($clinic->id);  ?>
                                    </td>
                                      
                                </tr>
                                <?php }} ?>
                                <tr>
                                    <td colspan="3">
                                        TOTAL ALUMNI
                                    </td>                                   
                                    <td>
                                        <?php echo $count_clients; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
              
                    
                </div>

            </div>
        </div>

    </div>
    <hr>