  <div id="page-wrapper">


      <div class="container-fluid">

          <!-- Page Heading -->
      
                

          <!--    header end -->

          <div class="panel">

              <div class="panel-heading bg-primary">
                  <h4> Edit Event <a href="<?php echo URL ?>event" class="pull-right btn btn-danger">Cancel</a></h4>
              </div>

              <div class="panel-body " >

               <div class="row">

              <div class="col-lg-12">


                  <form action="<?php echo URL ?>event/update" id="edit-event">

                      <input type="hidden" name="urlEnd" value="<?php echo URL ?>event">

                      <input type="hidden" name="created_by" value="<?php echo $user_info['names']; ?>">


                      <input type="hidden" name="id" value="<?php echo $event[0]->id; ?>">

                      <div class="form-group row">

                          <div class="col-md-6">
                              <label for="" class="control-label">Event Category</label>
                              <select name="category" id="" class="form-control">
                                  <option selected="" value="<?php echo $event[0]->category ?>">
                                      <?php echo $event[0]->category ?></option>
                                  <option value="upcoming">Upcoming</option>

                                  <option value="happening">Happening</option>

                                  <option value="Expired">Expired</option>
                              </select>
                          </div>
                          <div class="col-md-6">
                              <label for="" class="control-label">Event Location</label>
                              <input type="text" class="form-control" name="location"
                                  value="<?php echo $event[0]->location; ?>" required>
                          </div>
                      </div>

                      <div class="form-group row">
                          <div class="col-md-12">
                              <label for="" class="control-label">Event Title</label>
                              <input type="text" class="form-control" name="title"
                                  value="<?php echo $event[0]->title; ?>" required>
                          </div>
                      </div>
                      <div class=" row">
                          <div class="form-group col-md-6">
                              <label for="" class="control-label">Schedule</label>
                              <input type="date" class="form-control " name="schedule" required
                                  value="<?php echo $event[0]->schedule; ?>">
                          </div>

                          <div class=" col-md-6 form-group">
                              <label for="" class="control-label">Banner Image</label>
                              <input type="file" class="form-control" name="banner">
                          </div>

                      </div>
                      <div class="form-group row">
                          <div class="col-md-12">
                              <label for="" class="control-label">Description</label>
                              <textarea name="description" id="content" class="form-control text-jqte" cols="30"
                                  rows="5" required>
                                        <?php echo html_entity_decode($event[0]->content); ?>
                                </textarea>
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-md-12">
                              <button class="btn btn-lg btn-block btn-danger " type="submit" name="submit"> Edit
                                  Event</button>
                          </div>
                      </div>
                  </form>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>




  <!-- end of page -->


  <script>
jQuery(document).ready(function($) {

    $("form#edit-event").submit(function(e) {

        e.preventDefault();
        var formData = new FormData(this);
        var action = $('form#edit-event').attr('action');
        var urlEnd = $('input[name="urlEnd"]').val();

        $.ajax({
                url: action,
                type: 'POST',
                data: formData,
                success: function(res) {
                    swal({
                        title: "Good job!",
                        text: res.msg,
                        type: "success",
                        showCancelButton: !0,
                        confirmButtonClass: "btn btn-success",
                        cancelButtonClass: "btn btn-danger m-l-10"
                    })

                    setInterval(function() {

                        window.location.href = urlEnd;

                    }, 2000);


                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                },
                cache: false,
                contentType: false,
                processData: false
            })
            .done(function() {
                console.log("success");
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });




    });

});
  </script>