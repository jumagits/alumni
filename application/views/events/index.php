  <div id="page-wrapper">
      <div class="container-fluid">
          <div class="panel ">

              <h4> Events
                      <a href="" data-toggle="modal" data-target=".bs-edit-modal-lg"
                          class="pull-right btn btn-danger"><i class="fa fa-plus"></i> Add Event</a>
                  </h4>
                  <hr>
              
              <div class="panel-body ">
               
                     <div class="table-responsive ">
                      <table class="table table-hover table-bordered table-striped" id="datatable1" width="100%">
                          <thead>
                           
                              <tr>
                                  <th>S/N</th>
                                  <th>Category</th>
                                  <th>Title</th>
                                  <th>Banner</th>
                                  <th>Shedule</th>
                                  <th><i class="fa fa-edit fa-2x"></i></th>
                                  <th><i class="fa fa-trash fa-2x"></i></th>


                              </tr>
                          </thead>
                          <tbody>


                              <?php 

                                $count = 0;

                                foreach ($all_events as $key => $event) {

                                $count++;

                                ?>
                              <tr>

                                  <td>
                                      <?php 
                                        echo $count;

                                        ?>
                                  </td>
                                  <td>
                                      <?php echo strtoupper($event->category); ?>

                                  </td>
                                  <td><span class="list-img">
                                          <?php echo $event->title; ?>
                                      </span>
                                  </td>
                                  <td>
                                    <a href="#">
                                      <img src="<?php echo URL.'data/'.date('Y').'/'. date('M').'/'.'events/'.$event->banner; ?>" alt="banner" width="70" height="60">


                                      </a>
                                  </td>



                                  <td>
                                      <?php echo date_format(date_create($event->schedule), ' l jS F Y'); ?>
                                  </td>
                                  <td><a class="btn btn-xs btn-info" href="<?php echo URL; ?>event/edit/<?php echo base64_encode($event->id); ?>">Edit</a></td>

                                  <td><a onclick="confirm('Are you sure to delete this event, it is Irriversible')" class="btn btn-xs btn-danger"
                                          href="<?php echo URL ?>event/delete-event/<?php echo base64_encode($event->id); ?>">Trash
                                              </a>
                                    </td>


                              </tr>

                              <?php } ?>

                          </tbody>
                      </table>
                  </div>
                </div>
              </div>
            </div>
          </div>


 


  











  <!--  Modal  for the above example -->
  <div class="modal fade bs-edit-modal-lg " role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"
      data-backdrop="false">
      <div class="modal-dialog modal-md">
          <div class="modal-content">
              <div class="modal-header bg-info">
                  <h4 class="modal-title " id="myLargeModalLabel">Register Event
                      <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
                  </h4>
              </div>
              <div class="modal-body">


                  <form action="<?php echo URL ?>event/add" id="manage-event">

                      <div class="form-group row">
                          <div class="col-md-6">
                              <label for="" class="control-label">Event Category</label> <br>
                              <select name="category" id="">
                                  <option selected="" value="#">--Select Event Category --</option>
                                  <option value="upcoming">Upcoming</option>
                                  <option value="happening">Happening</option>
                                  <option value="Expired">Expired</option>
                              </select>
                          </div>
                          <div class="col-md-6">
                              <label for="" class="control-label">Event Location</label>
                              <select name="location" id="">
                                  <option selected="" value="#">--Select Event Location --</option>
                                 <?php echo($clinic_drop_two); ?>
                              </select>
                          </div>
                      </div>

                      <div class="form-group row">
                          <div class="col-md-6">
                              <label for="created_by" class="control-label">Created By</label>
                              <input type="text" class="form-control" name="created_by"
                                  value="<?php echo $user_info['names']; ?>" required>
                          </div>
                          <div class="col-md-6">
                              <label for="" class="control-label">Event Title</label>
                              <input type="text" class="form-control" name="title" required>
                          </div>
                      </div>
                      <div class=" row">
                          <div class="form-group col-md-6">
                              <label for="" class="control-label">Schedule</label>
                              <input type="date" class="form-control " value="<?php echo date('Y-m-d H:i:s'); ?>" name="schedule" required autocomplete="off">
                          </div>

                          <div class=" col-md-6 form-group">

                              <label for="" class="control-label">Banner Image</label>
                              <input type="file" class="form-control" name="banner">
                          </div>

                      </div>
                      <div class="form-group row">
                          <div class="col-md-12">
                              <label for="" class="control-label">Description</label>
                              <textarea name="description" id="content" class="form-control text-jqte" cols="30"
                                  rows="5" required>

                            </textarea>
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-md-12">
                              <button class="btn btn-lg btn-block btn-primary " type="submit" name="submit"> Add
                                  Event</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <!-- edit model -->


  <!-- end edit model -->

  <script>
jQuery(document).ready(function($) {

    $("form#manage-event").submit(function(e) {

        e.preventDefault();
        var formData = new FormData(this);
        var action = $('form#manage-event').attr('action');

        $.ajax({
                url: action,
                type: 'POST',
                data: formData,
                success: function(res) {
                    swal({
                        title: "Good job!",
                        text: res.msg,
                        type: "success",
                        showCancelButton: !0,
                        confirmButtonClass: "btn btn-success",
                        cancelButtonClass: "btn btn-danger m-l-10"
                    })

                    window.location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                },
                cache: false,
                contentType: false,
                processData: false
            })

    });

});
  </script>

  L