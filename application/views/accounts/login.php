<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="BUMAA">
    <meta name="description" content="BUMAA">
    <meta name="author" content="BUMAA">
    <title>BUMAA</title>
    <link rel="icon" href="<?php echo URL ?>static/assets/img/favicon.png" type="image/x-icon" /> 
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
        href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144"
        href="img/apple-touch-icon-144x144-precomposed.png">
    <link href="<?php echo URL ?>static/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL;?>static/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet"type="text/css">
    <link href="<?php echo URL ?>static/assets/css/style.css" rel="stylesheet">
    <script src="<?php echo URL ?>static/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/common_scripts_min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/functions.js"></script>

    <!--[if lt IE 9]>
      <script src="<?php echo URL ?>static/assets/js/html5shiv.min.js"></script>
      <script src="<?php echo URL ?>static/assets/js/respond.min.js"></script>
    <![endif]-->

</head>
<style>
    html, body {margin: 0;padding: 0; outline: none;}
    body{
         padding-top: 6%;        
         background-color: #364258;
    }
    .well{
        box-shadow: 0 0 5px #d3d3d3;
        background-color: #364258;
    }.btn{border-radius: 0px;} .card{border-radius: 0px;margin: 0px;}  
</style>
<body> 
<div class="container">             
<div class="row" >                             
    <div class=" col-lg-8 col-sm-12 col-md-12 m-auto" >
    <div class="card-group row">
    <div class="card  col-md-6 col-lg-6 col-sm-12" style="background-color: #f5f6fa">
        <img src="<?php echo URL ?>static/assets2/images/dede.png" style="margin: auto;width: 70%;" alt="image">
          </div>
        <div class=" card col-md-6 col-lg-6 col-sm-12" style="background-color: #f5f6fa;padding: 30px;">
            <div class="card-header" style="background-color: #f5f6fa;">
                <h2 class="text-center ">
                    <a href="<?php echo URL ?>">
                    <img src="<?php echo URL ?>static/assets/img/log.png" alt="logo" style="width:200px;"></a>
            </h2>  <hr>             
         <h4 class="font-weight-bold text-center"><b>LOGIN</b></h4>
            </div>           

       
          <?php if(isset($doLoginFeedback)){echo $doLoginFeedback;}?>
                <form method="POST">
                    <div class="form-group ">
                       <label for="username">Username <span class="text-danger">*</span></label>
                        
                          <input type="text" required="" class="form-control a-input-text" name="username"  aria-label="username" aria-describedby="username">
                                                         
                    </div>
                    <div class="form-group">
                        <label for="password">Password <span class="text-danger">*</span></label>
                        
                          <input type="password" required="" class="form-control a-input-text" name="password"  aria-label="Password" aria-describedby="basic">
                       
                    </div>
                    <div class="form-group "> 
                        <button type="submit" name="login" class="btn   btn-primary pull-right btn-lg ">Login
                        </button>                                
                    </div>

                    <div class="form-group">
                           <a href="<?php echo URL ?>accounts/forgot_password"
                class="text-info font-weight-bold col-6">Forgot Password?</a>     
                    </div>
            </form>
             

          
              <hr>  
                  <blockquote>                               
                       
                        <a class=" btn btn-sm btn-danger"
                        href="<?php echo URL ?>accounts/alumni_register">Create Account</a> or 
                        <a class="btn btn-sm btn-dark"
                        href="<?php echo URL ?>">
                        Back</a>                              
                  </blockquote>
                 </div>
               </div>


            </div>
        </div>
    </div>
</body>
</html>