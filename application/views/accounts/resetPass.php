<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="college, campus, university, courses, school, educational">
    <meta name="description" content="BUMAA">
    <meta name="author" content="Ansonika">
    <title>BUMAA</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
        href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144"
        href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- BASE CSS -->
    <link href="<?php echo URL ?>static/assets/css/bootstrap.min.css" rel="stylesheet">
  <!--   <link href="<?php echo URL ?>static/assets/css/default.css" rel="stylesheet"> -->
    <link href="<?php echo URL ?>static/assets/css/style.css" rel="stylesheet">

    <script src="<?php echo URL ?>static/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/common_scripts_min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/functions.js"></script>
    <!--[if lt IE 9]>
      <script src="<?php echo URL ?>static/assets/js/html5shiv.min.js"></script>
      <script src="<?php echo URL ?>static/assets/js/respond.min.js"></script>
    <![endif]-->

</head>
<style>
    html, body {margin: 0; height: 100%; }
    body{
         padding-top: 5%;
    }
    .well{
        box-shadow: 0 0 5px #d3d3d3;
    }   
</style>
<body style="background-color: #fff;"> 
    <div class="container"> 
            <div class="row">                
                   <div class=" col-lg-8 col-sm-12 col-md-8 m-auto" >
                     <div class="card-group">
                        <div class="card well">
                            <img src="<?php echo URL ?>static/assets/img/join.png" style="width: 100%;height: 500px;" alt="image">
                        </div>                       
                        <div class="well card" style="background-color: #fafafa;padding: 30px;">
                         <h2 class="text-center "  style="margin-top: 0px;">
                            <a href="<?php echo URL ?>">
                                    <img src="<?php echo URL ?>static/assets/img/log.png" alt="logo" style="width:200px;"></a>
                            </h2> 
                            <h3 class="text-center">
                            Reset Password <br>
                            </h3>
                    
                   
                          <?php if(isset($feedBack)){echo $feedBack;}?>
                             <form class="form-horizontal med p-3 "  action="" method="post">
                            <div class="ui error message"></div>
                            <div class="form-group">
                                <div class="ui left icon input">
                                    <input type="text" name="token" id="token" class="form-control" placeholder="Security Token" autocomplete="off" required>
                                    <i class="chevron right icon"></i>
                                </div>
                            </div>
                          
                            <div class="form-group">
                                    <div class="ui left icon input">
                                        <input type="password" name="pass1" id="pass1" class="form-control" placeholder="New Password" autocomplete="off" required>
                                        <i class="lock icon"></i>
                                    </div>
                            </div>
                            <div class="form-group">
                                    <div class="ui left icon input">
                                        <input type="password" name="pass2" id="pass2" class="form-control" placeholder="Re-Enter New Password" autocomplete="off" required>
                                        <i class="lock icon"></i>
                                    </div>
                            </div>
                            
                            <button class="btn btn-primary w-md waves-effect waves-light" type="submit" name="continue">Continue</button>
                            <div class="ui  divider"></div>
                            <div class="ui grid">
                                <div class="center aligned sixteen wide column"><br>
                                    <p><a href='<?php echo URL;?>accounts/login'><i class="left double angle icon"></i>Back to Login?</a></p>
                                </div>
                            </div>
                         <p>Login Your Account <a class="badge badge-danger" href="<?php echo URL ?>accounts/login">Click here</a></p>
                                            </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
    <!-- External JavaScripts -->
    <script src="<?php echo URL ?>static/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/common_scripts_min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/functions.js"></script>

</body>

</html>