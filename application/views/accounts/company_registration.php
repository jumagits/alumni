<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="keywords" content="college, campus, university, courses, school, educational">
    <meta name="description" content="BUMAA">
    <meta name="author" content="BUMAA">
    <title>BUMAA</title>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
        href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144"
        href="img/apple-touch-icon-144x144-precomposed.png">
    <link href="<?php echo URL ?>static/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL ?>static/assets/css/base.css" rel="stylesheet">

   <!--  -->
    <link href="<?php echo URL ?>static/assets/css/style.css" rel="stylesheet">

    <script src="<?php echo URL ?>static/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/common_scripts_min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/functions.js"></script>

    <!--[if lt IE 9]>
      <script src="<?php echo URL ?>static/assets/js/html5shiv.min.js"></script>
      <script src="<?php echo URL ?>static/assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<style>
    html, body {margin: 0; height: 100%; }

    body{
        padding-top: 5%;
        outline: none !important; 
        background-color: #d8d8d8;
        
    }  

    .btn{
        border-radius: 0px;
        background-color: #000;
    }
   
   
</style>


<body>
 
       <div class="container">
          
                <div class="card" style="padding: 30px;" >

                     <div class="row text-center">
                        <div class="col-md-4">
                           <img src="<?php echo URL ?>static/assets/img/log.png" alt="logo" style="width:200px;">
                        </div>
                        <div class="col-md-4">
                             <h3 class="text-center"> COMPANY REGISTRATION </h3>
                          <p class="text-center">Already have an account?, <a class="badge badge-danger" href="<?php echo URL ?>accounts/login">Login</a></p>
                        </div>
                        <div class="col-md-4">
                            <img src="<?php echo URL ?>static/assets/img/busitema.png" alt="logo" style="width:200px;">
                        </div>
                    </div>
                             <hr style="border:1px solid #000">

                        <div class="fb"></div>

                <form class=""  id="registerC"
                        action="<?php echo URL; ?>frontend/company-register-in"
                            >


                      <div class="row"> 

                       <div class=" col-lg-4 col-sm-12 col-md-4">

                        <h3><b>Why Join Bumaa?</b></h3><hr>

                        <div class="blockquote " >
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore neque, sunt laboriosam corrupti debitis soluta consectetur dolorum deleniti architecto voluptatem, unde!
                        </div>

                       </div> 


                        <div class=" col-lg-4 col-sm-12 col-md-4">                 
                    

                            <div class="form-group ">

                                <label for="companyName">Company Name<span class="text-danger">*</span>
                                </label>
                                <input type="text" name="companyName" class="form-control a-input-text" required>
                            </div>

                            <div class="form-group ">
                                <label for="companyLocation">Company Location<span class="text-danger">*</span></label>
                                <input type="text" name="companyLocation" class="form-control a-input-text" required>
                            </div>

                            

                            <div class="form-group ">
                                <label for="companyEmail">Company Email.<span class="text-danger">*</span>
                                </label>
                                <input type="email" name="companyEmail" class="form-control a-input-text" required>
                                <small class="eerror"></small>
                            </div>

                        </div>

                        <div class=" col-lg-4 col-sm-12 col-md-4">


                           
                           <div class="form-group ">
                                <label for="password">Password<span class="text-danger">*</span>
                                </label>
                                <input type="password" name="password" class="form-control a-input-text" required>
                            </div>

                            <div class="form-group ">
                                <label for="cpassword">Confirm Password<span class="text-danger">*</span>
                                </label>
                                <input type="password" name="cpassword" class="form-control a-input-text" required>
                            </div>
                            


                            <div class="form-group">
                                <label for="about_company">About Company<span class="text-danger">*</span></label>

                                <input type="text" name="about_company" class="form-control">
                            </div>

                       </div>

                    </div> 


                       <div class="form-group row">

                        <div class="col-6">
                            <label for="terms">
                                <input type="checkbox" name="accept" id=""> Accept Terms and Conditions
                            </label>
                        </div>
                        <div class="col-6">
                            <button type="submit" name="login" class="btn btn-block btn-dark btn-lg">SUBMIT 
                            REGISTRATION
                        </button>
                        </div>

                        

                    </div>

                 </form>


                </div>

            </div>



    <!-- Common scripts -->
    <script src="<?php echo URL ?>static/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/common_scripts_min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/functions.js"></script>
    <script src="<?php echo URL ?>static/assets/validate.js"></script>
   
    


    <script>
    jQuery(document).ready(function($) {


        $("form#registerC").submit(function(e) {

            e.preventDefault();
            var formData = new FormData(this);
            var action = $('form#registerC').attr('action');

            $.ajax({
                url: action,
                type: 'POST',
                data: formData,
                success: function(res) {


                    $('.fb').html(res.msg);

                    setTimeout(function() {

                        window.location.reload();

                    }, 5000)

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                },
                cache: false,
                contentType: false,
                processData: false
            });


        });





    });
    </script>

</body>

</html>