   <div id="page-wrapper">
       <div class="container">
           <div class="row ">
               <div class="col-lg-5 m-auto">
                   <?php if(isset($form_feedback)){echo form_feedback;} ?>
                   <form id="updatePass" action="<?php echo URL; ?>accounts/update_pass" method="POST"
                       class=" panel">
                       <div class="panel-heading bg-primary">
                           <h4>CHANGE PASSWORD</h4>
                       </div>
                       <div class="panel-body" style="padding: 20px">
                           <div class="form-group">
                               <label for="example-text-input" class="col-form-label">Old Password</label>
                               <input class="form-control" type="password" name="oldpassword" placeholder="Old Password"
                                   required>
                           </div>
                           <div class="form-group">
                               <label for="example-text-input" class="col-form-label">New Password</label>
                               <input class="form-control" type="password" name="password1" placeholder="New Password"
                                   required>
                           </div>
                           <div class="form-group">
                               <label for="example-text-input" class="col-form-label">Retype Password</label>
                               <input class="form-control" type="password" name="password2" placeholder="Re type Password" required>
                           </div>
                           <button type="submit" name="changePassword"
                               class="btn btn-primary btn-lg waves-effect btn-block">Change Password</button>
                       </div>
                   </form>
               </div>
           </div>           
       </div>
   </div>