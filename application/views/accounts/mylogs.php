<div class="wrapper">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title"><?php if(isset($user_info['names'])){echo $user_info['names'];} ?> 's Activity</h4>
                </div>
            </div>
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- page-title-box -->
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20 card-red">
                        <div class="card-body"><?php #var_dump($user_info); ?>
                            <?php #echo $segment; ?>
                            
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button> 
                            <strong>Hello <?php if(isset($user_info['names'])){echo $user_info['names'];} ?>!</strong> This docket consists of all the user activity of uls IMS. enjoy</span></div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card">
                                            <div class="card-body">
                                                <table id="datatable-buttons" class="table table-striped table-bordered dt-responsive " style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                    <thead>
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Activity</th>
                                                            <th>Username</th>
                                                            <th>Clinic Name</th>
                                                            <th>Time And Day</th>
                                                            <th>Log Type</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php echo $logModel->myLogs($logs) ;?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                </div>
                            </div>
                            <!-- end row --> 
                        </div>
                        
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end page content-->
</div>

