<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="college, campus, university, courses, school, educational">
    <meta name="description" content="BUMAA">
    <meta name="author" content="Ansonika">
    <title>BUMAA</title>

    <!-- Favicons-->
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
        href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144"
        href="img/apple-touch-icon-144x144-precomposed.png">

    <!-- BASE CSS -->
    <link href="<?php echo URL ?>static/assets/css/bootstrap.min.css" rel="stylesheet">
  <!--   <link href="<?php echo URL ?>static/assets/css/default.css" rel="stylesheet"> -->
    <link href="<?php echo URL ?>static/assets/css/style.css" rel="stylesheet">

    <script src="<?php echo URL ?>static/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/common_scripts_min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/functions.js"></script>
    <!--[if lt IE 9]>
      <script src="<?php echo URL ?>static/assets/js/html5shiv.min.js"></script>
      <script src="<?php echo URL ?>static/assets/js/respond.min.js"></script>
    <![endif]-->

</head>
<style>
    html, body {margin: 0; padding: 0px;margin: 0px; }
     body{
         padding-top: 6%;        
         background-color: #364258;
    }
    .well{
        box-shadow: 0 0 5px #d3d3d3;
        background-color: #f5f6fa;
    }
      
</style>
<body > 
    <div class="container"> 
            <div class="row">                
                   <div class=" col-lg-8 col-sm-12 col-md-12 m-auto" >
                     <div class="card-group row">
                        <div class="card well col-md-6 col-lg-6 col-sm-12">
                             <img src="<?php echo URL ?>static/assets2/images/dede.png" style="margin: auto;width: 70%;" alt="image">
                        </div>                       
                        <div class="well card col-md-6 col-lg-6 col-sm-12" style="background-color: #f5f6fa;padding: 30px;">
                         <h2 class="text-center "  style="margin-top: 0px;">
                            <a href="<?php echo URL ?>">
                                    <img src="<?php echo URL ?>static/assets/img/log.png" alt="logo" style="width:200px;"></a>
                            </h2> 
                            <h3 class="text-center">
                            Forgot Password 
                            </h3><hr>
                             <div class="blockquote text-dark" >Enter your registered Email address and  reset instructions will be sent to the provided Email if it exists!</div>  
                               <?php echo isset($feedBack) ? $feedBack : ''; ?>
                    
                               <form class=" " method="POST"> 
                                    <div class="form-group">                   
                                           
                                            <input name="useremail" type="email" required="" placeholder="Your Email Address" class="form-control a-input-text">
                                    </div>
                                    <div class="form-group">
                                        <button name="reset" type="submit" value="Submit" class="btn btn-primary btn-block btn-lg">Submit</button>
                                    </div>
                                </form>
                                <p>Login Your Account <a class="badge badge-danger" href="<?php echo URL ?>accounts/login">Click here</a></p>
                            </div>
                     </div>
                 </div>
             </div>
         </div>
    <!-- External JavaScripts -->
    <script src="<?php echo URL ?>static/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/common_scripts_min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/functions.js"></script>

</body>

</html>