<hr>
<h5 class="mb-1"> ACCOUNT DEACTIVATION </h5>
<ul class="list-unstyled <?= $floatRight ?>">
  <li class="lead mb-2">
    <strong> What happens when you deactivate your account? </strong>
  </li>
  <li><i class="fa fa-hand-o-right"></i> Your profile and services won't be shown on <?php echo $site_name; ?> anymore. </li>
  <li><i class="fa fa-hand-o-right"></i> Any open orders will be canceled and refunded. </li>
  <li><i class="fa fa-hand-o-right"></i> You won't be able to re-activate your proposals/services. </li>
  <li><i class="fa fa-hand-o-right"></i> You won't be able to restore your account. </li>
</ul>
<div class="clearfix"></div>
<form method="post">
 
  <div class="form-group">
    <!-- form-group Starts -->
    <h5 class="pt-3 pb-3"> Please withdraw your revenues before deactivating your account. </h5>
  </div>
  <!-- form-group Ends -->
  <button type="submit" name="deactivate_account" disabled class="btn btn-danger <?= $floatRight ?>">
  <i class="fa fa-frown-o"></i> Deactivate Account
  </button>

  <div class="form-group">
    <label> Why Are You Leaving? </label>
    <select name="deactivate_reason" class="form-control" required>
      <option class="hidden"> Choose A Reason </option>
      <option> The quality of service was less than expected </option>
      <option>I just don't have the time</option>
      <option>I can’t find what I am looking for</option>
      <option>I had a bad experience with a seller / buyer</option>
      <option>I found the site difficult to use</option>
      <option>The level of customer service was less than expected</option>
      <option>I have another <?php echo $site_name; ?> account</option>
      <option>I'm not receiving enough orders</option>
      <option>Other</option>
    </select>
  </div>
  <button type="submit" name="deactivate_account" class="btn btn-danger <?= $floatRight ?>">
  <i class="fa fa-frown-o"></i> Deactivate Account
  </button>
   
</form>