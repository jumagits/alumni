<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="keywords" content="college, campus, university, courses, school, educational">
    <meta name="description" content="BUMAA">
    <meta name="author" content="BUMAA">
    <title>BUMAA</title>
    <link rel="icon" href="<?php echo URL ?>static/assets/img/favicon.png" type="image/x-icon" /> 
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
        href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144"
        href="img/apple-touch-icon-144x144-precomposed.png">
    <link href="<?php echo URL ?>static/assets/css/bootstrap.min.css" rel="stylesheet">
   <!--  -->
   <link href="<?php echo URL ?>static/assets/css/base.css" rel="stylesheet">

    <link href="<?php echo URL ?>static/assets/css/style.css" rel="stylesheet">

    <script src="<?php echo URL ?>static/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/common_scripts_min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/functions.js"></script>

    <!--[if lt IE 9]>
      <script src="<?php echo URL ?>static/assets/js/html5shiv.min.js"></script>
      <script src="<?php echo URL ?>static/assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<style>
    html, body {margin: 0;padding: 0;  }
   .btn{border-radius: 0px;}
    body{
      
        outline: none !important; 
         background-color: #364258;
     
    } 
</style>


<body>

 
       <div class="container">
       
          
                <div class=" card mb-5 p-5" >

                    <div class="row text-center">
                        <div class="col-md-4">
                           <img src="<?php echo URL ?>static/assets/img/log.png" alt="logo" style="width:200px;">
                        </div>
                        <div class="col-md-4">
                             <h3 class="text-center"> ALUMNI REGISTRATION </h3>
                          <p class="text-center">Already have an account?, <a class="badge badge-danger" href="<?php echo URL ?>accounts/login">Login</a> | <a class="badge badge-primary" href="<?php echo URL ?>">Return Home</a></p>
                        </div>
                        <div class="col-md-4">
                            <img src="<?php echo URL ?>static/assets/img/busitema.png" alt="logo" style="width:200px;">
                        </div>
                    </div>

                       

                           <hr style="border-top: 1px solid #000;"> 

                                       

                        <div class="fb"></div>

                        <p><span class="badge badge-danger">Note !</span>  Please note that for a person to be able to fill this form, He/she must have been studied from Busitema University and must also have been a member of BUMSA association at University. Thanks, <b>Management</b>.</p><br>

                        

                            <form class="dede " id="registerA" action="<?php echo URL ?>frontend/alumni-register-in">

                                <div class="row">

                                  


                               <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="username">FIRSTNAME<span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="firstname" class="form-control" required>
                                        <small class="error"></small>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="lastname">LASTNAME<span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="lastname" class="form-control" required>
                                        <small class="error"></small>
                                    </div>
                                </div>

                                 <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="username">USERNAME<span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="username" class="form-control" required>
                                        <small class="error"></small>
                                    </div>

                                </div>


                              <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="username">GENDER
                                            <span class="text-danger">*</span>
                                        </label>
                                        <select name="sex" class="form-control custom-select">
                                            <option value="#" disabled="" selected="">-- Select Gender --</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                        <small class="error"></small>
                                    </div>
                                </div>

                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="lastname">PHONE
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="phone" class="form-control" required>
                                        <small class="error"></small>
                                    </div>
                                </div>


                                 <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="email">EMAIL
                                             <span class="text-danger">*</span>
                                        </label><input type="text" name="email" class="form-control" required>
                                        <small class="eerror"></small>
                                    </div>
                                </div>                             
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                        <label for="clinic">CAMPUS <span class="text-danger">*</span></label>
                                        <select name="clinic" id="clinic" class="form-control custom-select">
                                            <option selected="" disabled="">-- Select Campus --</option>

                                            <?php foreach ($all_clinics as $key => $clinic) { ?>

                                            <option value="<?php echo $clinic->id ?>">
                                                <?php echo $clinic->clinic_name;  ?>
                                            </option>

                                            <?php } ?>
                                        </select>
                                    </div>
                                    </div>

                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="clinic">COURSES <span class="text-danger">*</span></label>
                                        <select name="course" id="course" class="form-control custom-select">
                                            <option selected="" disabled="">-- Select Course --</option>

                                            <?php foreach ($getCourses as $key => $c) { ?>

                                            <option value="<?php echo $c->id ?>">
                                                <?php echo $c->course_name;  ?>
                                            </option>

                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="form-group ">
                                        <label for="student_number">REGISTRATION No.<span
                                                class="text-danger">*</span>
                                        </label>

                                        <input type="text" name="student_number" class="form-control" required
                                            placeholder="Prepend a '0' or '00' or '000' on RegNo">
                                        <small class="nerror"></small>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="transcript_serial">TRANSCRIPT SERIAL No.
                                        <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="transcript_serial" class="form-control" required>
                                        <small class="error"></small>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="transcript_serial">FROM YEAR
                                        <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="from_year" class="form-control" required>
                                        <small class="error"></small>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="transcript_serial">UPTO YEAR
                                        <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="upto_year" class="form-control" required>
                                        <small class="error"></small>
                                    </div>

                                </div>




                                 <div class="col-md-4">
                                  <div class="form-group">
                                    <label for="knowabout">ABOUT WORK <span class="text-danger">*</span>
                                    </label>
                                    <input type="text"  name="knowabout"  class="form-control">
                                </div>
                            </div>

                           
                            

                              
                               <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="password">PASSWORD<span class="text-danger">*</span>
                                        </label>
                                        <input type="password" name="password" class="form-control" required>
                                        <small class="perror"></small>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                     <div class="form-group ">
                                        <label for="password"> COMFIRM PASSWORD<span class="text-danger">*</span>
                                        </label>
                                        <input type="password" name="cpassword" class="form-control" required>
                                        <small class="cperror"></small>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                          <button type="submit" name="login" class=" btn btn-dark btn-lg btn-block" >SUBMIT REGISTRATION</button>
                                    </div>
                                </div>

                            </div>


                              
                    
                         </form>
                

                    </div>

             
            </div>




    <script>
    jQuery(document).ready(function($) {


        $("form#registerA").submit(function(e) {

            e.preventDefault();
            var formData = new FormData(this);
            var action = $('form#registerA').attr('action');

            $.ajax({
                url: action,
                type: 'POST',
                data: formData,
                success: function(res) {

                    $html =
                        `<div class="alert alert-success"><strong>${res.msg}</strong></div>`;

                    $('.fb').html($html);

                    setTimeout(function() {
                        window.location.reload();
                    }, 4000);

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                },
                cache: false,
                contentType: false,
                processData: false
            });


        });




    });
    </script>

</body>

</html>