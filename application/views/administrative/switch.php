 <div id="page-wrapper">
     <div class="container-fluid">
         <div class="panel ">

            
             
             <div class="panel-body text-center">

                <h4>CLINIC SWITCH ACCESS </h4><hr>

                <p> Please Be Reminded That Once You Switch Access, You Lose Access To The Current Clinic Until You
                     Switch Back.</p>

               

                 <h4 class="mt-0 header-title text-white"><i class="fa fa-arrow-down"></i> Switch Here</h4>
                 <button data-toggle="modal" data-target=".user-add" class="btn btn-primary btn-lg "><i
                         class="fa fa-recycle"></i> Switch
                 </button>            

                 <hr>  
            
             <h5 class="text-center">Current Access Role(s) That You Can Switch Between</h5>
             <div class="table-responsive " style="margin: 30px;">
                 <table class="table tabble-striped table-bordered " id="datatable1">
                     <thead>
                         <tr>
                             <th>#</th>
                             <th><b>Clinic</b></th>
                             <th><b>Role</b></th>
                             <th>Make Default</th>
                         </tr>
                     </thead>
                     <tbody>
                         <?php $i=0; foreach ($user_roles as $key) { $i++; ?>
                         <tr>
                             <td><?php echo $i;   ?></td>
                             <td><?php echo $accounts_model->get_clinic($key->clinic_id)[0]->clinic_name; ?></td>
                             <td><?php echo $accounts_model->get_usercatergory($key->clinicrole)[0]->category_name; ?>
                             </td>
                             <td>
                                 <button type="button"
                                     onclick="makeDefault('<?php echo  $accounts_model->get_usercatergory($key->clinicrole)[0]->id; ?>','<?php echo $accounts_model->get_clinic($key->clinic_id)[0]->id; ?>');"
                                     class="btn btn-info">Make Default</button>
                             </td>
                         </tr>
                         <?php } ?>
                     </tbody>
                 </table>
             </div>
         </div>
         <!-- end row -->
     </div>

 </div>
 </div>

 <!--  Modal content for the above example -->
 <div class="modal fade user-add" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"
     data-backdrop="false">
     <div class="modal-dialog modal-md">
         <div class="modal-content">
             <div class="modal-header modal-header-primary">
                 <h5 class="modal-title mt-0" id="myLargeModalLabel">Switch Access</h5>
                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
             </div>
             <div class="modal-body">
                 <form id="switchclinic" action="<?php echo URL;?>register_clients/switch_clinic">
                     <div class="row">
                         <div class="col-md-12">
                             <div class="form-group">
                                 <label class="col-form-label">Select Clinic </label>
                                 <select name="clinicId_roleId" class="form-control select2">
                                     <?php foreach ($user_roles as $key) { ?>
                                     <option
                                         value="<?php echo $accounts_model->get_clinic($key->clinic_id)[0]->id."_".$accounts_model->get_usercatergory($key->clinicrole)[0]->id;?>">
                                         <?php echo $accounts_model->get_clinic($key->clinic_id)[0]->clinic_name." - ".$accounts_model->get_usercatergory($key->clinicrole)[0]->category_name; ?>
                                         <?php //echo $accounts_model->get_usercatergory($key->clinicrole)[0]->id; ?>
                                     </option>

                                     <?php } ?>
                                 </select>
                             </div>
                         </div>
                     </div>
                     <button type="submit"
                         class="btn btn-primary btn-lg btn-block waves-effect waves-light">Switch</button>
                 </form>
             </div>
         </div>
         <!-- /.modal-content -->
     </div>
     <!-- /.modal-dialog -->
 </div>
 <!-- /.modal -->


