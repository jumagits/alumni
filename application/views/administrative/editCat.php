<div id="page-wrapper">
    <div class="container-fluid">  
        <div class="panel">
            <div class="panel-heading bg-primary">
                <h4>USER CATEGORY EDIT</h4>
            </div>
            <div class="panel-body " style="padding: 30px;">
                <strong><?php if(isset($do_update_feedback)){echo $do_update_feedback;}?>
                </strong>
                <div class="col-md-8">
                    <div class="panel m-b-20 panel-yellow">
                        <div class="panel-body">
                            <form action="<?php echo URL; ?>essentials/editUserCat/<?php echo base64_encode($ucat['id']); ?>"
                                method='POST'>
                                <div class="col-md-12">
                                    <div class="form-group">

                                        <label for="example-text-input" class="col-form-label">Category
                                            Name</label>
                                        <input class="form-control" type="text" name="catName"
                                            value="<?php echo $ucat['category_name']; ?>"
                                            autocomplete="off" required>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel m-b-20 panel-yellow">
                        <div class="panel-body">
                            <h4 class="mt-0 header-title">Action</h4>
                            <button style="align:right;" class="btn btn-primary" name="updateCat"
                                type="submit">Update</button><br>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end col -->
</div>
<!-- end row -->
