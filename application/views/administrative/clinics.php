<div id="page-wrapper">

    <div class="container-fluid">
        <div class="row">

            <div class="col-lg-12">

        <div class="panel">

            <?php function dateF($date){
                return date_format(date_create($date), ' l jS F Y'); 
                }  ?> 
                 <h4> CLINICS <button data-toggle="modal" data-target=".user-add"  class="btn btn-danger pull-right"><i class="fas fas fa-user-plus"></i> Add Clinic</button>
                </h4>
                <hr>

            

            <div class="panel-body " >

                <div class="table-responsive">

                    <table id="datatable1" class="table table-striped table-bordered dt-responsive nowrap"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Clinic Name</th>
                                <th>Clinic Code</th>
                                <th>Clinic Color</th>
                                <th>Clinic Phone</th>
                                <th>created On</th>
                                <th>Updated On</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php $i =0; foreach ($clinic as $key) { $i++; ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td><?php echo $key->clinic_name; ?></td>
                                <td><?php echo $key->clinic_code; ?></td>
                                <td style="background-color: <?php echo $key->clinic_color; ?>"></td>
                                <td><?php echo $key->clinicPhone; ?></td>
                                <td><?php echo dateF($key->created_on); ?></td>
                                <td><?php echo dateF($key->updated_on); ?></td>
                                <td>
                                    <a
                                        href="<?php echo URL; ?>essentials/manage-clinic/<?php echo base64_encode($key->id); ?>" class="btn btn-danger">Edit
                                </td>

                                <td>
                                    <a href="<?php echo URL; ?>essentials/delete-clinic/<?php echo base64_encode($key->id); ?>"
                                        onclick="return confirm('Are you sure you want to Delete clinic?');"class="btn btn-danger">Delete </a>
                                </td>
                            </tr>
                            <?php  } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end row -->
</div>

</div>
<!--  Modal content for the above example -->
<div class="modal fade user-add" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
    data-backdrop="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header modal-header-primary">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add Clinic</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form id="addclinic" action="<?php echo URL;?>essentials/add-clinic">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Clinic Name</label>
                                <input class="form-control" type="text" name="clinicName" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Clinic Color</label>
                            <input class="form-control" type="color" name="cliniColor" value="" autocomplete="off"
                                required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="example-text-input" class="col-form-label">Clinic Phone</label>
                            <input class="form-control" type="text" name="clinicPhone"   autocomplete="off" required>
                        </div>
                    </div>
            </div>
            <button type="submit" class="btn btn-info btn-lg btn-block waves-effect waves-light">Add Clinic</button>
            </form>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
