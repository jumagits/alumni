<div id="page-wrapper">


    <div class="container-fluid">


        <div class="panel">

               <h4>

                    USER CATEGORIES

                    <button data-toggle="modal" data-target=".user-add" class="btn btn-primary pull-right"><i
                            class="fa fa-plus"></i> Add User Category
                    </button>

                </h4>
                <hr>



            <div class="panel-body " >
                <strong><?php if(isset($do_update_feedback)){echo $do_update_feedback;}?></strong>
                <div class="row">
                    <div class="col-md-12">

                        <div class="table-responsive">

                            <table id="datatable1" class="table table-striped table-bordered dt-responsive nowrap">
                                <thead>
                                    <tr>
                                        <th>Category Name</th>
                                        <th>Created On</th>
                                        <th>Updated On</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php foreach ($ucats as $key) { ?>
                                    <tr>
                                        <td><?php echo $key->category_name; ?></td>
                                        <td><?php echo dateF($key->created_on); ?></td>
                                        <td><?php echo dateF($key->updated_on); ?></td>
                                        <td>
                                            <a
                                                href="<?php echo URL; ?>essentials/editUserCat/<?php echo base64_encode($key->id); ?>">
                                                <i class="fas fa fa-edit" style="font-size:25px;color:blue;"></i></a>
                                        </td>
                                        <td>
                                            <a href="<?php echo URL; ?>essentials/delete-ucat/<?php echo base64_encode($key->id); ?>"
                                                onclick="return confirm('Are you sure you want to Delete User Category?');"><i
                                                    class="fa fa-trash" style="font-size:25px;color:red;"></i> </a>
                                        </td>
                                    </tr>
                                    <?php  } ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!-- end row -->
            </div>

        </div>
    </div>
    <!-- end col -->
</div>
<!-- end row -->
</div>
<!-- end container-fluid -->
</div>
<!-- end page content-->
</div>

<!--  Modal content for the above example -->
<div class="modal fade user-add" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"
    data-backdrop="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header modal-header-primary">
                <h5 class="modal-title mt-0" id="myLargeModalLabel">Add User Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <form id="addcat" action="<?php echo URL;?>essentials/add-cat">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Category Name</label>
                                <input class="form-control" type="text" name="catName" value="" autocomplete="off"
                                    required>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block waves-effect waves-light">Add
                        Category</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<?php 

function dateF($date){
   return date_format(date_create($date), ' l jS F Y'); 
}

 ?>



<div class="modal fade bs-example-modal-center" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>