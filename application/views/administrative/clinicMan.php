<div id="page-wrapper">


<div class="container-fluid">



<!--    header end -->

<div class="panel">


<h4>CLINIC MANAGEMENT</h4><hr>

<div class="panel-body " style="padding: 30px;">
      <strong><?php if(isset($do_update_feedback)){echo $do_update_feedback;}?></strong>
                            
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel m-b-20 " style="border:2px solid #F16737;">
                                        <div class="panel-body">
                                        <form action="<?php echo URL; ?>essentials/manage-clinic/<?php echo base64_encode($clinic['id']); ?>" method = 'POST'>   
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                
                                                    <label for="example-text-input" class="col-form-label">Clinic Name</label>
                                                    <input class="form-control" type="text" name="clinicName"  value="<?php echo $clinic['clinic_name']; ?>" autocomplete="off" required>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="example-text-input" class="col-form-label">Clinic Code</label>
                                                    <input class="form-control" type="text" name="cliniCode" value="<?php echo $clinic['clinic_code']; ?>"  readonly >
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="example-text-input" class="col-form-label">Clinic Color</label>
                                                    <input class="form-control" type="color" name="cliniColor" value="<?php echo $clinic['clinic_color']; ?>"   autocomplete="off" required >
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="example-text-input" class="col-form-label">Clinic Phone</label>
                                                    <input class="form-control" type="text" name="clinicPhone" value="<?php echo $clinic['clinicPhone']; ?>" autocomplete="off" required >
                                                </div>


                                                <button  class="btn btn-info btn-block" name="update" type="submit">Update</button><br>
                                             </form>
                                            </div>

                                        </div>
                                    </div>
                                    
                                </div>
                                
                            </div>
                            <!-- end row --> 
                        </div>
                        
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end page content-->
</div>

</div>