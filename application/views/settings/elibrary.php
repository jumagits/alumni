                   <div id="page-wrapper">
                        <div class="container-fluid">
                           

                            <div class="panel">

                                 <h4 > BUMAA SHARED FILES

                                         <button type="button" data-toggle="modal" data-target=".bs-example-modal-lg" class="btn btn-danger  pull-right">Share Document / File</button>

                                       </h4>
                                       <hr>

                                   

                                       <div class="panel-body " >

                                        <div class="row">

                                             <div class="col-md-12">
                                       
                                           <div class="table-responsive">                    
                                                    <table id="datatable1" class="table table-striped table-bordered dt-responsive " style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                    <thead>
                                                        <tr>
                                                        <th>Shared Document</th>
                                                        <th>Title</th>
                                                        <th>Description</th>
                                                        <th>Shared On</th>
                                                        <th>Shared With</th>
                                                        <th>Shared By</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php echo $shared_doc; ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                               </div>
                           </div>
                       </div>
                   </div>
           



                        <!--  Modal content for the above example -->
                        <div class="modal fade bs-example-modal-lg " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="false">
                            <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                    <div class="modal-header bg-primary" >
                                        <h5 class="modal-title mt-0" id="myLargeModalLabel">E-Share / Library</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    </div>
                                    <div class="modal-body" >
                                      
                                        <form  id="eshare_form" action="<?php echo URL;?>e-share/upload-shared-document" enctype="multipart/form-data">
                                            <div id="upload_eshare"></div>
                                            <div class="form-group">
                                                <label class="control-label">Document Title</label>
                                                <input class="form-control" name="doc_title" required>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Document Note / Description</label>
                                                <textarea class="form-control " name="desc" required></textarea>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Share With: </label>
                                                
                                                <select name='shareWith[]' class='form-control chosen select2-multiple' multiple='multiple' data-placeholder="Choose Who To Share With">
                                                    <?php echo $user_options; ?>
                                                    
                                                </select> 

                                            </div>
                                            <div class="form-group">
                                                <label class=" control-label">Select Document</label>
                                               
                                                <input type="file" class="filestyle" name="files[]" data-input="false" data-buttonname="btn-secondary">
                                            </div>
                                            </br>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit"  class="btn btn-info">Share File</button>
                                        </form>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->