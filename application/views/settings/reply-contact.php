
    <div id="page-wrapper">
            <div class="container">               
             <div class="row">
                <div class="col-lg-6 m-auto">
                
                <div class="panel">
                    <div class="panel-heading bg-info">
                       Reply to Contact                    
                    </div>
                        <div class="panel-body " >  

                        <form>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" >
                            </div>
                            <div class="form-group">
                                <label for="Email message">Email Message</label>
                                <textarea name="email_message" id="" cols="2" class="form-control" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-danger btn-block ">Send Email </button>
                            </div>
                        </form>                    

                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
