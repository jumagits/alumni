<div id="page-wrapper">
<div class="container-fluid"> 
<div class="panel">
    <div class="panel-heading">
        <h4> <?php echo $pageTitle; ?> </h4>
    </div>
<div class="panel-body " >
   <div class="row">
        <div class="col-lg-12">
    <div class="table-responsive ">       
        <table class="table table-hover table-bordered table-striped" id="datatable1" width="100%">
    <thead>
        <tr>
            <th>#</th> 
            <th>FullName</th>
            <th>Contact</th>
            <th>Subject</th>
            <th>Message </th>    
            <th>Date</th>
            <th>Reply</th>
         </tr>
    </thead>
    <tbody>


        <?php 

        $count = 0;

       foreach ($get_contacts as $key => $contact) {  $count++;?>
        <tr>
            
        <td><?php echo $count; ?></td>
        <td><?php  echo $contact->fullname;  ?> </td>
        <td><?php  echo $contact->phone;  ?> </td>
        <td><?php echo $contact->subject;  ?></td>
        <td> <?php echo   $contact->message; ?></td>
        <td><?php echo f_date(  $contact->created_at);  ?></td>
        <td>
        <a class="btn btn-xs btn-danger"  
        href="<?php echo URL; ?>adminEnd/reply_newsletter/<?php echo base64_encode($contact->id); ?>" >Reply <i class="fa fa-reply"></i></a> </td>
              
                 
        </tr>

            <?php } ?>

                
            </tbody>
            
        </table>

    </div>

            </div>

                </div>
               
            </div>

     
        </div>
        </div>
    </div>
</div>
</div>
</div>

</div>
</div>










<?php 

function f_date($date){
return date_format(date_create($date), ' l jS F Y'); 
}

?>


