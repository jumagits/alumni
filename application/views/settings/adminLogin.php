<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="BUMAA">
    <meta name="description" content="BUMAA">
    <meta name="author" content="BUMAA">
    <title>BUMAA</title>
    <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114"
        href="img/apple-touch-icon-114x114-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144"
        href="img/apple-touch-icon-144x144-precomposed.png">
    <link href="<?php echo URL ?>static/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo URL;?>static/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet"type="text/css">
    <link href="<?php echo URL ?>static/assets/css/style.css" rel="stylesheet">
    <script src="<?php echo URL ?>static/assets/js/jquery-1.11.2.min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/common_scripts_min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/functions.js"></script>

    <!--[if lt IE 9]>
      <script src="<?php echo URL ?>static/assets/js/html5shiv.min.js"></script>
      <script src="<?php echo URL ?>static/assets/js/respond.min.js"></script>
    <![endif]-->

</head>
<style>
    html, body {margin: 0; height: 100%;background:linear-gradient(#e7e7e7,#ededed); }
    body{
         padding-top: 5%;
    }
    .well{
        box-shadow: 0 0 5px #d3d3d3;
    } 
    .lamb{
        background-color: #333;
    background-image: radial-gradient(circle farthest-side at center bottom,#152238,cyan 125%);padding: 30px;
    }  
</style>
<body> 
       <div class="container">       

            <div class="row"> 


                <div class=" col-lg-4 col-sm-12 col-md-4 "></div>      

                <div class=" col-lg-4 col-sm-12 col-md-4 " >


                    <div class="my-5">
                        
                    <h2 class="text-center mb-4"  >
                        <a href="<?php echo URL ?>">
                            <img src="<?php echo URL ?>static/assets/img/log.png" alt="logo"></a>
                    </h2>

                    <div class="well" >


                        <div class="lamb">

                        <div class="msg"></div>

                        <h3 class="font-weight-bold ">Enter your Admin Code<br>

                        </h3>
                        <hr>

                            <div> <?php echo isset($feedback) ? $feedback : ''; ?></div>

                           
                                
                                  <form method="POST" action="<?php echo URL ?>adminEnd/">
                                        <div class="form-group ">
                                            <label for="admincode">Admin Code <span class="text-danger">*</span></label>
                                            <input type="text" name="adminCode" class="form-control" required>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-dark btn-block btn-lg">Continue
                                            </button>                                    
                                        </div>                                        

                                    </form>   

                                    <p><a href="<?php echo URL ?>" class="btn btn-success">Back to Home</a></p>                       

                        </div>

                    </div>


                    </div>

                   
                </div>


                  <div class=" col-lg-4 col-sm-12 col-md-4 "></div>    


            </div>

        </div>

       
        </form>

    </div>

    <script>

    jQuery(document).ready(function($) { 
    var APP_URL = '<?php echo URL; ?>';
    $("form#login").submit(function(e) {
        e.preventDefault();
        // console.log($(this).serializeArray());
        $.ajax({
            type: "POST",
            dataType: "json",
            url: APP_URL + "accounts/login",
            data: $(this).serialize(),           
            success: function(res) {
            $(".msg")
                .empty()
                .html(res);
                setTimeout(function() {
                    window.location.href = '<?php echo URL; ?>';
                }, 5000);
            },
            error: function(jqXHR) {
            console.log(jqXHR);
            }
        });
    });


        });
        

    </script>


</body>

</html>