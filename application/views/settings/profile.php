
    <div id="page-wrapper">

            <div class="container-fluid">

                <?php  if (isset($details) AND !empty($details) ) {
                    foreach ($details as $key => $info) { }} ?>

                 <div class="row">

                    <div class="col-lg-9 m-auto">

                      
                        <div class="panel">

                             <h4> Profile</h4><hr>
                             
                            <div class="panel-body " >
                                 <ul class="list-group list-group-flush list-unstyled">
                          
                                  <li class="list-group-item ">
                                  <b>Firstname <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->fname; ?>
                                  </li>

                                  <li class="list-group-item ">
                                  <b>Lastname <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->lname; ?>
                                  </li>


                                  <li class="list-group-item ">
                                  <b>Username <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->username; ?>
                                  </li>

                                  <li class="list-group-item ">
                                  <b>Email <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->email; ?>
                                  </li>

                                  <li class="list-group-item ">
                                  <b>Sex <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->sex; ?>
                                  </li>

                                  <li class="list-group-item ">
                                  <b>Marital Status <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->maritalstatus; ?>
                                  </li>

                                  <li class="list-group-item ">
                                  <b>Contact <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->contactphone; ?>
                                  </li>

                                  <li class="list-group-item ">
                                  <b>Transcript No <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->transcript_no; ?>
                                  </li>

                                  <li class="list-group-item ">
                                  <b>Student No <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->student_no; ?>
                                  </li>

                                  <li class="list-group-item ">
                                  <b>Current Information About Member <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->knowabout; ?>
                                  </li>
                           </ul>

                         </div> 

                            </div>
                        </div>
                    </div>
                </div>
            </div>
       