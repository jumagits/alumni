
    <div id="page-wrapper">
            <div class="container">               
             <div class="row">
                <div class="col-lg-6 m-auto">

             <?php foreach ($newsletters_person as $key => $member) { }; ?>
                
                <div class="panel">
                    <div class="panel-heading bg-info">
                      Reply to Newsletter                    
                    </div>
                        <div class="panel-body " > 
                        <form id="sendNewletter" action="<?php echo URL ?>adminEnd/reply_newsletter_action">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" readonly="" value="<?php echo $member->email; ?>" name="email" class="form-control" >
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="id" value="<?php echo $member->id ?>" >
                            <label for="Email ">Newsletter Message</label>
                                <textarea name="message" name="message" cols="2" class="form-control" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <button name="send" class="btn btn-danger btn-block " type="submit">Send Email </button>
                            </div>
                        </form>                    

                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>


<script>
    
    jQuery(document).ready(function($) {        
         $("form#sendNewletter").submit(function(e) {
         e.preventDefault();       
         var formData = new FormData(this);
         var action = $('form#sendNewletter').attr('action');       
               $.ajax({
                   url: action,
                   type: 'POST',                  
                   data: formData,
                   success:function(res){
                  swal({
                       title: "Good job!",
                       text: res.msg,
                       type: "success",
                       showCancelButton: !0,
                       confirmButtonClass: "btn btn-success",
                       cancelButtonClass: "btn btn-danger m-l-10"
                     })

                  setInterval(function(){
                    window.location.href = '<?php echo URL; ?>adminEnd/newsletter';
                  },4000);
                  
                   },
                  error: function(jqXHR, textStatus, errorThrown) {
                         console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
               })
               .done(function() {
                   console.log("success");
               })
          });

    });
</script>