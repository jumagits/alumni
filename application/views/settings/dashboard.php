    <div id="page-wrapper">


            <div class="container-fluid">

            <!-- Page Heading -->
                  <div class="row"  >
                    <div class="col-lg-12">
                          <h4 class="page-header bg-primary ">
                         Hey <?php echo !isset($user_info) ? header('URL'): $user_info['names'];
                          ?>, Welcome to <?php echo $pageTitle; ?> 
                          </h4> 
                     </div>
                 </div>
                
                  <div class="row">

                    <div class="panel">
                       <div class="panel-heading">
                        OVER VIEW
                      </div>
                      <div class="panel-body"> 
                        <div class="col-lg-12">
                          <div class="row ">
                          
                             <div class="col-sm-2">
                               
                                   <div class="bet panel-green text-center">
                                   <span class="h6"><b>ALL MEMBERS</b>
                                      <br>
                                      <span class="h1 counter"><?php echo isset($count_clients) ? $count_clients :''; ?></span></span>
                                   
                                  <div class="panel-footer">
                                      <a href="<?php echo URL ?>/register-clients/members">
                                     <span >View Details <span class="pull-right"><i class="fa fa-exclamation-circle"></i></span></span>
                                     </a>
                                  </div>
                                  </div> 
                             
                             </div>


                                <div class="col-sm-2">
                             
                                   <div class="bet panel-green text-center">

                                   <span class="h5"><b>ALL POSTS</b>
                                      <br>
                                      <span class="h1 counter"><?php echo isset($count_posts) ? $count_posts :''; ?></span></span>
                                   
                                  <div class="panel-footer">
                                    <a href="<?php echo URL ?>/post">
                                     <span >View Details <span class="pull-right"><i class="fa fa-exclamation-circle"></i></span></span>
                                     </a>
                                   </div>
                                  
                              </div>
                             </div>

                                <div class="col-sm-2">
                               
                                   <div class="bet panel-green text-center">
                                    <span class="h5"><b>POSTED JOBS</b>
                                      <br>
                                      <span class="h1 counter"><?php echo isset($jobs_count) ? $jobs_count :''; ?></span></span>
                                 
                                  <div class="panel-footer">
                                       <a href="<?php echo URL ?>/job">
                                     <span >View Details <span class="pull-right"><i class="fa fa-exclamation-circle"></i></span></span>
                                     </a>
                                  </div> 
                              </div>
                             </div>

                                <div class="col-sm-2">
                              
                                   <div class="bet panel-green text-center">
                                    <span class="h5"><b>COMMENTS</b>
                                      <br>
                                      <span class="h1 counter"><?php echo isset($comments_count) ? $comments_count :''; ?></span></span>
                                   
                                  <div class="panel-footer">
                                       <a href="<?php echo URL ?>/post/comments">
                                     <span >View Details <span class="pull-right"><i class="fa fa-exclamation-circle"></i></span></span>
                                     </a>
                                  </div> 
                              </div>
                             </div>
                              

                               <div class="col-sm-2">
                               
                                   <div class="bet panel-green text-center">
                                    <span class="h5"><b>USERS</b>
                                      <br>
                                      <span class="h1 counter"><?php echo isset($users_count) ? $users_count :''; ?></span></span>
                               
                                  <div class="panel-footer">
                                       <a href="<?php echo URL ?>/post">
                                     <span >View Details <span class="pull-right"><i class="fa fa-exclamation-circle"></i></span></span>
                                     </a>
                                  </div> 
                              </div>
                             </div>

                                <div class="col-sm-2">
                               
                                   <div class="bet  panel-green text-center ">
                                    <span class="h5"><b>EVENTS</b>
                                      <br>
                                      <span class="h1 counter"><?php echo isset($count_events) ? $count_events :''; ?></span></span>
                                   
                                  <div class="panel-footer">
                                     <a href="<?php echo URL ?>/event">
                                     <span >View Details <span class="pull-right"><i class="fa fa-exclamation-circle"></i></span></span>
                                     </a>
                                  </div> 
                              </div>
                             </div>


                             <!-- row ends -->
                              
                             </div>

                           <!-- row ends -->

                          </div>

                        </div>

                        <div class="panel-footer">
                          <h4>Lorem, ipsum, dolor sit amet consectetur adipisicing elit. Beatae.</h4>
                        </div>
                   

                    </div>
                       

                  </div>


                 <!--  end middle section -->

                 <div class="row">
                   
                    <div class="col-lg-8">

                      <div class="panel">
                        <div class="panel-heading">
                          <h4>STATISTICS</h4>
                        </div>

                        <div class="panel-body med">
                            <canvas id="areaChart" style="height:250px"></canvas>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-4">
                      <div class="bet" style="font-size: 11px !important;"  id="user_login_status"></div>
                    </div>
                 </div>



                   <div class="row">
                   
                    <div class="col-lg-4">

                       <div class="bet">
                          <canvas id="lineChart" style="height:250px"></canvas>
                       </div>
                    </div>

                    <div class="col-lg-4">

                       <div class="bet">
                          <canvas id="pieChart" style="height:250px"></canvas>
                       </div>
                    </div>

                    <div class="col-lg-4">

                       <div class="bet">
                          <canvas id="barChart" style="height:250px"></canvas>
                       </div>
                    </div>
                 </div>