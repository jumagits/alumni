<div id="page-wrapper">
<div class="container-fluid">
<div class="panel">
<h4> <?php echo $pageTitle; ?> </h4>
<hr>
<div class="panel-body ">
<div class="row">
<div class="col-lg-12">
<div class="table-responsive ">
<table class="table table-hover table-bordered table-striped"  width="100%">
<thead>
    <tr>
        <th>#</th> 
        <th>Email</th>
        <th>Date</th>
        <th>Reply</th>
     </tr>
</thead>
<tbody>
    <?php 
    $count = 0;
   foreach ($newsletters as $key => $newsletter) {  $count++;?>
        <tr>            
            <td><?php echo $count; ?></td>
            <td><?php  echo $newsletter->email;  ?>                
            </td>
            <td><?php echo f_date(  $newsletter->subscribed_at);  ?></td>
            <td>
            <a class="btn btn-xs btn-danger"  
            href="<?php echo URL; ?>adminEnd/reply_newsletter/<?php echo base64_encode($newsletter->id); ?>" >Reply <i class="fa fa-reply"></i></a>
            </td> 
        </tr>

<?php } ?>    
</tbody>
</table>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<?php 

function f_date($date){
return date_format(date_create($date), 'l, j<\s\u\p>S</\s\u\p> F Y (h:i:s A)'); 
}

?>


