  <div class="clearfix"></div>  
  <div class="section_holder37 three">
                      <div class="container">                      
                          <div class=" table-responsive table-style">
                              <table class="table table-bordered table-list2" id="datatable1">
                                  <thead>
                                      <tr>
                                          <th>AVATAR</th>
                                          <th>FULLNAME</th>
                                          <th>EMAIL</th>
                                          <th>CAMPUS</th>
                                          <th>COURSE</th>
                                          <th>CONTACT</th>
                                          <th>FROM</th>
                                          <th>UPTO </th>
                                          <th>ABOUT</th>
                                      </tr>
                                  </thead>
                                  <tbody>

                                  <?php 
            											$count = 0;
            											foreach ($all_clients as $key => $client) { $count++; ?>
                                      <tr style="border-color: 1px solid #000;">
                                          <td>

                                            <a href="<?php echo URL ?>application/alumni-details/<?php echo base64_encode($client->id); ?>">
                                              <?php echo generate_avator($client->fname,$client->lname);?></a>
                                                
                                              
                                          </td>
                                          <td><?php echo strtoupper($client->fname)." ". strtoupper($client->lname); ?>
                                          </td>
                                          <td><?php echo $client->email; ?></td>
                                          <td><?php echo isset($client->clinic_id)? ($administrativeModel->get_clinic_name_today($client->clinic_id)) : "NOT KNOWN"; ?>
                                          </td>
                                          <td>
                                          <?php  echo (isset($client->course ) AND $client->course != "" AND !empty($client->course)) ? $administrativeModel->get_course_name_today($client->course) : "NOT KNOWN"; ?>
                                          </td>
                                      
                                          <td><?php echo (isset($client->contactphone) AND $client->contactphone != "") ? $client->contactphone : 'NOT KNOWN'; ?>
                                          </td>
                                           <td><?php echo (isset($client->from_year ) AND $client->from_year != "")? $client->from_year : 'NOT KNOWN'; ?>
                                          </td>
                                           <td><?php echo (isset($client->upto_year ) AND $client->upto_year != "")? $client->upto_year : 'NOT KNOWN'; ?>
                                          </td>
                                          <td><?php echo (isset($client->knowabout ) AND $client->knowabout != "") ? $client->knowabout : 'NOT KNOWN'; ?>
                                          </td>
                                      </tr>

                                      <?php } ?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                     </div>
                  </div>
              </div>
          </div>
          <div class="clearfix"></div>
        </div>
     

      <?php 

    function generate_color(){
      $red = 'btn-danger';
      $green = 'btn-success';
      $blue = 'btn-primary';
      $yellow = 'btn-warning';
      $info = 'btn-info';
      $secondary = 'btn-secondary';
      $color = array($red,$green,$blue,$yellow,$info,$secondary);      
      $pin = $color[mt_rand(0,5)];       
      return 'btn '.$pin;
    }


      function generate_avator($fname,$lname){
        $fn = $fname[0];
        $ln = $lname[0];    
        $color_code = generate_color();
        return "<button class='".$color_code." font-weight-bold btn-lg'>".$fn."-".$ln."</button>";
      }
       ?>

      <script>
$(document).ready(function() {
    $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".row .item").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });


});
      </script>