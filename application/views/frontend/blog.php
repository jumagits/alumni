 <div class="clearfix"></div>  
  <div class="section_holder37 three">
                      <div class="container"> 
                            <div class="table-responsive table-style">
                                <table class="table table-bordered table-list2" id="datatable1">
                                            <thead>
                                                <tr>
                                                    <th>BANNER</th>
                                                    <th>DATE</th>
                                                    <th>POST TITLE</th>
                                                    <th>AUTHOR</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                     <?php 
						if($all_posts):
								$c = 0;
							foreach ($all_posts as $key => $post) { $c++; ?>

                                      <tr>

                                        <td><img src="<?php echo URL.'data/'.date('Y').'/'. date('M').'/'.'posts/'.$post->post_image; ?>" alt="banner" width="120" height="120">
                                        </td>
                                        
                                        <td><?php echo (dateF($post->post_date));  ?>
                                        </td>

                                        <td>
                                           <a href="<?php echo URL ?>frontend/blog-details/<?php echo base64_encode($post->post_id) ?>"><?php echo strtoupper($post->post_title); ?></a> 
                                       </td>
                                       <td>
                                           <p class="text-info"><?php echo ($post->post_author); ?>
                                      </p> 
                                       </td>
                                   </tr>
                               

                           <?php }endif;?>
                           
                         </tbody>

                             <?php 
                             function dateF($date){
                                   return date_format(date_create($date), ' l jS F Y'); 
                                }   ?>

                        </table>
                        
                           </div>
                           
                        </div>
                        
                    </div><!--End col-md-9 -->
                    
                   
                    
                </div><!--End row -->
            </div><!--End container -->
        </div><!--End container_gray_bg -->