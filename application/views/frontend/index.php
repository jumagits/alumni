<style>


.one_third, .title_classic{
    color: #008000 !important;
}




.one_third strong{
    font-weight: bold !important;
    color: #008000 !important;
}


.item{

    
    width: 100%;
    height: 400px;
    background-size: cover;
    background-repeat: no-repeat;
}

#owl-demo4 .item {
   
   
    color: #fff !important;
    vertical-align: bottom;
    text-align: center;  
    padding: 50px;
}

.card{
    /*background-color: #edf0f2;*/
    background-color: #fff;   
    box-shadow: 0 0 2px #008000;
}
<?php 

function ukfirst($string){
    $r = strtolower($string);
    return ucfirst($r);
}


   function dateFF($date){
         return date_format(date_create($date), 'M jS, Y'); 
      }
    ?>
</style>
<div class="clearfix"></div>
<div id="section-1">
 
            <div id="owl-demo4" class="owl-carousel">
        
                <div class="item " style="background-image: url(<?php echo URL ?>static/assets2/images/sliders/1.jpg);">
                         <div class="margin_top"></div>
                    <div class="address_info two" >
                       
                    <h1 class="title_classic"><strong>WELCOME TO BUMAA</strong></h1>

                    <h2 class="">
                       BUMSA ALUMNI INFORMATION PLATFORM
                    </h2>

                     <a href="<?php echo URL ?>application/about" class=" btn btn-danger "><small class="white">Read More++</small></a>
                    
                    <div class="margin_top1"></div>
                    </div>
                    
                </div>


                 <div class="item "  style="background-image: url(<?php echo URL ?>static/assets2/images/sliders/3.jpg);">
                         <div class="margin_top"></div>
                    <div class="address_info two " >
                       
                     <h1 class="title_classic"><strong>WELCOME TO BUMAA</strong></h1>

                    <h2 class="">
                        UNITING BUMSA ALUMNI GLOBALLY
                    </h2>

                      <a href="<?php echo URL ?>application/about" class=" btn btn-danger text-center"><small class="white">Read More++</small></a>

                    <div class="margin_top1"></div>
                    </div>
                    
                </div>
               
            </div>
       

    <div id="section-5">
        <div class="section_holder30">
            <div class="container text-center">
                <div class=" one_half first ">

                    <div class="meme address_info two">
                    <h3 class="title_classic"><strong>BUSITEMA UNIVERSITY MUSLIM ALUMNI ASSOCIATION (BUMAA)</strong>
                    </h3>
                     <div class="divider_line"></div>
                    <p class=" lead"> <b> BUSITEMA UNIVERSITY MUSLIM ALUMNI ASSOCIATION</b> <span
                            class=""><b>(BUMAA)</b></span> is an association formed by the <b>BUSITEMA UNIVERSITY MUSLIM
                            STUDENTS ASSOCIATION </b><span class="bold green"><b>(BUMSA)</b></span> alumni with the aim
                        of unity, brotherhood and extending Daawa globally.</p>
                    <p class="b">BUMAA was established in 2020 by the Busitema Muslim alumni at main campus. BUMAA welcomes all alumni and well wishers.</p>
                    <p class="block"><br>
                        <a href="<?php echo URL ?>application/about" class="text-center but_st1 small two red">Read
                            more</a>
                    </p>
                  </div>
                </div>

                 <div class="one_half last">


                    <div class="">

                        <iframe width="600" height="437" src="https://www.youtube.com/embed/8Hs0sUcQrG8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

    <div class="clearfix"></div>
  <div class="section_holder30">
    <div class="container">
        <div class="one_full text-center">

            <h3 class="title_classic"><strong>Apply for Membership</strong>
                    </h3><br>
           

                    <div class=" fefe ">

                         <strong class="lead bold">
                        Become a member of the Busitema University Muslim Student's Association  <a href="<?php echo URL ?>accounts/alumni_register" class="btn button btn-primary text-center">Click Here to Apply</a>
                    </strong>

                  
                         
                    </div>

                   

            


            
        </div>
    </div>
  </div>

    



 <div class="clearfix"></div>
  <div class="section_holder30">
    <div class="container">
      <div class="one_third">

         <div class="posts_widget address_info two">
            <h3 class="title_classic uppercase">Latest Events <a href="<?php echo URL ?>application/events" class="pull-right btn btn-danger "><small class="white">More++</small></a></h3>
             <div class="divider_line_dashed2"></div><br>


              <?php foreach ($all_events as $key => $event) {  ?>

                <div class="card">
                  <div class="left "> 
                    <img src="<?php echo URL ?>data/events/<?php echo stripcslashes($event->banner); ?>" width="100"  alt="event"/> 

                  </div>
                  <div class="right"> 
                    <a href="#">
                    <p class="lead b bold"><b> <?php echo ukfirst($event->title); ?></b></p>
                    </a> 
                    <span class="info"> <?php echo dateFF($event->schedule); ?>-<?php echo ucfirst($event->location); ?></span><br/>
                       
                 </div>
             </div>

             <?php } ?>
         </div>        
      </div>
      <!--end item-->
      
      <div class="one_third">

          <div class="posts_widget address_info two">
            <h3 class="title_classic uppercase">Latest Posts  <a href="<?php echo URL ?>application/posts" class="pull-right btn btn-primary "><small class="white">More++</small></a></h3>
             <div class="divider_line_dashed2"></div><br>
             <?php  foreach ($all_posts as $key => $post) {  ?>
             <div class="card">
          <div class="left"> <img src="<?php echo URL.'data/posts/'.$post->post_image; ?>" width="100"  alt="Post image"/> </div>
          <div class="right">
           <a href="#">
              <p class="lead"><b> <?php echo ukfirst($post->post_title); ?></b></p>
            </a> 
            
            <span> <?php echo dateFF($post->post_date); ?></span><br>
            
            </div>
        
          </div>
          <?php } ?>
      </div>
  </div>
       

      
      <div class="one_third last">
        
        <div class="posts_widget address_info two">
            <h3 class="title_classic uppercase">Latest Jobs  <a href="<?php echo URL ?>application/job" class="pull-right btn btn-warning "><small class="white">More++</small></a></h3>
             <div class="divider_line_dashed2"></div><br>
             <?php foreach ($all_jobs as $key => $job) {  ?>
             <div class="card">
              <div class="left"> <img src="<?php echo URL?>static/assets2/images/jobs.jpg" width="100" alt="job"/> </div>
              <div class="right">
               <a href="#">
            
                 <p class="lead"><b><?php echo ukfirst($job->job_title); ?></b></p>
               </a>
                
                 <span><?php echo strtolower($job->company); ?> - <?php echo strtolower($job->location); ?></span><br>
               
            </div>
              
          </div>
      <?php } ?>
         
       


        
      </div>
      <!--end item--> 
      
    </div>
  </div>
</div>

<div class="clearfix"></div>

  <div class="section_holder13  " style="background-color:#333">
    <div class="margin_top2"></div>
    <div class="container">
      <div class="one_fourth">
        <div id="count-box"><i><b>
            <?php echo isset($members) ? $members : "00" ?>+</b></i></div>
        <div class="text">
          <h4 class="uppercase white"><b>MEMBERS</b> </h4>
        </div>
      </div>
      <!--end item-->
      
      <div class="one_fourth">
        <div id="count-box"><i><b>00+</b></i></div>
        <div class="text">
          <h4 class="uppercase b"><b>VOLUNTEERS</b> </h4>
        </div>
      </div>
      <!--end item-->
      
      <div class="one_fourth ">
         <div id="count-box"><i><b>01+</b></i></div>
        <div class="text">
          <h4 class="uppercase lessweight"><b>Projects</b></h4>
        </div>
      </div>
      <!--end item-->
      
      <div class="one_fourth last">
        <div id="count-box"><i><b>00+</b></i></div>
        <div class="text">
          <h4 class="uppercase lessweight"><b>SPONSORS</b> </h4>
        </div>
      </div>
      <!--end item--> 
      
    </div>
  </div>

  <style>
      .posts_widget p{
        font-size: 14px !important;
        text-transform: capitalize;
        font-weight: 500;
        color: #000 !important;
      }

      .posts_widget span{
        font-size: 10px !important;
        text-transform: capitalize;
        color: #333 !important;
      }
  </style>