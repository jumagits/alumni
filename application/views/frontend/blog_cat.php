
        <div class="sub_header med">
        	<div id="intro_txt">
			 
                    <h1 class="text-white">POST PER CATEGORY</h1>
                    <p>Information about Bumaa.</p>
            </div>
		</div> <!--End sub_header -->
        
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="<?php echo URL; ?>">Home</a></li>
                    
                    <li>Cat</li>
                </ul>
            </div>
        </div><!-- Position -->
 
 	<div class="container_gray_bg">
    	<div class="container margin_60">
    <div class="row">
         
      <div class="col-md-9">
      	<?php if ($all_posts): ?>

      		<?php foreach ($all_posts as $key => $post) { ?>


     		
				<div class="post">
					<a href="blog_post_right_sidebar.html" ><img src="<?php echo URL;?>data/posts/<?php echo $post->post_image; ?>" width="100%" style="height: 300px;" alt="banner" class="img-responsive"></a>
					<div class="post_info clearfix">
						<div class="post-left">
							<ul>
								<li><i class="icon-calendar-empty"></i><?php echo isset($post) ? dateFF($post->post_date):'' ;?> <em>by Mark</em></li>
                                <li><i class="icon-inbox-alt"></i><a href="#">Category</a></li>
								<li><i class="icon-tags"></i>
									<a href="#">Works</a>, <a href="#">Personal</a>
								</li>
							</ul>
						</div>
						<div class="post-right"><i class="icon-comment"></i><a href="#">25 </a></div>
					</div>
					<h2><?php echo strtoupper($post->post_title); ?></h2>
					<p>
						<?php echo($post->post_content); ?>
					</p>
                   
					<a href="<?php echo URL ?>frontend/blog-details/<?php echo base64_encode($post->post_id); ?>" class="button" >Read more</a>
				</div><!-- end post -->

			<?php }else: ?>


			<h3 class="text-center">No Posts Under this Category</h3>


		<?php endif; ?>
       
                
              <div class="text-center">
                    <ul class="pagination">
                        <li><a href="#">Prev</a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">Next</a></li>
                    </ul><!-- end pagination-->
                </div>
     </div><!-- End col-md-8--> 
     
		<?php 

		function dateFF($date){
		   return date_format(date_create($date), '  jS F Y'); 
		}

		 ?>
      <aside class="col-md-3" id="sidebar">

				<div class="widget">
					<div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" placeholder="Search" />
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="icon-search-1"></i>
                        </button>
                    </span>
                </div>
            </div>
				</div><!-- End Search -->
                <hr>
				<div class="widget">
					<h4>Categories</h4>
					<ul id="cat_nav">

					<?php foreach ($all_categories as $key => $c) {?>
                    	<li><a href="#"><?php echo $c->cat_title; ?></a></li>
                       <?php } ?>

                    </ul>
				</div><!-- End widget -->
 
               <hr>
            
				<div class="widget">
					<h4>Recent post</h4>
					<ul class="recent_post">

						<?php foreach ($recent_posts as $key => $post) {
							# code...
						 ?>

						<li>
						<i class="icon-calendar-empty"></i><?php echo dateFF($post->post_date); ?>
						<div><a href="#"><?php echo mb_strtolower($post->post_title); ?> </a></div>
						</li>

						<?php } ?>
					</ul>
				</div><!-- End widget -->
                <hr>
				<div class="widget tags">
					<h4>Tags</h4>
						<?php

						if($all_posts):

						foreach ($all_posts as $key => $post) {

					 	 $tags = $post->post_tags;//string

					 	 $tags = explode(',', $tags);//arrays

					 	 foreach ($tags as $key => $tag) {
						 	?>
						<a href="#"><?php echo( $tag); ?></a> 


					     <?php }} 

				           endif;

						?>
				</div><!-- End widget -->
                
     </aside><!-- End aside -->
	
  </div><!-- End row-->         
</div><!-- End container -->
    </div><!--End container_gray_bg -->
    