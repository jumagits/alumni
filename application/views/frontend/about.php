

  <div class="section_holder30">
    <div class="container">
      <div class="one_third ">
        <div class="address_info two"> 
        <div class="icon"><i class="fa fa-coffee"></i></div>
        <div class="text">
          <h4 class="uppercase">Our History</h4>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et justo. Praesent mattis commodo augue. Aliquam ornare hendrerit augue. Cras tellus.</p>
        </div>
      </div>
    </div>
      <!--end item-->
      
      <div class="one_third">
        <div class="address_info two"> 
        <div class="icon"><i class="fa fa-users"></i></div>
        <div class="text">
          <h4 class="uppercase">Our Mission</h4>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et justo. Praesent mattis commodo augue. Aliquam ornare hendrerit augue. Cras tellus.</p>
        </div>
      </div>
      </div>
      <!--end item-->
      
      <div class="one_third last">
        <div class="address_info two"> 
        <div class="icon"><i class="fa fa-code"></i></div>
        <div class="text">
          <h4 class="uppercase">What we Do</h4>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Suspendisse et justo. Praesent mattis commodo augue. Aliquam ornare hendrerit augue. Cras tellus.</p>
        </div>
      </div>
      </div>
      <!--end item--> 
      
    </div>
  </div>
<!--   <div class="clearfix"></div>
 -->
 
  <div class="section_holder30">
    <div class="container">
      <div class="one_full">
        <div class="address_info two">
          <h4><strong>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Vitae, iste adipisci pariatur.</strong></h4>
          <p class="">
            Lorem ipsum, dolor sit amet consectetur, adipisicing elit. Laudantium saepe nemo officiis sint quisquam, ad? Eum voluptas explicabo aliquam similique natus distinctio non, debitis quas, eveniet inventore! Corporis facere, eveniet, ut aperiam repudiandae deleniti harum neque soluta, ipsam culpa voluptatibus nesciunt est, vitae fugit. Officia.
          </p>
          <p class="">
            Lorem ipsum, dolor sit amet consectetur, adipisicing elit. Laudantium saepe nemo officiis sint quisquam, ad? Eum voluptas explicabo aliquam similique natus distinctio non, debitis quas, eveniet inventore! Corporis facere, eveniet, ut aperiam repudiandae deleniti harum neque soluta, ipsam culpa voluptatibus nesciunt est, vitae fugit. Officia.
          </p>
          <p class="">
            Lorem ipsum, dolor sit amet consectetur, adipisicing elit. Laudantium saepe nemo officiis sint quisquam, ad? Eum voluptas explicabo aliquam similique natus distinctio non, debitis quas, eveniet inventore! Corporis facere, eveniet, ut aperiam repudiandae deleniti harum neque soluta, ipsam culpa voluptatibus nesciunt est, vitae fugit. Officia.
          </p>
        </div>
      </div>
    </div>
  </div>
 <div class="clearfix"></div>

  <div class="section_holder13  " style="background-color:#333">
    <div class="margin_top2"></div>
    <div class="container">
      <div class="one_fourth">
        <div id="count-box"><i><b>203+</b></i></div>
        <div class="text">
          <h4 class="uppercase white"><b>MEMBERS</b> </h4>
        </div>
      </div>
      <!--end item-->
      
      <div class="one_fourth">
        <div id="count-box"><i><b>23+</b></i></div>
        <div class="text">
          <h4 class="uppercase b"><b>VOLUNTEERS</b> </h4>
        </div>
      </div>
      <!--end item-->
      
      <div class="one_fourth ">
         <div id="count-box"><i><b>80+</b></i></div>
        <div class="text">
          <h4 class="uppercase lessweight"><b>Projects</b></h4>
        </div>
      </div>
      <!--end item-->
      
      <div class="one_fourth last">
        <div id="count-box"><i><b>53+</b></i></div>
        <div class="text">
          <h4 class="uppercase lessweight"><b>SPONSORS</b> </h4>
        </div>
      </div>
      <!--end item--> 
      
    </div>
  </div>

