 <div class="clearfix"></div>  
  <div class="section_holder37 three">
                      <div class="container"> 
                            <div class="table-responsive table-style">
                                <table class="table table-bordered table-list2" id="datatable1">
                                 <thead>
                                    <tr>
                                        <th>#</th>    
                                        <th>JOB TITLE</th>
                                        <th>CATEGORY</th>
                                        <th> COMPANY  </th>
                                        <th>POSTED BY</th>
                                        <th>ACTIONS</th>
                                        <th>HOURS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                    if($all_jobs):
                                        $count = 0;                                       
                                    foreach ($all_jobs as $key => $job) {   $count++;?>
                                    <tr>
                                        <td>
                                             <div class="img_holder">
                                        <img src="<?php echo URL?>static/assets2/images/jobs.jpg"
                                            alt="job image" class="img-thumbnail" style="width: 100px;height: 100px;" />
                                     </div>
                                        </td>
                                         
                                        <td><?php echo strtoupper($job->job_title); ?>
                                        </td>
                                        <td class="text-muted"><?php echo strtoupper($job->field); ?>
                                        </td> 
                                        <td>
                                             <span>
                                                    <?php echo strtoupper($job->company);  ?></span>
                                        </td>                                             
                                        
                                        <td> <span>
                                            <?php echo !empty($job->user_id)? $accountsModel->get_client_details($job->user_id) : 'N/A'; ?></span>
                                        </td> 
                                         <td>
                                           <a href="<?php echo URL ?>application/job-details/<?php echo base64_encode($job->id) ?>" class="btn btn-sm btn-info">More Info</a>
                                            <a target="_blank" href="<?php echo $job->job_link;  ?>" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Go Apply</a>
                                           
                                        </td>  
                                        <td><span class="badge badge-danger">
                                            Full Time
                                        </span></td>                                 
                                   
                                </tr>
                                

                            <?php
                             }

                            endif;

                             ?>
                         </tbody>
                        </table>
                            </div>
                        </div>

                        
                 
                        
                    </div><!--End col-md-9 -->
                    
                  
                    
                </div><!--End row -->
            </div><!--End container -->
        </div><!--End container_gray_bg -->