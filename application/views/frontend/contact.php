
  <div class="clearfix"></div>  
  <div class="section_holder49 three">
    <div class="one_full">


    <div class="container"> <br/>
      <br/>
      <br/>
      <br/>
      <div class="two_third ">
          <form class="form-vertical  card p-4" action="<?php echo URL; ?>frontend/contact" id="contactform">
            <div class="ajax-message"></div>
              <div class=" card-info">
                
              <h3>GET IN TOUCH</h3>
              <p> Please fill in all spaces provided, for a better reply</p>
             <br>

              <div class="card-body">
              <div class="row ">
                <div class="col-md-6">
                  <div class="form-group">
                  
                      <label>Your Name</label>
                    <input name="fullname" type="text" required class="form-control valid-character">
                    
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    
                      <label>Your Email Address</label>
                      <input name="email" type="email" class="form-control" required >
                  
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    
                      <label>Your Phone</label>
                      <input name="phone" type="text" required class="form-control int-value">
                  
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                  
                      <label>Subject</label>
                      <input name="subject" type="text" required class="form-control">
                    
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                  
                      <label>Type Message</label>
                      <textarea name="message" cols="30" rows="5" class="form-control" required ></textarea>
                    
                  </div>
                </div>
              
                <div class="col-lg-12">
                  <button name="submit" type="submit" value="Submit" class="btn  a-button  btn-block a-button-primary"> Send Message</button>
                </div>
              </div>

            </div>

              </div>
            </form>
      </div>
      <!-- end section -->
      
      <div class="one_third last">
        <div class="address_info two">
          <h4 class="uppercase"><strong>Address Info</strong></h4>
          <p>Feel free to talk to our online representative at any time you please using our Live Chat system on our website or one of the below instant messaging programs.</p>
          <br />
          <p>Please be patient while waiting for response. (24/7 Support!) <strong>Phone General Inquiries:+256702499649</strong></p>
          <br />
        </div>
        <!-- end section -->
        
        <div class="address_info two">
          <h4 class="uppercase"><strong>Address Info Two</strong></h4>
          <ul>
            <li>
              <h5>BUMAA UGANDA</h5>
              29 Old Kampala, kampala Mukadde<br />
              Telephone: +256702499649<br />
              FAX: +256783975685<br />
              E-mail: <a href="mailto:email@example.com">info@bumaa.com</a><br />
              Website: <a href="index.html">https://bumaa.com</a> </li>
          </ul>
        </div>
        <!-- end section --> 
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<!--end section-->
