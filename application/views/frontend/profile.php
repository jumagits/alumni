 <div class="clearfix"></div>  
  <div class="section_holder37 three ">
          <div class="container"> 

            <div class="one_full">
                  <div class="address_info two" >
                     <h4 class="uppercase"><strong>DETAILS </strong></h4>
                     <div class="divider_line_dashed2"></div><br>
                      <p> <b>Firstname <i class="fa fa-arrow-right"></i></b> <?php  echo $details[0]->fname; ?></p>
                    <br />

                     <p>  <b>Lastname <i class="fa fa-arrow-right"></i></b> <?php  echo $details[0]->lname; ?></p>
                    <br />

                     <p> <b>Username <i class="fa fa-arrow-right"></i></b><?php  echo $details[0]->username; ?></p>
                    <br />

                     <p> <b>Email <i class="fa fa-arrow-right"></i></b> <?php  echo $details[0]->email; ?></p>
                    <br />

                     <p> <b>Sex <i class="fa fa-arrow-right"></i></b> <?php  echo $details[0]->sex; ?></p>
                    <br />

                     <p> <b>Transcript No <i class="fa fa-arrow-right"></i></b><?php  echo $details[0]->transcript_no; ?></p>
                    <br />

                     <p>  <b>Contact <i class="fa fa-arrow-right"></i></b><?php  echo $details[0]->contactphone; ?></p>
                    <br />

                     <p> <b>Student No <i class="fa fa-arrow-right"></i></b> <?php  echo $details[0]->student_no; ?></p>
                    <br />


                      <p><b>Info<i class="fa fa-arrow-right"></i></b> <?php  echo $details[0]->knowabout; ?></p>
                    <br />
                  </div>                  
              </div>
               


              <div class="two_third first ">
                  <div class="address_info two">  
                    <h4 class="uppercase"><strong> UPDATE PROFILE INFORMATION </strong></h4>
                     <p class="testinging text-success"></p>
                  <div class="divider_line_dashed2"></div><br>        
                    <div class="card-body">
                      <form class="card-body" id="updateProf" action="<?php echo URL ?>frontend/alumni-register-in-update">
                          <div class="row">
                              <div class="form-group col-md-4">
                                  <label for="clinic">CAMPUS <span class="text-danger">*</span></label>
                                  <select name="clinic" id="clinic" class="form-control custom-select">
                                      <?php foreach ($all_clinics as $key => $clinic) { ?>
                                      <option <?php if( $details[0]->clinic_id == $clinic->id){
                                        echo "selected"." "."disabled";
                                      } ?> value="<?php echo $clinic->id ?>">
                                      <?php echo $clinic->clinic_name;  ?>
                                      </option>
                                      <?php } ?>
                                  </select>
                              </div>

                              <div class="form-group col-md-8">
                                  <label for="course">COURSES <span class="text-danger">*</span></label>
                                  <select name="course" id="course" class="form-control custom-select">
                                      <?php foreach ($getCourses as $key => $c) { ?>
                                      <option <?php if( $details[0]->course == $c->id){
                                        echo "selected";
                                      } ?> value="<?php echo $c->id ?>">
                                          <?php echo $c->course_name;  ?>
                                      </option>
                                      <?php } ?>
                                  </select>
                              </div>

                          </div>
                          <div class="row">
                                 <div class="form-group col-md-4">
                                  <label for="student_number">From Year<span class="text-danger">*</span>
                                  </label>
                                  <input type="text" value="<?php  echo $details[0]->from_year; ?>"
                                      name="from_year" class="form-control" required>
                                  
                              </div>


                                 <div class="form-group col-md-4">
                                  <label for="upto_year">upto year<span
                                          class="text-danger">*</span>
                                  </label>

                                  <input type="text" value="<?php  echo $details[0]->upto_year; ?>"
                                      name="upto_year" class="form-control" required
                                   >
                                  
                              </div>

                              <input type="hidden" value="<?php  echo $details[0]->id; ?>" name='user_id'>



                               <div class="form-group col-md-4">
                                  <label for="student_number">REG NO<span
                                          class="text-danger">*</span>
                                  </label>

                                  <input type="text" value="<?php  echo $details[0]->student_no; ?>"
                                      name="student_number" class="form-control" required
                                      placeholder="Prepend a '0' or '00' or '000' on RegNo">
                                  <small class="nerror"></small>
                              </div>

                              <div class="form-group col-md-6">
                                  <label for="transcript_serial">TRANSCRIPT SERIAL No
                                      <span class="text-danger">*</span>
                                  </label>
                                  <input type="text" value="<?php  echo $details[0]->transcript_no; ?>"
                                      name="transcript_serial" class="form-control" required>
                                  <small class="error"></small>
                              </div>

                              <div class="form-group col-md-6">
                                  <label for="email">EMAIL
                                  <span class="text-danger">*</span></label>
                                  <input type="text" name="email" value="<?php  echo $details[0]->email; ?>"
                                      class="form-control" required>
                                  <small class="eerror"></small>
                              </div>
                          </div>
                         
                     
                          <div class="row">
                              <div class="form-group col-md-4">
                                  <label for="username">FIRSTNAME<span class="text-danger">*</span>
                                  </label>
                                  <input type="text" name="firstname" value="<?php  echo $details[0]->fname; ?>" class="form-control" required>
                                  <small class="error"></small>
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="lastname">LASTNAME<span class="text-danger">*</span>
                                  </label>
                                  <input type="text" value="<?php  echo $details[0]->lname; ?>" name="lastname"
                                      class="form-control" required>
                                  <small class="error"></small>
                              </div>
                              <div class="form-group col-md-4">
                                  <label for="username">USERNAME<span class="text-danger">*</span>
                                  </label>
                                  <input type="text" disabled="" name="username" value="<?php  echo $details[0]->username; ?>"
                                      class="form-control" required>
                                  <small class="error"></small>
                              </div>
                          </div>
                          <div class="row">
                              <div class="form-group col-md-6">
                                  <label for="username">GENDER
                                      <span class="text-danger">*</span>
                                  </label>
                                  <select name="sex" class="form-control custom-select">
                                      <option value="<?php  echo $details[0]->sex; ?>" selected=""><?php  echo $details[0]->sex; ?>
                                      </option>
                                      <option value="Male">Male</option>
                                      <option value="Female">Female</option>
                                  </select>
                                  <small class="error"></small>
                              </div>


                              <div class="form-group col-md-6">
                                  <label for="lastname">PHONE
                                      <span class="text-danger">*</span>
                                  </label>
                                  <input type="text" value="<?php  echo $details[0]->contactphone; ?>" name="phone"
                                      class="form-control" required>
                                  <small class="error"></small>
                              </div>

                               <div class="form-group col-md-12">
                                  <label for="knowabout">ABOUT CAREER
                                    <span class="text-danger">*</span>
                                  </label>
                                  <input type="text" value="<?php  echo $details[0]->knowabout; ?>" name="knowabout"
                                      class="form-control" required>   
                              </div>
                          </div>
                         
                          <div class="form-group ">
                              <button type="submit" name="login" class=" btn btn-dark btn-block btn-lg">UPDATE
                                  INFORMATION</button>
                          </div>
                       </form>
                     </div>
                  </div>
              </div>

                 <div class="one_third last">
                  <div class="address_info two " >
                   
                     <h4 class="uppercase"><strong>CHANGE PASSWORD </strong></h4>
                    <div class="divider_line_dashed2"></div><br>
                      <div class=" card-body">   
                      <p class="testing text-success"></p>
                         <form id="updatePasswordF" action="<?php echo URL; ?>accounts/update_pass" >
                                 <div class="form-group">
                                     <label for="example-text-input" class="col-form-label">Old Password</label>
                                     <input class="form-control" type="password" name="oldpassword" placeholder="Old Password"
                                         required>
                                 </div>
                                 <div class="form-group">
                                     <label for="example-text-input" class="col-form-label">New Password</label>
                                     <input class="form-control" type="password" name="password1" placeholder="New Password"
                                         required>
                                 </div>
                                 <div class="form-group">
                                     <label for="example-text-input" class="col-form-label">Retype Password</label>
                                     <input class="form-control" type="password" name="password2" placeholder="Re type Password" required>
                                 </div>
                                 <button type="submit" name="changePassword"
                                     class="btn btn-primary btn-lg waves-effect btn-block">Change Password</button>
                             
                           </form> 
                       </div>
                  </div>
              </div>

                
          </div>
        </div>
 



