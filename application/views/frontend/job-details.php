
    <?php if (isset($job_details) AND !empty($job_details)) {
         foreach ($job_details as $key => $job) {}} ?>
      <div class="clearfix"></div>  
           <div class="section_holder37 three">
               <div class="container"> 					
						<div class="one_half">
							<div class="address_info two">
					          <h4 class="uppercase"><strong>JOB DETAILS FOR  <?php echo $job->job_title; ?> </strong></h4>
					          <p> COMPANY:  <span class="value"><?php echo $job->company; ?></span>.</p>
					          <br />

					          <p>COMPANY LOCATION: <strong>	 <span class="value"><?php echo $job->location; ?></span></strong></p>
					          <br />

					          <p>JOB FIELD: <span class="value"><?php echo $job->field; ?></span></p>
					          <br>

					           <p>DATE POSTED: <span class="value"> <?php echo date_format(date_create($job->date_created), ' l jS F Y'); ?></span></p>
					          <br>

					           <p>DEADLINE: <span class="value"> <?php echo date_format(date_create($job->deadline), ' l jS F Y'); ?></span></p>
					          <br>

					           <p>APPLICATION LINK: <span class="value"> <code> <a target="_blank" href="<?php echo $job->job_link; ?>"><?php echo $job->job_link; ?></a></code></span></p>
					          <br>

					           <p>JOB FIELD: <span class="value"><?php echo $job->field; ?></span></p>
					          <br>

					          <div class="divider_line_dashed2"></div>

					          <p>
					          	<?php echo html_entity_decode($job->description); ?>
					          </p>
					        </div>	
							</div>									
						</div>	
			      </div>
   