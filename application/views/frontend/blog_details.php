<?php  foreach ($post_details as $key => $post) {}   ?>
<div class="clearfix"></div>  
  <div class="section_holder37 three">
   <div class="container"> 
    <div class="row">         
     <div class="col-md-9">
		<?php 	function dateFF($date){
		   return date_format(date_create($date), '  jS F Y'); 
		}	 ?>

		<div class="address_info two">
          <h4 class="uppercase"><strong>Address Info</strong></h4>
          <p>Feel free to talk to our online representative at any time you please using our Live Chat system on our website or one of the below instant messaging programs.</p>
          <br />
          <p>Please be patient while waiting for response. (24/7 Support!) <strong>Phone General Inquiries: 1-888-123-4567-8900</strong></p>
          <br />
        </div>
     	
     		<div class="address_info two" style="font-size: 20px;line-height: 25px;">

     			<div class="card-img-top">
     				
     				<img src="<?php echo URL;?>data/posts/<?php echo $post->post_image; ?>" alt="banner" class="img-responsive" width="100%" height="400px">

     			</div>
					

					<div class="card-body p-3">
						
							<ul class="list-inline">
								<li class="list-inline-item"><i class="icon-calendar-empty"></i><?php echo isset($post) ? dateFF($post->post_date):'' ;?> <em>by Mark</em></li>
                                <li class="list-inline-item"><i class="icon-inbox-alt"></i><a href="#">Category</a></li>
								<li class="list-inline-item"><i class="icon-tags"></i>
									<a href="#">Works</a>, <a href="#">Personal</a>
								</li>
							</ul>
							<hr>
							
							<h3><?php echo strtoupper($post->post_title); ?></h3>
							<p class="text-muted">
								<?php echo($post->post_content); ?>
							</p>

					</div>
                   
				</div><!-- end post -->
                                
		  
                
				<div class="card p-4" id="comments">

					<h4>3 comments</h4>
					<ol>
						<p style="border-bottom: 1px solid #333;"></p>

						<?php

						if( $all_comments){
						 foreach ($all_comments as $key => $comment) {
							# code...
					  ?>

						<li>
					
						<div class="comment_right clearfix" style="border-bottom: 1px solid #d3d3d3;">
							<div class="p-2 text-muted">

								Posted by <a href="#"><?php echo $comment->comment_author; ?></a>

								<span>|</span><?php echo dateFF($comment->comment_date); ?> 
								<span>|</span><a href="#">Reply</a>
							</div>
							<p>
								<?php echo $comment->comment_content; ?>
							</p>

						</div>
						</li>


					<?php }} else{?>
                        <li class="comment">

                        	<h3 class="comments-title">No Comments Yet</h3>
                        	
                        </li>

					<?php } ?>
											
					</ol>
				</div><!-- End Comments -->
                
				<h4>Leave a comment</h4>

				<p class="response text-success"></p>

				<form  class="comment-form address_info two" action="<?php echo URL; ?>post/create-comment" id="commentform" method="post">

					<div class="form-group">
						<input class="form-control styled" type="text" name="comment_author" placeholder="Enter name">
					</div>
					<div class="form-group">
						<input class="form-control styled" type="text" name="comment_email" placeholder="Enter email">
					</div>
					<div class="form-group">
						<textarea name="comment_content" class="form-control styled" placeholder="Message"></textarea>
					</div>
					<input type="hidden" name="post_id" value="<?php echo $post->post_id; ?>">
					<div class="form-group">
						<button type="submit" class="btn  a-button  btn-block a-button-primary" name="submit" >Post Comment</button>
                      
					</div>
				</form>          
        
                
            
     </div><!-- End col-md-8-->   
     
       <aside class="col-md-3" >

       	 
        <div class="pro_sidebar_search">
          <h5 class="padd_bot2 uppercase">Search</h5>
          <div class="search">
            <input class="serch_input" type="search" placeholder="Email Address">
            <input name="" value="Submit" class="search_submit" type="submit">
          </div>
        </div>
        <div class="categories_holder address_info two">
          <h5 class="padd_bot2">Categories</h5>
          <ul class="list">
          	<?php foreach ($all_categories as $key => $c) {?>
            	<li ><a href="<?php echo URL ?>frontend/category/<?php echo base64_encode($c->cat_id); ?>"><?php echo $c->cat_title; ?></a></li>
               <?php } ?>
           
          </ul>
        </div>
        <!--end categories-->
        
        <div class="clearfix"></div>
        <div class="sidebar_latest_posts address_info two">
          <h5 class="padd_bot2 uppercase">Latest Posts</h5>

          	<?php foreach ($recent_posts as $key => $post) { ?>

					
						<div class="post_holder">
            <div class="img"><img src="http://placehold.it/80x80" alt="" class="img_size1"/></div>
            <div class="text"> <a href="#">
              <h6 class="lessmar"><?php echo strtoupper($post->post_title); ?></h6>
              </a>
              <div> <span>By John Deo</span><span><?php echo dateFF($post->post_date); ?></span> </div>
            </div>
          </div>
          <!--end item-->

						<?php } ?>
        </div>
        <!--end top rated products-->
        <div class="clearfix"></div>
        <div class="products_tags">
          <h5 class="padd_bot2 uppercase">Tags</h5>
          <ul class="tags">
          	<?php
						if($all_posts):
						foreach ($all_posts as $key => $post) {
					 	 $tags = $post->post_tags;//string
					 	 $tags = explode(',', $tags);//arrays
					 	 foreach ($tags as $key => $tag) {
						 	?><li>	<a href=""><?php echo($tag); ?></a></li><?php }} 
             endif;?>          
          </ul>
        </div>
      
        <!--end featured works-->
        <div class="clearfix"></div>
        <br/>
        <div class="sidebar_social_icons">
          <h5 class="padd_bot2 uppercase">Social Media Icons</h5>
          <ul class="social_icon_st2">
            <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a class="googleplus" href="#"><i class="fa fa-google-plus"></i></a></li>
            <li><a class="in" href="#"><i class="fa fa-linkedin"></i></a></li>
            <li><a class="dribble" href="#"><i class="fa fa-dribbble"></i></a></li>
          </ul>
        </div>
      </div> 
                
     </aside><!-- End aside -->
	
  </div><!-- End row-->         
</div><!-- End container -->
 </div><!--End container_gray_bg -->
    