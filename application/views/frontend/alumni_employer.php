
    <!-- header END ==== -->
    <!-- Inner Content Box ==== -->
    <div class="page-content">
        <!-- Page Heading Box ==== -->
        <div class="page-banner ovbl-dark" style="background-image:url(<?php echo URL ?>static/assets/images/banner/bg2.jpg);">
            <div class="container">
                <div class="page-banner-entry">
                    <h1 class="text-white"><?php echo !empty($pageTitle) ? $pageTitle : 'About Us'; ?></h1>
         </div>
            </div>
        </div>
    <div class="breadcrumb-row">
      <div class="container">
        <ul class="list-inline">
          <li><a href="<?php echo URL; ?>">Home</a></li>
          <li><?php echo !empty($pageTitle) ? $pageTitle : 'About Us'; ?></li>
        </ul>
      </div>
    </div>
    <!-- Page Heading Box END ==== -->
    <!-- Page Content Box ==== -->
    <div class="content-block">
            <!-- About Us ==== -->
      <div class="section-area section-sp1">
                <div class="container">
           <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 m-b30">
              <div class="feature-container">
                <div class="feature-md text-white m-b20">
                  <a href="#" class="icon-cell"><img src="<?php echo URL;?>static/assets/images/icon/icon1.png" alt=""/></a> 
                </div>
                <div class="icon-content">
                  <h5 class="ttr-tilte">Our Philosophy</h5>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod..</p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 m-b30">
              <div class="feature-container">
                <div class="feature-md text-white m-b20">
                  <a href="#" class="icon-cell"><img src="<?php echo URL;?>static/assets/images/icon/icon2.png" alt=""/></a> 
                </div>
                <div class="icon-content">
                  <h5 class="ttr-tilte">Kingster's Principle</h5>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod..</p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 m-b30">
              <div class="feature-container">
                <div class="feature-md text-white m-b20">
                  <a href="#" class="icon-cell"><img src="<?php echo URL;?>static/assets/images/icon/icon3.png" alt=""/></a> 
                </div>
                <div class="icon-content">
                  <h5 class="ttr-tilte">Key Of Success</h5>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod..</p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 m-b30">
              <div class="feature-container">
                <div class="feature-md text-white m-b20">
                  <a href="#" class="icon-cell"><img src="<?php echo URL;?>static/assets/images/icon/icon4.png" alt=""/></a> 
                </div>
                <div class="icon-content">
                  <h5 class="ttr-tilte">Our Philosophy</h5>
                  <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod..</p>
                </div>
              </div>
            </div>
          </div>
        </div>
            </div>
      <!-- About Us END ==== -->

      <div class="container">

         <div class="col-lg-12">

                                <div class=" stepwizard " >
                                    <div class="stepwizard-row setup-panel">
                                        <div class="stepwizard-step col-xs-3"> 
                                            <a href="#step-1" type="button" class="btn btn-success btn-circle">1</a>
                                            <p><small>BIOGRAPHY</small></p>
                                        </div>
                                        <div class="stepwizard-step col-xs-3"> 
                                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                                            <p><small>LOCATION</small></p>
                                        </div>
                                        <div class="stepwizard-step col-xs-3"> 
                                            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                                            <p><small>JOB DETAILS</small></p>
                                        </div>
                                        <div class="stepwizard-step col-xs-3"> 
                                            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                                            <p><small>FINISH</small></p>
                                        </div>
                                    </div>
                                </div>
                        
                                 <form id="registerclientform" role="form">

                                    <div class="card mb-5 card-primary setup-content" id="step-1">
                                        <div class="card-header">
                                             MEMBER BIOGRAPHY
                                        </div>
                                        <div class="card-body p-3">


                                             <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label>Client Photo</label>
                                                        <input type="file" class="filestyle" name="photo" data-input="false" data-buttonname="btn-secondary">
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-form-label">New Client Classification:</label>
                                                        <select name="classification" class="form-control">
                                                            <option  disabled selected>---Select Classification---</option>
                                                            <option  value="employee" >EMPLOYEE</option>
                                                            <option  value="employer" >EMPLOYER</option>
                                                        </select>
                                                    </div>
                                                </div>


                                                 <div class="col-md-4">

                                                    <div class="form-group">
                                                        <label for="nationality">Nationality</label>
                                                        
                                                          <select name="nationality" class="form-control" id="">
                                                             <option  disabled selected="">--Select Nationality--</option>

                                                             <?php echo $countries; ?>
                                                          </select>

                                                    </div>
                                                    

                                                </div>


                                            </div>


                                              <div class="row">


                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <label for="tibe">Tribe</label>
                                                          <select name="tribe" class="form-control">
                                                            <option  disabled selected="">--Select Tribe--</option>

                                                            <?php echo $tribes; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="example-text-input" class="col-form-label">Surname</label>
                                                        <input class="form-control" type="text" name="fname" value="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="example-text-input" class="col-form-label">Given Name</label>
                                                        <input class="form-control" type="text" value="" name="lname" required>
                                                    </div>
                                                </div>


                                            </div>


                                            <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                                        </div>
                                    </div>
                                    
                                    <div class="panel panel-primary setup-content" id="step-2">
                                        <div class="panel-heading">
                                             <h3 class="panel-title">Information Details</h3>
                                        </div>
                                        <div class="panel-body">

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="example-text-input" class="col-form-label">Other Names</label>
                                                        <input class="form-control" type="text" value="" name="oname">
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail">Residence District:</label>
                                                <select class="form-control" id="districtSelect" name="resdistrict">
                                                        <?php echo $districts; ?> 
                                                </select>
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail">County:</label>
                                                <select class="form-control" id="countySelect" name="county">
                                                        <option value="" selected>-- Select County --</option> 
                                                </select>
                                                </div>
                                            </div>

                                            </div>

                                            <div class="row">

                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail">Sub County:</label>
                                                <select class="form-control" id="subCountySelect" name="subcounty">
                                                        <option value="" selected>-- Select Sub County --</option> 
                                                </select>
                                                </div>
                                            </div>

                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail">Parish: </label>
                                                <select class="form-control" id="parishSelect" name="parish">
                                                        <option value="" selected>-- Select Parish --</option>
                                                </select>
                                                </div>
                                            </div>
                                             <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="inputEmail">Village/Zone/Cell:</label>
                                                    <select class="form-control" id="villageSelect" name="village">
                                                            <option value="" selected>-- Select Village/Zone/Cell --</option>
                                                    </select>
                                                    </div>
                                              </div>


                                              </div>
                                   
                                      <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                                        </div>
                                    </div>
                            
                                    <div class="panel panel-primary setup-content" id="step-3">
                                        <div class="panel-heading">
                                             <h3 class="panel-title"> Job Details</h3>
                                        </div>
                                        <div class="panel-body">

                                             <div class="row">

                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="nextofkin">Next of Kin</label>
                                                    <input class="form-control" type="text" name="nextofkin" value="none">
                                                    </div>

                                                </div>

                                                      
                                                   
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Sex</label>
                                                        <select name="sex" class="form-control" required>
                                                            <option selected disabled>---Select Sex---</option>
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                               
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="example-text-input" class="col-form-label">Contact Phone</label>
                                                        <input class="form-control" name="contactphone" type="text" value="" required>
                                                    </div>
                                                </div>
                                               

                                               </div>

                                               <div class="row">
                                                
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="example-text-input" class="col-form-label">Date of Birth</label>
                                                        <input class="form-control" name="dob" type="date" value="" id="date1" required>
                                                    </div>
                                                </div>
                                                
                                               
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label class="col-form-label">Age Group</label>
                                                        <select id="ag"  name="agegroup" class="form-control">
                                                            <option selected disabled>---Select Age group---</option>
                                                            <option value="1-17" >Minor (1-17)</option>
                                                            <option value="18-35" >Youth (18-35)</option>
                                                            <option value="36-50" >Adult (36-50)</option>
                                                            <option value="above 50" >Elderly (Above 50)</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="col-md-4">
                                                    

                                                    <div class="form-group">

                                                        <label for="educationlevel">
                                                            Education Level
                                                        </label>
                                                         <select name="educationlevel" id="" class="form-control">
                                                             <option value="" disabled="" selected="">--Select Level--</option>

                                                             <option value="Diploma">Diploma</option>

                                                             <option value="Degree">Degree</option>
                                                         </select>
                                                    </div>

                                                </div>

                                            </div>
                                

                                                    <button class="btn btn-primary nextBtn pull-right" type="button">Next</button>
                                                </div>
                                            </div>
                                            
                                            <div class="panel panel-primary setup-content" id="step-4">
                                                <div class="panel-heading">
                                                     <h3 class="panel-title">control</h3>
                                                </div>
                                                <div class="panel-body">


                                             <div class="row">


                                        <div class="col-md-4">
                                            
                                            <div class="form-group">

                                                <label for="maritalstatus"> Marital Status</label>

                                                   <select name="maritalstatus" class="form-control">
                                                    <option selected disabled>---Select Marital Status---</option>
                                                    <option value="Defacto but separated">Defacto but separated</option>
                                                    <option value="Divorced">Divorced</option>
                                                    <option value="Living in defacto relationship">Living in Dificult relationship</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Married but separated">Married but separated</option>
                                                    <option value="Single">Single</option>
                                                    <option value="Widowed">Widowed</option>

                                                </select>
                                                
                                            </div>
                                        </div>

                                       
                                       
                                     
                                      

                                           <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Client Email</label>
                                                <div>
                                                    <input type="email" name="email" class="form-control"  id="email" required>
                                                </div>

                                            </div>
                                        </div>
                                     
                                       
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Registration Date</label>
                                                <div>
                                                    <input type="text" name="regdate" class="form-control floating-label" value="<?php echo date('Y-m-d'); ?>" id="date" required>
                                                </div>

                                            </div>
                                        </div>
                                               
                                    </div>

                                      <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="company">Company</label>

                                                <input type="text" name="company" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="contactAddress">Contact Address/Job Description</label>

                                                <input type="text" name="contactaddress" class="form-control">
                                            </div>
                                        </div>
                                    </div>


                                     <button type="submit" class="btn btn-success btn-block" type="submit">Finish!</button>

                                          
                                            

                                           </div>
                                  



                                   
                                </div>
                            </div>
                        </form>
                    </div>
                
        

      </div>
      
            <!-- Our Story ==== -->
      <div class="section-area bg-gray section-sp1 our-story">
        <div class="container">
          <div class="row align-items-center d-flex">
            <div class="col-lg-5 col-md-12 heading-bx">
              <h2 class="m-b10">Our Story</h2>
              <h5 class="fw4">It is a long established fact that a reade.</h5>
              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
              <a href="#" class="btn">Read More</a>
            </div>
            <div class="col-lg-7 col-md-12 heading-bx p-lr">
              <div class="video-bx">
                <img src="<?php echo URL;?>static/assets/images/about/pic1.jpg" alt=""/>
                <a href="https://www.youtube.com/watch?v=x_sJzVe9P_8" class="popup-youtube video"><i class="fa fa-play"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Our Story END ==== -->
      <!-- Our Status ==== -->
      <div class="section-area content-inner section-sp1">
                <div class="container">
                    <div class="section-content">
                         <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                                    <div class="text-primary">
                    <span class="counter">3000</span><span>+</span>
                  </div>
                  <span class="counter-text">Completed Projects</span>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                  <div class="text-black">
                    <span class="counter">2500</span><span>+</span>
                  </div>
                  <span class="counter-text">Happy Clients</span>
                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                  <div class="text-primary">
                    <span class="counter">1500</span><span>+</span>
                  </div>
                  <span class="counter-text">Questions Answered</span>
                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                                <div class="counter-style-1">
                  <div class="text-black">
                    <span class="counter">1000</span><span>+</span>
                  </div>
                  <span class="counter-text">Ordered Coffee's</span>
                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      <!-- Our Status END ==== -->
      <!-- About Content ==== -->
      <div class="section-area section-sp2 bg-fix ovbl-dark join-bx text-center" style="background-image:url(<?php echo URL ?>static/assets/images/background/bg1.jpg);">
                <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="join-content-bx text-white">
                <h2>Learn a new skill online on <br/> your time</h2>
                <h4><span class="counter">57,000 </span> Online Courses</h4>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                <a href="#" class="btn button-md">Join Now</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- About Content END ==== -->
      <!-- Testimonials ==== -->
      <div class="section-area section-sp2">
        <div class="container">
          <div class="row">
            <div class="col-md-12 heading-bx left">
              <h2 class="title-head text-uppercase">what people <span>say</span></h2>
              <p>It is a long established fact that a reader will be distracted by the readable content of a page</p>
            </div>
          </div>
          <div class="testimonial-carousel owl-carousel owl-btn-1 col-12 p-lr0">
            <div class="item">
              <div class="testimonial-bx">
                <div class="testimonial-thumb">
                  <img src="<?php echo URL;?>static/assets/images/testimonials/pic1.jpg" alt="">
                </div>
                <div class="testimonial-info">
                  <h5 class="name">Peter Packer</h5>
                  <p>-Art Director</p>
                </div>
                <div class="testimonial-content">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...</p>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="testimonial-bx">
                <div class="testimonial-thumb">
                  <img src="<?php echo URL;?>static/assets/images/testimonials/pic2.jpg" alt="">
                </div>
                <div class="testimonial-info">
                  <h5 class="name">Peter Packer</h5>
                  <p>-Art Director</p>
                </div>
                <div class="testimonial-content">
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type...</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Testimonials END ==== -->
    </div>
    <!-- Page Content Box END ==== -->
    </div>
    <!-- Inner Content Box END ==== -->
    <!-- Footer ==== -->
  