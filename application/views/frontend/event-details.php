   <div id="section-5">
        <div class="section_holder30">
            <div class="container">

                  <div class="row"> 

                        <div class="one_half first">
                            <div class="address_info two">

                                <img src="<?php echo URL.'data/'.date('Y').'/'. date('M').'/'.'events/'.$details[0]->banner; ?>" width="100%" height="500" alt="event"/>
                            </div>
                        </div>
                                    
                        <div class="one_half last">
                            <div class="address_info two">

                            <h4 class="uppercase"><strong> DETAILS FOR  <?php echo isset($details) ? $details[0]->title : 'Event Title'; ?> </strong>

                                <div class="btn-group pull-right">
                                
                                <button onclick="attendEvent('<?php echo base64_encode($details[0]->id); ?>','1')" class="btn btn-success "><span class="white">
                                     <i class="fa fa-check-circle"></i> Iam In
                                </span>
                              </button>

                                <button  onclick="dontAttendEvent('<?php echo base64_encode($details[0]->id); ?>','0')" class="btn btn-danger  "><span class="white">
                                   <i class="fa fa-times-circle"></i> Iam not in  
                                </span>
                            </button>

                                </div>
                            </h4>

                            <h5> Status :

                             <?php

                              $check1 = $model->checkifAdttending($details[0]->id,$user_info['id'],1);

                            if($check1){
                                echo '<span class="text-success">
                                   <i class="fa fa-check"></i>Registered  
                                </span>';
                            }else{
                                echo '<span class="text-danger">
                                   <i class="fa fa-times"></i> Not Registered  
                                </span>';
                            }


                             ?></h5>
                            <hr>  
                            <p class="response text-success" style="display:none"></p>                       

                              <p> <b>LOCATION</b>: <?php echo  $details[0]->location; ?></p>
                              <br />

                              <p> <b>DATE POSTED</b>: <?php echo date_format(date_create( $details[0]->date_created), ' l jS F Y'); ?>
                              </p>
                              <br />

                              <p><b>DESCRIPTION</b></p>
                              <br>

                      
                            <div class=" text-muted fefe">
                                <?php echo isset($details) ? $details[0]->content : ''; ?>
                            </div>

                           </div>                        
                      </div>
                   
               </div>
         </div>

     </div>
 </div>

