 <div class="clearfix"></div>  
  <div class="section_holder37 three">
                      <div class="container"> 
                            <div class="table-responsive table-style">
                                <table class="table table-bordered table-list2" id="datatable1">
                                            <thead>
                                                <tr>
                                                    <th>BANNER</th>
                                                    <th> TITLE</th>
                                                    <th>CATEGORY</th>
                                                    <th>LOCATION</th>
                                                    <th>POSTED BY</th>
                                                    <th>DATE</th>
                                                    <th>ACTIONS</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                <?php 

                                    if($all_events):
                                        foreach ($all_events as $key => $event) {?>
                                    <tr>

                                        <td><img src="<?php echo URL.'data/'.date('Y').'/'. date('M').'/'.'events/'.$event->banner; ?>" alt="banner" width="170" height="120">
                                        </td>
                                        
                                        <td><?php echo strtoupper($event->title); ?>
                                        </td>

                                        <td class="text-muted"><?php echo strtoupper($event->category); ?>
                                        </td>

                                        <td class="text-muted"><?php echo strtoupper($event->location); ?>
                                        </td>
                                            
                                        
                                        <td>
                                        <?php echo strtoupper($event->created_by);  ?>
                                        </td>
                                                
                                        
                                        <td> <span>
                                            <?php echo strtoupper(dateF($event->date_created));  ?></span>
                                        </td>
                                        <td>
                                            <a href="<?php echo URL ?>application/event-details/<?php echo base64_encode($event->id) ?>" class="btn btn-info">More Info</a>
                                            <a target="_blank" href="<?php //echo $event->event_link;  ?>" class="btn btn-danger"><i class="fa fa-arrow-right"></i> Register</a>
                                           
                                        </td>
                                    
                                    

                                </tr>
                                

                            <?php
                             }

                            endif;

                             ?>
                         </tbody>

                             <?php 

                                function dateF($date){
                                   return date_format(date_create($date), ' l jS F Y'); 
                                } 

                                ?>

                        </table>
                        
                           </div>
                           
                        </div>
                        
                    </div><!--End col-md-9 -->
                    
                   
                    
                </div><!--End row -->
            </div><!--End container -->
        </div><!--End container_gray_bg -->