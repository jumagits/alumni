  <div id="page-wrapper">
            <div class="container-fluid">   
            <div class="panel">
                  <h4>JOBS 
                  <a  data-toggle="modal" data-target=".bs-edit-modal-lg" class="pull-right btn  btn-danger"><i class="fa fa-plus"></i> Add Field</a>
              </h4><hr>
                
              <div class="panel-body">          
                <div class="table-responsive ">
                         <table class="table table-hover table-bordered table-striped" id="datatable1" width="100%">
                                <thead>
                                   <tr>                                             
                                    <th>S/N</th>
                                    <th>Field</th>
                                    <th>Company</th>
                                    <th>Location</th>
                                    <th>Title</th>
                                    <th>Posted By</th>
                                    <th>Posted On</th>
                                    <th><i class="fa fa-edit "></i>Edit</th>
                                    <th><i class="fa fa-trash "></i>Delete</th> 
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        $count = 0;
                                        foreach ($all_jobs as $key => $job) {  $count++;?>
                                    <tr>
                                        <td><?php echo $count; ?></td>
                                        <td><?php echo $job->field; ?></td>
                                        <td><?php echo $job->company; ?></td>
                                        <td><?php echo $job->location; ?></td>
                                        <td><?php echo $job->job_title; ?></td>
                                        <td>
                                        <?php echo $accountsModel->get_client_details($job->user_id);?>
                                        </td>

                                        <td><?php echo dateF($job->date_created); ?></td>

                                        <td><a class="btn btn-xs btn-info" href="<?php echo URL; ?>job/edit/<?php echo base64_encode($job->id); ?>"><i class="fa fa-edit"></i> Edit</a></td>

                                        <td><a class="btn btn-xs btn-danger" onclick="confirm('Are you sure to delete this job, it is Irriversible')"  href="<?php echo URL ?>job/delete-job/<?php echo base64_encode($job->id); ?>"><i class="fa fa-trash " ></i> Trash</a></td>
                                    </tr>
                                 <?php } ?>
                            </tbody>
                        </table>
                    </div>
                 </div>
             </div>
        </div>
    </div>

<?php
function dateF($date){
   return date_format(date_create($date), ' l jS F Y'); 
}?>

<div class="modal fade bs-edit-modal-lg "  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary" >
                <h4 class="modal-title " id="myLargeModalLabel">Register New Job
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
                </h4>
            </div>
            <div class="modal-body">
                      
                <form action="<?php echo URL;?>job/add" id="makeJob">                   
                         <div class="form-group ">
                            <label for="field" class="control-label">Job Link</label>
                            <input type="text" name="job_link" class="form-control">
                        </div>
                        <div class="form-group">
                             <label for="field" class="control-label">Job Field</label>
                            <select name="field" class="form-control">
                                <option value="#" selected="" required disabled="">-- Select Job Field --</option>
                                <?php foreach ($fields as $key => $value) { ?>
                                    <option value="<?php echo $value->field; ?>"><?php echo $value->field; ?></option>
                                  <?php } ?>
                            </select>
                        </div>
                   

                     <div class="form-group  ">
                        <label for="" class="control-label">Job Image/ Company Logo</label>
                        <input type="file" class="form-control" name="photo">
                    </div>
                   
                    <div class="form-group  ">
                    <label for="posted_by" class="control-label">Posted by</label>
                            <select name="posted_by" class="form-control">
                                <option value="#" disabled="" required  selected="">-- Select Member To Post --</option>
                                <?php echo $all_clients; ?>
                            </select>
                    </div>

                     <div class="form-group ">                           
                            <label for="" class="control-label">Company Name</label>
                            <input type="text" class="form-control" name="company"   required>                            
                    </div>
                   
                    <div class="form-group ">
                            <label for="" class="control-label">Job Title</label>
                            <input type="text" class="form-control" name="title"   required>
                    </div>                    

                     <div class="form-group ">                       
                            <label for="" class="control-label">Company Location</label>
                            <input type="text" class="form-control" name="location"   required>                        
                    </div>
                   
                        <div class="form-group  ">
                             <label for="" class="control-label">Deadline Date</label>
                            <input type="date" class="form-control " name="deadline" value="<?php echo date('Y-m-d H:i:s'); ?>"  required autocomplete="off">
                        </div>

                  
                   
                    <div class="form-group ">                       
                            <label for="" class="control-label">Job Description</label>
                            <textarea name="description" class="form-control text-jqte" cols="30" rows="5" required></textarea>
                    </div>

                    <div class="form-group">
                         <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light">Submit</button>
                    </div> 
                </form>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function(){
    
    var displayProduct = 10;
    $('#results').html(createSkeleton(displayProduct));
    
    setTimeout(function(){
      loadProducts(displayProduct);
    }, 5000);

    function createSkeleton(limit){
      var skeletonHTML = '';
      for(var i = 0; i < limit; i++){
        skeletonHTML += '<div class="ph-item">';
        skeletonHTML += '<div class="ph-col-4">';
        skeletonHTML += '<div class="ph-picture"></div>';
        skeletonHTML += '</div>';
        skeletonHTML += '<div>';
        skeletonHTML += '<div class="ph-row">';
        skeletonHTML += '<div class="ph-col-12 big"></div>';
        skeletonHTML += '<div class="ph-col-12"></div>';
        skeletonHTML += '<div class="ph-col-12"></div>';
        skeletonHTML += '<div class="ph-col-12"></div>';
        skeletonHTML += '<div class="ph-col-12"></div>';
        skeletonHTML += '</div>';
        skeletonHTML += '</div>';
        skeletonHTML += '</div>';
      }
      return skeletonHTML;
    }
    
    function loadProducts(limit){
      $.ajax({
        url:"load_action.php",
        method:"POST",
        data:{action: 'load_products', limit:limit},
        success:function(data) {
          $('#results').html(data);
        }
      });
    }

  });