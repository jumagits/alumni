  <div id="page-wrapper">
        <div class="container">
           
            <div class="row">
                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading">
                             <h4> Edit Field
                              <a href="<?php echo URL ?>job/fields" class="pull-right btn btn-danger">Cancel</a>
                             </h4>
                        </div>
                          <div class="panel-body " style="padding: 30px;">
                                    <form action="<?php echo URL ?>job/update-field" id="ejobF"> 
                                        <input type="hidden" name="id" value="<?php echo $job[0]->id; ?>">
                                            <div class="form-group ">
                                                    <label for="" class="control-label">Job Field</label>
                                            <input type="text" class="form-control" name="field"   required value="<?php echo $job[0]->field; ?>">
                                                </div>
                                            <input type="hidden" name="urlEnd" value="<?php echo URL ?>job/fields">
                                        <div class="form-group"> 
                                            <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light">Submit</button>
                                                </div>
                                            </form>        
                                        </div>
                                    </div>
                                </div>
                            </div>        
                         </div>
                    </div>
                       



<script>
    
    jQuery(document).ready(function($) {
        
         $("form#ejobF").submit(function(e) {

         e.preventDefault();       
         var formData = new FormData(this);
         var action = $('form#ejobF').attr('action');
         var urlEnd = $('input[name="urlEnd"]').val();

               $.ajax({
                   url: action,
                   type: 'POST',                  
                   data: formData,
                   success:function(res){
                  swal({
                       title: "Good job!",
                       text: res.msg,
                       type: "success",
                       showCancelButton: !0,
                       confirmButtonClass: "btn btn-success",
                       cancelButtonClass: "btn btn-danger m-l-10"
                     })

                  setInterval(function(){

                    window.location.href = urlEnd;

                  },2000);

                  
                   },
                  error: function(jqXHR, textStatus, errorThrown) {
                         console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
               })
               .done(function() {
                   console.log("success");
               })
              
               
               
             

          });

    });
</script>