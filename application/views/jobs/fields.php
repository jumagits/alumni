  <div id="page-wrapper">
            <div class="container-fluid">
                        <div class="panel">
                            <div class="panel-heading bg-primary">
                                <h4> EMPLOYMENT FIELDS
                                <a  data-toggle="modal" data-target=".bs-edit-modal-lg" class="pull-right btn  btn-danger"><i class="fa fa-plus"></i> Add Field</a>
                                </h4>                              
                            </div>
                            <div class="panel-body " >
                               <div class="row">              
                                  <div class="col-lg-12">
                                    <div class="table-responsive ">
                                        <table class="table table-hover table-bordered table-striped" id="datatable1" width="100%">
                                            <thead>
                                                <tr>
                                                   <tr>
                                                    <th>S/N</th>
                                                    <th>Field</th>        
                                                    <th><i class="fa fa-edit "></i> Edit</th>
                                                    <th><i class="fa fa-trash " ></i> Trash</th> 
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $count = 0;
                                        foreach ($fields as $key => $job) {  $count++;?>
                                            <tr>
                                                <td><?php echo $count; ?></td>
                                                 <td>
                                                <?php echo $job->field; ?> 
                                                    </td>
                                                      <td><a class="btn btn-xs btn-info" href="<?php echo URL; ?>job/edit-field/<?php echo base64_encode($job->id); ?>"><i class="fa fa-edit "></i> Edit</a></td>

                                                      <td><a class="btn btn-xs btn-danger" onclick="confirm('Are you sure to delete this job, it is Irriversible')"  href="<?php echo URL ?>job/delete-field/<?php echo base64_encode($job->id); ?>"><i class="fa fa-trash " ></i> Trash</a></td>
                                            </tr>
                                            <?php } ?>                
                                            </tbody>
                                        </table>
                                      </div>
                                    </div>
                                  </div>                                    
                                </div>                         
                              </div>
                            </div>
                   


<?php 

function dateF($date){
   return date_format(date_create($date), ' l jS F Y'); 
}
 ?>
<div class="modal fade bs-edit-modal-lg "  role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-primary" >
                <h4 class="modal-title " id="myLargeModalLabel">Register New Job Field
                <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
                </h4>
            </div>
            <div class="modal-body">
                      
                <form action="<?php echo URL;?>job/create_cat" id="cat">
                    
                       <div class="row">
                            <div class="form-group col-md-12 ">
                                 <label for="field" class="control-label">Job Field</label><br>
                                 <input type="text" name="field" placeholder="Field" class="form-control">
                            </div>
                       </div>

                    
                   <button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light">Submit</button>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<script>
    
    jQuery(document).ready(function($) {
        
         $("form#cat").submit(function(e) {

         e.preventDefault();       
         var formData = new FormData(this);
         var action = $('form#cat').attr('action');

               $.ajax({
                   url: action,
                   type: 'POST',                  
                   data: formData,
                   success:function(res){
                  swal({
                       title: "Good job!",
                       text: res.msg,
                       type: "success",
                       showCancelButton: !0,
                       confirmButtonClass: "btn btn-success",
                       cancelButtonClass: "btn btn-danger m-l-10"
                     })

                  window.location.reload();
                   },
                  error: function(jqXHR, textStatus, errorThrown) {
                         console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
               })
               .done(function() {
                   console.log("success");
               })
               .fail(function() {
                   console.log("error");
               })
               .always(function() {
                   console.log("complete");
               });

          });

    });
</script>

