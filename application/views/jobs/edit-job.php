<div id="page-wrapper">


<div class="container-fluid">


<div class="panel">

<div class="panel-heading bg-primary">
<h4> Edit job <a href="<?php echo URL ?>job" class="pull-right btn btn-danger">Cancel</a></h4>
</div>

<div class="panel-body " style="padding: 30px;">

<form action="<?php echo URL ?>job/update" id="edit-job">

<div class="row">

 <div class="form-group col-md-6 ">
 <label for="job_link" class="control-label">Job Link</label><br><input type="text" name="job_link" class="form-control" value="<?php echo $job[0]->job_link; ?>">

</div>


<div class="form-group col-md-6 ">
     <label for="field" class="control-label">Job Field</label><br>
    <select name="field" class="form-control">
        <option value="<?php echo $job[0]->field; ?>" selected="" required ><?php echo $job[0]->field; ?></option>

         <?php foreach ($fields as $key => $value) { ?>
            <option value="<?php echo $value->field; ?>"><?php echo $value->field; ?></option>
          <?php } ?>
   


    </select>
</div>

</div>

<input type="hidden" name="urlEnd" value="<?php echo URL ?>job">

<input type="hidden" name="id" value="<?php echo $job[0]->id; ?>">

<div class="row">

<div class="form-group col-md-6 ">
    <label for="" class="control-label">Job Image/ Company Logo</label>
    <input type="file" class="form-control" name="photo">

</div>



<div class="form-group col-md-6 ">
 <label for="posted_by" class="control-label">Posted by</label><br>
<select name="posted_by" class="form-control">
    <option value="<?php echo $job[0]->user_id ?>" selected="">
      <?php echo $accountsModel->get_client_details($job[0]->user_id); ?></option>

    <?php echo $all_clients; ?>

</select>
</div>

</div>




<div class="row">

<div class="form-group col-md-6 ">

    <label for="" class="control-label">Company Name</label>
    <input type="text" class="form-control" name="company"   required value="<?php echo $job[0]->company; ?>">

</div>

<div class="form-group col-md-6 ">
    <label for="" class="control-label">Job Title</label>
    <input type="text" class="form-control" name="title"   required value="<?php echo $job[0]->job_title; ?>">
</div>

</div>

<div class="row">

<div class="form-group col-md-6 ">

    <label for="" class="control-label">Company Location</label>
    <input type="text" class="form-control" name="location"  value="<?php echo $job[0]->location; ?>" required>

</div>

<div class="form-group col-md-6 ">
 <label for="" class="control-label">Deadline Date</label>
<input type="date" class="form-control " name="deadline"   required autocomplete="off" <?php echo $job[0]->deadline; ?>>
</div>

</div>

<div class="form-group row">
<div class="col-md-12">
<label for="" class="control-label">Job Description</label>
<textarea name="description" class="form-control text-jqte" cols="30" rows="5" required>
  <?php echo html_entity_decode($job[0]->description); ?>
    
</textarea>
</div>
</div>

<button type="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light">Submit</button>

</form>




</div>

</div>

</div>
</div>











<script>

jQuery(document).ready(function($) {

$("form#edit-job").submit(function(e) {

e.preventDefault();       
var formData = new FormData(this);
var action = $('form#edit-job').attr('action');
var urlEnd = $('input[name="urlEnd"]').val();

$.ajax({
url: action,
type: 'POST',                  
data: formData,
success:function(res){
swal({
title: "Good job!",
text: res.msg,
type: "success",
showCancelButton: !0,
confirmButtonClass: "btn btn-success",
cancelButtonClass: "btn btn-danger m-l-10"
})

setInterval(function(){

window.location.href = urlEnd;

},12000);


},
error: function(jqXHR, textStatus, errorThrown) {
console.log(jqXHR);
},
cache: false,
contentType: false,
processData: false
})
.done(function() {
console.log("success");
})
.fail(function() {
console.log("error");
})
.always(function() {
console.log("complete");
});




});

});
</script>