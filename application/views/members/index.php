  <div id="page-wrapper">

            <div class="container-fluid">

                <h4> <b> BUMAA ALUMNI MEMBERS <a href="<?php echo URL ?>register_clients/register_alumni"  class="pull-right btn btn-sm btn-danger"><i class="fa fa-plus"></i> Add New Member</a>
                                </b></h4><hr>

                
                        <div class="panel">
                            
                            <div class="panel-body ">
                                    <div class="table-responsive ">
                                        <table class="table table-hover table-bordered table-striped" id="datatable1" width="100%">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>S/N</th>                                                   
                                                    <th>FULLNAME</th>
                                                    <th>EMAIL</th> 
                                                    <th>VIEW</th>
                                                    <th>STATUS</th>
                                                    <th>UPDATE</th>                                        
                                                    <th>DELETE</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <?php

                                                $count =0;


                                                foreach ($all_clients as $key => $client) {

                                                    $count++;
                                                ?>


                                                <tr>                                                    
                                                    <td><?php echo $count; ?></td>
                                                  
                                                    <td>
                                                        <?php echo $client->fname ." ".$client->lname; ?>
                                                    </td>                                                   
                                                    <td><?php echo strtoupper($client->email); ?></td>
                                                 
                                                    <td><a href="<?php echo URL ?>register_clients/member-details/<?php echo base64_encode($client->client_refnumber); ?>" class="btn btn-info">VIEW</a>
                                                    </td>

                                                    <td>
                                                    <a href='<?php echo URL; ?>register-clients/NewDeativate/<?php echo base64_encode($client->id); ?>' >
                                                    <?php if(strtolower($client->status) == '1'){
                                                        echo "<span class='btn btn-sm btn-success'>ACTIVATED</span>";
                                                    } 
                                                   else{
                                                    echo "<span class='btn btn-sm btn-danger'>DEACTIVATED</span>";}?></a>
                                                   </td>
                                                  
                                                    <td>
                                                    <a href="<?php echo URL; ?>register-clients/manager-alumni/<?php echo base64_encode($client->id); ?>" class="btn btn-warning">Update 
                                                    </td>
                                                   
                                                    <td>
                                                        <a href="<?php echo URL; ?>register-clients/delete-user/<?php echo base64_encode($client->id); ?>" onclick="return confirm('Are you sure you want to Delete Alumni?');"  class="btn btn-danger">Delete </a>
                                                    </td>


                                                </tr>


                                            <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                         
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


<?php 

function dateF($date){
   return date_format(date_create($date), ' l jS F Y'); 
}

 ?>


<!-- end of page -->











<!--  Modal Prison for the above example -->

<!-- /.modal -->

