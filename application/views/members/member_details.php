  <div id="page-wrapper">

      <div class="container-fluid">
          <?php  if (isset($details) AND !empty($details) ) {
                    foreach ($details as $key => $info) { }} ?>
          <div class="row">

             <div class="col-md-6">
                <div class="panel"> 
                    <div class="panel-body " >

                  <div class="row">

                      <div class="col-md-12">

                        <ul class="list-group">

                          <li class="list-group-item active">
                               <?php echo isset($pageTitle)? $pageTitle: ""; ?> FOR <?php echo $info->fname." ".$info->lname; ?>  
                          </li>


                          <li class="list-group-item ">
                          <b>AdminCode <i class="fa fa-arrow-right"></i></b>  <?php  echo (isset($info->adminCode) AND !empty($info->adminCode) )? base64_decode($info->adminCode) : "Not Admin"; ?>
                          </li>
                          
                          <li class="list-group-item ">
                          <b>Firstname <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->fname; ?>
                          </li>

                          <li class="list-group-item ">
                          <b>Lastname <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->lname; ?>
                          </li>


                          <li class="list-group-item ">
                          <b>Username <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->username; ?>
                          </li>

                          <li class="list-group-item ">
                          <b>Email <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->email; ?>
                          </li>

                          <li class="list-group-item ">
                          <b>Sex <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->sex; ?>
                          </li>

                          <li class="list-group-item ">
                          <b>Marital Status <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->maritalstatus; ?>
                          </li>

                          <li class="list-group-item ">
                          <b>Contact <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->contactphone; ?>
                          </li>

                          <li class="list-group-item ">
                          <b>Transcript No <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->transcript_no; ?>
                          </li>

                          <li class="list-group-item ">
                          <b>Student No <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->student_no; ?>
                          </li>

                          <li class="list-group-item ">
                          <b>Current Information About Member <i class="fa fa-arrow-right"></i></b>  <?php  echo $info->knowabout; ?>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
             </div> 
             <div class="col-md-6">
                    <div class="panel">
                      <div class="panel-body">

                         <form id="sendCode" action="<?php echo URL ?>adminEnd/send_admin_code">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" readonly="" value="<?php  echo $info->email; ?>" name="email" class="form-control" >
                                      <input type="hidden" name="id" value="<?php echo $info->id ?>" >
                                </div>
                                
                                <div class="form-group">

                                  <?php 
                               
                               if($info->adminCode == ""){
                                   ?>
                                    <button name="send" class="btn btn-danger btn-block " type="submit">Send Code </button>
                                </div>
                              <?php }else{ ?>


                                <div class="alert alert-success">
                                  Already given Administration access
                                </div>
                                

                                <?php 
                                }
                                 ?>
                            </form> 
                        
                      </div>
                    </div>
                    <hr>

                     <div class="panel">
                      <div class="panel-body">
                        <p><span class="btn btn-danger " disabled>Message</span> Send a message/ information to <?php  echo $info->fname." ".$info->lname; ?></p><hr>

                         <form id="sendNewletter" action="<?php echo URL ?>adminEnd/reply_newsletter_action">
                                <div class="form-group">
                                    <label for="email">Email</label>
                                    <input type="email" readonly="" value="<?php  echo $info->email; ?>" name="email" class="form-control" >
                                      <input type="hidden" name="id" value="<?php echo $info->id ?>" >
                                </div>

                                <div class="form-group">
                                    <textarea name="message" id="" cols="2" rows="4" class="form-control" placeholder="Type your Message..."></textarea>
                                </div>
                                
                                <div class="form-group">      
                                    <button name="send" class="btn btn-danger btn-block " type="submit">Send Message</button>
                                </div>
                           
                            </form> 
                        
                      </div>
                    </div>
             </div>

          </div>         
      </div>
  </div>

  <?php 

function dateF($date){
   return date_format(date_create($date), ' l jS F Y'); 
}

 ?>


<script>
    
    jQuery(document).ready(function($) {        
         $("form#sendCode").submit(function(e) {
         e.preventDefault();       
         var formData = new FormData(this);
         var action = $('form#sendCode').attr('action');       
               $.ajax({
                   url: action,
                   type: 'POST',                  
                   data: formData,
                   success:function(res){
                  swal({
                       title: "Good job!",
                       text: res.msg,
                       type: "success",
                       showCancelButton: !0,
                       confirmButtonClass: "btn btn-success",
                       cancelButtonClass: "btn btn-danger m-l-10"
                     })

                  setInterval(function(){
                    window.location.reload();
                  },4000);
                  
                   },
                  error: function(jqXHR, textStatus, errorThrown) {
                         console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
               })
               .done(function() {
                   console.log("success");
               })
          });

    });


     jQuery(document).ready(function($) {        
         $("form#sendNewletter").submit(function(e) {
         e.preventDefault();       
         var formData = new FormData(this);
         var action = $('form#sendNewletter').attr('action');       
               $.ajax({
                   url: action,
                   type: 'POST',                  
                   data: formData,
                   success:function(res){
                  swal({
                       title: "Good job!",
                       text: res.msg,
                       type: "success",
                       showCancelButton: !0,
                       confirmButtonClass: "btn btn-success",
                       cancelButtonClass: "btn btn-danger m-l-10"
                     })

                  setInterval(function(){
                    window.location.href = '<?php echo URL; ?>adminEnd/newsletter';
                  },4000);
                  
                   },
                  error: function(jqXHR, textStatus, errorThrown) {
                         console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
               })
               .done(function() {
                   console.log("success");
               })
          });

    });
</script>