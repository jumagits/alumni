  <div id="page-wrapper">
      <div class="container-fluid"> 
          <div class="panel">
            <div class="panel-body bg-light ">
             <div class="row">                     
                <div class="col-md-6">
                      <p class="header-title"><b>Fullname:</b> <?php echo $user['fullname'] ?></p>
                      <p class="header-title"><b>Contact:</b> <?php echo $user['phonenumber']; ?></p>
                      <p class="header-title"> <b>Email Address:</b><?php echo $user['email']; ?></p>
               </div> 

                <div class="col-md-6">
                      <p class="header-title"><b>Campus:</b> <?php echo $user['title'] ?></p>
                      <p class="header-title"><b>Contact:</b> <?php echo $user['phonenumber']; ?></p>
                      <p class="header-title"> <b>Username:</b><?php echo $user['username']; ?></p>
               </div>
             </div>                       
            </div>
          </div>
                 

           <div class="panel">                     
                   <div class="panel-heading bg-primary">
                        Current Access Role(s)
                        </div>
                       <div class="panel-body">
                                <?php if(isset($revoke_feedback)){echo $revoke_feedback;}?>
                                  <button data-toggle="modal" data-target=".user-add-roles"
                                          style="margin-bottom:12px;align:right;" class="btn btn-primary"><i class="fas fas fa-code-branch"></i> Add New Clinic Access</button><br>
                                      <table class="table mb-0 " id="datatable1">
                                          <thead>
                                              <tr>
                                                  <th>#</th>
                                                  <th><b>Clinic</b></th>
                                                  <th><b>Role</b></th>
                                                  <th><b>Action</b></th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              <?php $i=1; foreach ($user_roles as $key) { ?>
                                              <tr>
                                                  <td><?php echo $i; $i++; ?></td>
                                                  <td><?php echo $accounts_model->get_clinic($key->clinic_id)[0]->clinic_name; ?>
                                                  </td>
                                                  <td><?php echo $accounts_model->get_usercatergory($key->clinicrole)[0]->category_name; ?>
                                                  </td>
                                                  <td id="revoke">
                                                    <a class="btn btn-danger"
                                                          href="<?php echo URL; ?>register-clients/revoke-access/<?php echo base64_encode($key->id); ?>"
                                                          onclick="return confirm('Are you sure you want Revoke Acesss?');"><i class="fa fa-trash" style="font-size:15px;"> </i> Revoke  Access</a>
                                                     </td>
                                              </tr>
                                              <?php } ?>

                                          </tbody>
                                      </table>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- end row -->
             
  

  <div class="modal fade user-add-roles" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true"
      data-backdrop="false">
      <div class="modal-dialog modal-dialog-centered card-red">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title mt-0"> Add New Clinic Access</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              </div>
              <div class="modal-body">
                  <form id="addclinicaccessrole" action="<?php echo URL; ?>register-clients/add-access">
                      <input class="form-control" type="hidden" name="username"
                          value="<?php echo $user['username']; ?>">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="col-form-label">Access Level</label>
                              <select name="accesslevel" class="form-control select2">
                                  <?php echo $accounts_model->getAllAccessLevels(); ?>
                              </select>
                          </div>
                      </div>
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="col-form-label">BUMAA Clinic</label>
                              <select name="clinics" class="form-control select2">
                                  <?php echo $accounts_model->getAllClinics(); ?>
                              </select>
                          </div>
                      </div>
                      <button type="submit" class="btn btn-primary btn-lg btn-block waves-effect waves-light">Add  Access</button>
                  </form>
              </div>
          </div>
          <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>