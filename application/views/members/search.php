  <div id="page-wrapper">


      <div class="container-fluid">

          <!-- Page Heading -->
                  <div class="row">
                      <div class="col-lg-12">
                          <h4 class="page-header ">
                              Hey <?php echo !isset($user_info) ? header('URL'): $user_info['names'];
                                        ?>, Welcome to <?php echo $pageTitle; ?>
                          </h4>
                      </div>
                  </div>

          <!--    header end -->

                        <div class="panel ">
    
                                    <div class="panel-heading">

                                      <div class="alert alert-info alert-dismissible " role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">X</span>
                                            </button> 
                                           <strong>Hello <span class="badge badge-primary"><?php if(isset($user_info['names'])){echo $user_info['names'];} ?></span>!</strong> Please search for the client information by the name and load the client to manage him/her and If client doesn't exist then register Him/Her</span>.
                                    </div>
                                        

                                        <form id="searchclientform" action="<?php echo URL;?>register-clients/makeSearch">

                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <input class="form-control input-lg" autocomplete="off" type="search" placeholder="Search by Name or Email Address" id="search-input">
                                                </div>
                                            </div>
                                        </form>

                                    </div>


                                        
                                      <div class="panel-body">
                                          


                                            <div class="search_table"></div>





                                      </div>
                                    

                                        
                                </div>
                                
                            </div>
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end container-fluid -->
            </div>
            <!-- end page content-->
        </div>
  
       
                                 
   
    <!-- page wrapper end -->
    <!-- Footer -->