<div id="page-wrapper">
<div class="container-fluid">

<style>
input{
border-radius: 0px !important;
}
label{
color:#286090 !important;
}
</style>

<!-- Page Heading -->
<div class="row">



<div class="col-lg-12">

 

            <div class="panel  ">
                <div class="panel-heading bg-primary">
                     <h3 class="panel-title">COMPANY REGISTRATION</h3>
                </div>
                  <div  class="panel-body" >


                            <h3 class="text-center">

                            COMPANY REGISTRATION 
                            </h3>


                        <div class="fb"></div>

                        <form class="" style="padding: 20px;" id="registerC"
                            action="<?php echo URL; ?>frontend/company-register-in"
                            style="background-color: #fff !important;">

                            <div class="form-group ">
                                <label for="companyName">Company Name<span class="text-danger">*</span>
                                </label>
                                <input type="text" name="companyName" class="form-control" required>
                            </div>

                            <div class="form-group ">
                                <label for="companyLocation">Company Location<span class="text-danger">*</span></label>
                                <input type="text" name="companyLocation" class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label for="username">Username<span class="text-danger">*</span>
                                </label>
                                <input type="text" name="username" class="form-control" required>
                            </div>

                            <div class="form-group ">
                                <label for="companyEmail">Company Email.<span class="text-danger">*</span>
                                </label>
                                <input type="email" name="companyEmail" class="form-control" required>
                                <small class="eerror"></small>
                            </div>

                            <div class="form-group">
                                <label for="password">Password<span class="text-danger">*</span>
                                </label>
                                <input type="password" name="password" class="form-control" required>
                            </div>

                            <div class="form-group ">
                                <label for="cpassword">Confirm Password<span class="text-danger">*</span>
                                </label>
                                <input type="password" name="cpassword" class="form-control" required>
                            </div>


                            <div class="form-group">
                                <label for="about_company">About Company<span class="text-danger">*</span></label>
                                <textarea name="about_company" cols="3" rows="2" class="form-control"></textarea>
                            </div>

                            <div class="form-group mar-bot-0 ">
                                <button type="submit" name="login" class="btn btn-info btn-block">SUBMIT
                                    REGISTRATION
                                </button>
                            </div>

                            <input type="hidden" name="url" value="<?php// echo URL ?>validate/check">
                        </form>

                    </div>
            </div>
          



           
        </div>
    </div>

</div>




<!-- end content -->


<script type="text/javascript">

    jQuery(document).ready(function($) {
        $("form#registerC").submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            var action = $('form#registerC').attr('action');
            $.ajax({
                url: action,
                type: 'POST',
                data: formData,
                success: function(res) {
                    $('.fb').html('<div class="alert alert-success">'+res.msg+'</div>');
                    setTimeout(function() {
                        window.location.reload();
                    }, 5000)
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);                },
                cache: false,
                contentType: false,
                processData: false
            });
        });
    });


</script>









