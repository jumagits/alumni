<div id="page-wrapper">
<div class="container-fluid">
     <div class="panel  ">

        <h4>ALUMNI REGISTRATION</h4><hr>
                
                <div class="panel-body">

                    <div class="fb"></div>


                         <form class="panel " id="registerA" action="<?php echo URL ?>frontend/alumni-register-in">

                                <div class="row">

                                  


                               <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="username">FIRSTNAME<span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="firstname" class="form-control" required>
                                        <small class="error"></small>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="lastname">LASTNAME<span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="lastname" class="form-control" required>
                                        <small class="error"></small>
                                    </div>
                                </div>

                                 <div class="col-md-4">

                                    <div class="form-group">
                                        <label for="username">USERNAME<span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="username" class="form-control" required>
                                        <small class="error"></small>
                                    </div>

                                </div>


                              <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="username">GENDER
                                            <span class="text-danger">*</span>
                                        </label>
                                        <select name="sex" class="form-control custom-select">
                                            <option value="#" disabled="" selected="">-- Select Gender --</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                        <small class="error"></small>
                                    </div>
                                </div>

                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="lastname">PHONE
                                            <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="phone" class="form-control" required>
                                        <small class="error"></small>
                                    </div>
                                </div>


                                 <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="email">EMAIL
                                             <span class="text-danger">*</span>
                                        </label><input type="text" name="email" class="form-control" required>
                                        <small class="eerror"></small>
                                    </div>
                                </div>                             
                                    <div class="col-md-4">
                                        <div class="form-group ">
                                        <label for="clinic">CAMPUS <span class="text-danger">*</span></label>
                                        <select name="clinic" id="clinic" class="form-control custom-select">
                                            <option selected="" disabled="">-- Select Campus --</option>

                                            <?php foreach ($all_clinics as $key => $clinic) { ?>

                                            <option value="<?php echo $clinic->id ?>">
                                                <?php echo $clinic->clinic_name;  ?>
                                            </option>

                                            <?php } ?>
                                        </select>
                                    </div>
                                    </div>

                                 <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="clinic">COURSES <span class="text-danger">*</span></label>
                                        <select name="course" id="course" class="form-control custom-select">
                                            <option selected="" disabled="">-- Select Course --</option>

                                            <?php foreach ($getCourses as $key => $c) { ?>

                                            <option value="<?php echo $c->id ?>">
                                                <?php echo $c->course_name;  ?>
                                            </option>

                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                        <div class="form-group ">
                                        <label for="student_number">REGISTRATION No.<span
                                                class="text-danger">*</span>
                                        </label>

                                        <input type="text" name="student_number" class="form-control" required
                                            placeholder="Prepend a '0' or '00' or '000' on RegNo">
                                        <small class="nerror"></small>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="transcript_serial">TRANSCRIPT SERIAL No.
                                        <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="transcript_serial" class="form-control" required>
                                        <small class="error"></small>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="transcript_serial">FROM YEAR
                                        <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="from_year" class="form-control" required>
                                        <small class="error"></small>
                                    </div>

                                </div>

                                <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="transcript_serial">UPTO YEAR
                                        <span class="text-danger">*</span>
                                        </label>
                                        <input type="text" name="upto_year" class="form-control" required>
                                        <small class="error"></small>
                                    </div>

                                </div>




                                 <div class="col-md-4">
                                  <div class="form-group">
                                    <label for="knowabout">ABOUT WORK <span class="text-danger">*</span>
                                    </label>
                                    <input type="text"  name="knowabout"  class="form-control">
                                </div>
                            </div>

                           
                            

                              
                               <div class="col-md-4">
                                    <div class="form-group ">
                                        <label for="password">PASSWORD<span class="text-danger">*</span>
                                        </label>
                                        <input type="password" name="password" class="form-control" required>
                                        <small class="perror"></small>
                                    </div>
                                </div>


                                <div class="col-md-4">
                                     <div class="form-group ">
                                        <label for="password"> COMFIRM PASSWORD<span class="text-danger">*</span>
                                        </label>
                                        <input type="password" name="cpassword" class="form-control" required>
                                        <small class="cperror"></small>
                                    </div>
                                </div>


                                <div class="col-md-12">
                                    <div class="form-group">
                                          <button type="submit" name="login" class=" btn btn-danger btn-lg btn-block" >SUBMIT REGISTRATION</button>
                                    </div>
                                </div>

                            </div>


                              
                    
                         </form>

                </div>
            </div>
          



           
        </div>
    </div>


<!-- end content -->


<script type="text/javascript">


     jQuery(document).ready(function($) {


        $("form#registerA").submit(function(e) {

            e.preventDefault();
            var formData = new FormData(this);
            var action = $('form#registerA').attr('action');

            $.ajax({
                url: action,
                type: 'POST',
                data: formData,
                success: function(res) {

                    $html =
                        `<div class="alert alert-success"><strong>${res.msg}</strong></div>`;

                    $('.fb').html($html);

                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);

                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                },
                cache: false,
                contentType: false,
                processData: false
            });


        });




    });

jQuery(document).ready(function($) {
function get_age()
{
var dateString = $('input[name="dob"]').val();
var today = new Date();
var birthDate = new Date(dateString);
var age = today.getFullYear() - birthDate.getFullYear();
var m = today.getMonth() - birthDate.getMonth();
if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
age--;
}
//console.log( age );

if(age >= 1 && age <= 17){
$('#ag').val("1-17");
}

if(age >= 18 && age <= 35){
$('#ag').val("18-35");
}

if(age >= 36 && age <= 50){
$('#ag').val("36-50");
}

if(age > 50)
{
$('#ag').val("above 50");
}


}

});
</script>









