<div id="page-wrapper">


<div class="container-fluid">

<style>
.hidden-xs-down {
color: #000 !important;
font-weight: bold;
}



.p-20 {
padding: 10px;
}
</style>

<!-- Page Heading -->
<div class="row">

<div class="col-lg-12">

<h4 class="page-header bet">
Hey <?php echo !isset($user_info) ? header('URL'): $user_info['names'];
?>, Welcome to <?php echo $pageTitle; ?>
</h4>



</div>

</div>
<div class="panel ">
<!-- Nav tabs -->
<ul class="nav  nav-justified nav-tabs bg-primary" role="tablist">
<li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab"><span
class="hidden-sm-up"></span> <span class="hidden-xs-down">FORUM TOPICS</span></a> </li>
<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#ee" role="tab"><span
class="hidden-sm-up"></span> <span class="hidden-xs-down">FORUM CATEGORIES</span></a>
</li>
<li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#lr" role="tab"><span
class="hidden-sm-up"></span> <span class="hidden-xs-down">FORUM DISCUSSION</span></a>
</li>

</ul>
<!-- Tab panes -->
<div class="tab-content tabcontent-border">
<div class="tab-pane active" id="home" role="tabpanel">
<div class="p-20">

<div class="text-center">
<button data-toggle="modal" data-target="#createTopic" class="btn btn-primary"> Create
Topic</button>
</div>

<p id="feedback"></p>
<div class="table-responsive ">
<table  class="table table-sm table-striped demo table-bordered">
<thead>
    <th>#</th>
    <th>TOPIC</th>
    <th>CONTENT</th>
    <th>TIME</th>
    <th>Update</th>
    <th>Trash</th>

</thead>
<tbody>
    <?php echo $all_topics; ?>
</tbody>
</table>
</div>
</div>
</div>
<div class="tab-pane  p-20" id="ee" role="tabpanel">
<div class="p-20">

<div class="text-center">
<button data-toggle="modal" data-target="#createCategory" class="btn btn-primary">
Create Category</button>
</div><br>
<div class="fb"></div>
<div class="table-responsive">
<table id="datatable1" class="table table-sm table-striped table-bordered">
<thead>
    <th>#</th>
    <th>Category Title</th>
    <th>Update</th>
    <th>Trash</th>

</thead>
<tbody>
    <?php echo $all_categories; ?>
</tbody>
</table>
</div>
</div>
</div>
<div class="tab-pane p-20" id="lr" role="tabpanel">
<div class="p-20">



<?php 

if($fetchAllCategories):

?>

<div class="table-responsive">

<table class="table table-bordered demo" >

<thead bgcolor="#00528E" >
    <tr>
        <th>
            <h1 class="text-center " style="color: #fff;">
              PROCEEDING  DISCUSSIONS 
            </h1>
        </th>
    </tr>
</thead>


<tbody>



    <?php

foreach ($fetchAllCategories as $key => $category) {  ?>

    <tr>
        <td>


            <div class="panel " style="border:1px solid #f1f1f1;margin-bottom: 20px;">
                <div class="card-header text-center bg-secondary">
                    <h5 class="card-title text-white">
                        CATEGORY:
                        <?php echo strtoupper($category->category_title);?>
                    </h5>
                </div>

                <div class="panel-body ">

                    <div class="table-responsive">

                        <table class=" table demo table-hover table-sm table-striped">

                            <thead bgcolor="lightgreen" id="disc">
                                <tr>
                                    <th>#</th>
                                    <th>Topic Title</th>
                                    <th>Category</th>
                                    <th>Action</th>
                                </tr>
                            </thead>

                            <tbody>

                                <?php 

$all_data = $fetchTopic-> fetchTopic($category->cat_id);
if($all_data):

$count = 0;

foreach ($all_data as $key => $data) { $count++;  ?>
                                <tr>
                                    <td><?php echo $count; ?></td>
                                    <td><?php echo $data->post_title; ?></td>

                                    <td>

                                        <?php echo strtoupper($category->category_title);?>

                                    </td>

                                    <td><a class="btn  btn-dark btn-sm"
                                            href="<?php echo URL ?>forum/topic/<?php echo base64_encode( $data->post_id); ?>">Join
                                            Discussion
                                            <i class="fa fa-arrow-right"></i> </a>
                                    </td>
                                </tr>

                                <?php }
else: ?>

                                <tr>
                                    <td colspan="4">

                                        <h6 class="text-center text-danger">
                                            NO TOPICS UNDER THIS CATEGORY
                                        </h6>

                                    </td>
                                </tr>


                                <?php endif; ?>

                            </tbody>
                        </table>
                    </div>




                </div>
            </div>

        </td>
    </tr>

    <?php }

endif; ?>


</tbody>
</table>
</div>
















</div>
</div>

</div>
</div>
</div>
</div>
