<div id="page-wrapper">


<div class="container-fluid">

<style>

.hidden-xs-down {
color: #000 !important;
font-weight: bold;
}

.well{
    padding: 10px !important;
}



.p-20 {
padding: 10px;
}

textarea {

    border: 2px solid lightblue !important;
}

small{
    padding-bottom: 16px !important;
    font-size: 10px !important;
    font-weight: bold;
    display: block;
}

.comment{
    margin-bottom: 10px;border:1px solid #f2f1f1;background: #f1f1f1; 
}

</style>

<!-- Page Heading -->
<div class="row">

<div class="col-lg-12">

<h4 class="page-header bet">
Hey <?php echo !isset($user_info) ? header('URL'): $user_info['names'];
?>, Welcome to <?php echo $pageTitle; ?>
</h4>



</div>

</div>
<div class="panel ">

   
            <?php if($topic): ?>
            <div class="panel">

                <div class="panel-heading text-center bg-primary">

                    <h3>FEEDBACK <a href="<?php echo URL ?>forum/" class="pull-right badge badge-danger">Back</a>
                    </h3>

                </div>
                <div class="panel-body ">



                    <div class=" bg-danger">
                        <?php if($topic): ?>
                        TOPIC : <?php echo $topic->post_title; ?> | Created On:
                        <?php echo $forumModel->dateF($topic->datetime); ?> | Created By:
                        <?php echo $forumModel->created_by($topic->user_id); ?> | Category:
                        <?php echo $forumModel->categoryName($topic->cat_id); ?>
                        <?php endif; ?>


                    </div>




                    <hr>

                    <form class="" id="comment">

                        <div class="well panel-default">
                            <div class=" row">
                                <div class="col-sm-9">

                                    <div class="form-group">

                                        <textarea type="text" class="form-control"
                                            placeholder="What is your Perspective About it" name="comment" required="" 
                                            ></textarea>
                                    </div>

                                    <input type="hidden" name="post_id" value="<?php echo $topic->post_id ?>">
                                    <input type="hidden" name="user_id" value="<?php echo $topic->user_id ?>">

                                </div>
                                <div class="col-sm-3">

                                    <div class="form-group">
                                        <button type="submit" id="save"
                                            class="btn btn-success float-right btn-block btn-lg">COMMENT
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </form>
                    <hr>
                


                 <div class="panel">
                    <div class="panel-heading bg-primary text-center">
                        COMMENTS
                    </div>

                    <div class="panel-body ">


                        <?php if($comments):

                                    foreach ($comments as $key => $comment) {
                                      
                                 ?>

                        <div class="comment  " >
                            <div style="padding: 10px;">
                                 <?php echo $comment->comment; ?> <br>
                                    <small  class="in text-primary pull-right"> 
                                 Comment by: <?php echo $forumModel->created_by($comment->user_id); ?> | Commented On:   <?php echo $forumModel->dateF($comment->datetime); ?> 
                             </small><br>
                               
                                   
                                

                                
                            </div>
                        </div>


                        <?php

                                 }

                                endif; ?>

                    </div>
                </div>



        <?php else: ?>

        <div class="card">


            <div class="card-header text-center">

                <h3>DISCUSSIONS</h3>

            </div>

            <div class="card-body">

                <div class="alert alert-info">
                    THANKS FOR VISITING BUT THERE ARE NO CONVERSATIONS YET :):):)
                </div>



            </div>




        </div>



        <?php endif; ?>
    </div>
</div>
</div>

<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
<!-- ============================================================== -->