<div class="wrapper">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Customised IMS Generated Reports</h4>
                </div>
            </div>
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- page-title-box -->
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20 card-red">
                        <div class="card-body"><?php #var_dump($user_info); ?>
                            <?php #echo $segment; ?>
                            <!-- <div class="col-xl-6">
                                <div class="card m-b-20">
                                    <div class="card-body">
                                        <h4 class="mt-0 header-title">Pie Chart</h4>
                                        <ul class="list-inline widget-chart m-t-20 m-b-15 text-center">
                                            <li class="list-inline-item">
                                                <h5 class="mb-0">2536</h5>
                                                <p class="text-muted">Activated</p>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5 class="mb-0">69421</h5>
                                                <p class="text-muted">Pending</p>
                                            </li>
                                            <li class="list-inline-item">
                                                <h5 class="mb-0">89854</h5>
                                                <p class="text-muted">Deactivated</p>
                                            </li>
                                        </ul>
                                        <canvas id="pie" height="260"></canvas>
                                    </div>
                                </div>
                            </div> -->
                            
                            <table class="table  table-borderless mb-0">
                                <thead>
                                    <tr>
                                        <th class="text-danger"><i class="fas fa-chart-bar" style="font-size:1em;color:green;"></i> REPORT NAME</th>
                                        <th class="text-danger"><i class="fas fa-warehouse" style="font-size:1em;color:green;"></i> CRITERIA / CLINIC NAME</th>
                                        <th class="text-danger"><i class="fas fa-balance-scale" style="font-size:1em;color:green;"></i> Target Query</th>
                                        <th class="text-danger"><i class="far fa-calendar-alt" style="font-size:1em;color:green;"></i> START DATE</th>
                                        <th class="text-danger"><i class="far fa-calendar-alt" style="font-size:1em;color:green;"></i> END DATE</th>
                                        
                                        <th class="text-danger" class="text-center"><i class="fas fa-cogs" style="font-size:1em;color:green;"></i> ACTIONS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if(@$user_info['role'] != '4'){ ?>
                                    <form id="regreport" action="<?php echo URL; ?>reports/reg-reports" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Client Registration</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td><td></td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="regreport" action="<?php echo URL; ?>reports/reg-reports" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Case Registration</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="crimminal">Crimminal</option>
                                                    <option value="civil">Civil</option>
                                                    <option value="land">Land</option>
                                                    <option value="family">Family</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="regreport" action="<?php echo URL; ?>reports/reg-reports" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Cases Completed In Court</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="crimminal">Crimminal</option>
                                                    <option value="civil">Civil</option>
                                                    <option value="land">Land</option>
                                                    <option value="family">Family</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="regreport" action="<?php echo URL; ?>reports/reg-reports" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Cases Completed ADR</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="crimminal">Crimminal</option>
                                                    <option value="civil">Civil</option>
                                                    <option value="land">Land</option>
                                                    <option value="family">Family</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="regreport" action="<?php echo URL; ?>reports/reg-reports" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Cases Pending In Court</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="crimminal">Crimminal</option>
                                                    <option value="civil">Civil</option>
                                                    <option value="land">Land</option>
                                                    <option value="family">Family</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="regreport" action="<?php echo URL; ?>reports/reg-reports" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Cases Pending ADR</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="crimminal">Crimminal</option>
                                                    <option value="civil">Civil</option>
                                                    <option value="land">Land</option>
                                                    <option value="family">Family</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="regreport" action="<?php echo URL; ?>reports/reg-reports" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Cases Withdrawn</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="crimminal">Crimminal</option>
                                                    <option value="civil">Civil</option>
                                                    <option value="land">Land</option>
                                                    <option value="family">Family</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="regreport" action="<?php echo URL; ?>reports/reg-reports" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Cases Closed</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="crimminal">Crimminal</option>
                                                    <option value="civil">Civil</option>
                                                    <option value="land">Land</option>
                                                    <option value="family">Family</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="regreport" action="<?php echo URL; ?>reports/reg-reports" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Basic Legal Advice Given</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td><td></td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            <td>
                                        </tr>
                                    </form>
                                <?php } ?>

                                <?php if(@$user_info['role'] == '4'){ ?>
                                    <form id="request_report" action="<?php echo URL; ?>reports/probono-requests" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Pro-Bono Requests</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td><td></td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <!-- <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button> -->
                                                
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="request_report" action="<?php echo URL; ?>reports/ppending-followup" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Pro-Bono Cases Pending Follow Up</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="crimminal">Crimminal</option>
                                                    <option value="civil">Civil</option>
                                                    <option value="land">Land</option>
                                                    <option value="family">Family</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <!-- <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button> -->
                                                
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="request_report" action="<?php echo URL; ?>reports/ppending-followup" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Pro-Bono Cases Closed</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="crimminal">Crimminal</option>
                                                    <option value="civil">Civil</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <!-- <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button> -->
                                                
                                            <td>
                                        </tr>
                                    </form>
                                    <form id="request_report" action="<?php echo URL; ?>reports/duty-counsel" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Duty Counsel</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="crimminal">Crimminal</option>
                                                    <option value="civil">Civil</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <!-- <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button> -->
                                                
                                            <td>
                                        </tr>
                                    </form>
                                    
                                <?php } ?>
                                    <form id="request_report" action="<?php echo URL; ?>reports/duty-counsel" method="POST" target="_blank">
                                        <tr>
                                            <th class="text-info">Sensitization</th>
                                            <td>
                                                <select name="clinic" class="form-control select2">
                                                    <option value="ALL">ALL</option>
                                                    <?php echo $accounts_model->getAllClinics(); ?>
                                                </select>
                                            </td>
                                            <td>
                                                <select name="case_type" class="form-control select3">
                                                    <option value="ALL">ALL</option>
                                                    <option value="school">Schools</option>
                                                    <option value="prisons">Prisons</option>
                                                    <option value="community">Community</option>
                                                </select>
                                            </td>
                                            <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                            
                                            <td>
                                                <button type="submit" name="summary"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="excel" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                                <button type="submit" name="pdf" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                                <!-- <button type="submit" name="chart" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button> -->
                                                
                                            <td>
                                        </tr>
                                    </form>
                                    <!-- <tr>
                                        <th class="text-info">LAP Cases</th>
                                        <td>
                                            <select name="clinics" class="form-control select2">
                                                <option value="ALL">ALL</option>
                                                <?php echo $accounts_model->getAllClinics(); ?>
                                            </select>
                                        </td><td></td>
                                        <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                        <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                       
                                        <td>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            
                                        <td>
                                    </tr>
                                    <tr>
                                        <th class="text-info">LAP Cases By Classification</th>
                                        <td>
                                            <select name="clinics" class="form-control select2">
                                                <option value="ALL">ALL</option>
                                                <?php echo $accounts_model->getAllClinics(); ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="clinics" class="form-control select2">
                                                <option value="ALL">ALL</option>
                                                <option value="Walk-in" >Walk-in to office</option>
                                                <option  value="Call-in" >Call-in</option>
                                            </select>
                                        </td>
                                        <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                        <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                        <td>
                                            <button type="button"  class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                            
                                        <td>
                                    </tr>
                                    <tr>
                                        <th class="text-info">LAP Cases By Sex</th>
                                        <td>
                                            <select name="clinics" class="form-control select2">
                                                <option value="ALL">ALL</option>
                                                <?php echo $accounts_model->getAllClinics(); ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="clinics" class="form-control select2">
                                                <option value="ALL">ALL</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </select>
                                        </td>
                                        <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                        <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                        <td>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                        <td>
                                    </tr>
                                    <tr>
                                        <th class="text-info">LAP Cases By Nature Of Case</th>
                                        <td>
                                            <select name="clinics" class="form-control select2">
                                                <option value="ALL">ALL</option>
                                                <?php echo $accounts_model->getAllClinics(); ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="clinics" class="form-control select2">
                                                <option value="ALL">ALL</option>
                                                <option value="Crimminal">Crimminal</option>
                                                <option value="Civil">Civil</option>
                                            </select>
                                        </td>
                                        <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                        <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                        <td>
                                        <button type="button" class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                        <td>
                                    </tr>
                                    <tr>
                                        <th class="text-info">LAP Cases By Nature Of Claim</th>
                                        <td>
                                            <select name="clinics" class="form-control select2">
                                                <option value="ALL">ALL</option>
                                                <?php echo $accounts_model->getAllClinics(); ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="clinics" class="form-control select2">
                                                <option value="ALL">ALL</option>
                                                <option value="Rape">Rape</option>
                                                <option value="Murder">Murder</option>
                                                <option value="Land/Property claim">Land/Property claim</option>
                                                <option value="Family/Divorce">Family/Divorce &amp; Separation</option>
                                                <option value="Custody Maintenance">Custody Maintenance</option>
                                                <option value="Accident Claim">Accident Claims</option>
                                                <option value="Administration of Estates">Administration of Estates</option>
                                                <option value="Debt claims">Debt claims</option>
                                                <option value="Employment Claims/Labour">Employment Claims/Labour</option>
                                                <option value="Civil General">Civil General</option>
                                                <option value="Treason">Treason</option>
                                                <option value="Burglary">Burglary</option>
                                                <option value="Arson">Arson</option>
                                                <option value="Criminal General">Criminal General</option>
                                            </select>
                                        </td>
                                        <td><input type="text" name="sd" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                        <td><input type="text" name="ed" value="" class="form-control floating-label" placeholder="" id="date" required></td>
                                        <td>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="fas fa-clipboard-list" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-excel" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="far fa-file-pdf" style="font-size:1.5em;"></i></button>
                                            <button type="button" class="btn btn-outline-secondary waves-effect"><i class="fas fa-chart-pie" style="font-size:1.5em;"></i></button>
                                        <td>
                                    </tr> -->

                                </tbody>
                            </table>
                        </div>
                        
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end page content-->
</div>

