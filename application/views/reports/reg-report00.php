<?php 
$sd     = $_POST['sd'];
$ed     = $_POST['ed'];
$clinic = $_POST['clinic'];
if($clinic != 'ALL')
{
    $clinic = $accounts_model->get_clinic($clinic)[0]->clinic_name;
}else{
    $clinic = "ALL";
}
 ?>
<div class="wrapper">
    <div class="page-title-box">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">Client Registration Summary Report</h4>
                </div>
            </div>
        </div>
        <!-- end container-fluid -->
    </div> 
    <!-- page-title-box -->
    <div class="page-content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20 card-red">
                        <div class="card-body"><?php #var_dump($user_info); ?>
                        <h2 class="mt-0 header-title" align="center"><?php echo $clinic; ?>  Clinic(s) Registration Summary Report | From (<?php echo date_format(date_create($_POST['sd']), ' l jS F Y'); ?>) to (<?php echo date_format(date_create($_POST['ed']), ' l jS F Y'); ?>)</h2>
                             <?php if(isset($_POST['summary'])){?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">Summary By Sex And Age Group</h4>
                                                <table class="table table-bordered mb-0">
                                                        <?php $sumary_s_a = $reports_model->summary_by_sex_age($clinic, $sd, $ed); ?>
                                                        <tr>
                                                            <td><b>Age Group</b></td>
                                                            <td><b>1-17</b></td>
                                                            <td><b>18-35</b></td>
                                                            <td><b>36-50</b></td>
                                                            <td><b>Above 50</b></td>
                                                            <td><b>Total</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Male</b></td>
                                                            <td><?php echo $sumary_s_a[0]; ?></td>
                                                            <td><?php echo $sumary_s_a[1]; ?></td>
                                                            <td><?php echo $sumary_s_a[2]; ?></td>
                                                            <td><?php echo $sumary_s_a[3]; ?></td>
                                                            <td><?php echo $sumary_s_a[0] + $sumary_s_a[1] + $sumary_s_a[2] + $sumary_s_a[3]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Female</b></td>
                                                            <td><?php echo $sumary_s_a[4]; ?></td>
                                                            <td><?php echo $sumary_s_a[5]; ?></td>
                                                            <td><?php echo $sumary_s_a[6]; ?></td>
                                                            <td><?php echo $sumary_s_a[7]; ?></td>
                                                            <td><?php echo $sumary_s_a[4] + $sumary_s_a[5] + $sumary_s_a[6] + $sumary_s_a[7]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Total</b></td>
                                                            <td><?php echo $sumary_s_a[0] + $sumary_s_a[4]; ?></td>
                                                            <td><?php echo $sumary_s_a[1] + $sumary_s_a[5]; ?></td>
                                                            <td><?php echo $sumary_s_a[2] + $sumary_s_a[6]; ?></td>
                                                            <td><?php echo $sumary_s_a[3] + $sumary_s_a[7]; ?></td>
                                                            <td><?php echo $sumary_s_a[0] + $sumary_s_a[1] + $sumary_s_a[2] + $sumary_s_a[3] + $sumary_s_a[4] + $sumary_s_a[5] + $sumary_s_a[6] + $sumary_s_a[7]; ?></td>
                                                        </tr>
                                                        
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">MMT Pending by Sex And Age Group</h4>
                                                <table class="table table-bordered mb-0">
                                                <?php $mmt_s_a = $reports_model->mmt_by_sex_age($clinic, $sd, $ed); ?>
                                                        <tr>
                                                            <td><b>Age Group</b></td>
                                                            <td><b>1-17</b></td>
                                                            <td><b>18-35</b></td>
                                                            <td><b>36-50</b></td>
                                                            <td><b>Above 50</b></td>
                                                            <td><b>Total</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Male</b></td>
                                                            <td><?php echo $mmt_s_a[0]; ?></td>
                                                            <td><?php echo $mmt_s_a[1]; ?></td>
                                                            <td><?php echo $mmt_s_a[2]; ?></td>
                                                            <td><?php echo $mmt_s_a[3]; ?></td>
                                                            <td><?php echo $mmt_s_a[0] + $mmt_s_a[1] + $mmt_s_a[2] + $mmt_s_a[3]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Female</b></td>
                                                            <td><?php echo $mmt_s_a[4]; ?></td>
                                                            <td><?php echo $mmt_s_a[5]; ?></td>
                                                            <td><?php echo $mmt_s_a[6]; ?></td>
                                                            <td><?php echo $mmt_s_a[7]; ?></td>
                                                            <td><?php echo $mmt_s_a[4] + $mmt_s_a[5] + $mmt_s_a[6] + $mmt_s_a[7]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Total</b></td>
                                                            <td><?php echo $mmt_s_a[0] + $mmt_s_a[4]; ?></td>
                                                            <td><?php echo $mmt_s_a[1] + $mmt_s_a[5]; ?></td>
                                                            <td><?php echo $mmt_s_a[2] + $mmt_s_a[6]; ?></td>
                                                            <td><?php echo $mmt_s_a[3] + $mmt_s_a[7]; ?></td>
                                                            <td><?php echo $mmt_s_a[0] + $mmt_s_a[1] + $mmt_s_a[2] + $mmt_s_a[3] + $mmt_s_a[4] + $mmt_s_a[5] + $mmt_s_a[6] + $mmt_s_a[7]; ?></td>
                                                        </tr>
                                                        
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">Call-in | Walk-in Classification By Sex</h4>
                                                <table class="table table-bordered mb-0">
                                                    <?php $classification =  $reports_model->classifications($clinic, $sd, $ed); ?>
                                                        <tr>
                                                            <td><b>Classification</b></td>
                                                            <td><b>Call-in</b></td>
                                                            <td><b>	Walk-in</b></td>
                                                            
                                                            <td><b>Total</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Male</b></td>
                                                            <td><?php echo $classification[0]; ?></td>
                                                            <td><?php echo $classification[1]; ?></td>
                                                            <td><?php echo $classification[0] + $classification[1]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Female</b></td>
                                                            <td><?php echo $classification[2]; ?></td>
                                                            <td><?php echo $classification[3]; ?></td>
                                                            <td><?php echo $classification[2] + $classification[3]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Total</b></td>
                                                            <td><?php echo $classification[0] + $classification[2]; ?></td>
                                                            <td><?php echo $classification[1] + $classification[3]; ?></td>
                                                            <td><?php echo $classification[0] + $classification[1] + $classification[2] + $classification[3]; ?></td>
                                                        </tr>
                                                        
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">Nature Of Claim By Sex</h4>
                                                <table class="table table-bordered mb-0">
                                                <?php $nature =  $reports_model->natureofclaim($clinic, $sd, $ed); ?>
                                                        <tr>
                                                            <td><b>Nature OF Claim</b></td>
                                                            <td><b>Land</b></td>
                                                            <td><b>Family</b></td>
                                                            <td><b>Civil</b></td>
                                                            <td><b>Criminal</b></td>
                                                            <td><b>Total</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Male</b></td>
                                                            <td><?php echo $nature[0]; ?></td>
                                                            <td><?php echo $nature[1]; ?></td>
                                                            <td><?php echo $nature[2]; ?></td>
                                                            <td><?php echo $nature[3]; ?></td>
                                                            <td><?php echo $nature[0] + $nature[1] + $nature[2] + $nature[3]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Female</b></td>
                                                            <td><?php echo $nature[4]; ?></td>
                                                            <td><?php echo $nature[5]; ?></td>
                                                            <td><?php echo $nature[6]; ?></td>
                                                            <td><?php echo $nature[7]; ?></td>
                                                            <td><?php echo $nature[4] + $nature[5] + $nature[6] + $nature[7]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Total</b></td>
                                                            <td><?php echo $nature[0] + $nature[4]; ?></td>
                                                            <td><?php echo $nature[1] + $nature[5]; ?></td>
                                                            <td><?php echo $nature[2] + $nature[6]; ?></td>
                                                            <td><?php echo $nature[3] + $nature[7]; ?></td>
                                                            <td><?php echo $nature[0] + $nature[1] + $nature[2] + $nature[3] + $nature[4] + $nature[5] + $nature[6] + $nature[7]; ?></td>
                                                        </tr>
                                                        
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php if($clinic == 'ALL'){
                                    $cs = $accounts_model->getAllClinicsData(); 
                                    for ($i=0; $i < sizeof($cs) ; $i++) 
                                    { 
                                ?>
                                <h2 class="mt-0 header-title" align="center"><?php echo $accounts_model->get_clinic($cs[$i]->id)[0]->clinic_name; ?>  Clinic(s) Registration Summary Report | From (<?php echo date_format(date_create($_POST['sd']), ' l jS F Y'); ?>) to (<?php echo date_format(date_create($_POST['ed']), ' l jS F Y'); ?>)</h2>
                                    <div class="row">
                                    <div class="col-md-6">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">Summary By Sex And Age Group</h4>
                                                <table class="table table-bordered mb-0">
                                                        <?php $sumary_s_a = $reports_model->summary_by_sex_age($cs[$i]->id, $sd, $ed); ?>
                                                        <tr>
                                                            <td><b>Age Group</b></td>
                                                            <td><b>1-17</b></td>
                                                            <td><b>18-35</b></td>
                                                            <td><b>36-50</b></td>
                                                            <td><b>Above 50</b></td>
                                                            <td><b>Total</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Male</b></td>
                                                            <td><?php echo $sumary_s_a[0]; ?></td>
                                                            <td><?php echo $sumary_s_a[1]; ?></td>
                                                            <td><?php echo $sumary_s_a[2]; ?></td>
                                                            <td><?php echo $sumary_s_a[3]; ?></td>
                                                            <td><?php echo $sumary_s_a[0] + $sumary_s_a[1] + $sumary_s_a[2] + $sumary_s_a[3]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Female</b></td>
                                                            <td><?php echo $sumary_s_a[4]; ?></td>
                                                            <td><?php echo $sumary_s_a[5]; ?></td>
                                                            <td><?php echo $sumary_s_a[6]; ?></td>
                                                            <td><?php echo $sumary_s_a[7]; ?></td>
                                                            <td><?php echo $sumary_s_a[4] + $sumary_s_a[5] + $sumary_s_a[6] + $sumary_s_a[7]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Total</b></td>
                                                            <td><?php echo $sumary_s_a[0] + $sumary_s_a[4]; ?></td>
                                                            <td><?php echo $sumary_s_a[1] + $sumary_s_a[5]; ?></td>
                                                            <td><?php echo $sumary_s_a[2] + $sumary_s_a[6]; ?></td>
                                                            <td><?php echo $sumary_s_a[3] + $sumary_s_a[7]; ?></td>
                                                            <td><?php echo $sumary_s_a[0] + $sumary_s_a[1] + $sumary_s_a[2] + $sumary_s_a[3] + $sumary_s_a[4] + $sumary_s_a[5] + $sumary_s_a[6] + $sumary_s_a[7]; ?></td>
                                                        </tr>
                                                        
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">MMT Pending by Sex And Age Group</h4>
                                                <table class="table table-bordered mb-0">
                                                <?php $mmt_s_a = $reports_model->mmt_by_sex_age($cs[$i]->id, $sd, $ed); ?>
                                                        <tr>
                                                            <td><b>Age Group</b></td>
                                                            <td><b>1-17</b></td>
                                                            <td><b>18-35</b></td>
                                                            <td><b>36-50</b></td>
                                                            <td><b>Above 50</b></td>
                                                            <td><b>Total</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Male</b></td>
                                                            <td><?php echo $mmt_s_a[0]; ?></td>
                                                            <td><?php echo $mmt_s_a[1]; ?></td>
                                                            <td><?php echo $mmt_s_a[2]; ?></td>
                                                            <td><?php echo $mmt_s_a[3]; ?></td>
                                                            <td><?php echo $mmt_s_a[0] + $mmt_s_a[1] + $mmt_s_a[2] + $mmt_s_a[3]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Female</b></td>
                                                            <td><?php echo $mmt_s_a[4]; ?></td>
                                                            <td><?php echo $mmt_s_a[5]; ?></td>
                                                            <td><?php echo $mmt_s_a[6]; ?></td>
                                                            <td><?php echo $mmt_s_a[7]; ?></td>
                                                            <td><?php echo $mmt_s_a[4] + $mmt_s_a[5] + $mmt_s_a[6] + $mmt_s_a[7]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Total</b></td>
                                                            <td><?php echo $mmt_s_a[0] + $mmt_s_a[4]; ?></td>
                                                            <td><?php echo $mmt_s_a[1] + $mmt_s_a[5]; ?></td>
                                                            <td><?php echo $mmt_s_a[2] + $mmt_s_a[6]; ?></td>
                                                            <td><?php echo $mmt_s_a[3] + $mmt_s_a[7]; ?></td>
                                                            <td><?php echo $mmt_s_a[0] + $mmt_s_a[1] + $mmt_s_a[2] + $mmt_s_a[3] + $mmt_s_a[4] + $mmt_s_a[5] + $mmt_s_a[6] + $mmt_s_a[7]; ?></td>
                                                        </tr>
                                                        
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">Call-in | Walk-in Classification By Sex</h4>
                                                <table class="table table-bordered mb-0">
                                                    <?php $classification =  $reports_model->classifications($cs[$i]->id, $sd, $ed); ?>
                                                        <tr>
                                                            <td><b>Classification</b></td>
                                                            <td><b>Call-in</b></td>
                                                            <td><b>	Walk-in</b></td>
                                                            
                                                            <td><b>Total</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Male</b></td>
                                                            <td><?php echo $classification[0]; ?></td>
                                                            <td><?php echo $classification[1]; ?></td>
                                                            <td><?php echo $classification[0] + $classification[1]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Female</b></td>
                                                            <td><?php echo $classification[2]; ?></td>
                                                            <td><?php echo $classification[3]; ?></td>
                                                            <td><?php echo $classification[2] + $classification[3]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Total</b></td>
                                                            <td><?php echo $classification[0] + $classification[2]; ?></td>
                                                            <td><?php echo $classification[1] + $classification[3]; ?></td>
                                                            <td><?php echo $classification[0] + $classification[1] + $classification[2] + $classification[3]; ?></td>
                                                        </tr>
                                                        
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">Nature Of Claim By Sex</h4>
                                                <table class="table table-bordered mb-0">
                                                <?php $nature =  $reports_model->natureofclaim($cs[$i]->id, $sd, $ed); ?>
                                                        <tr>
                                                            <td><b>Nature OF Claim</b></td>
                                                            <td><b>Land</b></td>
                                                            <td><b>Family</b></td>
                                                            <td><b>Civil</b></td>
                                                            <td><b>Criminal</b></td>
                                                            <td><b>Total</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Male</b></td>
                                                            <td><?php echo $nature[0]; ?></td>
                                                            <td><?php echo $nature[1]; ?></td>
                                                            <td><?php echo $nature[2]; ?></td>
                                                            <td><?php echo $nature[3]; ?></td>
                                                            <td><?php echo $nature[0] + $nature[1] + $nature[2] + $nature[3]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Female</b></td>
                                                            <td><?php echo $nature[4]; ?></td>
                                                            <td><?php echo $nature[5]; ?></td>
                                                            <td><?php echo $nature[6]; ?></td>
                                                            <td><?php echo $nature[7]; ?></td>
                                                            <td><?php echo $nature[4] + $nature[5] + $nature[6] + $nature[7]; ?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><b>Total</b></td>
                                                            <td><?php echo $nature[0] + $nature[4]; ?></td>
                                                            <td><?php echo $nature[1] + $nature[5]; ?></td>
                                                            <td><?php echo $nature[2] + $nature[6]; ?></td>
                                                            <td><?php echo $nature[3] + $nature[7]; ?></td>
                                                            <td><?php echo $nature[0] + $nature[1] + $nature[2] + $nature[3] + $nature[4] + $nature[5] + $nature[6] + $nature[7]; ?></td>
                                                        </tr>
                                                        
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }} ?>
                                </div>
                            <?php } ?>
                            <?php if(isset($_POST['chart'])){?>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?php $sumary_s_a = $reports_model->summary_by_sex_age($clinic, $sd, $ed); ?>
                                       
                                        <script>
                                            var  sumary_s_a_0 = <?php echo $sumary_s_a[0]; ?>;
                                            var  sumary_s_a_1 = <?php echo $sumary_s_a[1]; ?>;
                                            var  sumary_s_a_2 = <?php echo $sumary_s_a[2]; ?>;
                                            var  sumary_s_a_3 = <?php echo $sumary_s_a[3]; ?>;

                                            var  sumary_s_a_4 = <?php echo $sumary_s_a[4]; ?>;
                                            var  sumary_s_a_5 = <?php echo $sumary_s_a[5]; ?>;
                                            var  sumary_s_a_6 = <?php echo $sumary_s_a[6]; ?>;
                                            var  sumary_s_a_7 = <?php echo $sumary_s_a[7]; ?>;
                                        </script>
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">Summary By Sex And Age Group</h4>
                                                <canvas id="pie_sex_n_age" height="360"></canvas>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <?php $mmt_s_a = $reports_model->mmt_by_sex_age($clinic, $sd, $ed); ?>
                                        
                                        <script>
                                            var  mmt_s_a_0 = <?php echo $mmt_s_a[0]; ?>;
                                            var  mmt_s_a_1 = <?php echo $mmt_s_a[1]; ?>;
                                            var  mmt_s_a_2 = <?php echo $mmt_s_a[2]; ?>;
                                            var  mmt_s_a_3 = <?php echo $mmt_s_a[3]; ?>;

                                            var  mmt_s_a_4 = <?php echo $mmt_s_a[4]; ?>;
                                            var  mmt_s_a_5 = <?php echo $mmt_s_a[5]; ?>;
                                            var  mmt_s_a_6 = <?php echo $mmt_s_a[6]; ?>;
                                            var  mmt_s_a_7 = <?php echo $mmt_s_a[7]; ?>;
                                        </script>
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">MMT Pending by Sex And Age Group</h4>
                                                <canvas id="pie_mmt_pending" height="360"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        
                                        <?php $classification =  $reports_model->classifications($clinic, $sd, $ed); ?>
                                        <script>
                                            var  classification_0 = <?php echo $classification[0]; ?>;
                                            var  classification_1 = <?php echo $classification[1]; ?>;
                                           

                                            var  classification_2 = <?php echo $classification[2]; ?>;
                                            var  classification_3 = <?php echo $classification[3]; ?>;
                                            
                                        </script>
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">Call-in | Walk-in Classification By Sex</h4>
                                                <canvas id="classification" height="360"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <?php $nature =  $reports_model->natureofclaim($clinic, $sd, $ed); ?>
                                        <script>
                                            var  nature_0 = <?php echo $nature[0]; ?>;
                                            var  nature_1 = <?php echo $nature[1]; ?>;
                                            var  nature_2 = <?php echo $nature[2]; ?>;
                                            var  nature_3 = <?php echo $nature[3]; ?>;

                                            var  nature_4 = <?php echo $nature[4]; ?>;
                                            var  nature_5 = <?php echo $nature[5]; ?>;
                                            var  nature_6 = <?php echo $nature[6]; ?>;
                                            var  nature_7 = <?php echo $nature[7]; ?>;
                                        </script>
                                        <div class="card m-b-20">
                                            <div class="card-body">
                                                <h4 class="mt-0 header-title">Nature Of Claim By Sex</h4>
                                                <canvas id="nature" height="360"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                

                            <?php } ?>
                        
                    </div>
                </div>
                <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container-fluid -->
    </div>
    <!-- end page content-->
</div>