<html xmlns="">
	<head>
        <title>ICMS - Uganda Law Society - Introduction Of Clients Report - <?php echo date('Y-m-d H:mm:ss'); ?></title>
</head>
<body>
<?php

//var_dump($_POST);
$intro = $reports_model->introduction_client_report($_POST['clinic_id'],$_POST['sd'],$_POST['ed'] );
$total_cases = $reports_model->civil_case_report($_POST['clinic_id'],$_POST['sd'],$_POST['ed'] );
$break_down_cases = $reports_model->civil_report_breakdown($_POST['clinic_id'],$_POST['sd'],$_POST['ed'] );
//var_dump($total_cases);
if($_POST['reportname'] == 'into-client'){


//print_r($intro);
?>
<table border="3" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
    <body>
        <tr>
            <td style="border:none;">
                <table  border="0" align="center" style="background-color: #2e3192;color:#fff; padding-left:100px;padding-right:100px;">
                    <tbody>
                        <tr>
                            <td><img src="http://uls.datatrack.co.ug/static/images/logo.png" style="padding-right:40px;" alt="" width="100" /></td>
                            <td><b>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LEGAL AID PROJECT</p>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OF THE</p>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; UGANDA LAW SOCIETY <?php echo $_POST['clinci_name']; ?></p></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table align="center" style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;">
                    <tbody>
                        <tr>
                            <td><b>&nbsp;TO:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HEAD LEGAL AID AND PRO BONO SERVICES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;FROM:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SENIOR LEGAL OFFICER- CLINIC REPORT</td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;RE:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MONTHLY REPORT OF <b>(<?php echo $_POST['sd']; ?>) To (<?php echo $_POST['ed']; ?>)</b></td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;DATE OF SUBMISSION:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo date_format(date_create(date('Y-m-d')), ' l jS F Y'); ?></td>
                        </tr>
                    </tbody>
                </table>
                <table border="1" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
                    <tbody>
                        <tr style='background:green;color:#fff;'>
                            <td>&nbsp;<b>No.</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Introduction Of Clients</b> </td>
                            <td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Number</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                        </tr>
                        <tr>
                            <td>&nbsp; 1.</td>
                            <td>&nbsp;Self-Introduction</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Self-introduction']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 2.</td>
                            <td>&nbsp;Old Clients</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Old clients']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 3.</td>
                            <td>&nbsp;Radio</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Radio']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 4.</td>
                            <td>&nbsp;Civil servant i.e poliice, court, prison etc</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Civil-servant']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 5.</td>
                            <td>&nbsp;Local Leaders</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Local-leaders']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 6.</td>
                            <td>&nbsp;Advocates</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Advocates']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 7.</td>
                            <td>&nbsp;Friends &amp; Relatives</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Friends_Relatives']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 8.</td>
                            <td>&nbsp;Uganda Law Society</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Uganda-Law-Society']; ?></td>
                        </tr >
                        <tr>
                            <td>&nbsp; 9</td>
                            <td>NGOs &amp; CBOs</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['NGO_CBOS']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><b>Total</b></td>
                            <td align="center"  style='background:red;'>&nbsp;<b><?php echo $intro['Total']; ?></b></td>
                        </tr>
                    </tbody>
                </table>
                <p>@ <?php echo date('Y'); ?> Uganda Law Society</p>
            </td>
        </tr>
    </body>
</table>


<?php
}
if($_POST['reportname'] == 'nature-civil')  
{?>

<table border="3" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
    <body>
        <tr>
            <td style="border:none;">
                <table  border="0" align="center" style="background-color: #2e3192;color:#fff; padding-left:100px;padding-right:100px;">
                    <tbody>
                        <tr>
                            <td><img src="http://uls.datatrack.co.ug/static/images/logo.png" style="padding-right:40px;" alt="" width="100" /></td>
                            <td><b>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LEGAL AID PROJECT</p>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OF THE</p>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; UGANDA LAW SOCIETY <?php echo $_POST['clinci_name']; ?></p></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table align="center" style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;">
                    <tbody>
                        <tr>
                            <td><b>&nbsp;TO:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HEAD LEGAL AID AND PRO BONO SERVICES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;FROM:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SENIOR LEGAL OFFICER- CLINIC REPORT</td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;RE:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MONTHLY REPORT OF <b>(<?php echo $_POST['sd']; ?>) To (<?php echo $_POST['ed']; ?>)</b></td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;DATE OF SUBMISSION:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo date_format(date_create(date('Y-m-d')), ' l jS F Y'); ?></td>
                        </tr>
                    </tbody>
                </table>
                <table border="1" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
                    <tbody>
                        <tr style="height: 33.5333px;">
                            <td style="width: 159.383px; height: 99.5333px;" rowspan="3" align="center"><strong>&nbsp;Nature Of Cases</strong></td>
                            <td style="width: 69.1167px; height: 99.5333px;" rowspan="3" align="center">&nbsp;<strong>Number of Cases</strong></td>
                            <td style="width: 635.383px; height: 33.5333px;" colspan="6" align="center">
                                <p>&nbsp;<strong>Gender Disaggregation:</strong></p>
                                <p><strong>Juvenile=(12 to &lt;18 yrs), Youths=(18 to <span class="_hwx354t">&le;</span> 35yrs), Adults=(35+ yrs)</strong></p>
                            </td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 211.433px; height: 33px;" colspan="2" align="center"><strong>&nbsp; Juveniles</strong></td>
                            <td style="width: 211.433px; height: 33px;" colspan="2" align="center">&nbsp;<strong>Youths</strong></td>
                            <td style="width: 212.517px; height: 33px;" colspan="2" align="center">&nbsp;<strong> Adults</strong></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 105.717px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 105.717px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 105.733px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 105.7px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 105.733px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 106.783px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Land &amp; Property</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Land & Property'] != NULL){ $lp=$total_cases['Land & Property']; echo '<strong>'.$total_cases['Land & Property'].'</strong>'; }else{ $lp=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Land & Property'] != NULL){ $lp1=$break_down_cases['array_juveniles_male']['Land & Property']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Land & Property'].'</strong>'; }else{ $lp1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Land & Property'] != NULL){ $lp2=$break_down_cases['array_juveniles_female']['Land & Property']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Land & Property'].'</strong>'; }else{ $lp2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Land & Property'] != NULL){ $lp3=$break_down_cases['array_youth_male']['Land & Property']; echo '<strong>'.$break_down_cases['array_youth_male']['Land & Property'].'</strong>'; }else{ $lp3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Land & Property'] != NULL){ $lp4=$break_down_cases['array_youth_female']['Land & Property']; echo '<strong>'.$break_down_cases['array_youth_female']['Land & Property'].'</strong>'; }else{ $lp4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Land & Property'] != NULL){ $lp5=$break_down_cases['array_adult_male']['Land & Property']; echo '<strong>'.$break_down_cases['array_adult_male']['Land & Property'].'</strong>'; }else{ $lp5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Land & Property'] != NULL){ $lp6=$break_down_cases['array_adult_female']['Land & Property']; echo '<strong>'.$break_down_cases['array_adult_female']['Land & Property'].'</strong>'; }else{ $lp6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Divorce &amp; Separation</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Divorce & Separation'] != NULL){$ds = $total_cases['Divorce & Separation']; echo '<strong>'.$total_cases['Divorce & Separation'].'</strong>'; }else{$ds=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Divorce & Separation'] != NULL){$ds1 = $break_down_cases['array_juveniles_male']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Divorce & Separation'].'</strong>'; }else{$ds1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Divorce & Separation'] != NULL){$ds2 = $break_down_cases['array_juveniles_female']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Divorce & Separation'].'</strong>'; }else{$ds2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Divorce & Separation'] != NULL){$ds3 = $break_down_cases['array_youth_male']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_youth_male']['Divorce & Separation'].'</strong>'; }else{$ds3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Divorce & Separation'] != NULL){$ds4 = $break_down_cases['array_youth_female']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_youth_female']['Divorce & Separation'].'</strong>'; }else{$ds4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Divorce & Separation'] != NULL){$ds5 = $break_down_cases['array_adult_male']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_adult_male']['Divorce & Separation'].'</strong>'; }else{$ds5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Divorce & Separation'] != NULL){$ds6 = $break_down_cases['array_adult_female']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_adult_female']['Divorce & Separation'].'</strong>'; }else{$ds6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Custody &amp; Maintenance</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Custody & Maintenance'] != NULL){$cm = $total_cases['Custody & Maintenance'];  echo '<strong>'.$total_cases['Custody & Maintenance'].'</strong>'; }else{$cm = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Custody & Maintenance'] != NULL){$cm1 = $break_down_cases['array_juveniles_male']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_juveniles_male']['Custody & Maintenance'].'</strong>'; }else{$cm1 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Custody & Maintenance'] != NULL){$cm2 = $break_down_cases['array_juveniles_female']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_juveniles_female']['Custody & Maintenance'].'</strong>'; }else{$cm2 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Custody & Maintenance'] != NULL){$cm3 = $break_down_cases['array_youth_male']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_youth_male']['Custody & Maintenance'].'</strong>'; }else{$cm3 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Custody & Maintenance'] != NULL){$cm4 = $break_down_cases['array_youth_female']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_youth_female']['Custody & Maintenance'].'</strong>'; }else{$cm4 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Custody & Maintenance'] != NULL){$cm5 = $break_down_cases['array_adult_male']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_adult_male']['Custody & Maintenance'].'</strong>'; }else{$cm5 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Custody & Maintenance'] != NULL){$cm6 = $break_down_cases['array_adult_female']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_adult_female']['Custody & Maintenance'].'</strong>'; }else{$cm6 = 0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Accident Claims</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Accident claims'] != NULL){ $ac = $total_cases['Accident claims']; echo '<strong>'.$total_cases['Accident claims'].'</strong>'; }else{ $ac=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Accident claims'] != NULL){ $ac1 = $break_down_cases['array_juveniles_male']['Accident claims']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Accident claims'].'</strong>'; }else{ $ac1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Accident claims'] != NULL){ $ac2 = $break_down_cases['array_juveniles_female']['Accident claims']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Accident claims'].'</strong>'; }else{ $ac2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Accident claims'] != NULL){ $ac3 = $break_down_cases['array_youth_male']['Accident claims']; echo '<strong>'.$break_down_cases['array_youth_male']['Accident claims'].'</strong>'; }else{ $ac3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Accident claims'] != NULL){ $ac4 = $break_down_cases['array_youth_female']['Accident claims']; echo '<strong>'.$break_down_cases['array_youth_female']['Accident claims'].'</strong>'; }else{ $ac4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Accident claims'] != NULL){ $ac5 = $break_down_cases['array_adult_male']['Accident claims']; echo '<strong>'.$break_down_cases['array_adult_male']['Accident claims'].'</strong>'; }else{ $ac5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Accident claims'] != NULL){ $ac6 = $break_down_cases['array_adult_female']['Accident claims']; echo '<strong>'.$break_down_cases['array_adult_female']['Accident claims'].'</strong>'; }else{ $ac6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Debt claims</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Debt claims'] != NULL){ $dc =$total_cases['Debt claims']; echo '<strong>'.$total_cases['Debt claims'].'</strong>'; }else{ $dc =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Debt claims'] != NULL){ $dc1 =$break_down_cases['array_juveniles_male']['Debt claims']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Debt claims'].'</strong>'; }else{ $dc1 =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Debt claims'] != NULL){ $dc2 =$break_down_cases['array_juveniles_female']['Debt claims']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Debt claims'].'</strong>'; }else{ $dc2 =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Debt claims'] != NULL){ $dc3 =$break_down_cases['array_youth_male']['Debt claims']; echo '<strong>'.$break_down_cases['array_youth_male']['Debt claims'].'</strong>'; }else{ $dc3 =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Debt claims'] != NULL){ $dc4 =$break_down_cases['array_youth_female']['Debt claims']; echo '<strong>'.$break_down_cases['array_youth_female']['Debt claims'].'</strong>'; }else{ $dc4 =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Debt claims'] != NULL){ $dc5 =$break_down_cases['array_adult_male']['Debt claims']; echo '<strong>'.$break_down_cases['array_adult_male']['Debt claims'].'</strong>'; }else{ $dc5 =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Debt claims'] != NULL){ $dc6 =$break_down_cases['array_adult_female']['Debt claims']; echo '<strong>'.$break_down_cases['array_adult_female']['Debt claims'].'</strong>'; }else{ $dc6 =0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Employment Claims</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Employment Claims'] != NULL){ $ec = $total_cases['Employment Claims'];  echo '<strong>'.$total_cases['Employment Claims'].'</strong>'; }else{ $ec=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Employment Claims'] != NULL){ $ec1 = $break_down_cases['array_juveniles_male']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_juveniles_male']['Employment Claims'].'</strong>'; }else{ $ec1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Employment Claims'] != NULL){ $ec2 = $break_down_cases['array_juveniles_female']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_juveniles_female']['Employment Claims'].'</strong>'; }else{ $ec2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Employment Claims'] != NULL){ $ec3 = $break_down_cases['array_youth_male']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_youth_male']['Employment Claims'].'</strong>'; }else{ $ec3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Employment Claims'] != NULL){ $ec4 = $break_down_cases['array_youth_female']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_youth_female']['Employment Claims'].'</strong>'; }else{ $ec4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Employment Claims'] != NULL){ $ec5 = $break_down_cases['array_adult_male']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_adult_male']['Employment Claims'].'</strong>'; }else{ $ec5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Employment Claims'] != NULL){ $ec6 = $break_down_cases['array_adult_female']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_adult_female']['Employment Claims'].'</strong>'; }else{ $ec6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">Civil (Gen)</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Civil (Gen)'] != NULL){ $civil = $total_cases['Civil (Gen)'];  echo '<strong>'.$total_cases['Civil (Gen)'].'</strong>'; }else{ $civil = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Civil (Gen)'] != NULL){ $civil1 = $break_down_cases['array_juveniles_male']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_juveniles_male']['Civil (Gen)'].'</strong>'; }else{ $civil1 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Civil (Gen)'] != NULL){ $civil2 = $break_down_cases['array_juveniles_female']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_juveniles_female']['Civil (Gen)'].'</strong>'; }else{ $civil2 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Civil (Gen)'] != NULL){ $civil3 = $break_down_cases['array_youth_male']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_youth_male']['Civil (Gen)'].'</strong>'; }else{ $civil3 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Civil (Gen)'] != NULL){ $civil4 = $break_down_cases['array_youth_female']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_youth_female']['Civil (Gen)'].'</strong>'; }else{ $civil4 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Civil (Gen)'] != NULL){ $civil5 = $break_down_cases['array_adult_male']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_adult_male']['Civil (Gen)'].'</strong>'; }else{ $civil5 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Civil (Gen)'] != NULL){ $civil6 = $break_down_cases['array_adult_female']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_adult_female']['Civil (Gen)'].'</strong>'; }else{ $civil6 = 0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;<strong>Total</strong></td>
                            <td style="width: 69.1167px; height: 33px;"><b>&nbsp;<?php echo $civil + $ec + $dc + $ac + $cm + $ds + $lp; ?></b></td>
                            <td style="width: 105.717px; height: 33px;"><b>&nbsp;<?php echo $civil1 + $ec1 + $dc1 + $ac1 + $cm1 + $ds1 + $lp1; ?></b></td>
                            <td style="width: 105.717px; height: 33px;"><b>&nbsp;<?php echo $civil2 + $ec2 + $dc2 + $ac2 + $cm2 + $ds2 + $lp2; ?></b></td>
                            <td style="width: 105.733px; height: 33px;"><b>&nbsp;<?php echo $civil3 + $ec3 + $dc3 + $ac3 + $cm3 + $ds3 + $lp3; ?></b></td>
                            <td style="width: 105.7px; height: 33px;"><b>&nbsp;<?php echo $civil4 + $ec4 + $dc4 + $ac4 + $cm4 + $ds4 + $lp4; ?></b></td>
                            <td style="width: 105.733px; height: 33px;"><b>&nbsp;<?php echo $civil5 + $ec5 + $dc5 + $ac5 + $cm5 + $ds5 + $lp5; ?></b></td>
                            <td style="width: 106.783px; height: 33px;"><b>&nbsp;<?php echo $civil6 + $ec6 + $dc6 + $ac6 + $cm6 + $ds6 + $lp6; ?></b></td>
                        </tr>
                    </tbody>
                </table>
                <p>@ <?php echo date('Y'); ?> Uganda Law Society</p>
            </td>
        </tr>
    </body>
</table>
<?php 
}
if($_POST['reportname'] == 'nature-criminal')  
{?>
<table border="3" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
    <body>
        <tr>
            <td style="border:none;">
                <table  border="0" align="center" style="background-color: #2e3192;color:#fff; padding-left:100px;padding-right:100px;">
                    <tbody>
                        <tr>
                            <td><img src="http://uls.datatrack.co.ug/static/images/logo.png" style="padding-right:40px;" alt="" width="100" /></td>
                            <td><b>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LEGAL AID PROJECT</p>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OF THE</p>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; UGANDA LAW SOCIETY <?php echo $_POST['clinci_name']; ?></p></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table align="center" style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;">
                    <tbody>
                        <tr>
                            <td><b>&nbsp;TO:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HEAD LEGAL AID AND PRO BONO SERVICES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;FROM:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SENIOR LEGAL OFFICER- CLINIC REPORT</td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;RE:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MONTHLY REPORT OF <b>(<?php echo $_POST['sd']; ?>) To (<?php echo $_POST['ed']; ?>)</b></td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;DATE OF SUBMISSION:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo date_format(date_create(date('Y-m-d')), ' l jS F Y'); ?></td>
                        </tr>
                    </tbody>
                </table>
                <table border="1" style="height: 973px; width: 974px;" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
                    <tbody>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 99px;" rowspan="3" align="center"><strong>&nbsp;Nature Of Cases</strong></td>
                            <td style="width: 77.8333px; height: 99px;" rowspan="3" align="center">&nbsp;<strong>Number of Cases</strong></td>
                            <td style="width: 715.55px; height: 33px;" colspan="6" align="center">
                                <p>&nbsp;<strong>Gender Disaggregation:</strong></p>
                                <p><strong>Juvenile=(12 to &lt;18 yrs), Youths=(18 to <span class="_hwx354t">&le;</span> 35yrs), Adults=(35+ yrs)</strong></p>
                            </td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 238.1px; height: 33px;" colspan="2" align="center"><strong>&nbsp; Juveniles</strong></td>
                            <td style="width: 238.117px; height: 33px;" colspan="2" align="center">&nbsp;<strong>Youths</strong></td>
                            <td style="width: 239.333px; height: 33px;" colspan="2" align="center">&nbsp;<strong> Adults</strong></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 119.05px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 119.05px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 119.083px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 119.033px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 119.083px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 120.25px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Murder</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Murder'] != NULL){ $mu=$total_cases['Murder']; echo '<strong>'.$total_cases['Murder'].'</strong>'; }else{ $mu=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Murder'] != NULL){ $mu1=$break_down_cases['array_juveniles_male']['Murder']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Murder'].'</strong>'; }else{ $mu1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Murder'] != NULL){ $mu2=$break_down_cases['array_juveniles_female']['Murder']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Murder'].'</strong>'; }else{ $mu2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Murder'] != NULL){ $mu3=$break_down_cases['array_youth_male']['Murder']; echo '<strong>'.$break_down_cases['array_youth_male']['Murder'].'</strong>'; }else{ $mu3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Murder'] != NULL){ $mu4=$break_down_cases['array_youth_female']['Murder']; echo '<strong>'.$break_down_cases['array_youth_female']['Murder'].'</strong>'; }else{ $mu4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Murder'] != NULL){ $mu5=$break_down_cases['array_adult_male']['Murder']; echo '<strong>'.$break_down_cases['array_adult_male']['Murder'].'</strong>'; }else{ $mu5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Murder'] != NULL){ $mu6=$break_down_cases['array_adult_female']['Murder']; echo '<strong>'.$break_down_cases['array_adult_female']['Murder'].'</strong>'; }else{ $mu6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Kidnap with intent to obtain a Ransom &amp; Confine</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr=$total_cases['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$total_cases['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr1=$break_down_cases['array_juveniles_male']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr2=$break_down_cases['array_juveniles_female']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr3=$break_down_cases['array_youth_male']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_youth_male']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr4=$break_down_cases['array_youth_female']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_youth_female']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr5=$break_down_cases['array_adult_male']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_adult_male']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr6=$break_down_cases['array_adult_female']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_adult_female']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">&nbsp;Human Trafficking / Kidnap</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Human Trafficking'] != NULL){ $ht=$total_cases['Human Trafficking']; echo '<strong>'.$total_cases['Human Trafficking'].'</strong>'; }else{ $ht=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Human Trafficking'] != NULL){ $ht1=$break_down_cases['array_juveniles_male']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Human Trafficking'].'</strong>'; }else{ $ht1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Human Trafficking'] != NULL){ $ht2=$break_down_cases['array_juveniles_female']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Human Trafficking'].'</strong>'; }else{ $ht2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Human Trafficking'] != NULL){ $ht3=$break_down_cases['array_youth_male']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_youth_male']['Human Trafficking'].'</strong>'; }else{ $ht3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Human Trafficking'] != NULL){ $ht4=$break_down_cases['array_youth_female']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_youth_female']['Human Trafficking'].'</strong>'; }else{ $ht4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Human Trafficking'] != NULL){ $ht5=$break_down_cases['array_adult_male']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_adult_male']['Human Trafficking'].'</strong>'; }else{ $ht5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Human Trafficking'] != NULL){ $ht6=$break_down_cases['array_adult_female']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_adult_female']['Human Trafficking'].'</strong>'; }else{ $ht6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">&nbsp;Defilement</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Defilement'] != NULL){ $def=$total_cases['Defilement']; echo '<strong>'.$total_cases['Defilement'].'</strong>'; }else{ $def=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Defilement'] != NULL){ $def1=$break_down_cases['array_juveniles_male']['Defilement']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Defilement'].'</strong>'; }else{ $def1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Defilement'] != NULL){ $def2=$break_down_cases['array_juveniles_female']['Defilement']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Defilement'].'</strong>'; }else{ $def2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Defilement'] != NULL){ $def3=$break_down_cases['array_youth_male']['Defilement']; echo '<strong>'.$break_down_cases['array_youth_male']['Defilement'].'</strong>'; }else{ $def3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Defilement'] != NULL){ $def4=$break_down_cases['array_youth_female']['Defilement']; echo '<strong>'.$break_down_cases['array_youth_female']['Defilement'].'</strong>'; }else{ $def4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Defilement'] != NULL){ $def5=$break_down_cases['array_adult_male']['Defilement']; echo '<strong>'.$break_down_cases['array_adult_male']['Defilement'].'</strong>'; }else{ $def5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Defilement'] != NULL){ $def6=$break_down_cases['array_adult_female']['Defilement']; echo '<strong>'.$break_down_cases['array_adult_female']['Defilement'].'</strong>'; }else{ $def6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">&nbsp;Rape</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp; <?php if($total_cases['Rape'] != NULL){ $rape=$total_cases['Rape']; echo '<strong>'.$total_cases['Rape'].'</strong>'; }else{ $rape=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Rape'] != NULL){ $rape1=$break_down_cases['array_juveniles_male']['Rape']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Rape'].'</strong>'; }else{ $rape1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Rape'] != NULL){ $rape2=$break_down_cases['array_juveniles_female']['Rape']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Rape'].'</strong>'; }else{ $rape2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Rape'] != NULL){ $rape3=$break_down_cases['array_youth_male']['Rape']; echo '<strong>'.$break_down_cases['array_youth_male']['Rape'].'</strong>'; }else{ $rape3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Rape'] != NULL){ $rape4=$break_down_cases['array_youth_female']['Rape']; echo '<strong>'.$break_down_cases['array_youth_female']['Rape'].'</strong>'; }else{ $rape4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Rape'] != NULL){ $rape5=$break_down_cases['array_adult_male']['Rape']; echo '<strong>'.$break_down_cases['array_adult_male']['Rape'].'</strong>'; }else{ $rape5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Rape'] != NULL){ $rape6=$break_down_cases['array_adult_female']['Rape']; echo '<strong>'.$break_down_cases['array_adult_female']['Rape'].'</strong>'; }else{ $rape6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Negligent Act likely to spread disease</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp; <?php if($total_cases['Negligent Act likely to spread disease'] != NULL){ $na=$total_cases['Negligent Act likely to spread disease']; echo '<strong>'.$total_cases['Negligent Act likely to spread disease'].'</strong>'; }else{ $na=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Negligent Act likely to spread disease'] != NULL){ $na1=$break_down_cases['array_juveniles_male']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Negligent Act likely to spread disease'] != NULL){ $na2=$break_down_cases['array_juveniles_female']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Negligent Act likely to spread disease'] != NULL){ $na3=$break_down_cases['array_youth_male']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_youth_male']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Negligent Act likely to spread disease'] != NULL){ $na4=$break_down_cases['array_youth_female']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_youth_female']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Negligent Act likely to spread disease'] != NULL){ $na5=$break_down_cases['array_adult_male']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_adult_male']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Negligent Act likely to spread disease'] != NULL){ $na6=$break_down_cases['array_adult_female']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_adult_female']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Traffic Offence</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp; <?php if($total_cases['Traffic Offence'] != NULL){ $trafo=$total_cases['Traffic Offence']; echo '<strong>'.$total_cases['Traffic Offence'].'</strong>'; }else{ $trafo=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Traffic Offence'] != NULL){ $trafo1=$break_down_cases['array_juveniles_male']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Traffic Offence'].'</strong>'; }else{ $trafo1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Traffic Offence'] != NULL){ $trafo2=$break_down_cases['array_juveniles_female']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Traffic Offence'].'</strong>'; }else{ $trafo2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Traffic Offence'] != NULL){ $trafo3=$break_down_cases['array_youth_male']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_youth_male']['Traffic Offence'].'</strong>'; }else{ $trafo3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Traffic Offence'] != NULL){ $trafo4=$break_down_cases['array_youth_female']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_youth_female']['Traffic Offence'].'</strong>'; }else{ $trafo4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Traffic Offence'] != NULL){ $trafo5=$break_down_cases['array_adult_male']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_adult_male']['Traffic Offence'].'</strong>'; }else{ $trafo5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Traffic Offence'] != NULL){ $trafo6=$break_down_cases['array_adult_female']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_adult_female']['Traffic Offence'].'</strong>'; }else{ $trafo6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Malicious Damage To Property</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Malicious Damage to Property'] != NULL){ $mdp=$total_cases['Malicious Damage to Property']; echo '<strong>'.$total_cases['Malicious Damage to Property'].'</strong>'; }else{ $mdp=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Malicious Damage to Property'] != NULL){ $mdp1=$break_down_cases['array_juveniles_male']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Malicious Damage to Property'].'</strong>'; }else{ $mdp1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Malicious Damage to Property'] != NULL){ $mdp2=$break_down_cases['array_juveniles_female']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Malicious Damage to Property'].'</strong>'; }else{ $mdp2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Malicious Damage to Property'] != NULL){ $mdp3=$break_down_cases['array_youth_male']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_youth_male']['Malicious Damage to Property'].'</strong>'; }else{ $mdp3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Malicious Damage to Property'] != NULL){ $mdp4=$break_down_cases['array_youth_female']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_youth_female']['Malicious Damage to Property'].'</strong>'; }else{ $mdp4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Malicious Damage to Property'] != NULL){ $mdp5=$break_down_cases['array_adult_male']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_adult_male']['Malicious Damage to Property'].'</strong>'; }else{ $mdp5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Malicious Damage to Property'] != NULL){ $mdp6=$break_down_cases['array_adult_female']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_adult_female']['Malicious Damage to Property'].'</strong>'; }else{ $mdp6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Theft</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Theft'] != NULL){ $theft=$total_cases['Theft']; echo '<strong>'.$total_cases['Theft'].'</strong>'; }else{ $theft=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Theft'] != NULL){ $theft1=$break_down_cases['array_juveniles_male']['Theft']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Theft'].'</strong>'; }else{ $theft1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Theft'] != NULL){ $theft2=$break_down_cases['array_juveniles_female']['Theft']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Theft'].'</strong>'; }else{ $theft2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Theft'] != NULL){ $theft3=$break_down_cases['array_youth_male']['Theft']; echo '<strong>'.$break_down_cases['array_youth_male']['Theft'].'</strong>'; }else{ $theft3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Theft'] != NULL){ $theft4=$break_down_cases['array_youth_female']['Theft']; echo '<strong>'.$break_down_cases['array_youth_female']['Theft'].'</strong>'; }else{ $theft4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Theft'] != NULL){ $theft5=$break_down_cases['array_adult_male']['Theft']; echo '<strong>'.$break_down_cases['array_adult_male']['Theft'].'</strong>'; }else{ $theft5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Theft'] != NULL){ $theft6=$break_down_cases['array_adult_female']['Theft']; echo '<strong>'.$break_down_cases['array_adult_female']['Theft'].'</strong>'; }else{ $theft6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Criminal trespass</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Criminal trespass'] != NULL){ $ct=$total_cases['Criminal trespass']; echo '<strong>'.$total_cases['Criminal trespass'].'</strong>'; }else{ $ct=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Criminal trespass'] != NULL){ $ct1=$break_down_cases['array_juveniles_male']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Criminal trespass'].'</strong>'; }else{ $ct1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Criminal trespass'] != NULL){ $ct2=$break_down_cases['array_juveniles_female']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Criminal trespass'].'</strong>'; }else{ $ct2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Criminal trespass'] != NULL){ $ct3=$break_down_cases['array_youth_male']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_youth_male']['Criminal trespass'].'</strong>'; }else{ $ct3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Criminal trespass'] != NULL){ $ct4=$break_down_cases['array_youth_female']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_youth_female']['Criminal trespass'].'</strong>'; }else{ $ct4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Criminal trespass'] != NULL){ $ct5=$break_down_cases['array_adult_male']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_adult_male']['Criminal trespass'].'</strong>'; }else{ $ct5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Criminal trespass'] != NULL){ $ct6=$break_down_cases['array_adult_female']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_adult_female']['Criminal trespass'].'</strong>'; }else{ $ct6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Store Breaking / House Breaking</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Store Breaking /house breaking'] != NULL){ $sb=$total_cases['Store Breaking /house breaking']; echo '<strong>'.$total_cases['Store Breaking /house breaking'].'</strong>'; }else{ $sb=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Store Breaking /house breaking'] != NULL){ $sb1=$break_down_cases['array_juveniles_male']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Store Breaking /house breaking'].'</strong>'; }else{ $sb1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Store Breaking /house breaking'] != NULL){ $sb2=$break_down_cases['array_juveniles_female']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Store Breaking /house breaking'].'</strong>'; }else{ $sb2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Store Breaking /house breaking'] != NULL){ $sb3=$break_down_cases['array_youth_male']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_youth_male']['Store Breaking /house breaking'].'</strong>'; }else{ $sb3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Store Breaking /house breaking'] != NULL){ $sb4=$break_down_cases['array_youth_female']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_youth_female']['Store Breaking /house breaking'].'</strong>'; }else{ $sb4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Store Breaking /house breaking'] != NULL){ $sb5=$break_down_cases['array_adult_male']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_adult_male']['Store Breaking /house breaking'].'</strong>'; }else{ $sb5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Store Breaking /house breaking'] != NULL){ $sb6=$break_down_cases['array_adult_female']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_adult_female']['Store Breaking /house breaking'].'</strong>'; }else{ $sb6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Obtaining money by false pretence</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Obtaining money by false pretence'] != NULL){ $om=$total_cases['Obtaining money by false pretence']; echo '<strong>'.$total_cases['Obtaining money by false pretence'].'</strong>'; }else{ $om=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Obtaining money by false pretence'] != NULL){ $om1=$break_down_cases['array_juveniles_male']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Obtaining money by false pretence'].'</strong>'; }else{ $om1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Obtaining money by false pretence'] != NULL){ $om2=$break_down_cases['array_juveniles_female']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Obtaining money by false pretence'].'</strong>'; }else{ $om2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Obtaining money by false pretence'] != NULL){ $om3=$break_down_cases['array_youth_male']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_youth_male']['Obtaining money by false pretence'].'</strong>'; }else{ $om3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Obtaining money by false pretence'] != NULL){ $om4=$break_down_cases['array_youth_female']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_youth_female']['Obtaining money by false pretence'].'</strong>'; }else{ $om4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Obtaining money by false pretence'] != NULL){ $om5=$break_down_cases['array_adult_male']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_adult_male']['Obtaining money by false pretence'].'</strong>'; }else{ $om5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Obtaining money by false pretence'] != NULL){ $om6=$break_down_cases['array_adult_female']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_adult_female']['Obtaining money by false pretence'].'</strong>'; }else{ $om6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Robbery</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Robbery'] != NULL){ $rob=$total_cases['Robbery']; echo '<strong>'.$total_cases['Robbery'].'</strong>'; }else{ $rob=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Robbery'] != NULL){ $rob1=$break_down_cases['array_juveniles_male']['Robbery']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Robbery'].'</strong>'; }else{ $rob1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Robbery'] != NULL){ $rob2=$break_down_cases['array_juveniles_female']['Robbery']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Robbery'].'</strong>'; }else{ $rob2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Robbery'] != NULL){ $rob3=$break_down_cases['array_youth_male']['Robbery']; echo '<strong>'.$break_down_cases['array_youth_male']['Robbery'].'</strong>'; }else{ $rob3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Robbery'] != NULL){ $rob4=$break_down_cases['array_youth_female']['Robbery']; echo '<strong>'.$break_down_cases['array_youth_female']['Robbery'].'</strong>'; }else{ $rob4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Robbery'] != NULL){ $rob5=$break_down_cases['array_adult_male']['Robbery']; echo '<strong>'.$break_down_cases['array_adult_male']['Robbery'].'</strong>'; }else{ $rob5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Robbery'] != NULL){ $rob6=$break_down_cases['array_adult_female']['Robbery']; echo '<strong>'.$break_down_cases['array_adult_female']['Robbery'].'</strong>'; }else{ $rob6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Assault</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Assault'] != NULL){ $ass=$total_cases['Assault']; echo '<strong>'.$total_cases['Assault'].'</strong>'; }else{ $ass=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Assault'] != NULL){ $ass1=$break_down_cases['array_juveniles_male']['Assault']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Assault'].'</strong>'; }else{ $ass1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Assault'] != NULL){ $ass2=$break_down_cases['array_juveniles_female']['Assault']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Assault'].'</strong>'; }else{ $ass2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Assault'] != NULL){ $ass3=$break_down_cases['array_youth_male']['Assault']; echo '<strong>'.$break_down_cases['array_youth_male']['Assault'].'</strong>'; }else{ $ass3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Assault'] != NULL){ $ass4=$break_down_cases['array_youth_female']['Assault']; echo '<strong>'.$break_down_cases['array_youth_female']['Assault'].'</strong>'; }else{ $ass4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Assault'] != NULL){ $ass5=$break_down_cases['array_adult_male']['Assault']; echo '<strong>'.$break_down_cases['array_adult_male']['Assault'].'</strong>'; }else{ $ass5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Assault'] != NULL){ $ass6=$break_down_cases['array_adult_female']['Assault']; echo '<strong>'.$break_down_cases['array_adult_female']['Assault'].'</strong>'; }else{ $ass6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Threatening violence</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Threatening violence'] != NULL){ $tv=$total_cases['Threatening violence']; echo '<strong>'.$total_cases['Threatening violence'].'</strong>'; }else{ $tv=0; echo '<strong>0</strong>'; } ?></td>
                            
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Threatening violence'] != NULL){ $tv1=$break_down_cases['array_juveniles_male']['Threatening violence']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Threatening violence'].'</strong>'; }else{ $tv1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Threatening violence'] != NULL){ $tv2=$break_down_cases['array_juveniles_female']['Threatening violence']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Threatening violence'].'</strong>'; }else{ $tv2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Threatening violence'] != NULL){ $tv3=$break_down_cases['array_youth_male']['Threatening violence']; echo '<strong>'.$break_down_cases['array_youth_male']['Threatening violence'].'</strong>'; }else{ $tv3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Threatening violence'] != NULL){ $tv4=$break_down_cases['array_youth_female']['Threatening violence']; echo '<strong>'.$break_down_cases['array_youth_female']['Threatening violence'].'</strong>'; }else{ $tv4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Threatening violence'] != NULL){ $tv5=$break_down_cases['array_adult_male']['Threatening violence']; echo '<strong>'.$break_down_cases['array_adult_male']['Threatening violence'].'</strong>'; }else{ $tv5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Threatening violence'] != NULL){ $tv6=$break_down_cases['array_adult_female']['Threatening violence']; echo '<strong>'.$break_down_cases['array_adult_female']['Threatening violence'].'</strong>'; }else{ $tv6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 32.3px;">
                            <td style="width: 179.5px; height: 32.3px;">
                                <p>Reckless Driving&nbsp; &amp; Causing death</p>
                            </td>
                            <td style="width: 77.8333px; height: 32.3px;">&nbsp;<?php if($total_cases['Reckless Driving & Causing death'] != NULL){ $rd=$total_cases['Reckless Driving & Causing death']; echo '<strong>'.$total_cases['Reckless Driving & Causing death'].'</strong>'; }else{ $rd=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Reckless Driving & Causing death'] != NULL){ $rd1=$break_down_cases['array_juveniles_male']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Reckless Driving & Causing death'] != NULL){ $rd2=$break_down_cases['array_juveniles_female']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Reckless Driving & Causing death'] != NULL){ $rd3=$break_down_cases['array_youth_male']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_youth_male']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Reckless Driving & Causing death'] != NULL){ $rd4=$break_down_cases['array_youth_female']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_youth_female']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Reckless Driving & Causing death'] != NULL){ $rd5=$break_down_cases['array_adult_male']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_adult_male']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Reckless Driving & Causing death'] != NULL){ $rd6=$break_down_cases['array_adult_female']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_adult_female']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Uttering False Documents</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Uttering False Documents'] != NULL){ $ufd=$total_cases['Uttering False Documents']; echo '<strong>'.$total_cases['Uttering False Documents'].'</strong>'; }else{ $ufd=0; echo '<strong>0</strong>'; } ?></td>
                            
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Uttering False Documents'] != NULL){ $ufd1=$break_down_cases['array_juveniles_male']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Uttering False Documents'].'</strong>'; }else{ $ufd1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Uttering False Documents'] != NULL){ $ufd2=$break_down_cases['array_juveniles_female']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Uttering False Documents'].'</strong>'; }else{ $ufd2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Uttering False Documents'] != NULL){ $ufd3=$break_down_cases['array_youth_male']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_youth_male']['Uttering False Documents'].'</strong>'; }else{ $ufd3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Uttering False Documents'] != NULL){ $ufd4=$break_down_cases['array_youth_female']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_youth_female']['Uttering False Documents'].'</strong>'; }else{ $ufd4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Uttering False Documents'] != NULL){ $ufd5=$break_down_cases['array_adult_male']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_adult_male']['Uttering False Documents'].'</strong>'; }else{ $ufd5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Uttering False Documents'] != NULL){ $ufd6=$break_down_cases['array_adult_female']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_adult_female']['Uttering False Documents'].'</strong>'; }else{ $ufd6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Hawking without a license</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Hawking without a license'] != NULL){ $hwl=$total_cases['Hawking without a license']; echo '<strong>'.$total_cases['Hawking without a license'].'</strong>'; }else{ $hwl=0; echo '<strong>0</strong>'; } ?></td>
                                                        
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Hawking without a license'] != NULL){ $hwl1=$break_down_cases['array_juveniles_male']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Hawking without a license'].'</strong>'; }else{ $hwl1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Hawking without a license'] != NULL){ $hwl2=$break_down_cases['array_juveniles_female']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Hawking without a license'].'</strong>'; }else{ $hwl2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Hawking without a license'] != NULL){ $hwl3=$break_down_cases['array_youth_male']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_youth_male']['Hawking without a license'].'</strong>'; }else{ $hwl3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Hawking without a license'] != NULL){ $hwl4=$break_down_cases['array_youth_female']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_youth_female']['Hawking without a license'].'</strong>'; }else{ $hwl4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Hawking without a license'] != NULL){ $hwl5=$break_down_cases['array_adult_male']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_adult_male']['Hawking without a license'].'</strong>'; }else{ $hwl5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Hawking without a license'] != NULL){ $hwl6=$break_down_cases['array_adult_female']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_adult_female']['Hawking without a license'].'</strong>'; }else{ $hwl6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Rogue &amp; Vagabond</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Rogue'] != NULL){ $rogue=$total_cases['Rogue']; echo '<strong>'.$total_cases['Rogue'].'</strong>'; }else{ $rogue=0; echo '<strong>0</strong>'; } ?></td>
                                                        
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Rogue'] != NULL){ $rogue1=$break_down_cases['array_juveniles_male']['Rogue']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Rogue'].'</strong>'; }else{ $rogue1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Rogue'] != NULL){ $rogue2=$break_down_cases['array_juveniles_female']['Rogue']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Rogue'].'</strong>'; }else{ $rogue2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Rogue'] != NULL){ $rogue3=$break_down_cases['array_youth_male']['Rogue']; echo '<strong>'.$break_down_cases['array_youth_male']['Rogue'].'</strong>'; }else{ $rogue3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Rogue'] != NULL){ $rogue4=$break_down_cases['array_youth_female']['Rogue']; echo '<strong>'.$break_down_cases['array_youth_female']['Rogue'].'</strong>'; }else{ $rogue4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Rogue'] != NULL){ $rogue5=$break_down_cases['array_adult_male']['Rogue']; echo '<strong>'.$break_down_cases['array_adult_male']['Rogue'].'</strong>'; }else{ $rogue5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Rogue'] != NULL){ $rogue6=$break_down_cases['array_adult_female']['Rogue']; echo '<strong>'.$break_down_cases['array_adult_female']['Rogue'].'</strong>'; }else{ $rogue6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Frequenting a place of smoking opium</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Frequenting a place of smoking opium'] != NULL){ $opium=$total_cases['Frequenting a place of smoking opium']; echo '<strong>'.$total_cases['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Frequenting a place of smoking opium'] != NULL){ $opium1=$break_down_cases['array_juveniles_male']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Frequenting a place of smoking opium'] != NULL){ $opium2=$break_down_cases['array_juveniles_female']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Frequenting a place of smoking opium'] != NULL){ $opium3=$break_down_cases['array_youth_male']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_youth_male']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Frequenting a place of smoking opium'] != NULL){ $opium4=$break_down_cases['array_youth_female']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_youth_female']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Frequenting a place of smoking opium'] != NULL){ $opium5=$break_down_cases['array_adult_male']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_adult_male']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Frequenting a place of smoking opium'] != NULL){ $opium6=$break_down_cases['array_adult_female']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_adult_female']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Escaping from a lawful custody</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Escaping from a lawful custody'] != NULL){ $cust=$total_cases['Escaping from a lawful custody']; echo '<strong>'.$total_cases['Escaping from a lawful custody'].'</strong>'; }else{ $cust=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Escaping from a lawful custody'] != NULL){ $cust1=$break_down_cases['array_juveniles_male']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Escaping from a lawful custody'].'</strong>'; }else{ $cust1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Escaping from a lawful custody'] != NULL){ $cust2=$break_down_cases['array_juveniles_female']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Escaping from a lawful custody'].'</strong>'; }else{ $cust2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Escaping from a lawful custody'] != NULL){ $cust3=$break_down_cases['array_youth_male']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_youth_male']['Escaping from a lawful custody'].'</strong>'; }else{ $cust3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Escaping from a lawful custody'] != NULL){ $cust4=$break_down_cases['array_youth_female']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_youth_female']['Escaping from a lawful custody'].'</strong>'; }else{ $cust4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Escaping from a lawful custody'] != NULL){ $cust5=$break_down_cases['array_adult_male']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_adult_male']['Escaping from a lawful custody'].'</strong>'; }else{ $cust5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Escaping from a lawful custody'] != NULL){ $cust6=$break_down_cases['array_adult_female']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_adult_female']['Escaping from a lawful custody'].'</strong>'; }else{ $cust6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Insulting using vulgar language with intention to annoy</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Insulting using vulgar language with intention to annoy'] != NULL){ $insult=$total_cases['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$total_cases['Escaping from a lawful custody'].'</strong>'; }else{ $insult=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult1=$break_down_cases['array_juveniles_male']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult2=$break_down_cases['array_juveniles_female']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult3=$break_down_cases['array_youth_male']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_youth_male']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult4=$break_down_cases['array_youth_female']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_youth_female']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult5=$break_down_cases['array_adult_male']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_adult_male']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult6=$break_down_cases['array_adult_female']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_adult_female']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Child Neglect</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Child Neglect'] != NULL){ $cn=$total_cases['Child Neglect']; echo '<strong>'.$total_cases['Child Neglect'].'</strong>'; }else{ $cn=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Child Neglect'] != NULL){ $cn1=$break_down_cases['array_juveniles_male']['Child Neglect']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Child Neglect'].'</strong>'; }else{ $cn1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Child Neglect'] != NULL){ $cn2=$break_down_cases['array_juveniles_female']['Child Neglect']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Child Neglect'].'</strong>'; }else{ $cn2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Child Neglect'] != NULL){ $cn3=$break_down_cases['array_youth_male']['Child Neglect']; echo '<strong>'.$break_down_cases['array_youth_male']['Child Neglect'].'</strong>'; }else{ $cn3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Child Neglect'] != NULL){ $cn4=$break_down_cases['array_youth_female']['Child Neglect']; echo '<strong>'.$break_down_cases['array_youth_female']['Child Neglect'].'</strong>'; }else{ $cn4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Child Neglect'] != NULL){ $cn5=$break_down_cases['array_adult_male']['Child Neglect']; echo '<strong>'.$break_down_cases['array_adult_male']['Child Neglect'].'</strong>'; }else{ $cn5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Child Neglect'] != NULL){ $cn6=$break_down_cases['array_adult_female']['Child Neglect']; echo '<strong>'.$break_down_cases['array_adult_female']['Child Neglect'].'</strong>'; }else{ $cn6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">&nbsp;<strong>Total</strong></td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php echo $mu + $kinapr + $ht + $def + $rape + $na + $trafo + $mdp + $theft + $ct + $sb + $om + $rob + $ass + $tv + $rd + $ufd + $hwl + $rogue + $opium + $cust + $insult + $cn; ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php echo $mu1 + $kinapr1 + $ht1 + $def1 + $rape1 + $na1 + $trafo1 + $mdp1 + $theft1 + $ct1 + $sb1 + $om1 + $rob1 + $ass1 + $tv1 + $rd1 + $ufd1 + $hwl1 + $rogue1 + $opium1 + $cust1 + $insult1 + $cn1; ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php echo $mu2 + $kinapr2 + $ht2 + $def2 + $rape2 + $na2 + $trafo2 + $mdp2 + $theft2 + $ct2 + $sb2 + $om2 + $rob2 + $ass2 + $tv2 + $rd2 + $ufd2 + $hwl2 + $rogue2 + $opium2 + $cust2 + $insult2 + $cn2; ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php echo $mu3 + $kinapr3 + $ht3 + $def3 + $rape3 + $na3 + $trafo3 + $mdp3 + $theft3 + $ct3 + $sb3 + $om3 + $rob3 + $ass3 + $tv3 + $rd3 + $ufd3 + $hwl3 + $rogue3 + $opium3 + $cust3 + $insult3 + $cn3; ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php echo $mu4 + $kinapr4 + $ht4 + $def4 + $rape4 + $na4 + $trafo4 + $mdp4 + $theft4 + $ct4 + $sb4 + $om4 + $rob4 + $ass4 + $tv4 + $rd4 + $ufd4 + $hwl4 + $rogue4 + $opium4 + $cust4 + $insult4 + $cn4; ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php echo $mu5 + $kinapr5 + $ht5 + $def5 + $rape5 + $na5 + $trafo5 + $mdp5 + $theft5 + $ct5 + $sb5 + $om5 + $rob5 + $ass5 + $tv5 + $rd5 + $ufd5 + $hwl5 + $rogue5 + $opium5 + $cust5 + $insult5 + $cn5; ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php echo $mu6 + $kinapr6 + $ht6 + $def6 + $rape6 + $na6 + $trafo6 + $mdp6 + $theft6 + $ct6 + $sb6 + $om6 + $rob6 + $ass6 + $tv6 + $rd6 + $ufd6 + $hwl6 + $rogue6 + $opium6 + $cust6 + $insult6 + $cn6; ?></td>
                        </tr>
                    </tbody>
                </table>
                <p>&nbsp;</p>
                <p>@ <?php echo date('Y'); ?> Uganda Law Society</p>
            </td>
        </tr>
    </body>
</table>
<?php }
if($_POST['reportname'] == 'status-progress')  
{?> 
<table border="3" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
    <body>
        <tr>
            <td style="border:none;">
                <table  border="0" align="center" style="background-color: #2e3192;color:#fff; padding-left:100px;padding-right:100px;">
                    <tbody>
                        <tr>
                            <td><img src="http://uls.datatrack.co.ug/static/images/logo.png" style="padding-right:40px;" alt="" width="100" /></td>
                            <td><b>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LEGAL AID PROJECT</p>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OF THE</p>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; UGANDA LAW SOCIETY <?php echo $_POST['clinci_name']; ?></p></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table align="center" style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;">
                    <tbody>
                        <tr>
                            <td><b>&nbsp;TO:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HEAD LEGAL AID AND PRO BONO SERVICES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;FROM:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SENIOR LEGAL OFFICER- CLINIC REPORT</td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;RE:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MONTHLY REPORT OF <b>(<?php echo $_POST['sd']; ?>) To (<?php echo $_POST['ed']; ?>)</b></td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;DATE OF SUBMISSION:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo date_format(date_create(date('Y-m-d')), ' l jS F Y'); ?></td>
                        </tr>
                    </tbody>
                </table>
                <table style="height: 549px; width: 912px;" border="1" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
                    <tbody>
                        <tr style="height: 159px;">
                            <td style="width: 139px; height: 223px;" rowspan="3"><strong>Nature Of Cases</strong></td>
                            <td style="width: 151px; height: 223px;" rowspan="3"><strong>&nbsp;No of&nbsp; Cases</strong></td>
                            <td style="width: 113.583px; height: 223px;" rowspan="3" align="center">
                                <p><strong>&nbsp;No of </strong></p>
                                <p><strong>New Cases</strong></p>
                            </td>
                            <td style="width: 107.417px; height: 223px;" rowspan="3" align="center">
                                <p><strong>&nbsp;No. of&nbsp;</strong></p>
                                <p><strong> Old cases</strong></p>
                            </td>
                            <td style="width: 435px; height: 159px;" colspan="6" align="center">&nbsp;&nbsp;
                                <p>&nbsp;<strong>Gender Disaggregation:</strong></p>
                                <p><strong>Juvenile=(12 to &lt;18 yrs), Youths=(18 to <span class="_hwx354t">&le;</span> 35yrs), </strong></p>
                                <p><strong>Adults=(35+ yrs)</strong></p>
                            </td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 202px; height: 32px;" colspan="2" align="center"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Juvenile</strong></td>
                            <td style="width: 129px; height: 32px;" colspan="2" align="center">&nbsp;&nbsp; <strong>Youths</strong></td>
                            <td style="width: 104px; height: 32px;" colspan="2" align="center">&nbsp;&nbsp; <strong>Adults</strong></td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 113px; height: 32px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 89px; height: 32px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 77px; height: 32px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 52px; height: 32px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 52px; height: 32px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 52px; height: 32px;" align="center"><strong>&nbsp;F</strong></td>
                        </tr>
                        <tr style="height: 60px;">
                            <td style="width: 139px; height: 60px;">&nbsp;Completed in Office</td>
                            <td style="width: 151px; height: 60px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 60px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 60px;">&nbsp;</td>
                            <td style="width: 113px; height: 60px;">&nbsp;</td>
                            <td style="width: 89px; height: 60px;">&nbsp;</td>
                            <td style="width: 77px; height: 60px;">&nbsp;</td>
                            <td style="width: 52px; height: 60px;">&nbsp;</td>
                            <td style="width: 52px; height: 60px;">&nbsp;</td>
                            <td style="width: 52px; height: 60px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">Pending In Office</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32.8833px;">
                            <td style="width: 139px; height: 64.8833px;" rowspan="2">&nbsp;Completed In Court</td>
                            <td style="width: 151px; height: 32.8833px;">&nbsp;Criminal</td>
                            <td style="width: 113.583px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 113px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 89px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 77px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 52px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 52px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 52px; height: 32.8833px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 151px; height: 32px;">&nbsp;Civil</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>

                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">&nbsp;Pending in court</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">&nbsp;Bail applications</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">&nbsp;Given Legal Advice</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">&nbsp;Cases Referred</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">Closed Files</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">Withdrawn Files</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">&nbsp;<strong>Total</strong></td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>@ <?php echo date('Y'); ?> Uganda Law Society</p>
            </td>
        </tr>
    </body>
</table>

<?php }
if($_POST['reportname'] == 'general')  
{?> 
<table border="3" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
    <body>
        <tr>
            <td style="border:none;">
                <table  border="0" align="center" style="background-color: #2e3192;color:#fff; padding-left:100px;padding-right:100px;">
                    <tbody>
                        <tr>
                            <td><img src="http://uls.datatrack.co.ug/static/images/logo.png" style="padding-right:40px;" alt="" width="100" /></td>
                            <td><b>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LEGAL AID PROJECT</p>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; OF THE</p>
                                <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; UGANDA LAW SOCIETY <?php echo $_POST['clinci_name']; ?></p></b>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table align="center" style="padding-top:20px;padding-bottom:20px;padding-left:20px;padding-right:20px;">
                    <tbody>
                        <tr>
                            <td><b>&nbsp;TO:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HEAD LEGAL AID AND PRO BONO SERVICES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;FROM:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; SENIOR LEGAL OFFICER- CLINIC REPORT</td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;RE:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MONTHLY REPORT OF <b>(<?php echo $_POST['sd']; ?>) To (<?php echo $_POST['ed']; ?>)</b></td>
                        </tr>
                        <tr>
                            <td><b>&nbsp;DATE OF SUBMISSION:</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo date_format(date_create(date('Y-m-d')), ' l jS F Y'); ?></td>
                        </tr>
                    </tbody>
                </table>
                <h3 align="center">Introduction To Clients To LAP</h3>
                <table  border="1" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
                    <tbody>
                        <tr style='background:green;color:#fff;'>
                            <td>&nbsp;<b>No.</b></td>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>Introduction Of Clients</b> </td>
                            <td><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Number</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                        </tr>
                        <tr>
                            <td>&nbsp; 1.</td>
                            <td>&nbsp;Self-Introduction</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Self-introduction']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 2.</td>
                            <td>&nbsp;Old Clients</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Old clients']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 3.</td>
                            <td>&nbsp;Radio</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Radio']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 4.</td>
                            <td>&nbsp;Civil servant i.e poliice, court, prison etc</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Civil-servant']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 5.</td>
                            <td>&nbsp;Local Leaders</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Local-leaders']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 6.</td>
                            <td>&nbsp;Advocates</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Advocates']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 7.</td>
                            <td>&nbsp;Friends &amp; Relatives</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Friends_Relatives']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp; 8.</td>
                            <td>&nbsp;Uganda Law Society</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['Uganda-Law-Society']; ?></td>
                        </tr >
                        <tr>
                            <td>&nbsp; 9</td>
                            <td>NGOs &amp; CBOs</td>
                            <td align="center" style='background:yellow;'>&nbsp;<?php echo $intro['NGO_CBOS']; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><b>Total</b></td>
                            <td align="center"  style='background:red;'>&nbsp;<b><?php echo $intro['Total']; ?></b></td>
                        </tr>
                    </tbody>
                </table>
                <hr />
                <h3 align="center">Nature Of Civil Cases and Number of Clients Handled</h3>
                <table border="1" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
                    <tbody>
                        <tr style="height: 33.5333px;">
                            <td style="width: 159.383px; height: 99.5333px;" rowspan="3" align="center"><strong>&nbsp;Nature Of Cases</strong></td>
                            <td style="width: 69.1167px; height: 99.5333px;" rowspan="3" align="center">&nbsp;<strong>Number of Cases</strong></td>
                            <td style="width: 635.383px; height: 33.5333px;" colspan="6" align="center">
                                <p>&nbsp;<strong>Gender Disaggregation:</strong></p>
                                <p><strong>Juvenile=(12 to &lt;18 yrs), Youths=(18 to <span class="_hwx354t">&le;</span> 35yrs), Adults=(35+ yrs)</strong></p>
                            </td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 211.433px; height: 33px;" colspan="2" align="center"><strong>&nbsp; Juveniles</strong></td>
                            <td style="width: 211.433px; height: 33px;" colspan="2" align="center">&nbsp;<strong>Youths</strong></td>
                            <td style="width: 212.517px; height: 33px;" colspan="2" align="center">&nbsp;<strong> Adults</strong></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 105.717px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 105.717px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 105.733px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 105.7px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 105.733px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 106.783px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Land &amp; Property</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Land & Property'] != NULL){ $lp=$total_cases['Land & Property']; echo '<strong>'.$total_cases['Land & Property'].'</strong>'; }else{ $lp=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Land & Property'] != NULL){ $lp1=$break_down_cases['array_juveniles_male']['Land & Property']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Land & Property'].'</strong>'; }else{ $lp1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Land & Property'] != NULL){ $lp2=$break_down_cases['array_juveniles_female']['Land & Property']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Land & Property'].'</strong>'; }else{ $lp2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Land & Property'] != NULL){ $lp3=$break_down_cases['array_youth_male']['Land & Property']; echo '<strong>'.$break_down_cases['array_youth_male']['Land & Property'].'</strong>'; }else{ $lp3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Land & Property'] != NULL){ $lp4=$break_down_cases['array_youth_female']['Land & Property']; echo '<strong>'.$break_down_cases['array_youth_female']['Land & Property'].'</strong>'; }else{ $lp4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Land & Property'] != NULL){ $lp5=$break_down_cases['array_adult_male']['Land & Property']; echo '<strong>'.$break_down_cases['array_adult_male']['Land & Property'].'</strong>'; }else{ $lp5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Land & Property'] != NULL){ $lp6=$break_down_cases['array_adult_female']['Land & Property']; echo '<strong>'.$break_down_cases['array_adult_female']['Land & Property'].'</strong>'; }else{ $lp6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Divorce &amp; Separation</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Divorce & Separation'] != NULL){$ds = $total_cases['Divorce & Separation']; echo '<strong>'.$total_cases['Divorce & Separation'].'</strong>'; }else{$ds=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Divorce & Separation'] != NULL){$ds1 = $break_down_cases['array_juveniles_male']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Divorce & Separation'].'</strong>'; }else{$ds1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Divorce & Separation'] != NULL){$ds2 = $break_down_cases['array_juveniles_female']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Divorce & Separation'].'</strong>'; }else{$ds2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Divorce & Separation'] != NULL){$ds3 = $break_down_cases['array_youth_male']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_youth_male']['Divorce & Separation'].'</strong>'; }else{$ds3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Divorce & Separation'] != NULL){$ds4 = $break_down_cases['array_youth_female']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_youth_female']['Divorce & Separation'].'</strong>'; }else{$ds4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Divorce & Separation'] != NULL){$ds5 = $break_down_cases['array_adult_male']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_adult_male']['Divorce & Separation'].'</strong>'; }else{$ds5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Divorce & Separation'] != NULL){$ds6 = $break_down_cases['array_adult_female']['Divorce & Separation']; echo '<strong>'.$break_down_cases['array_adult_female']['Divorce & Separation'].'</strong>'; }else{$ds6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Custody &amp; Maintenance</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Custody & Maintenance'] != NULL){$cm = $total_cases['Custody & Maintenance'];  echo '<strong>'.$total_cases['Custody & Maintenance'].'</strong>'; }else{$cm = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Custody & Maintenance'] != NULL){$cm1 = $break_down_cases['array_juveniles_male']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_juveniles_male']['Custody & Maintenance'].'</strong>'; }else{$cm1 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Custody & Maintenance'] != NULL){$cm2 = $break_down_cases['array_juveniles_female']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_juveniles_female']['Custody & Maintenance'].'</strong>'; }else{$cm2 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Custody & Maintenance'] != NULL){$cm3 = $break_down_cases['array_youth_male']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_youth_male']['Custody & Maintenance'].'</strong>'; }else{$cm3 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Custody & Maintenance'] != NULL){$cm4 = $break_down_cases['array_youth_female']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_youth_female']['Custody & Maintenance'].'</strong>'; }else{$cm4 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Custody & Maintenance'] != NULL){$cm5 = $break_down_cases['array_adult_male']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_adult_male']['Custody & Maintenance'].'</strong>'; }else{$cm5 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Custody & Maintenance'] != NULL){$cm6 = $break_down_cases['array_adult_female']['Custody & Maintenance'];  echo '<strong>'.$break_down_cases['array_adult_female']['Custody & Maintenance'].'</strong>'; }else{$cm6 = 0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Accident Claims</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Accident claims'] != NULL){ $ac = $total_cases['Accident claims']; echo '<strong>'.$total_cases['Accident claims'].'</strong>'; }else{ $ac=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Accident claims'] != NULL){ $ac1 = $break_down_cases['array_juveniles_male']['Accident claims']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Accident claims'].'</strong>'; }else{ $ac1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Accident claims'] != NULL){ $ac2 = $break_down_cases['array_juveniles_female']['Accident claims']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Accident claims'].'</strong>'; }else{ $ac2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Accident claims'] != NULL){ $ac3 = $break_down_cases['array_youth_male']['Accident claims']; echo '<strong>'.$break_down_cases['array_youth_male']['Accident claims'].'</strong>'; }else{ $ac3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Accident claims'] != NULL){ $ac4 = $break_down_cases['array_youth_female']['Accident claims']; echo '<strong>'.$break_down_cases['array_youth_female']['Accident claims'].'</strong>'; }else{ $ac4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Accident claims'] != NULL){ $ac5 = $break_down_cases['array_adult_male']['Accident claims']; echo '<strong>'.$break_down_cases['array_adult_male']['Accident claims'].'</strong>'; }else{ $ac5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Accident claims'] != NULL){ $ac6 = $break_down_cases['array_adult_female']['Accident claims']; echo '<strong>'.$break_down_cases['array_adult_female']['Accident claims'].'</strong>'; }else{ $ac6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Debt claims</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Debt claims'] != NULL){ $dc =$total_cases['Debt claims']; echo '<strong>'.$total_cases['Debt claims'].'</strong>'; }else{ $dc =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Debt claims'] != NULL){ $dc1 =$break_down_cases['array_juveniles_male']['Debt claims']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Debt claims'].'</strong>'; }else{ $dc1 =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Debt claims'] != NULL){ $dc2 =$break_down_cases['array_juveniles_female']['Debt claims']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Debt claims'].'</strong>'; }else{ $dc2 =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Debt claims'] != NULL){ $dc3 =$break_down_cases['array_youth_male']['Debt claims']; echo '<strong>'.$break_down_cases['array_youth_male']['Debt claims'].'</strong>'; }else{ $dc3 =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Debt claims'] != NULL){ $dc4 =$break_down_cases['array_youth_female']['Debt claims']; echo '<strong>'.$break_down_cases['array_youth_female']['Debt claims'].'</strong>'; }else{ $dc4 =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Debt claims'] != NULL){ $dc5 =$break_down_cases['array_adult_male']['Debt claims']; echo '<strong>'.$break_down_cases['array_adult_male']['Debt claims'].'</strong>'; }else{ $dc5 =0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Debt claims'] != NULL){ $dc6 =$break_down_cases['array_adult_female']['Debt claims']; echo '<strong>'.$break_down_cases['array_adult_female']['Debt claims'].'</strong>'; }else{ $dc6 =0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;Employment Claims</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Employment Claims'] != NULL){ $ec = $total_cases['Employment Claims'];  echo '<strong>'.$total_cases['Employment Claims'].'</strong>'; }else{ $ec=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Employment Claims'] != NULL){ $ec1 = $break_down_cases['array_juveniles_male']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_juveniles_male']['Employment Claims'].'</strong>'; }else{ $ec1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Employment Claims'] != NULL){ $ec2 = $break_down_cases['array_juveniles_female']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_juveniles_female']['Employment Claims'].'</strong>'; }else{ $ec2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Employment Claims'] != NULL){ $ec3 = $break_down_cases['array_youth_male']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_youth_male']['Employment Claims'].'</strong>'; }else{ $ec3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Employment Claims'] != NULL){ $ec4 = $break_down_cases['array_youth_female']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_youth_female']['Employment Claims'].'</strong>'; }else{ $ec4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Employment Claims'] != NULL){ $ec5 = $break_down_cases['array_adult_male']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_adult_male']['Employment Claims'].'</strong>'; }else{ $ec5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Employment Claims'] != NULL){ $ec6 = $break_down_cases['array_adult_female']['Employment Claims'];  echo '<strong>'.$break_down_cases['array_adult_female']['Employment Claims'].'</strong>'; }else{ $ec6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">Civil (Gen)</td>
                            <td style="width: 69.1167px; height: 33px;">&nbsp;<?php if($total_cases['Civil (Gen)'] != NULL){ $civil = $total_cases['Civil (Gen)'];  echo '<strong>'.$total_cases['Civil (Gen)'].'</strong>'; }else{ $civil = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Civil (Gen)'] != NULL){ $civil1 = $break_down_cases['array_juveniles_male']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_juveniles_male']['Civil (Gen)'].'</strong>'; }else{ $civil1 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.717px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Civil (Gen)'] != NULL){ $civil2 = $break_down_cases['array_juveniles_female']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_juveniles_female']['Civil (Gen)'].'</strong>'; }else{ $civil2 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Civil (Gen)'] != NULL){ $civil3 = $break_down_cases['array_youth_male']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_youth_male']['Civil (Gen)'].'</strong>'; }else{ $civil3 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.7px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Civil (Gen)'] != NULL){ $civil4 = $break_down_cases['array_youth_female']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_youth_female']['Civil (Gen)'].'</strong>'; }else{ $civil4 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 105.733px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Civil (Gen)'] != NULL){ $civil5 = $break_down_cases['array_adult_male']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_adult_male']['Civil (Gen)'].'</strong>'; }else{ $civil5 = 0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 106.783px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Civil (Gen)'] != NULL){ $civil6 = $break_down_cases['array_adult_female']['Civil (Gen)'];  echo '<strong>'.$break_down_cases['array_adult_female']['Civil (Gen)'].'</strong>'; }else{ $civil6 = 0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 159.383px; height: 33px;">&nbsp;<strong>Total</strong></td>
                            <td style="width: 69.1167px; height: 33px;"><b>&nbsp;<?php echo $civil + $ec + $dc + $ac + $cm + $ds + $lp; ?></b></td>
                            <td style="width: 105.717px; height: 33px;"><b>&nbsp;<?php echo $civil1 + $ec1 + $dc1 + $ac1 + $cm1 + $ds1 + $lp1; ?></b></td>
                            <td style="width: 105.717px; height: 33px;"><b>&nbsp;<?php echo $civil2 + $ec2 + $dc2 + $ac2 + $cm2 + $ds2 + $lp2; ?></b></td>
                            <td style="width: 105.733px; height: 33px;"><b>&nbsp;<?php echo $civil3 + $ec3 + $dc3 + $ac3 + $cm3 + $ds3 + $lp3; ?></b></td>
                            <td style="width: 105.7px; height: 33px;"><b>&nbsp;<?php echo $civil4 + $ec4 + $dc4 + $ac4 + $cm4 + $ds4 + $lp4; ?></b></td>
                            <td style="width: 105.733px; height: 33px;"><b>&nbsp;<?php echo $civil5 + $ec5 + $dc5 + $ac5 + $cm5 + $ds5 + $lp5; ?></b></td>
                            <td style="width: 106.783px; height: 33px;"><b>&nbsp;<?php echo $civil6 + $ec6 + $dc6 + $ac6 + $cm6 + $ds6 + $lp6; ?></b></td>
                        </tr>
                    </tbody>
                </table>
                <hr />
                <h3 align="center">Nature Of Criminal Cases and Number of Clients Handled</h3>
                <table border="1" style="height: 973px; width: 974px;" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
                    <tbody>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 99px;" rowspan="3" align="center"><strong>&nbsp;Nature Of Cases</strong></td>
                            <td style="width: 77.8333px; height: 99px;" rowspan="3" align="center">&nbsp;<strong>Number of Cases</strong></td>
                            <td style="width: 715.55px; height: 33px;" colspan="6" align="center">
                                <p>&nbsp;<strong>Gender Disaggregation:</strong></p>
                                <p><strong>Juvenile=(12 to &lt;18 yrs), Youths=(18 to <span class="_hwx354t">&le;</span> 35yrs), Adults=(35+ yrs)</strong></p>
                            </td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 238.1px; height: 33px;" colspan="2" align="center"><strong>&nbsp; Juveniles</strong></td>
                            <td style="width: 238.117px; height: 33px;" colspan="2" align="center">&nbsp;<strong>Youths</strong></td>
                            <td style="width: 239.333px; height: 33px;" colspan="2" align="center">&nbsp;<strong> Adults</strong></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 119.05px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 119.05px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 119.083px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 119.033px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 119.083px; height: 33px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 120.25px; height: 33px;" align="center"><strong>&nbsp;F</strong></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Murder</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Murder'] != NULL){ $mu=$total_cases['Murder']; echo '<strong>'.$total_cases['Murder'].'</strong>'; }else{ $mu=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Murder'] != NULL){ $mu1=$break_down_cases['array_juveniles_male']['Murder']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Murder'].'</strong>'; }else{ $mu1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Murder'] != NULL){ $mu2=$break_down_cases['array_juveniles_female']['Murder']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Murder'].'</strong>'; }else{ $mu2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Murder'] != NULL){ $mu3=$break_down_cases['array_youth_male']['Murder']; echo '<strong>'.$break_down_cases['array_youth_male']['Murder'].'</strong>'; }else{ $mu3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Murder'] != NULL){ $mu4=$break_down_cases['array_youth_female']['Murder']; echo '<strong>'.$break_down_cases['array_youth_female']['Murder'].'</strong>'; }else{ $mu4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Murder'] != NULL){ $mu5=$break_down_cases['array_adult_male']['Murder']; echo '<strong>'.$break_down_cases['array_adult_male']['Murder'].'</strong>'; }else{ $mu5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Murder'] != NULL){ $mu6=$break_down_cases['array_adult_female']['Murder']; echo '<strong>'.$break_down_cases['array_adult_female']['Murder'].'</strong>'; }else{ $mu6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Kidnap with intent to obtain a Ransom &amp; Confine</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr=$total_cases['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$total_cases['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr1=$break_down_cases['array_juveniles_male']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr2=$break_down_cases['array_juveniles_female']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr3=$break_down_cases['array_youth_male']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_youth_male']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr4=$break_down_cases['array_youth_female']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_youth_female']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr5=$break_down_cases['array_adult_male']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_adult_male']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Kidnap with intent to obtain a Ransom & Confine'] != NULL){ $kinapr6=$break_down_cases['array_adult_female']['Kidnap with intent to obtain a Ransom & Confine']; echo '<strong>'.$break_down_cases['array_adult_female']['Kidnap with intent to obtain a Ransom & Confine'].'</strong>'; }else{ $kinapr6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">&nbsp;Human Trafficking / Kidnap</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Human Trafficking'] != NULL){ $ht=$total_cases['Human Trafficking']; echo '<strong>'.$total_cases['Human Trafficking'].'</strong>'; }else{ $ht=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Human Trafficking'] != NULL){ $ht1=$break_down_cases['array_juveniles_male']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Human Trafficking'].'</strong>'; }else{ $ht1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Human Trafficking'] != NULL){ $ht2=$break_down_cases['array_juveniles_female']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Human Trafficking'].'</strong>'; }else{ $ht2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Human Trafficking'] != NULL){ $ht3=$break_down_cases['array_youth_male']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_youth_male']['Human Trafficking'].'</strong>'; }else{ $ht3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Human Trafficking'] != NULL){ $ht4=$break_down_cases['array_youth_female']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_youth_female']['Human Trafficking'].'</strong>'; }else{ $ht4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Human Trafficking'] != NULL){ $ht5=$break_down_cases['array_adult_male']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_adult_male']['Human Trafficking'].'</strong>'; }else{ $ht5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Human Trafficking'] != NULL){ $ht6=$break_down_cases['array_adult_female']['Human Trafficking']; echo '<strong>'.$break_down_cases['array_adult_female']['Human Trafficking'].'</strong>'; }else{ $ht6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">&nbsp;Defilement</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Defilement'] != NULL){ $def=$total_cases['Defilement']; echo '<strong>'.$total_cases['Defilement'].'</strong>'; }else{ $def=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Defilement'] != NULL){ $def1=$break_down_cases['array_juveniles_male']['Defilement']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Defilement'].'</strong>'; }else{ $def1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Defilement'] != NULL){ $def2=$break_down_cases['array_juveniles_female']['Defilement']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Defilement'].'</strong>'; }else{ $def2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Defilement'] != NULL){ $def3=$break_down_cases['array_youth_male']['Defilement']; echo '<strong>'.$break_down_cases['array_youth_male']['Defilement'].'</strong>'; }else{ $def3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Defilement'] != NULL){ $def4=$break_down_cases['array_youth_female']['Defilement']; echo '<strong>'.$break_down_cases['array_youth_female']['Defilement'].'</strong>'; }else{ $def4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Defilement'] != NULL){ $def5=$break_down_cases['array_adult_male']['Defilement']; echo '<strong>'.$break_down_cases['array_adult_male']['Defilement'].'</strong>'; }else{ $def5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Defilement'] != NULL){ $def6=$break_down_cases['array_adult_female']['Defilement']; echo '<strong>'.$break_down_cases['array_adult_female']['Defilement'].'</strong>'; }else{ $def6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">&nbsp;Rape</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp; <?php if($total_cases['Rape'] != NULL){ $rape=$total_cases['Rape']; echo '<strong>'.$total_cases['Rape'].'</strong>'; }else{ $rape=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Rape'] != NULL){ $rape1=$break_down_cases['array_juveniles_male']['Rape']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Rape'].'</strong>'; }else{ $rape1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Rape'] != NULL){ $rape2=$break_down_cases['array_juveniles_female']['Rape']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Rape'].'</strong>'; }else{ $rape2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Rape'] != NULL){ $rape3=$break_down_cases['array_youth_male']['Rape']; echo '<strong>'.$break_down_cases['array_youth_male']['Rape'].'</strong>'; }else{ $rape3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Rape'] != NULL){ $rape4=$break_down_cases['array_youth_female']['Rape']; echo '<strong>'.$break_down_cases['array_youth_female']['Rape'].'</strong>'; }else{ $rape4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Rape'] != NULL){ $rape5=$break_down_cases['array_adult_male']['Rape']; echo '<strong>'.$break_down_cases['array_adult_male']['Rape'].'</strong>'; }else{ $rape5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Rape'] != NULL){ $rape6=$break_down_cases['array_adult_female']['Rape']; echo '<strong>'.$break_down_cases['array_adult_female']['Rape'].'</strong>'; }else{ $rape6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Negligent Act likely to spread disease</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp; <?php if($total_cases['Negligent Act likely to spread disease'] != NULL){ $na=$total_cases['Negligent Act likely to spread disease']; echo '<strong>'.$total_cases['Negligent Act likely to spread disease'].'</strong>'; }else{ $na=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Negligent Act likely to spread disease'] != NULL){ $na1=$break_down_cases['array_juveniles_male']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Negligent Act likely to spread disease'] != NULL){ $na2=$break_down_cases['array_juveniles_female']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Negligent Act likely to spread disease'] != NULL){ $na3=$break_down_cases['array_youth_male']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_youth_male']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Negligent Act likely to spread disease'] != NULL){ $na4=$break_down_cases['array_youth_female']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_youth_female']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Negligent Act likely to spread disease'] != NULL){ $na5=$break_down_cases['array_adult_male']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_adult_male']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Negligent Act likely to spread disease'] != NULL){ $na6=$break_down_cases['array_adult_female']['Negligent Act likely to spread disease']; echo '<strong>'.$break_down_cases['array_adult_female']['Negligent Act likely to spread disease'].'</strong>'; }else{ $na6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Traffic Offence</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp; <?php if($total_cases['Traffic Offence'] != NULL){ $trafo=$total_cases['Traffic Offence']; echo '<strong>'.$total_cases['Traffic Offence'].'</strong>'; }else{ $trafo=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Traffic Offence'] != NULL){ $trafo1=$break_down_cases['array_juveniles_male']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Traffic Offence'].'</strong>'; }else{ $trafo1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Traffic Offence'] != NULL){ $trafo2=$break_down_cases['array_juveniles_female']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Traffic Offence'].'</strong>'; }else{ $trafo2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Traffic Offence'] != NULL){ $trafo3=$break_down_cases['array_youth_male']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_youth_male']['Traffic Offence'].'</strong>'; }else{ $trafo3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Traffic Offence'] != NULL){ $trafo4=$break_down_cases['array_youth_female']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_youth_female']['Traffic Offence'].'</strong>'; }else{ $trafo4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Traffic Offence'] != NULL){ $trafo5=$break_down_cases['array_adult_male']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_adult_male']['Traffic Offence'].'</strong>'; }else{ $trafo5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Traffic Offence'] != NULL){ $trafo6=$break_down_cases['array_adult_female']['Traffic Offence']; echo '<strong>'.$break_down_cases['array_adult_female']['Traffic Offence'].'</strong>'; }else{ $trafo6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Malicious Damage To Property</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Malicious Damage to Property'] != NULL){ $mdp=$total_cases['Malicious Damage to Property']; echo '<strong>'.$total_cases['Malicious Damage to Property'].'</strong>'; }else{ $mdp=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Malicious Damage to Property'] != NULL){ $mdp1=$break_down_cases['array_juveniles_male']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Malicious Damage to Property'].'</strong>'; }else{ $mdp1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Malicious Damage to Property'] != NULL){ $mdp2=$break_down_cases['array_juveniles_female']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Malicious Damage to Property'].'</strong>'; }else{ $mdp2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Malicious Damage to Property'] != NULL){ $mdp3=$break_down_cases['array_youth_male']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_youth_male']['Malicious Damage to Property'].'</strong>'; }else{ $mdp3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Malicious Damage to Property'] != NULL){ $mdp4=$break_down_cases['array_youth_female']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_youth_female']['Malicious Damage to Property'].'</strong>'; }else{ $mdp4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Malicious Damage to Property'] != NULL){ $mdp5=$break_down_cases['array_adult_male']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_adult_male']['Malicious Damage to Property'].'</strong>'; }else{ $mdp5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Malicious Damage to Property'] != NULL){ $mdp6=$break_down_cases['array_adult_female']['Malicious Damage to Property']; echo '<strong>'.$break_down_cases['array_adult_female']['Malicious Damage to Property'].'</strong>'; }else{ $mdp6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Theft</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Theft'] != NULL){ $theft=$total_cases['Theft']; echo '<strong>'.$total_cases['Theft'].'</strong>'; }else{ $theft=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Theft'] != NULL){ $theft1=$break_down_cases['array_juveniles_male']['Theft']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Theft'].'</strong>'; }else{ $theft1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Theft'] != NULL){ $theft2=$break_down_cases['array_juveniles_female']['Theft']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Theft'].'</strong>'; }else{ $theft2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Theft'] != NULL){ $theft3=$break_down_cases['array_youth_male']['Theft']; echo '<strong>'.$break_down_cases['array_youth_male']['Theft'].'</strong>'; }else{ $theft3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Theft'] != NULL){ $theft4=$break_down_cases['array_youth_female']['Theft']; echo '<strong>'.$break_down_cases['array_youth_female']['Theft'].'</strong>'; }else{ $theft4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Theft'] != NULL){ $theft5=$break_down_cases['array_adult_male']['Theft']; echo '<strong>'.$break_down_cases['array_adult_male']['Theft'].'</strong>'; }else{ $theft5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Theft'] != NULL){ $theft6=$break_down_cases['array_adult_female']['Theft']; echo '<strong>'.$break_down_cases['array_adult_female']['Theft'].'</strong>'; }else{ $theft6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Criminal trespass</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Criminal trespass'] != NULL){ $ct=$total_cases['Criminal trespass']; echo '<strong>'.$total_cases['Criminal trespass'].'</strong>'; }else{ $ct=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Criminal trespass'] != NULL){ $ct1=$break_down_cases['array_juveniles_male']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Criminal trespass'].'</strong>'; }else{ $ct1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Criminal trespass'] != NULL){ $ct2=$break_down_cases['array_juveniles_female']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Criminal trespass'].'</strong>'; }else{ $ct2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Criminal trespass'] != NULL){ $ct3=$break_down_cases['array_youth_male']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_youth_male']['Criminal trespass'].'</strong>'; }else{ $ct3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Criminal trespass'] != NULL){ $ct4=$break_down_cases['array_youth_female']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_youth_female']['Criminal trespass'].'</strong>'; }else{ $ct4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Criminal trespass'] != NULL){ $ct5=$break_down_cases['array_adult_male']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_adult_male']['Criminal trespass'].'</strong>'; }else{ $ct5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Criminal trespass'] != NULL){ $ct6=$break_down_cases['array_adult_female']['Criminal trespass']; echo '<strong>'.$break_down_cases['array_adult_female']['Criminal trespass'].'</strong>'; }else{ $ct6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Store Breaking / House Breaking</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Store Breaking /house breaking'] != NULL){ $sb=$total_cases['Store Breaking /house breaking']; echo '<strong>'.$total_cases['Store Breaking /house breaking'].'</strong>'; }else{ $sb=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Store Breaking /house breaking'] != NULL){ $sb1=$break_down_cases['array_juveniles_male']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Store Breaking /house breaking'].'</strong>'; }else{ $sb1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Store Breaking /house breaking'] != NULL){ $sb2=$break_down_cases['array_juveniles_female']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Store Breaking /house breaking'].'</strong>'; }else{ $sb2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Store Breaking /house breaking'] != NULL){ $sb3=$break_down_cases['array_youth_male']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_youth_male']['Store Breaking /house breaking'].'</strong>'; }else{ $sb3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Store Breaking /house breaking'] != NULL){ $sb4=$break_down_cases['array_youth_female']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_youth_female']['Store Breaking /house breaking'].'</strong>'; }else{ $sb4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Store Breaking /house breaking'] != NULL){ $sb5=$break_down_cases['array_adult_male']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_adult_male']['Store Breaking /house breaking'].'</strong>'; }else{ $sb5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Store Breaking /house breaking'] != NULL){ $sb6=$break_down_cases['array_adult_female']['Store Breaking /house breaking']; echo '<strong>'.$break_down_cases['array_adult_female']['Store Breaking /house breaking'].'</strong>'; }else{ $sb6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Obtaining money by false pretence</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Obtaining money by false pretence'] != NULL){ $om=$total_cases['Obtaining money by false pretence']; echo '<strong>'.$total_cases['Obtaining money by false pretence'].'</strong>'; }else{ $om=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Obtaining money by false pretence'] != NULL){ $om1=$break_down_cases['array_juveniles_male']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Obtaining money by false pretence'].'</strong>'; }else{ $om1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Obtaining money by false pretence'] != NULL){ $om2=$break_down_cases['array_juveniles_female']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Obtaining money by false pretence'].'</strong>'; }else{ $om2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Obtaining money by false pretence'] != NULL){ $om3=$break_down_cases['array_youth_male']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_youth_male']['Obtaining money by false pretence'].'</strong>'; }else{ $om3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Obtaining money by false pretence'] != NULL){ $om4=$break_down_cases['array_youth_female']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_youth_female']['Obtaining money by false pretence'].'</strong>'; }else{ $om4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Obtaining money by false pretence'] != NULL){ $om5=$break_down_cases['array_adult_male']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_adult_male']['Obtaining money by false pretence'].'</strong>'; }else{ $om5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Obtaining money by false pretence'] != NULL){ $om6=$break_down_cases['array_adult_female']['Obtaining money by false pretence']; echo '<strong>'.$break_down_cases['array_adult_female']['Obtaining money by false pretence'].'</strong>'; }else{ $om6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Robbery</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Robbery'] != NULL){ $rob=$total_cases['Robbery']; echo '<strong>'.$total_cases['Robbery'].'</strong>'; }else{ $rob=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Robbery'] != NULL){ $rob1=$break_down_cases['array_juveniles_male']['Robbery']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Robbery'].'</strong>'; }else{ $rob1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Robbery'] != NULL){ $rob2=$break_down_cases['array_juveniles_female']['Robbery']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Robbery'].'</strong>'; }else{ $rob2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Robbery'] != NULL){ $rob3=$break_down_cases['array_youth_male']['Robbery']; echo '<strong>'.$break_down_cases['array_youth_male']['Robbery'].'</strong>'; }else{ $rob3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Robbery'] != NULL){ $rob4=$break_down_cases['array_youth_female']['Robbery']; echo '<strong>'.$break_down_cases['array_youth_female']['Robbery'].'</strong>'; }else{ $rob4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Robbery'] != NULL){ $rob5=$break_down_cases['array_adult_male']['Robbery']; echo '<strong>'.$break_down_cases['array_adult_male']['Robbery'].'</strong>'; }else{ $rob5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Robbery'] != NULL){ $rob6=$break_down_cases['array_adult_female']['Robbery']; echo '<strong>'.$break_down_cases['array_adult_female']['Robbery'].'</strong>'; }else{ $rob6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Assault</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Assault'] != NULL){ $ass=$total_cases['Assault']; echo '<strong>'.$total_cases['Assault'].'</strong>'; }else{ $ass=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Assault'] != NULL){ $ass1=$break_down_cases['array_juveniles_male']['Assault']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Assault'].'</strong>'; }else{ $ass1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Assault'] != NULL){ $ass2=$break_down_cases['array_juveniles_female']['Assault']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Assault'].'</strong>'; }else{ $ass2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Assault'] != NULL){ $ass3=$break_down_cases['array_youth_male']['Assault']; echo '<strong>'.$break_down_cases['array_youth_male']['Assault'].'</strong>'; }else{ $ass3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Assault'] != NULL){ $ass4=$break_down_cases['array_youth_female']['Assault']; echo '<strong>'.$break_down_cases['array_youth_female']['Assault'].'</strong>'; }else{ $ass4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Assault'] != NULL){ $ass5=$break_down_cases['array_adult_male']['Assault']; echo '<strong>'.$break_down_cases['array_adult_male']['Assault'].'</strong>'; }else{ $ass5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Assault'] != NULL){ $ass6=$break_down_cases['array_adult_female']['Assault']; echo '<strong>'.$break_down_cases['array_adult_female']['Assault'].'</strong>'; }else{ $ass6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Threatening violence</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Threatening violence'] != NULL){ $tv=$total_cases['Threatening violence']; echo '<strong>'.$total_cases['Threatening violence'].'</strong>'; }else{ $tv=0; echo '<strong>0</strong>'; } ?></td>
                            
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Threatening violence'] != NULL){ $tv1=$break_down_cases['array_juveniles_male']['Threatening violence']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Threatening violence'].'</strong>'; }else{ $tv1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Threatening violence'] != NULL){ $tv2=$break_down_cases['array_juveniles_female']['Threatening violence']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Threatening violence'].'</strong>'; }else{ $tv2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Threatening violence'] != NULL){ $tv3=$break_down_cases['array_youth_male']['Threatening violence']; echo '<strong>'.$break_down_cases['array_youth_male']['Threatening violence'].'</strong>'; }else{ $tv3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Threatening violence'] != NULL){ $tv4=$break_down_cases['array_youth_female']['Threatening violence']; echo '<strong>'.$break_down_cases['array_youth_female']['Threatening violence'].'</strong>'; }else{ $tv4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Threatening violence'] != NULL){ $tv5=$break_down_cases['array_adult_male']['Threatening violence']; echo '<strong>'.$break_down_cases['array_adult_male']['Threatening violence'].'</strong>'; }else{ $tv5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Threatening violence'] != NULL){ $tv6=$break_down_cases['array_adult_female']['Threatening violence']; echo '<strong>'.$break_down_cases['array_adult_female']['Threatening violence'].'</strong>'; }else{ $tv6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 32.3px;">
                            <td style="width: 179.5px; height: 32.3px;">
                                <p>Reckless Driving&nbsp; &amp; Causing death</p>
                            </td>
                            <td style="width: 77.8333px; height: 32.3px;">&nbsp;<?php if($total_cases['Reckless Driving & Causing death'] != NULL){ $rd=$total_cases['Reckless Driving & Causing death']; echo '<strong>'.$total_cases['Reckless Driving & Causing death'].'</strong>'; }else{ $rd=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Reckless Driving & Causing death'] != NULL){ $rd1=$break_down_cases['array_juveniles_male']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Reckless Driving & Causing death'] != NULL){ $rd2=$break_down_cases['array_juveniles_female']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Reckless Driving & Causing death'] != NULL){ $rd3=$break_down_cases['array_youth_male']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_youth_male']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Reckless Driving & Causing death'] != NULL){ $rd4=$break_down_cases['array_youth_female']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_youth_female']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Reckless Driving & Causing death'] != NULL){ $rd5=$break_down_cases['array_adult_male']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_adult_male']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Reckless Driving & Causing death'] != NULL){ $rd6=$break_down_cases['array_adult_female']['Reckless Driving & Causing death']; echo '<strong>'.$break_down_cases['array_adult_female']['Reckless Driving & Causing death'].'</strong>'; }else{ $rd6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Uttering False Documents</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Uttering False Documents'] != NULL){ $ufd=$total_cases['Uttering False Documents']; echo '<strong>'.$total_cases['Uttering False Documents'].'</strong>'; }else{ $ufd=0; echo '<strong>0</strong>'; } ?></td>
                            
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Uttering False Documents'] != NULL){ $ufd1=$break_down_cases['array_juveniles_male']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Uttering False Documents'].'</strong>'; }else{ $ufd1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Uttering False Documents'] != NULL){ $ufd2=$break_down_cases['array_juveniles_female']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Uttering False Documents'].'</strong>'; }else{ $ufd2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Uttering False Documents'] != NULL){ $ufd3=$break_down_cases['array_youth_male']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_youth_male']['Uttering False Documents'].'</strong>'; }else{ $ufd3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Uttering False Documents'] != NULL){ $ufd4=$break_down_cases['array_youth_female']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_youth_female']['Uttering False Documents'].'</strong>'; }else{ $ufd4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Uttering False Documents'] != NULL){ $ufd5=$break_down_cases['array_adult_male']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_adult_male']['Uttering False Documents'].'</strong>'; }else{ $ufd5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Uttering False Documents'] != NULL){ $ufd6=$break_down_cases['array_adult_female']['Uttering False Documents']; echo '<strong>'.$break_down_cases['array_adult_female']['Uttering False Documents'].'</strong>'; }else{ $ufd6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Hawking without a license</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Hawking without a license'] != NULL){ $hwl=$total_cases['Hawking without a license']; echo '<strong>'.$total_cases['Hawking without a license'].'</strong>'; }else{ $hwl=0; echo '<strong>0</strong>'; } ?></td>
                                                        
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Hawking without a license'] != NULL){ $hwl1=$break_down_cases['array_juveniles_male']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Hawking without a license'].'</strong>'; }else{ $hwl1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Hawking without a license'] != NULL){ $hwl2=$break_down_cases['array_juveniles_female']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Hawking without a license'].'</strong>'; }else{ $hwl2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Hawking without a license'] != NULL){ $hwl3=$break_down_cases['array_youth_male']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_youth_male']['Hawking without a license'].'</strong>'; }else{ $hwl3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Hawking without a license'] != NULL){ $hwl4=$break_down_cases['array_youth_female']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_youth_female']['Hawking without a license'].'</strong>'; }else{ $hwl4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Hawking without a license'] != NULL){ $hwl5=$break_down_cases['array_adult_male']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_adult_male']['Hawking without a license'].'</strong>'; }else{ $hwl5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Hawking without a license'] != NULL){ $hwl6=$break_down_cases['array_adult_female']['Hawking without a license']; echo '<strong>'.$break_down_cases['array_adult_female']['Hawking without a license'].'</strong>'; }else{ $hwl6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Rogue &amp; Vagabond</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Rogue'] != NULL){ $rogue=$total_cases['Rogue']; echo '<strong>'.$total_cases['Rogue'].'</strong>'; }else{ $rogue=0; echo '<strong>0</strong>'; } ?></td>
                                                        
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Rogue'] != NULL){ $rogue1=$break_down_cases['array_juveniles_male']['Rogue']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Rogue'].'</strong>'; }else{ $rogue1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Rogue'] != NULL){ $rogue2=$break_down_cases['array_juveniles_female']['Rogue']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Rogue'].'</strong>'; }else{ $rogue2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Rogue'] != NULL){ $rogue3=$break_down_cases['array_youth_male']['Rogue']; echo '<strong>'.$break_down_cases['array_youth_male']['Rogue'].'</strong>'; }else{ $rogue3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Rogue'] != NULL){ $rogue4=$break_down_cases['array_youth_female']['Rogue']; echo '<strong>'.$break_down_cases['array_youth_female']['Rogue'].'</strong>'; }else{ $rogue4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Rogue'] != NULL){ $rogue5=$break_down_cases['array_adult_male']['Rogue']; echo '<strong>'.$break_down_cases['array_adult_male']['Rogue'].'</strong>'; }else{ $rogue5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Rogue'] != NULL){ $rogue6=$break_down_cases['array_adult_female']['Rogue']; echo '<strong>'.$break_down_cases['array_adult_female']['Rogue'].'</strong>'; }else{ $rogue6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Frequenting a place of smoking opium</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Frequenting a place of smoking opium'] != NULL){ $opium=$total_cases['Frequenting a place of smoking opium']; echo '<strong>'.$total_cases['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Frequenting a place of smoking opium'] != NULL){ $opium1=$break_down_cases['array_juveniles_male']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Frequenting a place of smoking opium'] != NULL){ $opium2=$break_down_cases['array_juveniles_female']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Frequenting a place of smoking opium'] != NULL){ $opium3=$break_down_cases['array_youth_male']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_youth_male']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Frequenting a place of smoking opium'] != NULL){ $opium4=$break_down_cases['array_youth_female']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_youth_female']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Frequenting a place of smoking opium'] != NULL){ $opium5=$break_down_cases['array_adult_male']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_adult_male']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Frequenting a place of smoking opium'] != NULL){ $opium6=$break_down_cases['array_adult_female']['Frequenting a place of smoking opium']; echo '<strong>'.$break_down_cases['array_adult_female']['Frequenting a place of smoking opium'].'</strong>'; }else{ $opium6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Escaping from a lawful custody</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Escaping from a lawful custody'] != NULL){ $cust=$total_cases['Escaping from a lawful custody']; echo '<strong>'.$total_cases['Escaping from a lawful custody'].'</strong>'; }else{ $cust=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Escaping from a lawful custody'] != NULL){ $cust1=$break_down_cases['array_juveniles_male']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Escaping from a lawful custody'].'</strong>'; }else{ $cust1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Escaping from a lawful custody'] != NULL){ $cust2=$break_down_cases['array_juveniles_female']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Escaping from a lawful custody'].'</strong>'; }else{ $cust2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Escaping from a lawful custody'] != NULL){ $cust3=$break_down_cases['array_youth_male']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_youth_male']['Escaping from a lawful custody'].'</strong>'; }else{ $cust3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Escaping from a lawful custody'] != NULL){ $cust4=$break_down_cases['array_youth_female']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_youth_female']['Escaping from a lawful custody'].'</strong>'; }else{ $cust4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Escaping from a lawful custody'] != NULL){ $cust5=$break_down_cases['array_adult_male']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_adult_male']['Escaping from a lawful custody'].'</strong>'; }else{ $cust5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Escaping from a lawful custody'] != NULL){ $cust6=$break_down_cases['array_adult_female']['Escaping from a lawful custody']; echo '<strong>'.$break_down_cases['array_adult_female']['Escaping from a lawful custody'].'</strong>'; }else{ $cust6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Insulting using vulgar language with intention to annoy</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Insulting using vulgar language with intention to annoy'] != NULL){ $insult=$total_cases['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$total_cases['Escaping from a lawful custody'].'</strong>'; }else{ $insult=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult1=$break_down_cases['array_juveniles_male']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult2=$break_down_cases['array_juveniles_female']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult3=$break_down_cases['array_youth_male']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_youth_male']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult4=$break_down_cases['array_youth_female']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_youth_female']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult5=$break_down_cases['array_adult_male']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_adult_male']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Insulting using vulgar language with intention to annoy'] != NULL){ $insult6=$break_down_cases['array_adult_female']['Insulting using vulgar language with intention to annoy']; echo '<strong>'.$break_down_cases['array_adult_female']['Insulting using vulgar language with intention to annoy'].'</strong>'; }else{ $insult6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">Child Neglect</td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php if($total_cases['Child Neglect'] != NULL){ $cn=$total_cases['Child Neglect']; echo '<strong>'.$total_cases['Child Neglect'].'</strong>'; }else{ $cn=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_male']['Child Neglect'] != NULL){ $cn1=$break_down_cases['array_juveniles_male']['Child Neglect']; echo '<strong>'.$break_down_cases['array_juveniles_male']['Child Neglect'].'</strong>'; }else{ $cn1=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php if($break_down_cases['array_juveniles_female']['Child Neglect'] != NULL){ $cn2=$break_down_cases['array_juveniles_female']['Child Neglect']; echo '<strong>'.$break_down_cases['array_juveniles_female']['Child Neglect'].'</strong>'; }else{ $cn2=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_male']['Child Neglect'] != NULL){ $cn3=$break_down_cases['array_youth_male']['Child Neglect']; echo '<strong>'.$break_down_cases['array_youth_male']['Child Neglect'].'</strong>'; }else{ $cn3=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php if($break_down_cases['array_youth_female']['Child Neglect'] != NULL){ $cn4=$break_down_cases['array_youth_female']['Child Neglect']; echo '<strong>'.$break_down_cases['array_youth_female']['Child Neglect'].'</strong>'; }else{ $cn4=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_male']['Child Neglect'] != NULL){ $cn5=$break_down_cases['array_adult_male']['Child Neglect']; echo '<strong>'.$break_down_cases['array_adult_male']['Child Neglect'].'</strong>'; }else{ $cn5=0; echo '<strong>0</strong>'; } ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php if($break_down_cases['array_adult_female']['Child Neglect'] != NULL){ $cn6=$break_down_cases['array_adult_female']['Child Neglect']; echo '<strong>'.$break_down_cases['array_adult_female']['Child Neglect'].'</strong>'; }else{ $cn6=0; echo '<strong>0</strong>'; } ?></td>
                        </tr>
                        <tr style="height: 33px;">
                            <td style="width: 179.5px; height: 33px;">&nbsp;<strong>Total</strong></td>
                            <td style="width: 77.8333px; height: 33px;">&nbsp;<?php echo $mu + $kinapr + $ht + $def + $rape + $na + $trafo + $mdp + $theft + $ct + $sb + $om + $rob + $ass + $tv + $rd + $ufd + $hwl + $rogue + $opium + $cust + $insult + $cn; ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php echo $mu1 + $kinapr1 + $ht1 + $def1 + $rape1 + $na1 + $trafo1 + $mdp1 + $theft1 + $ct1 + $sb1 + $om1 + $rob1 + $ass1 + $tv1 + $rd1 + $ufd1 + $hwl1 + $rogue1 + $opium1 + $cust1 + $insult1 + $cn1; ?></td>
                            <td style="width: 119.05px; height: 33px;">&nbsp;<?php echo $mu2 + $kinapr2 + $ht2 + $def2 + $rape2 + $na2 + $trafo2 + $mdp2 + $theft2 + $ct2 + $sb2 + $om2 + $rob2 + $ass2 + $tv2 + $rd2 + $ufd2 + $hwl2 + $rogue2 + $opium2 + $cust2 + $insult2 + $cn2; ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php echo $mu3 + $kinapr3 + $ht3 + $def3 + $rape3 + $na3 + $trafo3 + $mdp3 + $theft3 + $ct3 + $sb3 + $om3 + $rob3 + $ass3 + $tv3 + $rd3 + $ufd3 + $hwl3 + $rogue3 + $opium3 + $cust3 + $insult3 + $cn3; ?></td>
                            <td style="width: 119.033px; height: 33px;">&nbsp;<?php echo $mu4 + $kinapr4 + $ht4 + $def4 + $rape4 + $na4 + $trafo4 + $mdp4 + $theft4 + $ct4 + $sb4 + $om4 + $rob4 + $ass4 + $tv4 + $rd4 + $ufd4 + $hwl4 + $rogue4 + $opium4 + $cust4 + $insult4 + $cn4; ?></td>
                            <td style="width: 119.083px; height: 33px;">&nbsp;<?php echo $mu5 + $kinapr5 + $ht5 + $def5 + $rape5 + $na5 + $trafo5 + $mdp5 + $theft5 + $ct5 + $sb5 + $om5 + $rob5 + $ass5 + $tv5 + $rd5 + $ufd5 + $hwl5 + $rogue5 + $opium5 + $cust5 + $insult5 + $cn5; ?></td>
                            <td style="width: 120.25px; height: 33px;">&nbsp;<?php echo $mu6 + $kinapr6 + $ht6 + $def6 + $rape6 + $na6 + $trafo6 + $mdp6 + $theft6 + $ct6 + $sb6 + $om6 + $rob6 + $ass6 + $tv6 + $rd6 + $ufd6 + $hwl6 + $rogue6 + $opium6 + $cust6 + $insult6 + $cn6; ?></td>
                        </tr>
                    </tbody>
                </table>
                <hr />
                <h3 align="center">Status Of Progress Of Cases Handled</h3>
                <table style="height: 549px; width: 912px;" border="1" align="center" style="padding-top:40px;padding-bottom:40px;padding-left:40px;padding-right:40px;">
                    <tbody>
                        <tr style="height: 159px;">
                            <td style="width: 139px; height: 223px;" rowspan="3"><strong>Nature Of Cases</strong></td>
                            <td style="width: 151px; height: 223px;" rowspan="3"><strong>&nbsp;No of&nbsp; Cases</strong></td>
                            <td style="width: 113.583px; height: 223px;" rowspan="3" align="center">
                                <p><strong>&nbsp;No of </strong></p>
                                <p><strong>New Cases</strong></p>
                            </td>
                            <td style="width: 107.417px; height: 223px;" rowspan="3" align="center">
                                <p><strong>&nbsp;No. of&nbsp;</strong></p>
                                <p><strong> Old cases</strong></p>
                            </td>
                            <td style="width: 435px; height: 159px;" colspan="6" align="center">&nbsp;&nbsp;
                                <p>&nbsp;<strong>Gender Disaggregation:</strong></p>
                                <p><strong>Juvenile=(12 to &lt;18 yrs), Youths=(18 to <span class="_hwx354t">&le;</span> 35yrs), </strong></p>
                                <p><strong>Adults=(35+ yrs)</strong></p>
                            </td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 202px; height: 32px;" colspan="2" align="center"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Juvenile</strong></td>
                            <td style="width: 129px; height: 32px;" colspan="2" align="center">&nbsp;&nbsp; <strong>Youths</strong></td>
                            <td style="width: 104px; height: 32px;" colspan="2" align="center">&nbsp;&nbsp; <strong>Adults</strong></td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 113px; height: 32px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 89px; height: 32px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 77px; height: 32px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 52px; height: 32px;" align="center"><strong>&nbsp;F</strong></td>
                            <td style="width: 52px; height: 32px;" align="center"><strong>&nbsp;M</strong></td>
                            <td style="width: 52px; height: 32px;" align="center"><strong>&nbsp;F</strong></td>
                        </tr>
                        <tr style="height: 60px;">
                            <td style="width: 139px; height: 60px;">&nbsp;Completed in Office</td>
                            <td style="width: 151px; height: 60px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 60px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 60px;">&nbsp;</td>
                            <td style="width: 113px; height: 60px;">&nbsp;</td>
                            <td style="width: 89px; height: 60px;">&nbsp;</td>
                            <td style="width: 77px; height: 60px;">&nbsp;</td>
                            <td style="width: 52px; height: 60px;">&nbsp;</td>
                            <td style="width: 52px; height: 60px;">&nbsp;</td>
                            <td style="width: 52px; height: 60px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">Pending In Office</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32.8833px;">
                            <td style="width: 139px; height: 64.8833px;" rowspan="2">&nbsp;Completed In Court</td>
                            <td style="width: 151px; height: 32.8833px;">&nbsp;Criminal</td>
                            <td style="width: 113.583px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 113px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 89px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 77px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 52px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 52px; height: 32.8833px;">&nbsp;</td>
                            <td style="width: 52px; height: 32.8833px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 151px; height: 32px;">&nbsp;Civil</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>

                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">&nbsp;Pending in court</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">&nbsp;Bail applications</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">&nbsp;Given Legal Advice</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">&nbsp;Cases Referred</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">Closed Files</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">Withdrawn Files</td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                        <tr style="height: 32px;">
                            <td style="width: 139px; height: 32px;">&nbsp;<strong>Total</strong></td>
                            <td style="width: 151px; height: 32px;">&nbsp;</td>
                            <td style="width: 113.583px; height: 32px;">&nbsp;</td>
                            <td style="width: 107.417px; height: 32px;">&nbsp;</td>
                            <td style="width: 113px; height: 32px;">&nbsp;</td>
                            <td style="width: 89px; height: 32px;">&nbsp;</td>
                            <td style="width: 77px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                            <td style="width: 52px; height: 32px;">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
                <p>@ <?php echo date('Y'); ?> Uganda Law Society</p>
            </td>
        </tr>
    </body>
</table>
<?php } ?>
</body>
</html>