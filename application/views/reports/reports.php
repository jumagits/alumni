  <div id="page-wrapper" style="padding-top: 20px;">

      <div class="container-fluid">

          <?php  if (isset($details) AND !empty($details) ) {
                    foreach ($details as $key => $info) { }} ?>

          <!-- Page Heading -->
          <div class="row">

              <div class="col-lg-12">

                  <h4 class="page-title">Customised IMS System Generated Reports /
                      <?php echo isset($pageTitle) ? $pageTitle :''; ?> </h4>

              </div>

          </div>
          <hr>

          <div class="row">

              <div class="col-lg-3">

                  <div class="bet panel">
                      <div class="panel-heading bg-warning">
                          <h6 class="text-center">CHOOSE FIELDS FOR CUSTOM REPORTS</h6>
                      </div>

                      <form action="#" method="POST" style="padding: 20px;">

                          <div class="form-group">
                              <label for="ReportName"></label>
                              <select name="reportName" id="reportName" class="form-control custom-select">
                                  <option value="">--Select Report --</option>
                                  <option value=""> Registered Companies</option>
                                  <option value=""> Registered Alumni</option>
                                  <option value=""> Registered Jobs</option>
                                  <option value=""> Registered Events</option>
                              </select>
                          </div>

                          <div class="form-group">
                              <label for="ReportName"></label>
                              <select name="reportName" class="form-control" id="reportName">
                                  <option value="">--Select Clinic --</option>
                                  <?php foreach ($all_clinics as $key => $clinic) { ?>

                                    <option value="<?php echo $clinic->id ?>">
                                        <?php echo $clinic->clinic_name;  ?>
                                    </option>

                                    <?php } ?>
                              </select>
                          </div>

                          <div class="form-group">
                              <label for="ReportName"></label>
                              <select name="reportName" id="reportName" class="form-control">
                                  <option value="">-- Report Type --</option>
                                  <option value=""> Line </option>
                                  <option value=""> Bar</option>
                                  <option value=""> Summary</option>
                                  <option value=""> Pie chart</option>
                              </select>
                          </div>

                          <div class="form-group">
                              <label for="">Start Date</label>
                              <input type="date" class="form-control" name="start_date">
                          </div>

                          <div class="form-group">
                              <label for="">End Date</label>
                              <input type="date" class="form-control" name="end_date">
                          </div>

                          <div class="form-group">

                              <button class="btn btn-danger btn-block" type="button">Generate</button>
                          </div>
                      </form>

                  </div>

              </div>

              <div class="col-lg-9">

                  <div class="panel bet">

                  </div>

              </div>

          </div>




      </div>
      <!-- end col -->
  </div>
  