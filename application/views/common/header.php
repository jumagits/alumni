       <!DOCTYPE html>
       <html lang="en">
       <head>
           <meta charset="utf-8">
           <meta http-equiv="X-UA-Compatible" content="IE=edge">
           <meta name="viewport" content="width=device-width, initial-scale=1">
           <meta name="description" content="">
           <meta name="author" content="">
           <title>BUMAA </title>
            <link rel="icon" href="<?php echo URL ?>static/assets/img/favicon.png" type="image/x-icon" /> 
            <link href="<?php echo URL;?>static/admin/css/bootstrap.min.css" rel="stylesheet">
            <link href="<?php echo URL;?>static/admin/css/sb-admin.css" rel="stylesheet">
            <link href="<?php echo URL;?>static/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet"type="text/css">
            <link href="<?php echo URL ?>static/assets/css/style.css" rel="stylesheet">           
            <link href="<?php echo URL;?>static/admin/css/bootstrap-select.min.css" rel="stylesheet">
            <link href="<?php echo URL;?>static/admin/css/datepicker.css" rel="stylesheet">
            <link href="<?php echo URL;?>static/admin/css/select2.min.css" rel="stylesheet">
            <link href="<?php echo URL;?>static/admin/css/sweetalert2.min.css" rel="stylesheet">
            <link href="<?php echo URL;?>static/admin/css/jquery-te-1.4.0.css" rel="stylesheet">
            <link href="<?php echo URL;?>static/admin/js/switchery/dist/switchery.min.css" rel="stylesheet">
            <!-- datatable -->
            <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">

            <!-- skeleton -->

           <link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css">
           
            
           
            <script src="<?php echo URL;?>static/admin/js/jquery.min.js"></script>
            <script src="<?php echo URL; ?>static/admin/js/bootstrap.min.js"></script>
            <script src="<?php echo URL; ?>static/admin/js/canvasjs.min.js"></script>                   
            <script src="<?php echo URL; ?>static/admin/js/bootstrap-select.min.js"></script>
            <script src="<?php echo URL;?>static/admin/js/select2.min.js"></script>
            <script src="<?php echo URL; ?>static/admin/js/sweetalert2.min.js"></script>
            <script src="<?php echo URL; ?>static/admin/js/switchery/dist/switchery.min.js"></script>
            <script src="<?php echo URL; ?>static/admin/js/parsley.min.js"></script>
            <script src="<?php echo URL; ?>static/admin/js/jquery-te-1.4.0.min.js"></script>
            <script type="text/javascript">
            var APP_URL = "<?php echo URL; ?>";
            var fileList = "<?php if(isset($list)){echo $list;} ?>";
            </script>
            <script src="<?php echo URL;?>static/admin/js/action.js"></script>
            <script src="<?php echo URL;?>static/admin/js/forum.js"></script>
            <script src="<?php echo URL;?>static/admin/js/ajax-districts.js"></script>
            <!--Amcharts-->
            <script src="<?php echo URL; ?>static/admin/js/core.js" type="text/javascript"></script>
            <script src="<?php echo URL; ?>static/admin/js/charts.js" type="text/javascript"></script>
           <!--  datatable -->
          <!--   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>       -->    
            <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
           

           
            
            <script>
                jQuery(document).ready(function($) {

                  $('.table').DataTable( {
                       
                    });
                });
            </script>   
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
            <![endif]-->

       </head>
       <style>
        .wrapper, #page-wrapper {
        outline: none !important;       
        background:#f5f6fa !important;         
      } 
      .btn-danger{
      background-color: #3c4876 !important;border: none;outline: none; }
       </style>      
       <body>
           <div id="wrapper" >
               <nav class="navbar navbar-default navbar-fixed-top " style="background-color: #3c4876" role="navigation" style="">
                   <div class="navbar-header">
                       <button type="button" class="navbar-toggle" data-toggle="collapse"
                           data-target=".navbar-ex1-collapse">
                           <span class="sr-only">In</span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                           <span class="icon-bar"></span>
                       </button>
                       <a class="navbar-brand" href="<?php echo URL ?>adminEnd">BUMAA ADMIN</a>
                   </div>
                   <!-- Top Menu Items -->
                   <ul class="nav navbar-right top-nav">
                      
                       <li class="dropdown">
                           <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi, <?php echo !isset($user_info) ? header('URL'): $user_info['names'];?>
                               <b class="caret"></b></a>
                           <ul class="dropdown-menu">
                               <li> <a
                                       href="<?php echo URL; ?>adminEnd/profile/<?php echo base64_encode($user_info['id']); ?>"><i
                                           class="fa fa-fw fa-cogs"></i> Profile</a>
                               </li>
                               <li class="divider"></li>
                                <li>
                                   <a href="<?php echo URL; ?>" target="_blank"><i class="fa fa-home"></i> Back</a>
                               </li>
                                <li class="divider"></li>
                               <li>
                                   <a href="<?php echo URL; ?>accounts/pass-reset"><i
                                           class="fa fa-fw fa-lock"></i>Password</a>
                               </li>
                               <li class="divider"></li>
                               <li> <a href="<?php echo URL ?>register_clients/switchaccess"> <i class="fa fa-refresh"></i> Switch
                                   </a>
                               </li>
                                <li class="divider"></li>
                                <li><a href="<?php echo URL; ?>accounts/system_logs">
                                   <i class="fa fa-bars"></i>
                                Clinic History</a></li>
                                <li class="divider"></li>
                                <li><a href="<?php echo URL; ?>adminEnd/accessLogs">
                                   <i class="fa fa-list"></i>
                                Switch Access Logs</a></li>
                               <li class="divider"></li>
                               <li> <a href="<?php echo URL ?>accounts/logout"><i class="fa fa-fw fa-power-off"></i> Log
                                       Out</a>
                               </li>
                           </ul>
                       </li>
                   </ul>


                   <div class="collapse navbar-collapse navbar-ex1-collapse" >
                       <ul class="nav navbar-nav side-nav " style="background-color: #212e5a !important;" >
                           <li class="text-center " style="background-color: #6d1458;">
                               <a href="<?php echo URL ?>adminEnd/admin" style="color: #fff;"><i class="fa fa-user"></i>
                                   <?php echo !isset($user_info) ? header('URL'): $user_info['names'];?> -
                                   From <span
                                       class="badge badge-danger"><?php echo !isset($user_info) ? header('URL'): $user_info['default_clinic_name'];?></span>
                                   Campus But Logged in As <span
                                       class="badge badge-primary"><?php echo !isset($user_info) ? header('URL'): $user_info['default_role_name'];?></span>
                                   From
                                   <?php echo !isset($user_info) ? header('URL'): $user_info['clinic_name'];?> Campus.
                               </a>
                           </li>
                           <li>
                               <a href="<?php echo URL ?>adminEnd/admin"><i class="fa fa-fw fa-dashboard "></i> Dashboard</a>
                           </li>
                            <li>
                               <a href="javascript:;" data-toggle="collapse" data-target="#dmo"><i class="fa fa-fw fa-users"></i> Alumni Management<i
                                       class="fa fa-fw fa-angle-double-down"></i></a>
                               <ul id="dmo" class="collapse">
                                   <li><a href="<?php echo URL ?>register_clients/main"><i class="fa fa-fw fa-angle-double-down"></i> Add Alumni</a>
                                   </li>
                                  
                                   <li><a href="<?php echo URL ?>register_clients/companies"><i class="fa fa-fw fa-angle-double-down"></i> View Companies</a>
                                   </li>
                                   <li><a href="<?php echo URL ?>register_clients/register_company"><i class="fa fa-fw fa-angle-double-down"></i> Register Company</a>
                                  </li>                                   
                                   <li>
                                       <a href="<?php echo URL ?>register_clients/members"><i class="fa fa-fw fa-angle-double-down"></i> View Alumni</a>
                                   </li>

                               </ul>
                           </li>
                           <li>
                             <a href="javascript:;" data-toggle="collapse" data-target="#dxo"><i
                                       class="fa fa-fw fa-cogs"></i> Categories Mgt<i
                                       class="fa fa-fw fa-angle-double-down"></i></a>
                              <ul id="dxo" class="collapse">
                                <li><a href="<?php echo URL; ?>essentials/userLevel">
                                  <i class="fa fa-fw fa-angle-double-down"></i>
                                User Roles</a></li>
                                <li><a href="<?php echo URL; ?>essentials/clinics">
                                  <i class="fa fa-fw fa-angle-double-down"></i>
                                Bumaa Clinics</a></li>
                                <li> <a href="<?php echo URL ?>adminEnd/newsletter"><i class="fa fa-fw fa-angle-double-down"></i>
                                         Newsletter</a>
                                 </li>


                                 <li> <a href="<?php echo URL ?>adminEnd/contacts"><i class="fa fa-fw fa-angle-double-down"></i>
                                         Contacts</a>
                                 </li>
                                 
                                 <li> <a href="<?php echo URL ?>adminEnd/site_settings"><i class="fa fa-fw fa-angle-double-down"></i> All Settings</a>
                                 </li>

                                 <li>
                                      <a href="<?php echo URL ?>forum"><i class="fa fa-fw fa-angle-double-down"></i>
                                           Add Topic</a>
                                   </li>
                               
                              </ul>
                           </li>
                           <li>
                               <a href="javascript:;" data-toggle="collapse" data-target="#demohh"><i
                                       class="fa fa-fw fa-image"></i> News Management<i
                                       class="fa fa-fw fa-angle-double-down"></i></i></a>
                               <ul id="demohh" class="collapse">
                                   <li>
                                       <a href="<?php echo URL ?>event"><i class="fa fa-fw fa-angle-double-down"></i>
                                           All Events</a>
                                   </li>
                                   <li>
                                       <a href="<?php echo URL ?>category"><i class="fa fa-fw fa-angle-double-down"></i> News    Categories</a>
                                   </li>
                                   <li>
                                       <a href="<?php echo URL ?>post"><i class="fa fa-fw fa-angle-double-down"></i>
                                           All News</a>
                                   </li>
                                   <li>
                                       <a href="<?php echo URL ?>post/comments"><i class="fa fa-angle-double-down"></i>
                                           All Comments</a>
                                   </li>
                                   <li>
                                       <a href="<?php echo URL ?>post/subscribers"><i
                                               class="fa fa-angle-double-down"></i> All
                                           Subscribers</a>
                                   </li>
                                   <li>
                                       <a href="<?php echo URL ?>post/add-post"><i class="fa fa-angle-double-down"></i>
                                           Add Post</a>
                                   </li>
                               </ul>
                           </li>
                           <li>
                               <a href="<?php echo URL ?>repository"><i class="fa  fa-folder-open"></i> Bumaa Documents</a>
                           </li>
                           <li>
                               <a href="<?php echo URL ?>e_share"><i class="fa fa-fw fa-share"></i>Share Files</a>
                           </li>
                           <li><a href="javascript:;" data-toggle="collapse" data-target="#demod"><i class="fa fa-fw fa-briefcase"></i> Jobs Management<i class="fa fa-fw fa-angle-double-down"></i></a>
                               <ul id="demod" class="collapse">
                                   <li><a href="<?php echo URL ?>job/index"><i
                                               class="fa fa-fw fa-angle-double-down"></i> All Jobs</a>
                                   </li>
                                    <li><a href="<?php echo URL ?>course"><i class="fa fa-fw fa-angle-double-down"></i>
                                           All Courses</a>
                                   </li>
                                   <li><a href="<?php echo URL ?>job/fields"><i
                                               class="fa fa-fw fa-angle-double-down"></i> Fields</a>
                                   </li>
                               </ul>
                           </li>
                           <li> <a href="<?php echo URL ?>reports"><i class="fa fa-fw fa-bar-chart"></i> Reports</a>
                           </li>

                           <li><a href="<?php echo URL ?>accounts/logout"><i class="fa fa-fw fa-power-off"></i> Log
                                   Out</a>
                           </li>
                       </ul>
                   </div>
               </nav>

            <style>

            .fa, .fas,.side-nav a {color: #fff !important;}
            input,select,textarea{
             border: 1px solid #e9e8ef;
            font-weight: 400;
            font-size: 0.875rem; }.bord{
                border: 2px solid #3c4876;
                padding: 5px;
            }
            td {
                font-size: 16px;
                padding: 10px 9px !important;
                font-weight: bolder;
            }
            </style>
           