
<div class="modal fade none-border" id="createTopic" data-backdrop="false">
<div class="modal-dialog modal-dialog-vertical-center">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><strong>Create</strong> New Topic</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
            <form id="addTopic">
                <div class="form-group">
                    <label>Topic Title</label>
                    <input type="text" class="form-control" name="post_title" required>
                </div>


                <div class="form-group">
                    <label>Category</label>
                    <select name="cat_id" class="form-control">
                        <option selected="" disabled=""> -- Select Category --</option>
                        <?php echo isset($catOptions) ? $catOptions :'' ?>
                    </select>
                </div>

                <div class="form-group">
                    <input type="hidden" name="created_by" value="<?php echo  isset($user_info) ? $user_info['id'] : ""; ?>">
                    <label>Content</label>
                    <textarea name="post_content" class="form-control">
                    </textarea>
                </div>
            </div>


        <div class="modal-footer bg-light">
            <button type="submit" class="btn btn-danger waves-effect waves-light save-category">Save</button>
            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
        </div>
        </form>
    </div>
</div>
</div>
<!-- END MODAL -->

<!-- Modal Add Topic -->
<div class="modal fade none-border" id="createCategory" data-backdrop="false">
<div class="modal-dialog modal-dialog-vertical-center">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title"><strong>Create</strong> New Category</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
            <form id="addCategory">
                <div class="form-group">
                    <label>Category Title</label>
                    <input type="text" class="form-control" name="category_title" required>
                </div>
        </div>

        <div class="modal-footer bg-light">
            <button type="submit" class="btn btn-danger waves-effect waves-light save-category">Save</button>
            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
        </div>
        </form>
    </div>
</div>
</div>
 <script>

jQuery(document).ready(function($) {
         $("#search-input").on('keyup', function() {
             var name = $('#search-input').val();
             if (name == "") {
                 $("#display").html("");
                 $(".search_table").html("");
             }
             else {
                 var action = $('form#searchclientform').attr('action');
                 $.ajax({
                     type: "POST",
                     url: action,
                     data: {
                         search: name
                     },
                     success: function(html) {
                         console.log(html);
                         var table_view = `<div class="table-responsive">
                            <table class="table  table-bordered table-striped table-hover mb-0">
                                <thead bgcolor="#F5B225">
                                     <tr>
                                        <th scope="col">Client Name</th>
                                        <th scope="col">Contact Number</th>
                                        <th scope="col">Registered On</th>
                                        <th scope="col">View </th>
                                    </tr>
                                </thead>
                                <tbody id="display">
                                </tbody>
                            </table>
                        </div>`;
                         $(".search_table").html(table_view).show();
                         $("#display").html(html).show();
                     }

                 });
             }
         });
         
//bar chart

    const barData = <?php echo ((!empty($DataPointsForMembers) AND isset($DataPointsForMembers)) ?  $DataPointsForMembers : "4"); ?>;
    var barchart = new CanvasJS.Chart("bargraph",
        {
            title:{
                text: "Registered Alumni Per Campus"
            },
            data: [
                {
                    type: "bar",
                     dataPoints: barData                    
                }
            ]
        });
    
        barchart.render();

});
    $(document).ready(function($) {
        $("form#testimony").submit(function(e) {
            e.preventDefault();
            var formData = new FormData(this);
            var action = $('form#testimony').attr('action');
            $.ajax({
                    url: action,
                    type: 'POST',
                    data: formData,
                    success: function(res) {
                        $('.fb').html('<p class="text-success">' + res.msg + '</p>')
                        setInterval(function() {
                            window.location.reload();
                        }, 2000);
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
                })
           });
    });

    </script>
 </body>
 </html>
