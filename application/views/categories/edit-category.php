<div id="page-wrapper">
<div class="container-fluid">                
<div class="panel">
    <div class="panel-heading bg-primary">
        <h4> Edit Category <a href="<?php echo URL ?>category" class="pull-right btn btn-danger">Cancel</a></h4>
    </div>
       <div class="panel-body " >
           <div class="row">
              <div class="col-lg-12">
                <form action="<?php echo URL ?>category/update" id="updateC"><input type="hidden" name="cat_id" value="<?php echo $category[0]->cat_id; ?>">
                <div class="form-group ">
                <label for="" class="control-label">Category</label>
                <input type="text" class="form-control" name="cat_title"   required value="<?php echo $category[0]->cat_title; ?>">
                </div>                 
                <div class="form-group"> 
                   <button type="submit" class="btn btn-primary btn-md btn-block ">Submit</button>
                </div>
            </form>         
      </div>
</div>
</div>
</div>
</div>
</div>

<script>
    
    jQuery(document).ready(function($) {        
         $("form#updateC").submit(function(e) {
         e.preventDefault();       
         var formData = new FormData(this);
         var action = $('form#updateC').attr('action');       
               $.ajax({
                   url: action,
                   type: 'POST',                  
                   data: formData,
                   success:function(res){
                  swal({
                       title: "Good job!",
                       text: res.msg,
                       type: "success",
                       showCancelButton: !0,
                       confirmButtonClass: "btn btn-success",
                       cancelButtonClass: "btn btn-danger m-l-10"
                     })

                  setInterval(function(){

                    window.location.href = '<?php echo URL; ?>category';

                  },4000);

                  
                   },
                  error: function(jqXHR, textStatus, errorThrown) {
                         console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
               })
               .done(function() {
                   console.log("success");
               })
          });

    });
</script>