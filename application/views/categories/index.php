  <div id="page-wrapper">
      <div class="container-fluid">
          <div class="panel">
            <h4> POST CATEGORIES <a href="" data-toggle="modal" data-target=".bs-edit-modal-lg" class="pull-right btn btn-danger"><i class="fa fa-plus"></i> Add Category</a>
                  </h4><hr>
              <div class="panel-body ">
                 <div class="row">
                  <div class="col-lg-12">
                  <div class="table-responsive ">
                      <table class="table table-hover table-bordered table-striped" id="datatable1" width="100%">
                          <thead>
                              <tr>
                                  <th>No.</th>
                                  <th>Category Name</th>
                                  <th>Edit</th>
                                  <th>Delete</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php $count = 0;
                               foreach ($all_cats as $key => $cat) {  $count++;?>
                              <tr>
                                  <td><?php echo $count; ?></td>
                                  <td><?php echo $cat->cat_title; ?></td>
                                  <td><a class="btn btn-info " href="<?php echo URL ?>category/show/<?php echo base64_encode($cat->cat_id); ?>">Edit</a></td>
                                  <td><a class="btn btn-danger " onclick="deleteCat(<?php echo $cat->cat_id; ?>)">Delete</a>
                                  </td>
                              </tr>

                              <?php } ?>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
 

  <?php 

function dateF($date){
   return date_format(date_create($date), ' l jS F Y'); 
}

 ?>


  <!-- end of page -->











  <!--  Modal Prison for the above example -->

  <!-- /.modal -->

  <!--  Modal Prison for the above example -->
  <div class="modal fade bs-edit-modal-lg " role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"
      data-backdrop="false">
      <div class="modal-dialog modal-md">
          <div class="modal-content">
              <div class="modal-header bg-primary">
                  <h4 class="modal-title " id="myLargeModalLabel">Register New Category
                      <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
                  </h4>
              </div>
              <div class="modal-body">


                  <form action="<?php echo URL ?>category/add-category" id="add-cat">

                      <div class="form-group row">
                          <div class="col-md-12">
                              <label for="" class="control-label">Category Name</label>
                              <input type="text" class="form-control" placeholder="Enter Category Name" name="cat_title"
                                  required>
                          </div>
                      </div>

                      <div class="row">
                          <div class="col-md-12">
                              <button class="btn btn-lg btn-block btn-primary " type="submit"> Add Category</button>
                          </div>
                      </div>
                  </form>
              </div>
          </div>
          <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->


      <script>
            

            jQuery(document).ready(function($) {
                
                deleteCat = function ($id){

                    if(confirm('Are you sure to Delete this Category')){

                    var id = Number($id);

                    $.ajax({
                        url: APP_URL+"category/delete/",
                        type: 'GET',                        
                        dataType: 'json',                     
                        data: {id: id},
                        success:function(res){

                             swal({
                                   title: "Good job!",
                                   text: res.msg,
                                   type: "success",
                                   showCancelButton: !0,
                                   confirmButtonClass: "btn btn-success",
                                   cancelButtonClass: "btn btn-danger m-l-10"
                              });

                             
                            setTimeout(function() {
                                window.location.reload();
                           }, 1000);

                        }
                    });

                   }

                }
            });
        </script>