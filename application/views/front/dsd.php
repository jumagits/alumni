<!DOCTYPE html>
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="BUMAA">
    <meta name="description" content="BUMAA">
    <meta name="author" content="MUKOOVA JUMA">
    <title>BUMAA</title>
    <link rel="shortcut icon" href="<?php echo URL ?>static/assets/img/favicon.png" type="image/x-icon">
    <link rel="apple-touch-icon" type="image/x-icon" href="img/apple-touch-icon-57x57-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png">
    <link href="<?php echo URL;?>static/admin/font-awesome/css/font-awesome.min.css" rel="stylesheet"type="text/css">
    <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png">
    <link href="<?php echo URL ?>static/assets/css/bootstrap.min.css" rel="stylesheet">    
    <link href="<?php echo URL ?>static/assets/css/style.css" rel="stylesheet">
    <link href="<?php echo URL ?>static/assets/css/base.css" rel="stylesheet">
  
    <script src="<?php echo URL ?>static/assets/js/jquery.min.js"></script>  
    <script src="<?php echo URL ?>static/assets/js/common_scripts_min.js"></script>
    <script src="<?php echo URL ?>static/assets/js/functions.js"></script>
   
    <script src="<?php echo URL; ?>static/admin/js/canvasjs.min.js"></script> 


  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&family=Merriweather+Sans:ital,wght@0,300;0,400;0,500;0,600;0,800;1,400&display=swap" rel="stylesheet">   
  <link href="https://fonts.googleapis.com/css2?family=Lato:wght@100;300;400;700;900&display=swap" rel="stylesheet">

   

    <!--[if lt IE 9]>
      <script src="<?php echo URL ?>static/assets/js/html5shiv.min.js"></script>
      <script src="<?php echo URL ?>static/assets/js/respond.min.js"></script>
    <![endif]-->

</head>



<body>
<nav class="navbar navbar-expand-lg .navbar-inverse  " style="border-bottom:3px solid #1e73be;background-color: #fffdd0 !important;" >
 
  <a  class="navbar-brand" href="<?php echo URL ?>">
    <img src="<?php echo URL ?>static/assets/img/log.png" alt="logo" style="width:200px;">
  </a>

  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon">M</span>
  </button>

  <div class="collapse navbar-collapse container" id="navbarSupportedContent" >
    <ul class="navbar-nav m-auto ">
      
         <li class="nav-item active"><a class="nav-link" href="<?php echo URL; ?>/">Home </a></li>
                    
            <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>/application/about">About </a></li>

            <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>frontend/blog">News </a></li>

            <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/job">Jobs </a></li>

            <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/events">Events </a></li>

            <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/alumni">Alumni </a></li>

            <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/contact">Contact </a></li>

            <li class="nav-item"> 
              <a class='nav-link' href="<?php echo URL; ?>application/profile/<?php echo base64_encode($user_info['id']); ?>"><i class=" fa fa-user"></i>
                    Profile</a>
            </li>        

             <li class="nav-item"> 
              <a class='nav-link' href="<?php echo URL ?>accounts/logout"  ><i class=" icon-power"></i><span class="badge badge-danger">Logout</span></a>
            </li>
      
    </ul>
   
  </div>
</nav>






 <!-- End Header --> 
<!-- 76963971 coding faculty id-->
