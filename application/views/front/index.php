    <div class="container-fluid "
        style="background: url(<?php echo URL ?>static/assets/img/8.png);  background-position: 5px 50px !important;background-size: cover;height: auto !important">
        <div class="row">
            <div class="col-lg-7 col-md-7 col-sm-12">
                <div class="d-block m-5" style="margin: 60px !important;">
                    <div class="text-left " style="padding-top: 80px;">
                        <div class="mt-4">
                            <h1
                                style="color: #fff !important;font-size: 42px;line-height: 52px;font-weight: 700;font-style: normal;">
                                Welcome to (BUMAA),<br>
                                <span style="color: #d7c002;">Busitema University Muslim Alumni Association</span>
                            </h1>
                        </div>

                        <h5 class="" style="color: #fff !important;line-height: 30px;font-size: 25px;">
                            Our Main Objectives are Uniting Alumni Members,Finding Jobs and Supporting the Association
                            through Charity.
                        </h5>
                        <br>

                        <p class="d-block mt-2">
                            <button class="btn btn-danger"><b>
                                    <a href="<?php echo URL ?>application/about"> Learn More About Us!</a>
                                </b></button>
                        </p>
                        <br>


                    </div>
                </div>


            </div>

            <div class="col-lg-5">

                <div class="d-block " style="padding: 60px !important;">


                    <div class="panel text-left"
                        style="margin-top: 60px;padding: 20px;background: url(<?php echo URL ?>static/assets/img/ss.jpg);">
                        <label>Your Name</label>
                        <input name="fullname" type="text" required class="form-control valid-character">



                        <label>Your Email Address</label>
                        <input name="email" type="email" class="form-control" required>




                        <label>Subject</label>
                        <input name="subject" type="text" required class="form-control">



                        <label>Type Message</label>
                        <textarea name="message" cols="10" rows="2" class="form-control" required></textarea><br>



                        <button name="submit" type="submit" value="Submit" class="btn btn-block btn-sm button"> Send
                            Message</button>
                    </div>



                </div>
            </div>

        </div>

    </div>

    <div class="" style="background-color: #81b634;height: 10px;"></div>


    <!--   New -->

    <div class="container-fluid" style="background: url(<?php echo URL ?>static/assets/img/ss.jpg);padding: 40px;">

        <div class="row">

            <div class="col-lg-6">

                <div class="panel" style="height: 300px;width: 100%;">
                    <img src="<?php echo URL ?>static/assets/img/join.png" alt="Unity" height="100%" width="100%;">
                </div>


            </div>

            <div class="col-lg-6 ">

                <h3>During Charity in Ndaiga</h3>
                <p>On this platform, We will be Posting News of the current,upcoming occasions, Functions and Other
                    happenings coming up in the association.</p>
                <p>On this platform, We will be Posting News of the current,upcoming occasions, Functions and Other
                    happenings coming up in the association.</p>
                <p>On this platform, We will be Posting News of the current,upcoming occasions, Functions and Other
                    happenings coming up in the association.</p>

            </div>
        </div><!-- End row -->

    </div><!-- End container -->




    <!-- End container_gray_bg -->


    <div class="" style="background-color: #81b634;height: 10px;"></div>

    <div class="container-fluid" style="background: url(<?php echo URL ?>static/assets/img/ss.jpg);padding: 20px;">
        <div class="main_title">
            <h2>BUMAA OBJECTIVES</h2>

        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="box_feat_home">
                    <i class=" icon-users"></i>
                    <h3>ALUMNI UNITY</h3>
                    <p>As the Alumni association, we want to ensure that all BUMSA students are connected, United and
                        reachable whenever they are.</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="box_feat_home">
                    <i class=" icon-briefcase"></i>
                    <h3>EMPLOYMENT OPPORTUNITIES</h3>
                    <p>As BUMAA, we want to ensure that all the BUMSA alumni are employed by posting job links for the
                        different field studies and Expertise.</p>
                </div>
            </div>
        </div><!-- End row -->
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="box_feat_home">
                    <i class=" icon-calendar"></i>
                    <h3>BUMAA EVENTS</h3>
                    <p>On this platform, We will be Posting updates of the upcoming Events, Functions and Other
                        Cerebrations coming up in the association.</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="box_feat_home">
                    <i class=" icon-list"></i>
                    <h3>BUMAA NEWS</h3>
                    <p>On this platform, We will be Posting News of the current,upcoming occasions, Functions and Other
                        happenings coming up in the association.</p>
                </div>
            </div>
        </div><!-- End row -->
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="box_feat_home">
                    <i class=" icon-share"></i>
                    <h3>ALUMNI SHARE</h3>
                    <p>In this docket, Members shall be sharing important information using emails, for easy reach and
                        access.</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="box_feat_home">
                    <i class=" icon-money"></i>
                    <h3>CONTRIBUTION AND PROJECTS</h3>
                    <p>In this docket, we shall making simple contributions to come up with projects that can help the
                        community, such as constructing boreholes, Buying health facilities in Hospitals, and many
                        others.</p>
                </div>
            </div>
        </div><!-- End row -->


    </div><!-- End container -->





    <!--   end container -->

    <div class="bg_content testimonials" style="background:#004d63;">

        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <h1 class="text-center " style="color: #fff">WHAT PEOPLE SAY</h1>
                <hr>
                <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                    <!-- Bottom Carousel Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#quote-carousel" data-slide-to="1"></li>
                        <li data-target="#quote-carousel" data-slide-to="2"></li>
                    </ol><!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner">

                        <?php



                             foreach ($tests as $key => $t) { ?>
                        <div class="item  <?php if($t->id == 1){echo "active";} ?> ">
                            <blockquote style="color:orange;">
                                <p>
                                    <?php echo($t->testimony); ?>
                                </p>
                            </blockquote>
                            <small><img class="img-circle" src="<?php echo URL ?>static/assets/img/2.jpg"
                                    alt=""><?php echo $catsModel->get_username_today($t->user_id); ?></small>
                        </div>

                        <?php } ?>

                    </div>
                </div>
            </div>
        </div><!-- End row -->
    </div><!-- End bg_content -->

    <div class="container-fluid" style="background: url(<?php echo URL ?>static/assets/img/ss.jpg);padding-top: 70px">
        <div class="main_title">
            <h2>RECENTLY REGISTERED</h2>
            <p>This Section shows the Recently Registered Alumni, Companies</p>
        </div>


        <div class="row ">

            <div class="col-md-6 col-sm-12">

                <div class="panel panel-primary">

                    <div class="panel-heading">
                        CURRENT REGISTERED ALUMNI
                    </div>

                    <ul class="list-group list-group-flush">

                        <?php 

                                $count = 0;
                                  function dateFF($date){
                                       return date_format(date_create($date), '  jS F Y'); 
                                    }


                                foreach ($current_clients as $key => $client) { $count++; ?>

                        <li class="list-group-item">

                            <h5 class="">
                                <strong><b><?php echo $client->fname; ?></b></strong>
                                <?php echo $client->lname; ?> <i class="icon-user"></i> Registered On:
                                <span><?php echo dateFF($client->created_on); ?></span></a>
                            </h5>


                        </li>


                        <?php } ?>

                    </ul>




                </div>


            </div>

            <div class="col-md-6 col-sm-12">


                <div class="panel ">

                    <div class="panel-heading">

                        CURRENT REGISTERED COMPANIES

                    </div>


                    <ul class="list-group list-group-flush">



                        <?php 

                                $count = 0;
                               


                                foreach ($current_company as $key => $client) { $count++; ?>

                        <li class="list-group-item">

                            <h5 class="">
                                <strong><b><?php echo $client->companyName; ?></b></strong>
                                <?php echo $client->companyLocation; ?> <i class="icon-pen"></i>
                                Registered On:
                                <span><?php echo dateFF($client->created_on); ?></span></a>
                            </h5>

                        </li>



                        <?php } ?>

                    </ul>

                    </table>

                </div>


            </div>
        </div>
        </section>


    </div><!-- /content -->
    </div><!-- End tabs -->
    </div>