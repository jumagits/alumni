 <div class="sub_header med">
            <div id="intro_txt">
                <h1><strong><?php echo isset($pageTitle) ? $pageTitle: ""; ?></strong> </h1>
                <p>
                    Find Jobs according to your Expertise.
                </p>
            </div>
        </div><!--End sub_header -->
        
            <div class="container-fluid">
               <div style="margin-top: 20px;">
                 <div class="row">
                    <div class="col-md-12">

                        <div class="panel">
                            <div class="panel-heading">
                                POSTED JOBS
                            </div>

                            <div class="panel-body">
                                   <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="datatable1">

                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>JOB TITLE</th>
                                                    <th>CATEGORY</th>
                                                    <th>  ACTIONS   </th>
                                                    <th> COMPANY  </th>
                                                    <th>POSTED BY</th>
                                                </tr>
                                            </thead>

                                            <tbody>

                                <?php 

                                    if($all_jobs):

                                        $count = 0;
                                        
                                    foreach ($all_jobs as $key => $job) {   $count++;?>
                                
                                    <tr>
                                       <th><?php  echo $count; ?></th>                                        
                                        <td><a href="#"><?php echo strtoupper($job->job_title); ?></a>
                                        </td>
                                        <td class="text-muted"><?php echo strtoupper($job->field); ?>
                                        </td>                                            
                                        <td>
                                           <a href="<?php echo URL ?>application/job-details/<?php echo base64_encode($job->id) ?>" class="btn btn-sm btn-info">More Info</a>
                                            <a target="_blank" href="<?php echo $job->job_link;  ?>" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> Go Apply</a>
                                           
                                        </td>
                                        <td>
                                             <span>
                                                    <?php echo strtoupper($job->company);  ?></span>
                                        </td>                                             
                                        
                                        <td> <span>
                                            <?php echo !empty($job->user_id)? $accountsModel->get_client_details($job->user_id) : 'N/A'; ?></span>
                                        </td>                                    
                                    </div>
                                </tr>
                                

                            <?php
                             }

                            endif;

                             ?>
                         </tbody>
                        </table>
                            </div>
                        </div>

                        
                    </div>
                        
                    </div><!--End col-md-9 -->
                    
                  
                    
                </div><!--End row -->
            </div><!--End container -->
        </div><!--End container_gray_bg -->