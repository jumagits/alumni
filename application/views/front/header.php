<!doctype html>
<html lang="en-gb">
<!--<![endif]-->
<head>
<title>BUMAA - BUSITEMA UNIVERSITY MUSLIM ALUMNI ASSOCIATION</title>
<meta charset="utf-8">
<!-- Meta -->
<meta name="keywords" content="" />
<meta name="author" content="">
<meta name="robots" content="" />
<meta name="description" content="" />

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicon -->
<link rel="shortcut icon" href="<?php echo URL ?>static/assets2/images/favicon.png">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

<!-- ######### CSS STYLES ######### -->
<!-- <link href="<?php echo URL ?>static/assets/css/bootstrap.min.css" rel="stylesheet">    
 -->
<link rel="stylesheet" href="<?php echo URL ?>static/assets2/css/reset.css" type="text/css" />
<link rel="stylesheet" href="<?php echo URL ?>static/assets2/css/style.css" type="text/css" />
<link rel="stylesheet" href="<?php echo URL ?>static/assets2/css/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="<?php echo URL ?>static/assets2/js/form/css/sky-forms.css" type="text/css" media="all">


<!-- responsive devices styles -->
<link rel="stylesheet" media="screen" href="<?php echo URL ?>static/assets2/css/responsive-leyouts.css" type="text/css" />

<!-- mega menu -->
<link href="<?php echo URL ?>static/assets2/js/mainmenu/sticky.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets2/js/mainmenu/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets2/js/mainmenu/demo.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets2/js/mainmenu/menu.css" rel="stylesheet">

<!-- revolution slider -->

<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>static/assets2/js/revolutionslider/css/extralayers.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/css/settings.css" media="screen" />




<!-- Owl Carousel Assets -->
<link href="<?php echo URL ?>static/assets2/js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets2/js/owl-carousel/owl.theme.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets2/js/owl-carousel/owl.transitions.css" rel="stylesheet">

<!-- simple line icons -->
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>static/assets2/css/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />

<!-- Accordion styles -->
<link rel="stylesheet" href="<?php echo URL ?>static/assets2/js/accordion/smk-accordion.css" />


<link href="<?php echo URL ?>static/assets/css/style.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets/css/base.css" rel="stylesheet">

<!-- skeleton -->

<link rel="stylesheet" href="https://unpkg.com/placeholder-loading/dist/css/placeholder-loading.min.css">

<!-- Remove the below comments to use your color option -->
<!--<link rel="stylesheet" href="css/colors/red.css" />-->
<!--<link rel="stylesheet" href="css/colors/green.css" />-->
<!--<link rel="stylesheet" href="css/colors/pink.css" />-->
<!--<link rel="stylesheet" href="css/colors/orange.css" />-->
<!--<link rel="stylesheet" href="css/colors/liteblue.css" />-->
<!--<link rel="stylesheet" href="css/colors/purple.css" />-->
<!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
<!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
<!--<link rel="stylesheet" href="css/colors/darkred.css" />-->

<!-- just remove the below comments witch bg patterns you want to use -->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-ten.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-eleven.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-twelve.css" />-->



</head>

<style>
 body{
  background-color: #edf0f2 !important;
 }
</style>

<body>
<div class="site_wrapper">
  <header id="header"> 
    
    <!-- Top header bar -->
    <div id="topHeader ">
      <div class="wrapper">
        <div class="top_nav one" style="background-color: #edf0f2  !important;border-bottom: 2px solid #3a3d3e;color:#000 !important;">
          <div class="container">
            <ul>
              <li class="line"><a href="<?php echo URL ?>accounts/login"> <?php echo isset($user_info) ? '<b>'.$user_info['names'].'</b>' :"Login"; ?></a></li>
              <li class="line"><a href="<?php echo URL ?>accounts/alumni_register"> Alumni Registration</a></li>
              <li class="line"><a href="<?php echo URL ?>accounts/alumni_register"> Well-wisher Registration</a></li>
              <li><a target="_blank" href="https://www.facebook.com/codelayers"><i class="fa fa-facebook"></i></a></li>
              <li><a target="_blank" href="https://twitter.com/codelayers"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li class="last"><a href="#"><i class="fa fa-linkedin"></i></a></li>
            </ul>
            <ul class="left">
              <li class="line"> BUMAA</li>
              <li> (+256) 702499649</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- end top navigation -->
    
    <div class="scrollto_sticky"  style="background-color:#000 !important;">
      <div class="container"> 
        
        <!-- Logo -->
        <div class="logo"><a href="<?php echo URL; ?>" id="logo"></a></div>
        
        <!-- Menu -->
        <div class="menu_main">
          <div class="navbar yamm navbar-default"  style="background-color:#000 !important;">
            <div class="container-fluid ">
              <div class="navbar-header">
                <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  ><span></span>
                  <button type="button"> <i class="fa fa-bars"></i></button>
                </div>
              </div>
              <div id="navbar-collapse-1" class="navbar-collapse collapse ">
                <nav>
                  <ul class="nav navbar-nav pull-right">

                  <li class="nav-item active"><a class="nav-link" href="<?php echo URL; ?>/">Home </a></li>
                    
                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>/application/about">About </a></li>

                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>frontend/blog">News </a></li>

                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/job">Jobs </a></li>

                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/events">Events </a></li>

                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/alumni">Alumni </a></li>

                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/contact">Contact </a></li>     
                  
                    <li class="dropdown"> <a href="#" class="dropdown-toggle">  Account</a>
                      <ul class="dropdown-menu" role="menu">
                      
                        <li><a href="<?php echo URL; ?>application/profile"><i class=" fa fa-user"></i> Profile</a> </li>

                        <li><a href="<?php echo URL ?>accounts/logout"><i class="fa fa-power-off"></i> Logout</a> </li>
                      </ul>
                    </li>
                    
                  
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <!-- end menu --> 
        
      </div>
    </div>
  </header>
  <div class="clearfix"></div>
  <?php if(isset($page) AND $page == 'home'){
  ?><?php }else{ ?>

  
   <div class="header_medium two">
    <div class="container">
      <h2 class="align_center white uppercase weight6"> <?php echo isset($pageTitle)? $pageTitle: "BUMAA"; ?></h2>
      <h3 class="uppercase white"> <?php echo isset($pageSlug)? $pageSlug: "UNITING BUMSA ALUMNI GLOBALLY"; ?></h3>
    </div>
  </div>

<?php } ?>


  <!--  <div class="section_holder57"> -->