 <div class="clearfix"></div>

    <div class="footer" style="background-color:#d8d8d8 !important;padding-bottom: 40px !important;">
    <div class="container">
      <div class="one_third">
        <h4> <strong>Address</strong></h4>
        <div class="footer_logo"><img src="<?php echo URL ?>static/assets2/images/logo.png" alt="logo"/></div>
        <br>
        <span class="address"><strong class="">
          Address: No.28 - Kampala Mukadde, Uganda
        </strong></span> <span class="address white">Phone: +(256) 702499649</span> <span class="address white">Fax: +(256) 783975685</span> <span class="address">Email: info@bumaa.com </span> </div>
      <!--end item-->
      
      <div class="one_third">
        <h4> <strong>Newsletter</strong></h4>
        <div class="divider_line_dashed2"></div>   
          
          <form action="#" class="dede">
            <p class="bold">Subscribe for the latest updates</p>
            <br>
            <div class="form-group">
              <label for="#">Email</label>
              <input type="email" name="email" id="" class="form-control form-control-lg">
            </div>
            <div class="form-group">
              <button type="submit" class=" btn btn-block small btn-success two green" name="subscribe"><strong>Subscribe</strong></button>
            </div>
          </form>
        
       
        
      </div>
      <!--end item-->
      
     
      <!--end item-->
      
      <div class="one_third last">
       
       <h4> <strong>Quick Links</strong></h4>
      
        <div class="divider_line_dashed2"></div>  
        <ul class="nav">
          <li class="nav-item"><a href="#">About Us</a></li>
          <li class="nav-item"><a href="">Events</a></li>
          <li class="nav-item"><a href="#">Alumni</a></li>
          <li class="nav-item"><a href="#">Contact us</a></li>
          
        </ul>
      </div>
      <!--end item--> 
      
    </div>
  </div>
  <style>.nav li a{color:#333}</style>
  <!--end footer-->
<!--   background: #4565a6;
border: 1px solid #a7bde8; -->
  <div class="copyrights" style="background: #040404;
border-top: 1px solid #222;">
    <div class="container">
      <div class="one_half"><span>Copyright © 2020 - <?php echo date('Y'); ?> ~ BUMAA. All Rights Reserved | All rights reserved.</span></div>
      <div class="one_half last">
        <ul class="social_icons">
          <li><a target="_blank" href="https://www.facebook.com/codelayers"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a href="#"><i class="fa fa-wordpress"></i></a></li>
          <li><a href="#"><i class="fa fa-android"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
  <!--end copyrights--> 
  <a href="#" class="scrollup"></a><!-- end scroll to top of the page--> 
  
</div>
<!--end sitewraper--> 



<!-- ######### JS FILES ######### --> 
<!-- get jQuery from the google apis --> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/universal/jquery.js"></script> 

<!-- style switcher --> 
<script src="<?php echo URL ?>static/assets2/js/style-switcher/jquery-1.js"></script> 
<script src="<?php echo URL ?>static/assets2/js/style-switcher/styleselector.js"></script> 

<!-- scroll to fixied sticky --> 
<script src="<?php echo URL ?>static/assets2/js/mainmenu/jquery-scrolltofixed.js" type="text/javascript"></script> 
<script src="<?php echo URL ?>static/assets2/js/mainmenu/ScrollToFixed_custom.js" type="text/javascript"></script> 

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/custom1.js"></script> 

<!-- mega menu --> 
<script src="<?php echo URL ?>static/assets2/js/mainmenu/bootstrap.min.js"></script> 
<script src="<?php echo URL ?>static/assets2/js/mainmenu/customeUI.js"></script> 

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/custom1.js"></script> 

<!-- scroll up --> 
<script src="<?php echo URL ?>static/assets2/js/scrolltotop/totop.js" type="text/javascript"></script> 

<!-- accordion --> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/accordion/smk-accordion.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/accordion/custom.js"></script>


<!-- owl carousel --> 
<script src="<?php echo URL ?>static/assets2/js/owl-carousel/owl.carousel.js"></script> 
<script src="<?php echo URL ?>static/assets2/js/owl-carousel/custom.js"></script>



        <script>
          jQuery(document).ready(function($) {
              $("form#updatePasswordF").submit(function(e) {
              e.preventDefault();
              var formData = new FormData(this);
              var action = $('form#updatePasswordF').attr('action');
              $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,                
                 success: function(res) { 
                   $('p.testing').text(res.msg);                 
                  setTimeout(function() {
                    window.location.reload();
                 }, 2000); 
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
              });
         });

              //update prof
         $("form#updateProf").submit(function(e) {
              e.preventDefault();
              var formData = new FormData(this);
              console.log(formData);
              var action = $('form#updateProf').attr('action');
              $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,                
                 success: function(res) {                 
                 $('p.testinging').text(res.msg);
                  setTimeout(function() {
                    window.location.reload();
                 }, 2000); 
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
              });
         });

         //attend event


       
         dontAttendEvent = (event_id,status)=>{
          
          
             const action = `<?php echo URL ?>event/attendance-registration/${event_id}/${status}`; 
             const data = {
              status:status,
              event_id:event_id
             } 

              $.ajax({
                 url: action,
                 type: 'POST',
                 data: data,                
                 success: function(response) { 

                  if(response.status){

                      $('p.response').css({'display':'block','font-weight':'bold','margin-bottom':'10px'});
                      $('p.response').text(response.msg); 
                      setTimeout(()=>{
                      $('p.response').css({'display':'none'});
                       window.location.reload();
                      },2000);

                  }else{

                      $('p.response').css({'display':'block','font-weight':'bolder','color':'red','margin-bottom':'10px'});
                      $('p.response').text(response.msg); 
                      setTimeout(()=>{
                      $('p.response').css({'display':'none'});
                       window.location.reload();
                      },2000);

                  }
                           
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
              });

         }
         //cancel attendance

         attendEvent = (event_id,status)=>{
          
          
             const action = `<?php echo URL ?>event/attendance-registration/${event_id}/${status}`; 
             const data = {
              status:status,
              event_id:event_id
             } 

              $.ajax({
                 url: action,
                 type: 'POST',
                 data: data,                
                 success: function(response) { 

                  if(response.status){

                      $('p.response').css({'display':'block','font-weight':'bold','margin-bottom':'10px'});
                      $('p.response').text(response.msg); 
                      setTimeout(()=>{
                      $('p.response').css({'display':'none'});
                      },2000);

                  }else{

                      $('p.response').css({'display':'block','font-weight':'bolder','color':'red','margin-bottom':'10px'});
                      $('p.response').text(response.msg); 
                      setTimeout(()=>{
                      $('p.response').css({'display':'none'});
                      },2000);

                  }
                           
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
              });

         }




         //memem




         $("form#sendNewletter").submit(function(e) {
         e.preventDefault();       
         var formData = new FormData(this);
         var action = $('form#sendNewletter').attr('action');       
               $.ajax({
                   url: action,
                   type: 'POST',                  
                   data: formData,
                   success:function(res){
                
                       $('p.notify').text(res.msg);
                      

                  setInterval(function(){
                    window.location.reload();
                  },4000);
                  
                   },
                  error: function(jqXHR, textStatus, errorThrown) {
                         console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
               })
               .done(function() {
                   console.log("success");
               })
          });

  



         //
      });
        </script>


</body>
</html>
