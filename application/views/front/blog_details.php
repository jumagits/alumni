
        <div class="sub_header med">
        	<div id="intro_txt">
			 <?php  foreach ($post_details as $key => $post) {}   ?>
                    <h1 class="text-white"><?php echo strtoupper($post->post_title); ?></h1>
            <p>Information about Bumaa.</p>
            </div>
		</div> <!--End sub_header -->
        
        <div id="position">
            <div class="container">
                <ul>
                    <li><a href="<?php echo URL; ?>">Home</a></li>
                    
                    <li><?php echo isset($post) ? $post->post_title:'' ;?></li>
                </ul>
            </div>
        </div><!-- Position -->
 
 	<div class="container_gray_bg">
    <div class="container margin_60">
    <div class="row">
         
     <div class="col-md-9">

		<?php 

		function dateFF($date){
		   return date_format(date_create($date), '  jS F Y'); 
		}

		 ?>
     	
     		<div class="post">
					<img src="<?php echo URL;?>data/posts/<?php echo $post->post_image; ?>" alt="banner" class="img-responsive">
					<div class="post_info clearfix">
						<div class="post-left">
							<ul>
								<li><i class="icon-calendar-empty"></i><?php echo isset($post) ? dateFF($post->post_date):'' ;?> <em>by Mark</em></li>
                                <li><i class="icon-inbox-alt"></i><a href="#">Category</a></li>
								<li><i class="icon-tags"></i>
									<a href="#">Works</a>, <a href="#">Personal</a>
								</li>
							</ul>
						</div>
						<div class="post-right"><i class="icon-comment"></i><a href="#">25 </a></div>
					</div>
					<h2><?php echo strtoupper($post->post_title); ?></h2>
					<p>
						<?php echo($post->post_content); ?>
					</p>
                   
				</div><!-- end post -->
                                
		  <h4>3 comments</h4>
                
				<div id="comments">
					<ol>
						<p style="border-bottom: 1px solid #333;"></p>

						<?php

						if( $all_comments){
						 foreach ($all_comments as $key => $comment) {
							# code...
					  ?>

						<li>
					
						<div class="comment_right clearfix" style="border-bottom: 1px solid #d3d3d3;">
							<div class="comment_info">

								Posted by <a href="#"><?php echo $comment->comment_author; ?></a>

								<span>|</span><?php echo dateFF($comment->comment_date); ?> 
								<span>|</span><a href="#">Reply</a>
							</div>
							<p>
								<?php echo $comment->comment_content; ?>
							</p>

						</div>
						</li>


					<?php }} else{?>
                        <li class="comment">

                        	<h2 class="comments-title">No Comments Yet</h2>
                        	
                        </li>

					<?php } ?>
											
					</ol>
				</div><!-- End Comments -->
                
				<h4>Leave a comment</h4>
				<p class="response text-success"></p>

				<form  class="comment-form" action="<?php echo URL; ?>post/create-comment" id="commentform" method="post">

					<div class="form-group">
						<input class="form-control styled" type="text" name="comment_author" placeholder="Enter name">
					</div>
					<div class="form-group">
						<input class="form-control styled" type="text" name="comment_email" placeholder="Enter email">
					</div>
					<div class="form-group">
						<textarea name="comment_content" class="form-control styled" style="height:150px;" placeholder="Message"></textarea>
					</div>
					<input type="hidden" name="post_id" value="<?php echo $post->post_id; ?>">
					<div class="form-group">
						<input type="submit" class="button" name="submit" value="Post Comment">
                        <input type="reset" class="button_outline" value="Clear form">
					</div>
				</form>          
        
                
            
     </div><!-- End col-md-8-->   
     
      <aside class="col-md-3" id="sidebar">

				<div class="widget">
					<div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" placeholder="Search" />
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button">
                            <i class="icon-search-1"></i>
                        </button>
                    </span>
                </div>
            </div>
				</div><!-- End Search -->
                <hr>
				<div class="widget">
					<h4>Categories</h4>
					<ul id="cat_nav">

					<?php foreach ($all_categories as $key => $c) {?>
                    	<li><a href="<?php echo URL ?>frontend/category/<?php echo base64_encode($c->cat_id); ?>"><?php echo $c->cat_title; ?></a></li>
                       <?php } ?>

                    </ul>
				</div><!-- End widget -->
 
               <hr>
            
				<div class="widget">
					<h4>Recent post</h4>
					<ul class="recent_post">

						<?php foreach ($recent_posts as $key => $post) {
							# code...
						 ?>

						<li>
						<i class="icon-calendar-empty"></i><?php echo dateFF($post->post_date); ?>
						<div><a href="#"><?php echo mb_strtolower($post->post_title); ?> </a></div>
						</li>

						<?php } ?>
					</ul>
				</div><!-- End widget -->
                <hr>
				<div class="widget tags">
					<h4>Tags</h4>
						<?php

						if($all_posts):

						foreach ($all_posts as $key => $post) {

					 	 $tags = $post->post_tags;//string

					 	 $tags = explode(',', $tags);//arrays

					 	 foreach ($tags as $key => $tag) {
						 	?>
						<a href="#"><?php echo( $tag); ?></a> 


					     <?php }} 

				           endif;

						?>
				</div><!-- End widget -->
                
     </aside><!-- End aside -->
	
  </div><!-- End row-->         
</div><!-- End container -->
    </div><!--End container_gray_bg -->
    