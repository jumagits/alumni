      <div class="sub_header med">
          <div id="intro_txt">
              <h1><strong><?php echo isset($pageTitle) ? $pageTitle: ""; ?></strong> </h1>
              <p>
                  Find Jobs according to your Expertise.
              </p>
          </div>
      </div>
      <!--End sub_header -->
      <div class="container-fluid">
          <div style="margin-top: 20px;">
              <div class="row">
                  <div class="col-lg-9">
                      <div class="panel">
                          <div class="panel-heading text-center ">
                              SEARCH FROM ALL REGISTERED ALUMNI
                          </div>
                          <div class="panel-body table-responsive">
                              <table class="table table-hover" id="datatable1">
                                  <thead>
                                      <tr>
                                          <th>FULLNAME</th>
                                          <th>EMAIL</th>
                                          <th>CAMPUS</th>
                                          <th>COURSE</th>
                                          <th>ABOUT</th>
                                      </tr>
                                  </thead>
                                  <tbody>

                                      <?php 
											$count = 0;
											foreach ($all_clients as $key => $client) { $count++; ?>
                                      <tr style="border-color: 1px solid #000;">
                                          <td><?php echo strtoupper($client->fname)." ". strtoupper($client->lname); ?>
                                          </td>
                                          <td><?php echo $client->email; ?></td>
                                          <td><?php echo isset($client->clinic_id)? ($catsModel->get_clinic_name_today($client->clinic_id)) : "N/A"; ?>
                                          </td>
                                          <td><?php echo isset($client->course)? ($catsModel->get_course_name_today($client->course)) : "N/A"; ?>
                                          </td>
                                          <td><?php echo isset($client->knowabout) ? $client->knowabout : 'NOT KNOWN'; ?>
                                          </td>
                                      </tr>

                                      <?php } ?>
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>


                  <div class="col-lg-3">
                      <div class="panel ">
                          <div class="panel-heading">
                              RECENTLY REGISTERED ALUMNI
                          </div>

                          <div class="panel-body">
                              <table class="table table-active table-hover">
                                  <!--  -->

                                  <tbody>


                                      <?php 
									$count = 0;
									foreach ($current_clients as $key => $client) { $count++; ?>

                                      <tr>

                                          <td bgcolor="#ffffff">
                                              <b class="text-dark"><?php echo $client->fname." ".$client->lname; ?>
                                              </b><br><?php echo $client->email; ?>- <b><?php echo $client->sex; ?></b>
                                          </td>


                                          <?php } ?>



                                      </tr>
                                  </tbody>

                              </table>



                          </div>


                      </div>

                      <!-- companies -->
                      <hr>


                      <div class="panel ">

                          <div class="panel-heading">
                              RECENTLY REGISTERED COMPANIES
                          </div>

                          <div class="panel-body">
                              <ul class="list-group list-group-flush">
                                  <?php 
							$count = 0;
							foreach ($current_company as $key => $client) { $count++; ?>

                                  <li class="list-group-item">

                                      <h5 class="font-weight-bold"><b><?php echo $client->companyName; ?></b></span>
                                          <span class="badge badge-primary"> <?php echo $client->email; ?></span></h5>

                                  </li>


                                  <?php } ?>

                              </ul>

                          </div>


                      </div>



                      <!-- companies end -->




                  </div>

              </div>

          </div>


      </div>


      </div>

      </div>

      </div>

      </div>

      <!-- Footer ==== -->

      <script>
$(document).ready(function() {
    $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $(".row .item").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
      </script>