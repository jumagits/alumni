  <div id="page-wrapper">
      <div class="container-fluid">
          <div class="panel">
               <h4>COURSES  <a  data-toggle="modal" data-target=".bs-edit-modal-lg" class="pull-right btn btn-danger"><i class="fa fa-plus"></i> Add Course</a></h4><hr>
              <div class="panel-body " >              
               <div class="table-responsive ">
                <table class="table table-hover table-bordered table-striped" id="datatable1" width="100%">
                    <thead>                       
                        <tr>
                            <th>S/N</th>
                            <th>Course</th>
                            <th>Code</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php 

                      $count = 0;

                     foreach ($all_courses as $key => $course) {

                      $count++;

                      ?>

                        <tr>
                            <td><?php echo $count; ?></td>

                            <td>
                                <?php echo strtoupper($course->course_name); ?>

                            </td>

                            <td>
                                <?php echo $course->course_code; ?>

                            </td>

                            <td>
                                <a class="btn btn-info btn-sm" href="<?php echo URL ?>course/show/<?php echo base64_encode($course->id);?>">Update</a>

                            </td>

                            <td>
                                <a class="btn btn-danger btn-sm" href="#" onclick="deleteCourse(<?php echo $course->id; ?>)">Delete</a>

                            </td>
                        </tr>

                        <?php } ?>

                    </tbody>
                </table>
          </div>
      </div>
  </div>
 </div>
</div>



  <!-- end of page -->



  <!--  Modal Prison for the above example -->
  <div class="modal fade bs-edit-modal-lg " role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"
      data-backdrop="false">
      <div class="modal-dialog modal-md">
          <div class="modal-content">
              <div class="modal-header bg-primary">
                  <h4 class="modal-title " id="myLargeModalLabel">Add Course
                      <button type="button" class="close pull-right" data-dismiss="modal" aria-hidden="true">×</button>
                  </h4>
              </div>
              <div class="modal-body">


                  <form action="<?php echo URL ?>course/add" id="manage-course">

                      <div class="form-group">
                          <label class="control-label">Course</label>
                          <input type="text" class="form-control" name="course_name">
                      </div>

                      <div class="form-group">
                          <label class="control-label">Course Code</label>
                          <input type="text" class="form-control" name="course_code">
                      </div>


                      <div class="form-group">
                          <button class="btn btn-lg btn-secondary btn-block"> Add Course</button>


                      </div>

                  </form>
              </div>
          </div>
          <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->

  <script>
jQuery(document).ready(function($) {

          deleteCourse = function ($id){

                    if(confirm('Are you sure to Delete this Course')){

                    var id = Number($id);

                    $.ajax({
                        url: APP_URL+"course/delete/",
                        type: 'GET',                        
                        dataType: 'json',                     
                        data: {id: id},
                        success:function(res){

                             swal({
                                   title: "Good job!",
                                   text: res.msg,
                                   type: "success",
                                   showCancelButton: !0,
                                   confirmButtonClass: "btn btn-success",
                                   cancelButtonClass: "btn btn-danger m-l-10"
                              });

                             
                            setTimeout(function() {
                                window.location.reload();
                           }, 1000);

                        }
                    });

                   }

                }

    $("form#manage-course").submit(function(e) {

        e.preventDefault();
        var formData = new FormData(this);
        var action = $('form#manage-course').attr('action');

        $.ajax({
                url: action,
                type: 'POST',
                data: formData,
                success: function(res) {
                    swal({
                        title: "Good job!",
                        text: res.msg,
                        type: "success",
                        showCancelButton: !0,
                        confirmButtonClass: "btn btn-success",
                        cancelButtonClass: "btn btn-danger m-l-10"
                    })

                    setTimeout(function() {
                        window.location.reload();
                    }, 1000);
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR);
                },
                cache: false,
                contentType: false,
                processData: false
            })
            .done(function() {
                console.log("success");
            })
            .fail(function() {
                console.log("error");
            })
            .always(function() {
                console.log("complete");
            });




    });

});
  </script>