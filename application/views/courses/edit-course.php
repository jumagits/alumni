  <div id="page-wrapper">


            <div class="container-fluid">

            <!-- Page Heading -->
                    <div class="row">

                            <div class="col-lg-12">

                                <h4 class="page-header bg-warning">
                               Hey <?php echo !isset($user_info) ? header('URL'): $user_info['names'];
                                ?>, Welcome to <?php echo $pageTitle; ?> 
                                </h4>

                                <?php if (isset($course)) {
                                 foreach ($course as $key => $cat) {
                                    // var_dump($cat->cat_id);
                                  }
                                } ?>

                            </div>

                    </div>

                   <!--    header end -->
                
                              <div class="panel">

                                    <div class="panel-heading bg-primary">
                                        <h4> Edit Course <a href="<?php echo URL ?>course" class="pull-right btn btn-danger">Cancel</a></h4>
                                    </div>

                                       <div class="panel-body " style="padding: 30px;">

                                                <form action="<?php echo URL ?>course/update" id="updateCourse"> 

                                                   <input type="hidden" name="id" value="<?php echo $cat->id; ?>">

                                                <div class="form-group ">
                                                    <label for="" class="control-label">course</label>
                                                    <input type="text" class="form-control" name="course_name"   required value="<?php echo $cat->course_name; ?>">
                                                </div>


                                                <div class="form-group ">
                                                    <label for="" class="control-label">Course Code</label>
                                                    <input type="text" class="form-control" name="course_code"   required value="<?php echo $cat->course_code; ?>">
                                                </div>

                                                 
                                                <div class="form-group"> 
                                                   <button type="submit" class="btn btn-primary btn-md btn-block ">Submit</button>
                                                </div>

                                            </form>
                                         
                                      </div>

                         
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>



<!-- end of page -->

<?php //var_dump($job); ?>






<script>
    
    jQuery(document).ready(function($) {
        
         $("form#updateCourse").submit(function(e) {

         e.preventDefault();       
         var formData = new FormData(this);
         var action = $('form#updateCourse').attr('action');
       
               $.ajax({
                   url: action,
                   type: 'POST',                  
                   data: formData,
                   success:function(res){
                  swal({
                       title: "Good job!",
                       text: res.msg,
                       type: "success",
                       showCancelButton: !0,
                       confirmButtonClass: "btn btn-success",
                       cancelButtonClass: "btn btn-danger m-l-10"
                     })

                  setInterval(function(){

                    window.location.href = '<?php echo URL; ?>course';

                  },2000);

                  
                   },
                  error: function(jqXHR, textStatus, errorThrown) {
                         console.log(jqXHR);
                    },
                    cache: false,
                    contentType: false,
                    processData: false
               })
               .done(function() {
                   console.log("success");
               })
          });

    });
</script>