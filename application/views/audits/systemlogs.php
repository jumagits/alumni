  <div id="page-wrapper bg-white">
      <div class="container-fluid">
          <div class="row">
              <div class="col-lg-12">
              <div class="panel-body " style="padding: 30px;">
                  <div class="table-responsive">
                      <table class="table table-striped table-bordered " id="datatable1">
                          <thead>
                              <tr>
                                  <th>#</th>
                                  <th>Activity</th>
                                  <th>Username</th>
                                  <th>Clinic Name</th>
                                  <th>Time And Day</th>
                                  <th>Log Type</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php echo $logModel->fetchAllLogs() ;?>
                          </tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
<?php 
function dateF($date){
   return date_format(date_create($date), ' l jS F Y'); 
}

 ?>


  <!-- end of page -->