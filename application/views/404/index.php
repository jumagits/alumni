<!doctype html>
<html lang="en-gb">
<!--<![endif]-->
<head>
<title>BUMAA - BUSITEMA UNIVERSITY MUSLIM ALUMNI ASSOCIATION</title>
<meta charset="utf-8">
<!-- Meta -->
<meta name="keywords" content="" />
<meta name="author" content="">
<meta name="robots" content="" />
<meta name="description" content="" />

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Favicon -->
<link rel="shortcut icon" href="<?php echo URL ?>static/assets2/images/favicon.png">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->

<!-- ######### CSS STYLES ######### -->
<!-- <link href="<?php echo URL ?>static/assets/css/bootstrap.min.css" rel="stylesheet">    
 -->
<link rel="stylesheet" href="<?php echo URL ?>static/assets2/css/reset.css" type="text/css" />
<link rel="stylesheet" href="<?php echo URL ?>static/assets2/css/style.css" type="text/css" />
<link rel="stylesheet" href="<?php echo URL ?>static/assets2/css/font-awesome/css/font-awesome.min.css">

<link rel="stylesheet" href="<?php echo URL ?>static/assets2/js/form/css/sky-forms.css" type="text/css" media="all">


<!-- responsive devices styles -->
<link rel="stylesheet" media="screen" href="<?php echo URL ?>static/assets2/css/responsive-leyouts.css" type="text/css" />

<!-- mega menu -->
<link href="<?php echo URL ?>static/assets2/js/mainmenu/sticky.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets2/js/mainmenu/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets2/js/mainmenu/demo.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets2/js/mainmenu/menu.css" rel="stylesheet">

<!-- revolution slider -->

<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>static/assets2/js/revolutionslider/css/extralayers.css" media="screen" />
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/css/settings.css" media="screen" />




<!-- Owl Carousel Assets -->
<link href="<?php echo URL ?>static/assets2/js/owl-carousel/owl.carousel.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets2/js/owl-carousel/owl.theme.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets2/js/owl-carousel/owl.transitions.css" rel="stylesheet">

<!-- simple line icons -->
<link rel="stylesheet" type="text/css" href="<?php echo URL ?>static/assets2/css/Simple-Line-Icons-Webfont/simple-line-icons.css" media="screen" />

<!-- Accordion styles -->
<link rel="stylesheet" href="<?php echo URL ?>static/assets2/js/accordion/smk-accordion.css" />


<link href="<?php echo URL ?>static/assets/css/style.css" rel="stylesheet">
<link href="<?php echo URL ?>static/assets/css/base.css" rel="stylesheet">

<!-- Remove the below comments to use your color option -->
<!--<link rel="stylesheet" href="css/colors/red.css" />-->
<!--<link rel="stylesheet" href="css/colors/green.css" />-->
<!--<link rel="stylesheet" href="css/colors/pink.css" />-->
<!--<link rel="stylesheet" href="css/colors/orange.css" />-->
<!--<link rel="stylesheet" href="css/colors/liteblue.css" />-->
<!--<link rel="stylesheet" href="css/colors/purple.css" />-->
<!--<link rel="stylesheet" href="css/colors/bridge.css" />-->
<!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
<!--<link rel="stylesheet" href="css/colors/darkred.css" />-->

<!-- just remove the below comments witch bg patterns you want to use -->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-ten.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-eleven.css" />-->
<!--<link rel="stylesheet" href="css/bg-patterns/pattern-twelve.css" />-->



</head>

<style>
 body{
  background-color: #f5f5f5 !important;
 }
</style>

<body>
<div class="site_wrapper">
  <header id="header"> 
    
    <!-- Top header bar -->
    <div id="topHeader ">
      <div class="wrapper">
        <div class="top_nav one" style="background-color: #78cc0d !important;border-bottom: 2px solid #009900;color:#000 !important;">
          <div class="container">
            <ul>
              <li class="line"><a href="<?php echo URL ?>accounts/login"> Login</a></li>
              <li class="line"><a href="<?php echo URL ?>accounts/alumni_register"> Alumni Registration</a></li>
              <li class="line"><a href="<?php echo URL ?>accounts/alumni_register"> Well-wisher Registration</a></li>
              <li><a target="_blank" href="https://www.facebook.com/codelayers"><i class="fa fa-facebook"></i></a></li>
              <li><a target="_blank" href="https://twitter.com/codelayers"><i class="fa fa-twitter"></i></a></li>
              <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
              <li class="last"><a href="#"><i class="fa fa-linkedin"></i></a></li>
            </ul>
            <ul class="left">
              <li class="line"> 24x7  live Technical Support</li>
              <li> (+256) 702499649</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <!-- end top navigation -->
    
    <div class="scrollto_sticky"  style="background-color:#fff !important;border-bottom: 2px solid #009900;">
      <div class="container"> 
        
        <!-- Logo -->
        <div class="logo"><a href="<?php echo URL; ?>" id="logo"></a></div>
        
        <!-- Menu -->
        <div class="menu_main">
          <div class="navbar yamm navbar-default"  style="background-color:#fff !important;">
            <div class="container-fluid ">
              <div class="navbar-header">
                <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  ><span></span>
                  <button type="button"> <i class="fa fa-bars"></i></button>
                </div>
              </div>
              <div id="navbar-collapse-1" class="navbar-collapse collapse ">
                <nav>
                  <ul class="nav navbar-nav pull-right">

                  <li class="nav-item active"><a class="nav-link" href="<?php echo URL; ?>/">Home </a></li>
                    
                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>/application/about">About </a></li>

                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>frontend/blog">News </a></li>

                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/job">Jobs </a></li>

                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/events">Events </a></li>

                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/alumni">Alumni </a></li>

                    <li class="nav-item"><a class="nav-link" href="<?php echo URL; ?>application/contact">Contact </a></li>     
                  
                    <li class="dropdown"> <a href="#" class="dropdown-toggle">  Account</a>
                      <ul class="dropdown-menu" role="menu">
                      
                        <li><a href="<?php echo URL; ?>application/profile"><i class=" fa fa-user"></i> Profile</a> </li>

                        <li><a href="<?php echo URL ?>accounts/logout"><i class="fa fa-power-off"></i> Logout</a> </li>
                      </ul>
                    </li>
                    
                  
                  </ul>
                </nav>
              </div>
            </div>
          </div>
        </div>
        <!-- end menu --> 
        
      </div>
    </div>
  </header>
  <div class="clearfix"></div>
  <?php if(isset($page) AND $page == 'home'){
  ?><?php }else{ ?>

  
   <div class="header_medium two">
    <div class="container">
      <h2 class="align_center white uppercase weight6"> <?php echo isset($pageTitle)? $pageTitle: "BUMAA"; ?></h2>
      <h3 class="uppercase white"> <?php echo isset($pageSlug)? $pageSlug: "UNITING BUMSA ALUMNI GLOBALLY"; ?></h3>
    </div>
  </div>

<?php } ?>


  <!--end pagenation-->
  <div class="clearfix"></div>
  
  <div class="section_holder30">
    <div class="container">
      <div class="error_holder">
        <h1 class="uppercase title cyan">404</h1>
        <br/>
        <h2 class="uppercase">Oops... Page Not Found!</h2>
        <p>Sorry the Page Could not be Found here. Please send us a message for any ideas</p>
        <br/>
        <br/>
        <br/>
        <div class="newsletter two">
          <form method="get" action="index.html">
            <input class="email_input" name="samplees" id="samplees" value="E-mail Address" onFocus="if(this.value == 'E-mail Address') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'E-mail Address';}" type="text">
            <input name="" value="Search" class="input_submit" type="submit">
          </form>
        </div>
      </div>
    </div>
  </div>
 
  

</div>
<!--end sitewraper--> 
 <div class="clearfix"></div>

    <div class="footer" style="background-color:#fff !important;border-top: 2px solid #009900;">
    <div class="container">
      <div class="one_third">
        <h4> <strong>Address</strong></h4>
        <div class="footer_logo"><img src="<?php echo URL ?>static/assets2/images/logo.png" alt="logo"/></div>
        <br>
        <span class="address"><strong class="">
          Address: No.28 - Kampala Mukadde, Uganda
        </strong></span> <span class="address white">Phone: +(256) 702499649</span> <span class="address white">Fax: +(256) 783975685</span> <span class="address">Email: info@bumaa.com </span> </div>
      <!--end item-->
      
      <div class="one_third">
        <h4> <strong>Newsletter</strong></h4>
        <div class="title_line"></div>

       
          
          <form action="#" class="meme">
            <p class="bold">Subscribe for the latest updates</p>
            <br>
            <div class="form-group">
              <label for="#">Email</label>
              <input type="email" name="email" id="" class="form-control form-control-lg">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary" name="subscribe"><strong>Subscribe</strong></button>
            </div>
          </form>
        
       
        
      </div>
      <!--end item-->
      
     
      <!--end item-->
      
      <div class="one_third last">
       
       <h4> <strong>Support us convieniently using</strong></h4>
      
        <div class="title_line"></div>
        <ul class="payment_logos">
          <li><img src="<?php echo URL ?>static/assets2/images/airtel.jpg" alt="airtel" width="100" height="100"/></li>
         
          <li><img src="<?php echo URL ?>static/assets2/images/mtn.jpg" alt="mtn"  width="100" height="100"/></li>
          
        </ul>
      </div>
      <!--end item--> 
      
    </div>
  </div>
  <!--end footer-->
<!--   background: #4565a6;
border: 1px solid #a7bde8; -->
  <div class="copyrights" style="background: #000;
border: 1px solid #000;">
    <div class="container">
      <div class="one_half"><span>Copyright © 2020 - <?php echo date('Y'); ?> ~ BUMAA. All Rights Reserved | All rights reserved.</span></div>
      <div class="one_half last">
        <ul class="social_icons">
          <li><a target="_blank" href="https://www.facebook.com/codelayers"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
          <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
          <li><a href="#"><i class="fa fa-wordpress"></i></a></li>
          <li><a href="#"><i class="fa fa-android"></i></a></li>
        </ul>
      </div>
    </div>
  </div>
  <!--end copyrights--> 
  <a href="#" class="scrollup"></a><!-- end scroll to top of the page--> 
  
</div>
<!--end sitewraper--> 



<!-- ######### JS FILES ######### --> 
<!-- get jQuery from the google apis --> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/universal/jquery.js"></script> 

<!-- style switcher --> 
<script src="<?php echo URL ?>static/assets2/js/style-switcher/jquery-1.js"></script> 
<script src="<?php echo URL ?>static/assets2/js/style-switcher/styleselector.js"></script> 

<!-- scroll to fixied sticky --> 
<script src="<?php echo URL ?>static/assets2/js/mainmenu/jquery-scrolltofixed.js" type="text/javascript"></script> 
<script src="<?php echo URL ?>static/assets2/js/mainmenu/ScrollToFixed_custom.js" type="text/javascript"></script> 

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/custom1.js"></script> 

<!-- mega menu --> 
<script src="<?php echo URL ?>static/assets2/js/mainmenu/bootstrap.min.js"></script> 
<script src="<?php echo URL ?>static/assets2/js/mainmenu/customeUI.js"></script> 

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/revolutionslider/custom1.js"></script> 

<!-- scroll up --> 
<script src="<?php echo URL ?>static/assets2/js/scrolltotop/totop.js" type="text/javascript"></script> 

<!-- accordion --> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/accordion/smk-accordion.js"></script> 
<script type="text/javascript" src="<?php echo URL ?>static/assets2/js/accordion/custom.js"></script>


<!-- owl carousel --> 
<script src="<?php echo URL ?>static/assets2/js/owl-carousel/owl.carousel.js"></script> 
<script src="<?php echo URL ?>static/assets2/js/owl-carousel/custom.js"></script>
</body>
</html>
