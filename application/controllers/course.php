<?php
class course extends Controller
{
    public function index()
    {
        $user_info = $this->bootstrap();
        $courseModel = $this->loadModel('courseModel');
        $all_courses = $courseModel->all_courses();
        $head = $this->loadView('common/header');
        $user_info = $this->bootstrap();
        $head->set('user_info',$user_info);
        $head->set('pageTitle','Courses - ');
        $head->render();
        //
        $content = $this->loadView('courses/index');       
        $content->set('pageTitle', 'Courses - ');
        $content->set('all_courses', $all_courses);
        $content->render();
        //
        $footer = $this->loadView('common/footer');
        $footer->render();
    }


    //add course

     public function add(){

        $user_info      = $this->bootstrap();
        $courseModel      = $this->loadModel('courseModel');
        if(isset($_POST['course_name']))
        {
            $form_feedback = $courseModel->add_course($_POST["course_name"],$_POST["course_code"]);
        }
        $res_pass = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res_pass;
    }


    //show 


      public function show($id){
        $user_info      = $this->bootstrap();
        $d_id = base64_decode($id);
        $courseModel = $this->loadModel('courseModel');
        $fetchC = $courseModel->fetchCourse($d_id);            
        $head   =  $this->loadView('common/header'); 
        $head->set('user_info',$user_info);  
        $head->set('pageTitle','Edit Course  ');
        $head->render();

        //
        $content     = $this->loadView('courses/edit-course');
        $content->set('user_info',$user_info);
        $content->set('course',$fetchC);
        $content->render();
        //
        $footer      = $this->loadView('common/footer');
        $footer->render();
    }



    //delete course


    public function delete($id)
    {
        $del_id = $_GET['id'];
        $user_info = $this->bootstrap();
        $courseModel = $this->loadModel("courseModel");
        $result = $courseModel->delete_course($del_id);        
        $res_pass = json_encode($result);
        header('Content-Type: application/json');
        echo $res_pass;
    }


    //update


    public function update(){

           $courseModel    = $this->loadModel('courseModel');
             if(isset($_POST['course_name']))
            {
                $form_feedback = $courseModel->editCourse($_POST["course_name"],$_POST["course_code"],$_POST["id"]
            );       

            if($form_feedback['status']){
            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;

            }

        }

    }


    //end of methods
}