<?php
class event extends Controller
{
    public function index()
    {
        $eventsModel     = $this->loadModel('eventsModel');
        $all_events = $eventsModel->getAllEvents();
        $user_info = $this->bootstrap();           
        $administrativeModel = $this->loadModel('administrativeModel');
        $clinic_drop_two = $administrativeModel->clinic_drop_two();

        $head =     $this->loadView('common/header');  
        $head->set('user_info',$user_info);            
        $head->render();
        $content = $this->loadView('events/index');
        $content->set('pageTitle', 'Events ');
        $content->set('model',$eventsModel);
        $content->set('user_info',$user_info);  
        $content->set('clinic_drop_two',$clinic_drop_two);    
        $content->set('all_events', $all_events);
        $content->render();
        $footer  = $this->loadView('common/footer');       
        $footer->render();
    }

    //event
    public function edit($event_id="")
    {
        $eventsModel     = $this->loadModel('eventsModel');       
        $user_info = $this->bootstrap();
        $event     = $eventsModel->get_event(base64_decode($event_id));        
        $head =     $this->loadView('common/header');  
        $head->set('user_info',$user_info);            
        $head->render();
        $content = $this->loadView('events/edit-event');
        $content->set('pageTitle', 'Edit Event - ');
        $content->set('user_info',$user_info);
        $content->set('event',$event);
        $content->render();
        $footer  = $this->loadView('common/footer');       
        $footer->render();
    }

    public function attendance_registration($event_id,$status){

    $user_info = $this->bootstrap();
    $user_id = $user_info['id'];
    $event_ida = base64_decode($event_id);
    $statuss = (int)$status;
    $eventsModel  = $this->loadModel('eventsModel');
    $feedback = $eventsModel->register_for_event($event_ida,$user_id,$statuss);
    $response = json_encode($feedback);
    header('Content-Type: application/json');
    echo $response;

    }


    //add
      public function add(){

        $eventsModel  = $this->loadModel('eventsModel');
        $uploader = $this->loadHelper('uploader');       
        $data = $uploader->upload($_FILES['banner'],$this->upload_array());
        if($data['isComplete'])
        {
            $info      = $data['data'];           
            if(!empty($info['metas'][0]['name'])){
                $photo     = urlencode($info['metas'][0]['name']);
            }else{
                $photo  = "";
            }           

            $form_feedback = $eventsModel->addEvent(  
                $_POST['category'],             
                $photo,
                $_POST['schedule'],                
                $_POST['title'],
                $_POST['location'],               
                $_POST['description'],
                $_POST['created_by'],
               
            );          

            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
        }

        if($data['hasErrors']){
            $errors = json_encode( $data['errors'] );
            header('Content-Type: application/json');
            echo $errors;
        }
   }

    //update 

    public function update(){

        $eventsModel  = $this->loadModel('eventsModel');
        $uploader = $this->loadHelper('uploader');       
        $data = $uploader->upload($_FILES['banner'],$this->upload_array());        
        if($data['isComplete'])
        {
            $info      = $data['data'];           
            if(!empty($info['metas'][0]['name'])){
                $photo     = urlencode($info['metas'][0]['name']);
            }else{
                $photo  = "";
            }            

            $form_feedback = $eventsModel->editEvent( 
                $_POST['category'],               
                $photo,
                $_POST['schedule'],                
                $_POST['title'],
                $_POST['location'],               
                $_POST['description'],                
                $_POST['created_by'],
                $_POST['id']               
            );            

            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
        }
        if($data['hasErrors']){
            $errors = json_encode( $data['errors'] );
            header('Content-Type: application/json');
            echo $errors;
        }
        
    }


    //delete
    public function delete_event($event_id)
    {
        $user_info  = $this->bootstrap();
        $eventsModel  = $this->loadModel('eventsModel');
        $event  = $eventsModel->get_del_event(base64_decode($event_id));
        $delete  =  @($eventsModel->del_event($event[0]->id));
        if($delete)
        {
            $this->redirect('event');
        }

    }


public function upload_array(){
    return array(
            'limit' => 100, //Maximum Limit of files. {null, Number}
            'maxSize' => 1024, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => UPLOAD_DIR."/events/", //Upload directory {String}
            'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => true, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        );
}






    //end of methods
}