<?php class reports extends controller
{
    public function index()
    {
        $user_info = $this->bootstrap();
        $homeModel = $this->loadModel('homeModel');
        $locationModel = $this->loadModel('location');
        $regModel = $this->loadModel('regModel');
        $catsModel = $this->loadModel('catsModel');
        $all_clinics = $catsModel->getClinics();
        

        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $head->set('pageTitle', 'Reports - BUMAA IMS');
        $head->render();

        $content = $this->loadView('reports/reports');
        $content->set('all_clinics',$all_clinics);
        $content->set('user_info', $user_info);
       
        $content->render();

        $footer = $this->loadView('common/footer');
        $footer->render();
    }


   
    public function reg_reports()
    {
        $user_info      = $this->bootstrap();
        $homeModel      = $this->loadModel('homeModel');
        $locationModel  = $this->loadModel('location');
        $regModel       = $this->loadModel('regModel');
        $accounts_model      = $this->loadModel('accountsModel');
        $reports_model      = $this->loadModel('reportsModel');
        //var_dump($user_info);

        
        // $head           = $this->loadView('common/header');
        // $head->set('user_info',$user_info);

        // $head->set('pageTitle','Reports - BUMAA IMS');
        // $head->render();

        $content       = $this->loadView('reports/reg-report');
        
        $content->set('user_info',$user_info);
        $content->set('reports_model',$reports_model);
        $content->set('accounts_model',$accounts_model);
        $content->render();


        //pdf start



        //pdf end

        if(isset($_POST['chart']))
        {
            $footer = $this->loadView('common/footer');
            $footer->render();
        }
    }
    public function reg_reportold()
    {
        // $user_info      = $this->bootstrap();
        // $homeModel      = $this->loadModel('homeModel');
        // $locationModel  = $this->loadModel('location');
        // $regModel       = $this->loadModel('regModel');
        // $accounts_model      = $this->loadModel('accountsModel');
        $reports_model      = $this->loadModel('reportsModel');
        global $config;
        @$db_connect = new mysqli($config['db_host'], $config['db_username'], $config['db_password'],$config['db_name']);
        
        if(isset($_POST['excel']))
        {
            $clinic_id = $_POST['clinics'];
            $startdate = $_POST['sd'];
            $enddate   = $_POST['ed'];
            if($clinic_id == 'ALL'){
                $output = "";
                $sql = "SELECT client_refnumber,
                fname,
                lname,
                oname,
                claim,
                classification,
                sex,
                olanguage,
                dob,
                nationality,
                educationlevel,
                agegroup,
                nextofkin,
                contactphone,
                contactaddress,
                clinic_name AS Clinic,
                created_on AS Registered_On FROM BUMAA_clients WHERE regdate BETWEEN '{$startdate}' AND '{$enddate}'";
                
                $result = $db_connect->query($sql);
                $resultObjects = array();
                

                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

               $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'Offence')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'Clinic') {
                            $clinic = @$reports_model->get_clinic($row[$key]);
                            $clinic_name = @$clinic[0]->clinic_name;
                            $output .= '"'.$clinic_name.'",';
                        }
                        else{
                            $output .= '"'.$row[$key].'",';
                        }
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',"Registered_clients_for_all_clinics_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }else{
                $clinic_id = $_POST['clinics'];
                $clinic = $reports_model->get_clinic($clinic_id);
                $startdate = $_POST['sd'];
                $enddate   = $_POST['ed'];
                $output = "";
                $sql = "SELECT client_refnumber,
                fname,
                lname,
                oname,
                claim,
                classification,
                sex,
                olanguage,
                dob,
                nationality,
                educationlevel,
                agegroup,
                nextofkin,
                contactphone,
                contactaddress,
                created_on  FROM BUMAA_clients WHERE clinic_name = '{$clinic_id}' AND regdate BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $resultObjects = array();
                

                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'Offence')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',"Registered_clients_for_".$clinic[0]->clinic_name."_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }
            
        }


        //pdf report 
      if(isset($_POST['pdf'])){

                $clinic_id = $_POST['clinics'];
                $startdate = $_POST['sd'];
                $enddate   = $_POST['ed'];
                require_once APP_DIR."thirdparty/mpdf/mpdf.php";

               if($clinic_id == 'ALL'){

                $sqlma = "SELECT client_refnumber,
                fname,
                lname,
                oname,
                claim,
                classification,
                sex,
                olanguage,
                dob,
                nationality,
                educationlevel,
                agegroup,
                nextofkin,
                contactphone,
                contactaddress,
                created_on  FROM BUMAA_clients WHERE regdate BETWEEN '{$startdate}' AND '{$enddate}' LIMIT 300";
                $resultma = $db_connect->query($sqlma);

                $tab_all = '
                <center align="center"> <img src="static/images/logo.png" style="width:90px;display:block;" /></center>
                <h3 style="padding-bottom:12px;text-align:center;border-bottom:1px solid #333;">REGISTERED CLIENTS  FOR ALL DISTRICTS BETWEEN '.$startdate.'_TO_'.$enddate.'</h3>
                <p style="font-size:14px;text-align:center">This Table shows all the Introduction to lap cases for all DISTRICTS </p>

                <table class="table" border="1" cellspacing="0" cellpadding="0" style="width:100%;" id="customers" >
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>REF NO</th>
                        <th>FNAME</th>
                        <th>LNAME</th>
                        <th>ONAME</th>
                        <th>CLAIM</th>
                        <th>CLASSIFICATION</th>
                        <th>SEX</th>
                        <th>LANGUAGE</th>                       
                        <th>DOB</th>
                        <th>NATIONALITY</th>
                        <th>EDUCATION</th>
                        <th>AGE</th>
                        <th>NEXTOFKIN</th>
                        <th>CONTACTPHONE</th>
                        <th>CONTACTADDRESS</th>                       
                        <th>REGISTERED ON</th>
                    </tr>
                </thead>
                <tbody>';

               $num_rowsm = $resultma->num_rows;
                if($num_rowsm > 0){
                $countall = 0;
                while ($rowm = $resultma->fetch_array(MYSQLI_ASSOC)) {

                    $countall++;  

                        $tab_all .='<tr>
                        <td>'.$countall.'</td>
                        <td>'.$rowm['client_refnumber'].'</td>
                        <td>'.$rowm['fname'].'</td>
                        <td>'.$rowm['lname'].'</td>
                        <td>'.$rowm['oname'].'</td>
                        <td>'.$rowm['claim'].'</td>
                        <td>'.$rowm['classification'].'</td>
                        <td>'.$rowm['sex'].'</td>
                        <td>'.$rowm['olanguage'].'</td>
                        <td>'.$rowm['dob'].'</td>
                        <td>'.$rowm['nationality'].'</td>
                        <td>'.$rowm['educationlevel'].'</td>                
                        <td>'.$rowm['agegroup'].'</td>
                        <td>'.$rowm['nextofkin'].'</td>
                        <td>'.$rowm['contactphone'].'</td>
                        <td>'.$rowm['contactaddress'].'</td>
                        <td>'.$rowm['created_on'].'</td>
                        </tr>';
                     
                }

               }else{

                $tab_all .='<tr>
                      <td colspan="23"> <center>No Matching Records.</center></td>
                </tr>';


            }

                $tab_all .='</tbody>
                </table>';



                $mpdf=new mPDF('c','A4-L');
                $mpdf->debug = true;
                $mpdf->mirrorMargins = 1;
                $mpdf->list_indent_first_level = 0;
                $mpdf->SetDisplayMode('fullpage');
                $stylesheet = file_get_contents('static/assets/css/receipt.css');
                $mpdf->WriteHTML($stylesheet,1);       
                $mpdf->WriteHTML($tab_all);        
                $filename = 'All Registered Clients.pdf';       
                $mpdf->Output($filename, "I");

                exit;


               }else{

            
                $clinic = $reports_model->get_clinic($clinic_id);              
               
                $sqlm = "SELECT client_refnumber,
                fname,
                lname,
                oname,
                claim,
                classification,
                sex,
                olanguage,
                dob,
                nationality,
                educationlevel,
                agegroup,
                nextofkin,
                contactphone,
                contactaddress,
                created_on  FROM BUMAA_clients WHERE clinic_name = '{$clinic_id}' AND regdate BETWEEN '{$startdate}' AND '{$enddate}'";
                $resultm = $db_connect->query($sqlm);

    

                $table = '
                <center align="center"> <img src="static/images/logo.png" style="width:90px;display:block;" /></center>
            <h3 style="padding-bottom:12px;text-align:center;border-bottom:1px solid #333;">REGISTERED CLIENTS  FOR '.$clinic[0]->clinic_name.' DISTRICT BETWEEN '.$startdate.'_TO_'.$enddate.'</h3>
            <p style="font-size:14px;text-align:center">This Table shows all the Introduction to lap cases for '.$clinic[0]->clinic_name.' DISTRICT BETWEEN '.$startdate.'_TO_'.$enddate.'</p>

            <table class="table" border="1" cellspacing="0" cellpadding="0" style="width:100%;" id="customers" >
                <thead>
                    <tr>
                        <th>S/N</th>
                        <th>REF NO</th>
                        <th>FNAME</th>
                        <th>LNAME</th>
                        <th>ONAME</th>
                        <th>CLAIM</th>
                        <th>CLASSIFICATION</th>
                        <th>SEX</th>
                        <th>LANGUAGE</th>                       
                        <th>DOB</th>
                        <th>NATIONALITY</th>
                        <th>EDUCATION</th>
                        <th>AGE</th>
                        <th>NEXTOFKIN</th>
                        <th>CONTACTPHONE</th>
                        <th>CONTACTADDRESS</th>                       
                        <th>REGISTERED ON</th>
                    </tr>
                </thead>
                <tbody>';

               $num_rows = $resultm->num_rows;
                if($num_rows > 0){
                $count = 0;
                while ($row2 = $resultm->fetch_array(MYSQLI_ASSOC)) {

                    $count++;
                          

                  

                        $table .='<tr>
                        <td>'.$count.'</td>
                        <td>'.$row2['client_refnumber'].'</td>
                        <td>'.$row2['fname'].'</td>
                        <td>'.$row2['lname'].'</td>
                        <td>'.$row2['oname'].'</td>
                        <td>'.$row2['claim'].'</td>
                        <td>'.$row2['classification'].'</td>
                        <td>'.$row2['sex'].'</td>
                        <td>'.$row2['olanguage'].'</td>
                        <td>'.$row2['dob'].'</td>
                        <td>'.$row2['nationality'].'</td>
                        <td>'.$row2['educationlevel'].'</td>                
                        <td>'.$row2['agegroup'].'</td>
                        <td>'.$row2['nextofkin'].'</td>
                        <td>'.$row2['contactphone'].'</td>
                        <td>'.$row2['contactaddress'].'</td>
                        <td>'.$row2['created_on'].'</td>
                        </tr>';
                     
                }

            }else{

                $table .='<tr>
                      <td colspan="23"> <center>No Matching Records.</center></td>
                </tr>';


            }

                $table .='</tbody>
                </table>';

 

                $mpdf=new mPDF('c','A4-L');
                $mpdf->debug = true;
                $mpdf->mirrorMargins = 1;
                $mpdf->list_indent_first_level = 0;
                $stylesheet = file_get_contents('static/assets/css/receipt.css');
                $mpdf->WriteHTML($stylesheet,1); 
                $mpdf->WriteHTML($table);        
                $filename = 'Registered Clients.pdf';      
                $mpdf->Output($filename, "I");




            }
       

        }

        //end pdf reports

        if(isset($_POST['chart']))
        {
            $footer = $this->loadView('common/footer');
            $footer->render();
        }
 
    }









    public function intotolap()
    {
        global $config;$reports_model      = $this->loadModel('reportsModel');
        @$db_connect = new mysqli($config['db_host'], $config['db_username'], $config['db_password'],$config['db_name']);
        if(isset($_POST['excel']))
        {
            $clinic_id = $_POST['clinics'];
            $startdate = $_POST['sd'];
            $enddate   = $_POST['ed'];
            if($clinic_id == 'ALL'){
                $sql = "SELECT refno,
                clientname,
                sex,
                language,
                dob,
                nationality,
                educationlevel,
                resdistrict,
                county,
                subcounty,
                parish,
                village,
                ContactPhones,
                altphone,
                nameofowner,
                relationship,
                nextofkin,
                contactphone,
                contactaddress,
                maritalstatus,
                sourceofreferral,
                qus34 AS Offence,
                clinic_id AS Clinic,
                created_on AS Registered_On FROM mmt_data WHERE created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $output = "";
                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }
                $output .="\n";
                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'Offence')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'Clinic') {
                            $clinic = @$reports_model->get_clinic($row[$key]);
                            $clinic_name = @$clinic[0]->clinic_name;
                            $output .= '"'.$clinic_name.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }
                        else{
                            $output .= '"'.$row[$key].'",';
                        }
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',"Introduction_to_LAP_for_all_clinics_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }else{
                $clinic_id = $_POST['clinics'];
                $clinic = $reports_model->get_clinic($clinic_id);
                $output = "";
                $sql = "SELECT refno,
                clientname,
                sex,
                language,
                dob,
                nationality,
                educationlevel,
                resdistrict,
                county,
                subcounty,
                parish,
                village,
                ContactPhones,
                altphone,
                nameofowner,
                relationship,
                nextofkin,
                contactphone,
                contactaddress,
                maritalstatus,
                sourceofreferral,
                qus34 AS Offence,
                created_on AS Registered_On FROM mmt_data WHERE clinic_id = '{$clinic_id}' AND created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $resultObjects = array();
                

                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'Offence')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',"Introduction_to_LAP_for_".$clinic[0]->clinic_name."_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }
            
        }


              //PDF REPORT

        if(isset($_POST['pdf'])){

        $clinic_id = $_POST['clinics'];
        $startdate = $_POST['sd'];
        $enddate   = $_POST['ed'];
        require_once APP_DIR."thirdparty/mpdf/mpdf.php";


        if($clinic_id == 'ALL'){


             $sql22 = "SELECT refno,
                clientname,
                sex,
                language,
                dob,
                nationality,
                educationlevel,
                resdistrict,
                county,
                subcounty,
                parish,
                village,
                ContactPhones,
                altphone,
                nameofowner,
                relationship,
                nextofkin,
                contactphone,
                contactaddress,
                maritalstatus,
                sourceofreferral,
                qus34 AS Offence,
                created_on AS Registered_On FROM mmt_data WHERE  created_on BETWEEN '{$startdate}' AND '{$enddate}' LIMIT 300";

         $result22 = $db_connect->query($sql22); 
            $table2 = '
                <center align="center"> <img src="static/images/logo.png" style="width:90px;display:block;" /></center>
                <h3 style="padding-bottom:12px;text-align:center;border-bottom:1px solid #333;">INTRODUCTION TO LAP REPORT FOR ALL DISTRICTS BETWEEN '.$startdate.'_TO_'.$enddate.'</h3>
                <p style="font-size:14px;text-align:center">This Table shows all the Introduction to lap cases for all Districts Between '.$startdate.'_TO_'.$enddate.'</p>

                <table class="table" border="1" cellspacing="0" cellpadding="0" style="width:100%;" id="customers" >
                <thead>
                <tr>
                <th>S/N</th>
                <th>REF NO</th>
                <th>CLIENT NAME</th>
                <th>SEX</th>
                <th>DOB</th>
                <th>NATIONALITY</th>
                <th>EDUCATION</th>
                <th>DISTRICT</th>
                <th>COUNTY</th>
                <th>SCOUNTY</th>
                <th>PARISH</th>
                <th>VILLAGE</th>
                <th>ALT PHONE</th>
                <th>NAMEOFOWNER</th>
                <th>RELATIONSHIP</th>
                <th>NEXTOFKIN</th>
                <th>CONTACTPHONE</th>
                <th>CONTACTADDRESS</th>
                <th>M.STATUS</th>
                <th>S.O.REFERAL</th>
                <th>OFFENCE</th>
                <th>REGISTERED ON</th>
            </tr>
        </thead>
        <tbody>';

       $num_rows2 = $result22->num_rows;
        if($num_rows2 > 0){
        $count2 = 0;
        while ($row22 = $result22->fetch_array(MYSQLI_ASSOC)) {

            $count2++;
                  

            // district                                              
            $district = @$reports_model->getDistrict($row22['resdistrict']);
            $district_name = @$district[0]->districtName;

            //county                      
            $county = @$reports_model->getCounty($row22['county']);
            $county_name = @$county[0]->countyName; 

            //sub county

            $subcounty = @$reports_model->getSubCounties($row22['subcounty']);
            $subcounty_name = @$subcounty[0]->subCountyName;
            //parish

            $parish = @$reports_model->getParish($row22['parish']);
            $parish_name = @$parish[0]->parishName; 

            //village

            $village = @$reports_model->getVillages($row22['village']);
            $village_name = @$village[0]->villageName;

                $table2 .='<tr>
                <td>'.$count2.'</td>
                <td>'.$row22['refno'].'</td>
                <td>'.$row22['clientname'].'</td>
                <td>'.$row22['sex'].'</td>
                <td>'.$row22['dob'].'</td>
                <td>'.$row22['nationality'].'</td>
                <td>'.$row22['educationlevel'].'</td>
                <td>'.$district_name.'</td>
                <td>'.$county_name.'</td>
                <td>'.$subcounty_name.'</td>
                <td>'.$parish_name.'</td>
                <td>'.$village_name.'</td>                
                <td>'.$row22['altphone'].'</td>
                <td>'.$row22['nameofowner'].'</td>
                <td>'.$row22['relationship'].'</td>
                <td>'.$row22['nextofkin'].'</td>
                <td>'.$row22['contactphone'].'</td>
                <td>'.$row22['contactaddress'].'</td>
                <td>'.$row22['maritalstatus'].'</td>
                <td>'.$row22['sourceofreferral'].'</td>
                <td>'.$row22['Offence'].'</td>                
                <td>'.$row22['Registered_On'].'</td>
                </tr>';
             
        }

    }else{

        $table2 .='<tr>
              <td colspan="23"> <center>No Matching Records.</center></td>
        </tr>';


    }

        $table2 .='</tbody>
        </table>';

        $mpdf=new mPDF('c','A4-L');
        $mpdf->debug = true;
        $mpdf->mirrorMargins = 1;
        $mpdf->list_indent_first_level = 0;
        $mpdf->SetDisplayMode('fullpage');       
        $stylesheet = file_get_contents('static/assets/css/receipt.css');
        $mpdf->WriteHTML($stylesheet,1); 
        $mpdf->WriteHTML($table2);        
        $filename = 'ALL LAP REPORTS.pdf';        
        $mpdf->Output($filename, "I");
        exit;


        }else{

        
                $clinic = $reports_model->get_clinic($clinic_id);               
                $sql2 = "SELECT refno,
                clientname,
                sex,
                language,
                dob,
                nationality,
                educationlevel,
                resdistrict,
                county,
                subcounty,
                parish,
                village,
                ContactPhones,
                altphone,
                nameofowner,
                relationship,
                nextofkin,
                contactphone,
                contactaddress,
                maritalstatus,
                sourceofreferral,
                qus34 AS Offence,
                created_on AS Registered_On FROM mmt_data WHERE clinic_id = '$clinic_id' AND created_on BETWEEN '{$startdate}' AND '{$enddate}'";

         $result2 = $db_connect->query($sql2); 
            $table = '
                <center align="center"> <img src="static/images/logo.png" style="width:90px;display:block;" /></center>
                <h3 style="padding-bottom:12px;text-align:center;border-bottom:1px solid #333;">INTRODUCTION TO LAP REPORT FOR '.$clinic[0]->clinic_name.' DISTRICT BETWEEN '.$startdate.'_TO_'.$enddate.' </h3>
                <p style="font-size:14px;text-align:center">This Table shows all the Introduction to lap cases for '.$clinic[0]->clinic_name.' DISTRICT BETWEEN '.$startdate.'_TO_'.$enddate.'</p>

                <table class="table" border="1" cellspacing="0" cellpadding="0" style="width:100%;" id="customers" >
                <thead>
                <tr>
                <th>S/N</th>
                <th>REF NO</th>
                <th>CLIENT NAME</th>
                <th>SEX</th>
                <th>DOB</th>
                <th>NATIONALITY</th>
                <th>EDUCATION</th>
                <th>DISTRICT</th>
                <th>COUNTY</th>
                <th>SCOUNTY</th>
                <th>PARISH</th>
                <th>VILLAGE</th>
                <th>ALT PHONE</th>
                <th>NAMEOFOWNER</th>
                <th>RELATIONSHIP</th>
                <th>NEXTOFKIN</th>
                <th>CONTACTPHONE</th>
                <th>CONTACTADDRESS</th>
                <th>M.STATUS</th>
                <th>S.O.REFERAL</th>
                <th>OFFENCE</th>
                <th>REGISTERED ON</th>
            </tr>
        </thead>
        <tbody>';

       $num_rows = $result2->num_rows;
        if($num_rows > 0){
        $count = 0;
        while ($row2 = $result2->fetch_array(MYSQLI_ASSOC)) {

            $count++;
                  

            // district                                              
            $district = @$reports_model->getDistrict($row2['resdistrict']);
            $district_name = @$district[0]->districtName;

            //county                      
            $county = @$reports_model->getCounty($row2['county']);
            $county_name = @$county[0]->countyName; 

            //sub county

            $subcounty = @$reports_model->getSubCounties($row2['subcounty']);
            $subcounty_name = @$subcounty[0]->subCountyName;
            //parish

            $parish = @$reports_model->getParish($row2['parish']);
            $parish_name = @$parish[0]->parishName; 

            //village

            $village = @$reports_model->getVillages($row2['village']);
            $village_name = @$village[0]->villageName;

                $table .='<tr>
                <td>'.$count.'</td>
                <td>'.$row2['refno'].'</td>
                <td>'.$row2['clientname'].'</td>
                <td>'.$row2['sex'].'</td>
                <td>'.$row2['dob'].'</td>
                <td>'.$row2['nationality'].'</td>
                <td>'.$row2['educationlevel'].'</td>
                <td>'.$district_name.'</td>
                <td>'.$county_name.'</td>
                <td>'.$subcounty_name.'</td>
                <td>'.$parish_name.'</td>
                <td>'.$village_name.'</td>                
                <td>'.$row2['altphone'].'</td>
                <td>'.$row2['nameofowner'].'</td>
                <td>'.$row2['relationship'].'</td>
                <td>'.$row2['nextofkin'].'</td>
                <td>'.$row2['contactphone'].'</td>
                <td>'.$row2['contactaddress'].'</td>
                <td>'.$row2['maritalstatus'].'</td>
                <td>'.$row2['sourceofreferral'].'</td>
                <td>'.$row2['Offence'].'</td>                
                <td>'.$row2['Registered_On'].'</td>
                </tr>';
             
        }

    }else{

        $table .='<tr>
              <td colspan="23"> <center>No Matching Records.</center></td>
        </tr>';


    }

        $table .='</tbody>
        </table>';

        $mpdf=new mPDF('c','A4-L');
        $mpdf->debug = true;
        $mpdf->mirrorMargins = 1;
        $mpdf->list_indent_first_level = 0;
        $mpdf->SetDisplayMode('fullpage');       
        $stylesheet = file_get_contents('static/assets/css/receipt.css');
        $mpdf->WriteHTML($stylesheet,1); 
        $mpdf->WriteHTML($table);        
        $filename = 'PDP.pdf';        
        $mpdf->Output($filename, "I");

        exit;

        }


    }











        //end  pdf reports
    

        if(isset($_POST['chart']))
        {
            $footer = $this->loadView('common/footer');
            $footer->render();
        }
    }












    public function lapcases()
    {
        $reports_model      = $this->loadModel('reportsModel');
        global $config;
        @$db_connect = new mysqli($config['db_host'], $config['db_username'], $config['db_password'],$config['db_name']);
        
        if(isset($_POST['excel']))
        {
            $clinic_id = $_POST['clinics'];
            $startdate = $_POST['sd'];
            $enddate   = $_POST['ed'];
            if($clinic_id == 'ALL'){
                $output = "";
                $sql = "SELECT uc.client_refnumber,c.lapno ,
                c.casetitle,
                uc.fname,
                uc.lname,
                uc.oname,
                uc.claim,
                uc.classification,
                uc.sex,
                uc.olanguage,
                uc.dob,
                uc.nationality,
                uc.educationlevel,
                uc.agegroup,
                uc.nextofkin,
                uc.contactphone,
                uc.contactaddress,
                uc.clinic_name AS Clinic,
                uc.created_on AS Registered_On ,
                c.is_pro_bono, 
                c.casecategory,
                c.casesubcategory,
                c.probonofileno, c.resolution_type, c.brieffacts FROM BUMAA_clients AS uc INNER JOIN cases AS c ON uc.client_refnumber = c.client_refnumber  WHERE c.regdate BETWEEN '{$startdate}' AND '{$enddate}'";
                
                $result = $db_connect->query($sql);
                $resultObjects = array();
                

                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

               $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'Offence')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'Clinic') {
                            $clinic = @$reports_model->get_clinic($row[$key]);
                            $clinic_name = @$clinic[0]->clinic_name;
                            $output .= '"'.$clinic_name.'",';
                        }elseif ($key == 'resolution_type') {
                            $res = '';
                            if($row[$key] == 1)
                            {
                                $res = 'Mediation';
                            }
                            if($row[$key] == 2)
                            {
                                $res = 'Litigation';
                            }
                            if($row[$key] == 3)
                            {
                                $res = 'Self Representation';
                            }
                            $output .= '"'.$res.'",';
                        }
                        else{
                            $output .= '"'.$row[$key].'",';
                        }
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',"LAP_cases_for_all_clinics_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);
                echo $output;
                exit;
            }else{
                $clinic_id = $_POST['clinics'];
                $clinic = $reports_model->get_clinic($clinic_id);
                $startdate = $_POST['sd'];
                $enddate   = $_POST['ed'];
                $output = "";
                $sql = "SELECT uc.client_refnumber,c.lapno ,
                c.casetitle,
                uc.fname,
                uc.lname,
                uc.oname,
                uc.claim,
                uc.classification,
                uc.sex,
                uc.olanguage,
                uc.dob,
                uc.nationality,
                uc.educationlevel,
                uc.agegroup,
                uc.nextofkin,
                uc.contactphone,
                uc.contactaddress,
                uc.clinic_name AS Clinic,
                uc.created_on AS Registered_On ,
                c.is_pro_bono, 
                c.casecategory,
                c.casesubcategory,
                c.probonofileno, c.resolution_type, c.brieffacts FROM BUMAA_clients AS uc INNER JOIN cases AS c ON uc.client_refnumber = c.client_refnumber WHERE c.clinic_id = '{$clinic_id}' AND c.regdate BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $resultObjects = array();
                

                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'Offence')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resolution_type') {
                            $res = '';
                            if($row[$key] == 1)
                            {
                                $res = 'Mediation';
                            }
                            if($row[$key] == 2)
                            {
                                $res = 'Litigation';
                            }
                            if($row[$key] == 3)
                            {
                                $res = 'Self Representation';
                            }
                            $output .= '"'.$res.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',"LAP_Cases_for_".$clinic[0]->clinic_name."_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);
                echo $output;
                exit;
            }
            
        }


                   //PDF REPORT

        if(isset($_POST['pdf'])){

        $clinic_id = $_POST['clinics'];
        $startdate = $_POST['sd'];
        $enddate   = $_POST['ed'];
        require_once APP_DIR."thirdparty/mpdf/mpdf.php";


        if($clinic_id == 'ALL'){


             $sql22 = "SELECT refno,
                clientname,
                sex,
                language,
                dob,
                nationality,
                educationlevel,
                resdistrict,
                county,
                subcounty,
                parish,
                village,
                ContactPhones,
                altphone,
                nameofowner,
                relationship,
                nextofkin,
                contactphone,
                contactaddress,
                maritalstatus,
                sourceofreferral,
                qus34 AS Offence,
                created_on AS Registered_On FROM mmt_data WHERE  created_on BETWEEN '{$startdate}' AND '{$enddate}' LIMIT 300";

         $result22 = $db_connect->query($sql22); 
            $table2 = '
                <center align="center"> <img src="static/images/logo.png" style="width:90px;display:block;" /></center>
                <h3 style="padding-bottom:12px;text-align:center;border-bottom:1px solid #333;">INTRODUCTION TO LAP REPORT FOR ALL DISTRICTS BETWEEN '.$startdate.'_TO_'.$enddate.'</h3>
                <p style="font-size:14px;text-align:center">This Table shows all the Introduction to lap cases for all Districts Between '.$startdate.'_TO_'.$enddate.'</p>

                <table class="table" border="1" cellspacing="0" cellpadding="0" style="width:100%;" id="customers" >
                <thead>
                <tr>
                <th>S/N</th>
                <th>REF NO</th>
                <th>CLIENT NAME</th>
                <th>SEX</th>
                <th>DOB</th>
                <th>NATIONALITY</th>
                <th>EDUCATION</th>
                <th>DISTRICT</th>
                <th>COUNTY</th>
                <th>SCOUNTY</th>
                <th>PARISH</th>
                <th>VILLAGE</th>
                <th>ALT PHONE</th>
                <th>NAMEOFOWNER</th>
                <th>RELATIONSHIP</th>
                <th>NEXTOFKIN</th>
                <th>CONTACTPHONE</th>
                <th>CONTACTADDRESS</th>
                <th>M.STATUS</th>
                <th>S.O.REFERAL</th>
                <th>OFFENCE</th>
                <th>REGISTERED ON</th>
            </tr>
        </thead>
        <tbody>';

       $num_rows2 = $result22->num_rows;
        if($num_rows2 > 0){
        $count2 = 0;
        while ($row22 = $result22->fetch_array(MYSQLI_ASSOC)) {

            $count2++;
                  

            // district                                              
            $district = @$reports_model->getDistrict($row22['resdistrict']);
            $district_name = @$district[0]->districtName;

            //county                      
            $county = @$reports_model->getCounty($row22['county']);
            $county_name = @$county[0]->countyName; 

            //sub county

            $subcounty = @$reports_model->getSubCounties($row22['subcounty']);
            $subcounty_name = @$subcounty[0]->subCountyName;
            //parish

            $parish = @$reports_model->getParish($row22['parish']);
            $parish_name = @$parish[0]->parishName; 

            //village

            $village = @$reports_model->getVillages($row22['village']);
            $village_name = @$village[0]->villageName;

                $table2 .='<tr>
                <td>'.$count2.'</td>
                <td>'.$row22['refno'].'</td>
                <td>'.$row22['clientname'].'</td>
                <td>'.$row22['sex'].'</td>
                <td>'.$row22['dob'].'</td>
                <td>'.$row22['nationality'].'</td>
                <td>'.$row22['educationlevel'].'</td>
                <td>'.$district_name.'</td>
                <td>'.$county_name.'</td>
                <td>'.$subcounty_name.'</td>
                <td>'.$parish_name.'</td>
                <td>'.$village_name.'</td>                
                <td>'.$row22['altphone'].'</td>
                <td>'.$row22['nameofowner'].'</td>
                <td>'.$row22['relationship'].'</td>
                <td>'.$row22['nextofkin'].'</td>
                <td>'.$row22['contactphone'].'</td>
                <td>'.$row22['contactaddress'].'</td>
                <td>'.$row22['maritalstatus'].'</td>
                <td>'.$row22['sourceofreferral'].'</td>
                <td>'.$row22['Offence'].'</td>                
                <td>'.$row22['Registered_On'].'</td>
                </tr>';
             
        }

    }else{

        $table2 .='<tr>
              <td colspan="23"> <center>No Matching Records.</center></td>
        </tr>';


    }

        $table2 .='</tbody>
        </table>';

        $mpdf=new mPDF('c','A4-L');
        $mpdf->debug = true;
        $mpdf->mirrorMargins = 1;
        $mpdf->list_indent_first_level = 0;
        $mpdf->SetDisplayMode('fullpage');       
        $stylesheet = file_get_contents('static/assets/css/receipt.css');
        $mpdf->WriteHTML($stylesheet,1); 
        $mpdf->WriteHTML($table2);        
        $filename = 'ALL LAP REPORTS.pdf';        
        $mpdf->Output($filename, "I");
        exit;


        }else{

        
                $clinic = $reports_model->get_clinic($clinic_id);               
                $sql2 = "SELECT refno,
                clientname,
                sex,
                language,
                dob,
                nationality,
                educationlevel,
                resdistrict,
                county,
                subcounty,
                parish,
                village,
                ContactPhones,
                altphone,
                nameofowner,
                relationship,
                nextofkin,
                contactphone,
                contactaddress,
                maritalstatus,
                sourceofreferral,
                qus34 AS Offence,
                created_on AS Registered_On FROM mmt_data WHERE clinic_id = '$clinic_id' AND created_on BETWEEN '{$startdate}' AND '{$enddate}'";

         $result2 = $db_connect->query($sql2); 
            $table = '
                <center align="center"> <img src="static/images/logo.png" style="width:90px;display:block;" /></center>
                <h3 style="padding-bottom:12px;text-align:center;border-bottom:1px solid #333;">INTRODUCTION TO LAP REPORT FOR '.$clinic[0]->clinic_name.' DISTRICT BETWEEN '.$startdate.'_TO_'.$enddate.' </h3>
                <p style="font-size:14px;text-align:center">This Table shows all the Introduction to lap cases for '.$clinic[0]->clinic_name.' DISTRICT BETWEEN '.$startdate.'_TO_'.$enddate.'</p>

                <table class="table" border="1" cellspacing="0" cellpadding="0" style="width:100%;" id="customers" >
                <thead>
                <tr>
                <th>S/N</th>
                <th>REF NO</th>
                <th>CLIENT NAME</th>
                <th>SEX</th>
                <th>DOB</th>
                <th>NATIONALITY</th>
                <th>EDUCATION</th>
                <th>DISTRICT</th>
                <th>COUNTY</th>
                <th>SCOUNTY</th>
                <th>PARISH</th>
                <th>VILLAGE</th>
                <th>ALT PHONE</th>
                <th>NAMEOFOWNER</th>
                <th>RELATIONSHIP</th>
                <th>NEXTOFKIN</th>
                <th>CONTACTPHONE</th>
                <th>CONTACTADDRESS</th>
                <th>M.STATUS</th>
                <th>S.O.REFERAL</th>
                <th>OFFENCE</th>
                <th>REGISTERED ON</th>
            </tr>
        </thead>
        <tbody>';

       $num_rows = $result2->num_rows;
        if($num_rows > 0){
        $count = 0;
        while ($row2 = $result2->fetch_array(MYSQLI_ASSOC)) {

            $count++;
                  

            // district                                              
            $district = @$reports_model->getDistrict($row2['resdistrict']);
            $district_name = @$district[0]->districtName;

            //county                      
            $county = @$reports_model->getCounty($row2['county']);
            $county_name = @$county[0]->countyName; 

            //sub county

            $subcounty = @$reports_model->getSubCounties($row2['subcounty']);
            $subcounty_name = @$subcounty[0]->subCountyName;
            //parish

            $parish = @$reports_model->getParish($row2['parish']);
            $parish_name = @$parish[0]->parishName; 

            //village

            $village = @$reports_model->getVillages($row2['village']);
            $village_name = @$village[0]->villageName;

                $table .='<tr>
                <td>'.$count.'</td>
                <td>'.$row2['refno'].'</td>
                <td>'.$row2['clientname'].'</td>
                <td>'.$row2['sex'].'</td>
                <td>'.$row2['dob'].'</td>
                <td>'.$row2['nationality'].'</td>
                <td>'.$row2['educationlevel'].'</td>
                <td>'.$district_name.'</td>
                <td>'.$county_name.'</td>
                <td>'.$subcounty_name.'</td>
                <td>'.$parish_name.'</td>
                <td>'.$village_name.'</td>                
                <td>'.$row2['altphone'].'</td>
                <td>'.$row2['nameofowner'].'</td>
                <td>'.$row2['relationship'].'</td>
                <td>'.$row2['nextofkin'].'</td>
                <td>'.$row2['contactphone'].'</td>
                <td>'.$row2['contactaddress'].'</td>
                <td>'.$row2['maritalstatus'].'</td>
                <td>'.$row2['sourceofreferral'].'</td>
                <td>'.$row2['Offence'].'</td>                
                <td>'.$row2['Registered_On'].'</td>
                </tr>';
             
        }

    }else{

        $table .='<tr>
              <td colspan="23"> <center>No Matching Records.</center></td>
        </tr>';


    }

        $table .='</tbody>
        </table>';

        $mpdf=new mPDF('c','A4-L');
        $mpdf->debug = true;
        $mpdf->mirrorMargins = 1;
        $mpdf->list_indent_first_level = 0;
        $mpdf->SetDisplayMode('fullpage');       
        $stylesheet = file_get_contents('static/assets/css/receipt.css');
        $mpdf->WriteHTML($stylesheet,1); 
        $mpdf->WriteHTML($table);        
        $filename = 'PDP.pdf';        
        $mpdf->Output($filename, "I");

        exit;

        }


    }











        //end  pdf reports











        //end pdf reports 













    }





















    public function pdpcases()
    {
        $reports_model      = $this->loadModel('reportsModel');
        global $config;
        @$db_connect = new mysqli($config['db_host'], $config['db_username'], $config['db_password'],$config['db_name']);
        
        if(isset($_POST['excel']))
        {
            $clinic_id = $_POST['clinics'];
            $startdate = $_POST['sd'];
            $enddate   = $_POST['ed'];
            if($clinic_id == 'ALL'){
                $output = "";
                $sql = "SELECT id,BUMAAno,lapno,prisonername,prisonname,nationality,sex,age,educationlevel,tribe,maritalstatus,religion,addressocc,prisonno,policeno,courtno,offence,restype AS resolution_type,chargedstatus,reasondenial,longremand,longsentence,dateoflastcourt,dateofnextappear,courtName,statusofmatter,lapassist,nextofkin,nextofkincontact,briefsyno,actiontaken,casefile,regdate,clinic_id AS Clinic,created_on FROM pdp_cases  WHERE regdate BETWEEN '{$startdate}' AND '{$enddate}'";
                
                $result = $db_connect->query($sql);
                $resultObjects = array();
                

                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    //echo $heading.",";
                    $output .= '"'.$heading.'",';
                }

               $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'Offence')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'Clinic') {
                            $clinic = @$reports_model->get_clinic($row[$key]);
                            $clinic_name = @$clinic[0]->clinic_name;
                            $output .= '"'.$clinic_name.'",';
                        }elseif ($key == 'resolution_type') {
                            $res = '';
                            if($row[$key] == 1)
                            {
                                $res = 'Mediation';
                            }
                            if($row[$key] == 2)
                            {
                                $res = 'Litigation';
                            }
                            if($row[$key] == 3)
                            {
                                $res = 'Self Representation';
                            }
                            $output .= '"'.$res.'",';
                        }elseif ($key == 'courtName') {
                            $court = @$reports_model->get_court($row[$key]);
                            $court_name = @$court[0]->Court;
                            $output .= '"'.$court_name.'",';
                        }
                        else{
                            $output .= '"'.$row[$key].'",';
                        }
                    }
                    $output .="\n";
                }
            //     // Download the file

                $filename = str_replace(' ', '',"PDP_cases_for_all_clinics_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);
                echo $output;
                exit;
            }else{
                $clinic_id = $_POST['clinics'];
                $clinic = $reports_model->get_clinic($clinic_id);
                $startdate = $_POST['sd'];
                $enddate   = $_POST['ed'];
                $output = "";
                $sql = "SELECT id,BUMAAno,lapno,prisonername,prisonname,nationality,sex,age,educationlevel,tribe,maritalstatus,religion,addressocc,prisonno,policeno,courtno,offence,restype AS resolution_type,chargedstatus,reasondenial,longremand,longsentence,dateoflastcourt,dateofnextappear,courtName,statusofmatter,lapassist,nextofkin,nextofkincontact,briefsyno,actiontaken,casefile,regdate,clinic_id AS Clinic,created_on FROM pdp_cases  WHERE clinic_id = '{$clinic_id}' AND regdate BETWEEN '{$startdate}' AND '{$enddate}'";
                
                $result = $db_connect->query($sql);
                $resultObjects = array();
                

                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    //echo $heading.",";
                    $output .= '"'.$heading.'",';
                }

               $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'Offence')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'Clinic') {
                            $clinic = @$reports_model->get_clinic($row[$key]);
                            $clinic_name = @$clinic[0]->clinic_name;
                            $output .= '"'.$clinic_name.'",';
                        }elseif ($key == 'resolution_type') {
                            $res = '';
                            if($row[$key] == 1)
                            {
                                $res = 'Mediation';
                            }
                            if($row[$key] == 2)
                            {
                                $res = 'Litigation';
                            }
                            if($row[$key] == 3)
                            {
                                $res = 'Self Representation';
                            }
                            $output .= '"'.$res.'",';
                        }elseif ($key == 'courtName') {
                            $court = @$reports_model->get_court($row[$key]);
                            $court_name = @$court[0]->Court;
                            $output .= '"'.$court_name.'",';
                        }
                        else{
                            $output .= '"'.$row[$key].'",';
                        }
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',"PDP_Cases_for_".$clinic[0]->clinic_name."_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);
                echo $output;
                exit;
            }
            
        }
    }
    public function mmtinterview()
    {
        $reports_model      = $this->loadModel('reportsModel');
        global $config;
        @$db_connect = new mysqli($config['db_host'], $config['db_username'], $config['db_password'],$config['db_name']);
        if(isset($_POST['excel']))
        {
            $clinic_id = $_POST['clinics'];
            $startdate = $_POST['sd'];
            $enddate   = $_POST['ed'];
            if($clinic_id == 'ALL'){
                $sql = "SELECT * FROM mmt_data WHERE created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $output = "";
                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }
                $output .="\n";
                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'qus34')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'clinic_id') {
                            $clinic = @$reports_model->get_clinic($row[$key]);
                            $clinic_name = @$clinic[0]->clinic_name;
                            $output .= '"'.$clinic_name.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }
                        else{
                            $output .= '"'.$row[$key].'",';
                        }
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',"Clients_Interviewed_for_all_clinics_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
               exit;
            }else{
                $clinic_id = $_POST['clinics'];
                $clinic = $reports_model->get_clinic($clinic_id);
                $output = "";
                $sql = "SELECT * FROM mmt_data WHERE clinic_id = '{$clinic_id}' AND created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $resultObjects = array();
                

                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'qus34')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',"Clients_Interviewed_for_".$clinic[0]->clinic_name."_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }
            
        }





            //PDF REPORT

        if(isset($_POST['pdf'])){

        $clinic_id = $_POST['clinics'];
        $startdate = $_POST['sd'];
        $enddate   = $_POST['ed'];
        require_once APP_DIR."thirdparty/mpdf/mpdf.php";


        if($clinic_id == 'ALL'){


             $sql22 = "SELECT refno,
                clientname,
                sex,
                language,
                dob,
                nationality,
                educationlevel,
                resdistrict,
                county,
                subcounty,
                parish,
                village,
                ContactPhones,
                altphone,
                nameofowner,
                relationship,
                nextofkin,
                contactphone,
                contactaddress,
                maritalstatus,
                sourceofreferral,
                qus34 AS Offence,
                created_on AS Registered_On FROM mmt_data WHERE  created_on BETWEEN '{$startdate}' AND '{$enddate}' LIMIT 300";

         $result22 = $db_connect->query($sql22); 
            $table2 = '
                <center align="center"> <img src="static/images/logo.png" style="width:90px;display:block;" /></center>
                <h3 style="padding-bottom:12px;text-align:center;border-bottom:1px solid #333;">INTERVIEWED CLIENTS FOR ALL DISTRICTS </h3>
                <p style="font-size:14px;text-align:center">Interviewed Clients for all the  Districts Between '.$startdate.'_TO_'.$enddate.'</p>

                <table class="table" border="1" cellspacing="0" cellpadding="0" style="width:100%;" id="customers" >
                <thead>
                <tr>
                <th>S/N</th>
                <th>REF NO</th>
                <th>CLIENT NAME</th>
                <th>SEX</th>
                <th>DOB</th>
                <th>NATIONALITY</th>
                <th>EDUCATION</th>
                <th>DISTRICT</th>
                <th>COUNTY</th>
                <th>SCOUNTY</th>
                <th>PARISH</th>
                <th>VILLAGE</th>
                <th>ALT PHONE</th>
                <th>NAMEOFOWNER</th>
                <th>RELATIONSHIP</th>
                <th>NEXTOFKIN</th>
                <th>CONTACTPHONE</th>
                <th>CONTACTADDRESS</th>
                <th>M.STATUS</th>
                <th>S.O.REFERAL</th>
                <th>OFFENCE</th>
                <th>REGISTERED ON</th>
            </tr>
        </thead>
        <tbody>';

       $num_rows2 = $result22->num_rows;
        if($num_rows2 > 0){
        $count2 = 0;
        while ($row22 = $result22->fetch_array(MYSQLI_ASSOC)) {

            $count2++;
                  

            // district                                              
            $district = @$reports_model->getDistrict($row22['resdistrict']);
            $district_name = @$district[0]->districtName;

            //county                      
            $county = @$reports_model->getCounty($row22['county']);
            $county_name = @$county[0]->countyName; 

            //sub county

            $subcounty = @$reports_model->getSubCounties($row22['subcounty']);
            $subcounty_name = @$subcounty[0]->subCountyName;
            //parish

            $parish = @$reports_model->getParish($row22['parish']);
            $parish_name = @$parish[0]->parishName; 

            //village

            $village = @$reports_model->getVillages($row22['village']);
            $village_name = @$village[0]->villageName;

                $table2 .='<tr>
                <td>'.$count2.'</td>
                <td>'.$row22['refno'].'</td>
                <td>'.$row22['clientname'].'</td>
                <td>'.$row22['sex'].'</td>
                <td>'.$row22['dob'].'</td>
                <td>'.$row22['nationality'].'</td>
                <td>'.$row22['educationlevel'].'</td>
                <td>'.$district_name.'</td>
                <td>'.$county_name.'</td>
                <td>'.$subcounty_name.'</td>
                <td>'.$parish_name.'</td>
                <td>'.$village_name.'</td>                
                <td>'.$row22['altphone'].'</td>
                <td>'.$row22['nameofowner'].'</td>
                <td>'.$row22['relationship'].'</td>
                <td>'.$row22['nextofkin'].'</td>
                <td>'.$row22['contactphone'].'</td>
                <td>'.$row22['contactaddress'].'</td>
                <td>'.$row22['maritalstatus'].'</td>
                <td>'.$row22['sourceofreferral'].'</td>
                <td>'.$row22['Offence'].'</td>                
                <td>'.$row22['Registered_On'].'</td>
                </tr>';
             
        }

    }else{

        $table2 .='<tr>
              <td colspan="23"> <center>No Matching Records.</center></td>
        </tr>';


    }

        $table2 .='</tbody>
        </table>';

        $mpdf=new mPDF('c','A4-L');
        $mpdf->debug = true;
        $mpdf->mirrorMargins = 1;
        $mpdf->list_indent_first_level = 0;
        $mpdf->SetDisplayMode('fullpage');       
        $stylesheet = file_get_contents('static/assets/css/receipt.css');
        $mpdf->WriteHTML($stylesheet,1); 
        $mpdf->WriteHTML($table2);        
        $filename = 'ALL LAP REPORTS.pdf';        
        $mpdf->Output($filename, "I");
        exit;


        }else{

        
                $clinic = $reports_model->get_clinic($clinic_id);               
                $sql2 = "SELECT refno,
                clientname,
                sex,
                language,
                dob,
                nationality,
                educationlevel,
                resdistrict,
                county,
                subcounty,
                parish,
                village,
                ContactPhones,
                altphone,
                nameofowner,
                relationship,
                nextofkin,
                contactphone,
                contactaddress,
                maritalstatus,
                sourceofreferral,
                qus34 AS Offence,
                created_on AS Registered_On FROM mmt_data WHERE clinic_id = '$clinic_id' AND created_on BETWEEN '{$startdate}' AND '{$enddate}'";

         $result2 = $db_connect->query($sql2); 
            $table = '
                <center align="center"> <img src="static/images/logo.png" style="width:90px;display:block;" /></center>
                <h3 style="padding-bottom:12px;text-align:center;border-bottom:1px solid #333;">INTERVIEWED CLIENTS FOR '.$clinic[0]->clinic_name.' DISTRICT BETWEEN '.$startdate.'_TO_'.$enddate.' </h3>
                <p style="font-size:14px;text-align:center">Interviewed Clients for '.$clinic[0]->clinic_name.' DISTRICT BETWEEN '.$startdate.'_TO_'.$enddate.' </p>

                <table class="table" border="1" cellspacing="0" cellpadding="0" style="width:100%;" id="customers" >
                <thead>
                <tr>
                <th>S/N</th>
                <th>REF NO</th>
                <th>CLIENT NAME</th>
                <th>SEX</th>
                <th>DOB</th>
                <th>NATIONALITY</th>
                <th>EDUCATION</th>
                <th>DISTRICT</th>
                <th>COUNTY</th>
                <th>SCOUNTY</th>
                <th>PARISH</th>
                <th>VILLAGE</th>
                <th>ALT PHONE</th>
                <th>NAMEOFOWNER</th>
                <th>RELATIONSHIP</th>
                <th>NEXTOFKIN</th>
                <th>CONTACTPHONE</th>
                <th>CONTACTADDRESS</th>
                <th>M.STATUS</th>
                <th>S.O.REFERAL</th>
                <th>OFFENCE</th>
                <th>REGISTERED ON</th>
            </tr>
        </thead>
        <tbody>';

       $num_rows = $result2->num_rows;
        if($num_rows > 0){
        $count = 0;
        while ($row2 = $result2->fetch_array(MYSQLI_ASSOC)) {

            $count++;
                  

            // district                                              
            $district = @$reports_model->getDistrict($row2['resdistrict']);
            $district_name = @$district[0]->districtName;

            //county                      
            $county = @$reports_model->getCounty($row2['county']);
            $county_name = @$county[0]->countyName; 

            //sub county

            $subcounty = @$reports_model->getSubCounties($row2['subcounty']);
            $subcounty_name = @$subcounty[0]->subCountyName;
            //parish

            $parish = @$reports_model->getParish($row2['parish']);
            $parish_name = @$parish[0]->parishName; 

            //village

            $village = @$reports_model->getVillages($row2['village']);
            $village_name = @$village[0]->villageName;

                $table .='<tr>
                <td>'.$count.'</td>
                <td>'.$row2['refno'].'</td>
                <td>'.$row2['clientname'].'</td>
                <td>'.$row2['sex'].'</td>
                <td>'.$row2['dob'].'</td>
                <td>'.$row2['nationality'].'</td>
                <td>'.$row2['educationlevel'].'</td>
                <td>'.$district_name.'</td>
                <td>'.$county_name.'</td>
                <td>'.$subcounty_name.'</td>
                <td>'.$parish_name.'</td>
                <td>'.$village_name.'</td>                
                <td>'.$row2['altphone'].'</td>
                <td>'.$row2['nameofowner'].'</td>
                <td>'.$row2['relationship'].'</td>
                <td>'.$row2['nextofkin'].'</td>
                <td>'.$row2['contactphone'].'</td>
                <td>'.$row2['contactaddress'].'</td>
                <td>'.$row2['maritalstatus'].'</td>
                <td>'.$row2['sourceofreferral'].'</td>
                <td>'.$row2['Offence'].'</td>                
                <td>'.$row2['Registered_On'].'</td>
                </tr>';
             
        }

    }else{

        $table .='<tr>
              <td colspan="23"> <center>No Matching Records.</center></td>
        </tr>';


    }

        $table .='</tbody>
        </table>';

        $mpdf=new mPDF('c','A4-L');
        $mpdf->debug = true;
        $mpdf->mirrorMargins = 1;
        $mpdf->list_indent_first_level = 0;
        $mpdf->SetDisplayMode('fullpage');       
        $stylesheet = file_get_contents('static/assets/css/receipt.css');
        $mpdf->WriteHTML($stylesheet,1); 
        $mpdf->WriteHTML($table);        
        $filename = 'PDP.pdf';        
        $mpdf->Output($filename, "I");

        exit;

        }


    }







    

        if(isset($_POST['chart']))
        {
            $footer = $this->loadView('common/footer');
            $footer->render();
        }
    }

    public function sensitization()
    {
        $reports_model      = $this->loadModel('reportsModel');
        global $config;
        @$db_connect = new mysqli($config['db_host'], $config['db_username'], $config['db_password'],$config['db_name']);
        $clinic = $_POST['clinics'];
        $visit_category = $_POST['visit_category'];
        $startdate = $_POST['sd'];
        $enddate   = $_POST['ed'];

        if($clinic == 'ALL')
        {

            if($visit_category == 'school')
            {
                $sql = "SELECT * FROM schools_visits WHERE created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $output = "";
                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'qus34')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',$visit_category."_sensitization_for_all_clinics_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }

            if($visit_category == 'police')
            {
                $sql = "SELECT * FROM police_visits WHERE created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $output = "";
                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'qus34')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',$visit_category."_sensitization_for_all_clinics_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }

            if($visit_category == 'prison')
            {
                $sql = "SELECT * FROM prison_visits WHERE created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $output = "";
                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'qus34')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',$visit_category."_sensitization_for_all_clinics_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }

            if($visit_category == 'community')
            {
                $sql = "SELECT * FROM community_visits WHERE created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $output = "";
                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'qus34')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',$visit_category."_sensitization_for_all_clinics_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }
        }else{
            $clinic_id = $_POST['clinics'];
            $clinic = $reports_model->get_clinic($clinic_id);
            if($visit_category == 'school')
            {
                $sql = "SELECT * FROM schools_visits WHERE clinic_id = '{$clinic_id}' AND created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $output = "";
                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'qus34')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',$visit_category."_sensitization__for_".$clinic[0]->clinic_name."_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;

            }

            if($visit_category == 'police')
            {
                $sql = "SELECT * FROM police_visits WHERE clinic_id = '{$clinic_id}' AND created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $output = "";
                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'qus34')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',$visit_category."_sensitization__for_".$clinic[0]->clinic_name."_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }

            if($visit_category == 'prison')
            {
                $sql = "SELECT * FROM prison_visits WHERE clinic_id = '{$clinic_id}' AND created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $output = "";
                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'qus34')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',$visit_category."_sensitization__for_".$clinic[0]->clinic_name."_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }

            if($visit_category == 'community')
            {
                $sql = "SELECT * FROM community_visits WHERE clinic_id = '{$clinic_id}' AND created_on BETWEEN '{$startdate}' AND '{$enddate}'";
                $result = $db_connect->query($sql);
                $output = "";
                while ($finfo = $result->fetch_field()) {
                    $heading = strtoupper($finfo->name);
                    $output .= '"'.$heading.'",';
                }

                $output .="\n";

                while($row = $result->fetch_array(MYSQLI_ASSOC)){
                    foreach ($row as $key => $value) {
                        if($key == 'qus34')
                        {
                            $r = @explode('|',$row[$key])[1];
                            $output .= '"'.$r.'",';
                        }elseif ($key == 'resdistrict') {
                            $district = @$reports_model->getDistrict($row[$key]);
                            $district_name = @$district[0]->districtName;
                            $output .= '"'.$district_name.'",';
                        }
                        elseif ($key == 'county') {
                            $county = @$reports_model->getCounty($row[$key]);
                            $county_name = @$county[0]->countyName;
                            $output .= '"'.$county_name.'",';
                        }
                        elseif ($key == 'subcounty') {
                            $subcounty = @$reports_model->getSubCounties($row[$key]);
                            $subcounty_name = @$subcounty[0]->subCountyName;
                            $output .= '"'.$subcounty_name.'",';
                        }
                        elseif ($key == 'parish') {
                            $parish = @$reports_model->getParish($row[$key]);
                            $parish_name = @$parish[0]->parishName;
                            $output .= '"'.$parish_name.'",';
                        }
                        elseif ($key == 'village') {
                            $village = @$reports_model->getVillages($row[$key]);
                            $village_name = @$village[0]->villageName;
                            $output .= '"'.$village_name.'",';
                        }else{
                            $output .= '"'.$row[$key].'",';
                        }
                        
                    }
                    $output .="\n";
                }
                // Download the file

                $filename = str_replace(' ', '',$visit_category."_sensitization__for_".$clinic[0]->clinic_name."_between_".$startdate."_to_".$enddate.".csv");
                header('Content-type: application/csv');
                header('Content-Disposition: attachment; filename='.$filename);

                echo $output;
                exit;
            }
        }


    }
}

?>

