<?php
class page_not_found extends controller
{
    function index()
	{
		$this->error404();
	}
	
	function error404()
	{
		
        $errorPage = $this->loadView("404/index");
        $errorPage->render();
	}
}