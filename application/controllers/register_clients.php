<?php 

class register_clients extends controller {

    public function main()
    {
        $user_info      = $this->bootstrap();
        $homeModel      = $this->loadModel('homeModel');
        $bumaa_clients = $this->loadModel('regModel');
        $all_clients = $bumaa_clients->get_bumaa_clients();
        $locationModel  = $this->loadModel('location');       
        $logModel = $this->loadModel("systemLogModel"); 
        $url_seg = $this->loadHelper('url_helper');
        $current_clients = $bumaa_clients->get_bumaa_clients_current();
        $current_company = $bumaa_clients->get_bumaa_company_current();
        $segment = $url_seg->segment(3);     
        $districts      = $locationModel->get_districts();       
        $head    = $this->loadView('common/header');
        $head->set('user_info',$user_info); 
        $head->render();
        $content    = $this->loadView('members/search');
        $content->set('pageTitle',' MEMBERS - BUMAA IMS');
        $content->set('user_info',$user_info);
        $content->set('all_clients',$all_clients);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();
    }



//pending registration

    public function companies()
    {
        $user_info      = $this->bootstrap();       
        $bumaa_clients = $this->loadModel('regModel');
        $registered_companies = $bumaa_clients->registeredCompanies();
        $head    = $this->loadView('common/header');
        $head->set('user_info',$user_info);
        $head->render();
        $content = $this->loadView('members/pending_acknowledgement');
        $content->set('pageTitle',' REGISTERED COMPANIES- BUMAA IMS');
        $content->set('user_info',$user_info);
        $content->set('pending_registration',$registered_companies);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();
    }



//all clients

 public function members()
    {
        $user_info      = $this->bootstrap();
        $homeModel      = $this->loadModel('homeModel');
        $bumaa_clients = $this->loadModel('regModel');
        $all_clients = $bumaa_clients->get_bumaa_clients();
        $locationModel  = $this->loadModel('location');       
        $logModel = $this->loadModel("systemLogModel"); 
        $url_seg = $this->loadHelper('url_helper');
        $segment = $url_seg->segment(3);     
        $districts      = $locationModel->get_districts();       
        $head    = $this->loadView('common/header');
        $head->set('user_info',$user_info);      
       
        $head->render();
        $content    = $this->loadView('members/index');
        $content->set('pageTitle',' CLIENTS - BUMAA IMS');
        $content->set('user_info',$user_info);
        $content->set('all_clients',$all_clients);
        
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();
    }



    //add client

    public function register_alumni()
    {
        $user_info      = $this->bootstrap();
        $locationModel  = $this->loadModel('location');
        $regModel       = $this->loadModel('regModel');
        $administrativeModel = $this->loadModel('administrativeModel');
        $all_clinics = $administrativeModel->getClinics();
        $getCourses = $administrativeModel->getCourses();        
        $head    = $this->loadView('common/header');
        $head->set('user_info',$user_info);
        $head->render();
        $content  = $this->loadView('members/add_member');
        $content->set('pageTitle',' REGISTER ALUMNI - BUMAA IMS');        
        $content->set('all_clinics',$all_clinics);
        $content->set('getCourses', $getCourses);
        $content->set('user_info',$user_info);        
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();
    }


    public function register_company()
    {
        $user_info      = $this->bootstrap();
        $locationModel  = $this->loadModel('location');
        $regModel       = $this->loadModel('regModel');        
        $head    = $this->loadView('common/header');
        $head->set('user_info',$user_info);
        $head->render();
        $content  = $this->loadView('members/add_company');
        $content->set('pageTitle',' REGISTER COMPANY - BUMAA IMS');
        $content->set('user_info',$user_info);        
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();
    }

 public function member_details($id){

    $user_info      = $this->bootstrap();
    $segment = base64_decode($id);
    $bumaa_clients = $this->loadModel('regModel');
    $details = $bumaa_clients->get_details($segment);
    $head    = $this->loadView('common/header');
    $head->set('user_info',$user_info); 
    $head->render();
    //
    $content    = $this->loadView('members/member_details');
    $content->set('pageTitle',' DETAILS');
    $content->set('user_info',$user_info);
    $content->set('details',$details);
    $content->render();
    //
    $footer = $this->loadView('common/footer');
    $footer->render();

 }



  public function company_details($id){

    $user_info      = $this->bootstrap();
    $segment = base64_decode($id);
    $bumaa_clients = $this->loadModel('regModel');
    $details = $bumaa_clients->get_details($segment);
    $head    = $this->loadView('common/header');
    $head->set('user_info',$user_info); 
    $head->render();
    //
    $content    = $this->loadView('members/companyDetails');
    $content->set('pageTitle',' DETAILS');
    $content->set('user_info',$user_info);
    $content->set('details',$details);
    $content->render();
    //
    $footer = $this->loadView('common/footer');
    $footer->render();

 }




    public function med(){

        $regModel  = $this->loadModel('regModel');
        $uploader = $this->loadHelper('uploader');
        $logModel = $this->loadModel("systemLogModel");
        $data = $uploader->upload($_FILES['photo'], $this->upload_array());
        if($data['isComplete'])
        {
            $info      = $data['data'];           
            if(!empty($info['metas'][0]['name'])){
                $photo     = urlencode($info['metas'][0]['name']);
            }else{
                $photo  = "";
            }
             $client_refnumber = $this->generate_bumaa_client_refnumber();

            $form_feedback = $regModel->addClient(
                $client_refnumber,
                $photo,                                
                $_POST['fname'],
                $_POST['lname'],               
                $_POST['contactphone'],                
                $_POST['sex'],
                $_POST['tribe'],                              
                $_POST['educationlevel'],
                $_POST['maritalstatus'],                
                $_POST['nationality'],
                $_POST['contactaddress'],               
                $_POST['resdistrict'],
                $_POST['county'],
                $_POST['subcounty'],
                $_POST['parish'],
                $_POST['village'],
                $user_info['id'],
                $_POST['clinic_id'],
                $_POST['email'],
                $_POST['company'],
 
            );
            

            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
        }

        if($data['hasErrors']){
            $errors = json_encode( $data['errors'] );
            header('Content-Type: application/json');
            echo $errors;
        }        
    }


    //generate pin
    public function generatePIN($digits = 3)
    {
        $i = 0;
        $pin = "";
        while ($i <= $digits) {
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }
//generating number
    public function generate_bumaa_client_refnumber()
    {                     
        $client_count =  $this->generatePIN();
        $year         = date('Y');
        $no_ref       = $client_count;
        $ref_number   = "BUMAA/{$no_ref}/{$year}";
        return $ref_number;
    }


      public function generate_bumaa_backlog_client_refnumber($clinic_name)
    {
          
        $client_count =  $this->generatePIN();
        $year         = date('Y');
        $no_ref       = $client_count;
        $ref_number   = "BUMAA/{$no_ref}/{$year}B";
        return $ref_number;
    }


    public function change_photo()
    {
        $user_info = $this->bootstrap();
        $regModel  = $this->loadModel('regModel');
        $uploader = $this->loadHelper('uploader');
        $logModel = $this->loadModel("systemLogModel");
        $data = $uploader->upload($_FILES['photo'],$this->upload_array());
        if($data['isComplete'])
        {
            $info      = $data['data'];
            if(!empty($info['metas'][0]['name'])){
                $photo     = urlencode($info['metas'][0]['name']);
            }else{
                $photo  = "";
            }
        
            $form_feedback = $regModel->updatephoto(
                $_POST['ref_no_'],
                $photo
            );
            $lapno = $_POST['ref_no_'];
            
            if($form_feedback['status'])
            {
                $log = $logModel->writeLog("changed client photo with refnumber : {$lapno}  from IP : ".IP,$user_info['id'],$user_info['clinic_id'],"SUCCESS");
            }
            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
        }

        if($data['hasErrors']){
            $errors = json_encode( array('msg'=>$data['errors'], 'status'=>true )  );
            header('Content-Type: application/json');
            echo $errors;
        }
    }

   
    public function makeSearch()
    {
        $regModel       = $this->loadModel('regModel');       
        $form_feedback  = $regModel->search_client($_POST['search']);
        $res = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res;
    }



    public function delete_user($user_id)
    {
        $user_info  = $this->bootstrap();
        $regModel  = $this->loadModel('regModel');
        $deluser  = $regModel->get_del_user(base64_decode($user_id));
        $delete  =  @($regModel->del_user($deluser[0]->id));
        if($delete)
        {          
            $this->redirect('register_clients/members');
        }        

    }

//deactivate

    public function NewDeativate($user_id)
    {
        $user_info  = $this->bootstrap();
        $regModel  = $this->loadModel('regModel');
        $deactuser = $regModel->get_user_to_change(base64_decode($user_id));
        $form_feedback  =  ($regModel->new_deact_user($deactuser[0]->id,$deactuser[0]->status,$deactuser[0]->username));
        if($form_feedback)
        {            
            $this->redirect('register_clients/members');           
        }
    }


    public function manager_alumni($user_id)
    {
        $user_info      = $this->bootstrap();     
        $homeModel      = $this->loadModel('homeModel');
        $accounts_model      = $this->loadModel('accountsModel');
        $regModel = $this->loadModel('regModel'); 
        $user     = $regModel->user_information(base64_decode($user_id));
        $user_roles    = $regModel->get_user_roles($user['username']);
        $head   = $this->loadView('common/header');
        $head->set('user_info',$user_info);        
        $head->set('pageTitle','Manage Account - BUMAA IMS');
        $head->render();
        $content = $this->loadView('members/manageAlumni');
        $content->set('pageTitle','Manage Account - BUMAA IMS');
        $content->set('users',$users);
        $content->set('user',$user);
        $content->set('user_roles',$user_roles);
        $content->set('user_info',$user_info);
        $content->set('accounts_model',$accounts_model);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();
    }


//default role

    public function default_role(){

        $user_info  = $this->bootstrap();        
        $logModel      = $this->loadModel('systemLogModel');
        $usermodel  = $this->loadModel('userModel'); 
        //load accounts model
         $accountsModel  = $this->loadModel('accountsModel');       
        $id = $user_info['id'];
         @$default_clinic = $_GET['default_clinic'];
         @$default_role = $_GET['default_role'];
         $default_role_name = $accountsModel->get_usercatergory($default_role); 
         $default_clinic_name = $accountsModel->get_clinic($default_clinic);
         $real_role_name = $default_role_name[0]->category_name; 
         $real_clinic_name =  $default_clinic_name[0]->clinic_name;     
        $changeDefault  = $usermodel->defaultRole($default_role,$id, $default_clinic,$real_role_name);        
       // $this->redirect('user_accounts/swtichaccess');        
        $reswitch = json_encode($changeDefault);
        header('Content-Type: application/json');
        echo $reswitch;



    }


    public function add_access()
    {
        $user_info      = $this->bootstrap();
        $regModel      = $this->loadModel('regModel');
        $form_feedback = $regModel->addAccess(
            $_POST['username'],
            $_POST['accesslevel'],
            $_POST['clinics']
        );
        $res = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res;
    }
   

    public function revoke_access($roleId)
    {
        $user_info      = $this->bootstrap();
        $regModel      = $this->loadModel('regModel');
        $roleID = base64_decode($roleId);
        $revoke_feedback = $regModel->revokeAccess($roleID);
        $this->redirect('register_clients/members');       
    }

    //switch access
    public function switchaccess()
    {
        $user_info      = $this->bootstrap();  
        $accounts_model      = $this->loadModel('accountsModel');
        $regModel = $this->loadModel('regModel');  
        $user           = $regModel->user_information($user_info['id']);
        $user_roles    = $regModel->get_user_roles($user['username']);
        $head   = $this->loadView('common/header');
        $head->set('user_info',$user_info);  
        $head->set('pageTitle','Switch Access - BUMAA IMS');
        $head->render();
        $content = $this->loadView('administrative/switch');
        $content->set('user_info',$user_info);
        $content->set('user_roles',$user_roles);
        $content->set('accounts_model',$accounts_model);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();
    }


    public function switch_clinic(){
        $user_info      = $this->bootstrap();
        $logModel      = $this->loadModel('systemLogModel');
        $regModel  = $this->loadModel('regModel');
        $curClinic = $user_info['clinic_name'];
        $curRole = $user_info['default_role_name'];
        $Id = $user_info['id'];
        list($clinicId, $roleId) = explode("_", $_POST['clinicId_roleId'], 2);       
        $switch_feedback = $regModel->switchClinic($Id,$roleId,$clinicId);
        $log = $logModel->logSwitch($Id,"switched clinic to ",$clinicId,"role ",$roleId,$curClinic,$curRole,"SUCCESS");
        $reswitch = json_encode($switch_feedback);
        header('Content-Type: application/json');
        echo $reswitch;
    }
  

    public function my_history()
    {
        $user_info      = $this->bootstrap();
        $logModel      = $this->loadModel('systemLogModel');
        $regModel = $this->loadModel('regModel');
        $catModel= $this->loadModel('catModel');    
        $logs          = ($user_info['id']);
        
        $head   = $this->loadView('common/header');
        $head->set('user_info',$user_info); 
        $head->set('pageTitle','System Logs - BUMAA IMS');
        $head->render();
         //
        $content = $this->loadView('accounts/mylogs');
        $content->set('user_info',$user_info);
        $content->set('logModel',$logModel);
        $content->set('logs',$logs);
        $content->set('accounts_model',$accounts_model);
        $content->render();
          //
        $footer = $this->loadView('common/footer');
        $footer->render();
    }


    private function upload_array(){

        return  array(
            'limit' => 100, //Maximum Limit of files. {null, Number}
            'maxSize' => 1024, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => true, //Minimum one file is required for upload {Boolean}
            'uploadDir' => UPLOAD_DIR, //Upload directory {String}
            'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => true, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        );
    }
}