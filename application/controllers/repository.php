<?php class repository extends controller
{
    
    public function index()
    {
        $user_info = $this->bootstrap();
        $pageTitle = "Document Repository"; 
        $docsModel = $this->loadModel('docsModel');
        $files = $docsModel->getAlldocs();
        $head =     $this->loadView('common/header');  
        $head->set('user_info',$user_info);            
        $head->render();
        $content = $this->loadView('repository/index');
        $content->set('pageTitle', $pageTitle);
        $content->set('user_info',$user_info);
        $content->set('files',$files);
        $content->render();
        $footer  = $this->loadView('common/footer');       
        $footer->render();
    }


      public function create(){
        $user_info = $this->bootstrap();
        $regModel = $this->loadModel('regModel');
        $pageTitle = "Document Repository";        
        $docsModel = $this->loadModel('docsModel');
        $userList = $regModel->getAllUsers();
        $head =  $this->loadView('common/header');
        $head->set('user_info',$user_info); 
        $head->render();
        $content = $this->loadView('repository/create');
        $content->set('pageTitle', $pageTitle);
        $content->set('user_info',$user_info);
        $content->set('userList',$userList);
        $content->set('docsModel',$docsModel);       
        $content->render();
        $footer  = $this->loadView('common/footer');       
        $footer->render();
      }



    public function add_document()
    { 
        if(isset($_POST['upload']))
        {  
            $uploader = $this->loadHelper('uploader');
            //$uploader = new upload_helper();
            $data = $uploader->upload($_FILES['files'], array(
                'limit' => 10, //Maximum Limit of files. {null, Number}
                'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
                'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
                'required' => false, //Minimum one file is required for upload {Boolean}
                'uploadDir' => UPLOAD_DIR, //Upload directory {String}
                'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
                'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
                'perms' => null, //Uploaded file permisions {null, Number}
                'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
                'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
                'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
                'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
                'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
                'onRemove' => 'onFilesRemoveCallback' //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
            ));

            if($data['isComplete']){
                $files = $data['data'];                
                $title = $_POST["title"];
                $description = $_POST["description"];
                $owner = $user_info['id'];
                $sharedWith = json_encode($_POST["sharewith"]);                
                $docsModel = $this->loadModel('docsModel');
                $filename = $files['metas'][0]['name'];
                @$feedback = "<div class='alert alert-block alert-success'><strong>SUCCESS : </strong> File(s) Uploaded Successfully ....</div>";
                @$feedback = $docsModel->upload_doc($title,$description,$sharedWith,$filename,$owner);
                 $this->redirect('repository');
            }

            if($data['hasErrors']){               
                @$feedback = "<div class='alert alert-block alert-danger'><strong>ERROR : </strong> File(s) Uploaded Unsuccessfully .... {$errors[0]}</div>";
                  $this->redirect('repository');

            }
            function onFilesRemoveCallback($removed_files){
                foreach($removed_files as $key=>$value){
                    $file =  UPLOAD_DIR.$value;
                    if(file_exists($file)){
                        unlink($file);
                    }
                }
                return $removed_files;
            }
        }
           
    }



    public function getFiles()
	{
        $excludes = array(".","..");
        $path = UPLOAD_DIR;
        $basepath = UPLOAD_DIR;
        $files = "";
        $count = 0;
        if ($handle = scandir($path)) { 
        foreach($handle as $file)
        {   
            $filename = $path."".$file;
            $size = $this->formatBytes(filesize($filename));
            $time = date ("Y-m-j H:i:s",fileatime($filename));//2010-04-28 17:25:43
            $time_sorted = strtotime($time);
            $modified = $this->humanTiming($time_sorted).' ago';
            
            $filetype = $this->getFileType(substr(strrchr($file,"."),1));
            if(is_dir($filename)&& !(in_array($file,$excludes)))
            {    
                    $count++;   
                    $filetype = "dir";
                    $files .="<tr>
                    <td>{$count}</td>
                    <td><span class='file file-{$filetype}'>{$file}</span></td>
                    <td>{$size}</td>
                    <td class='text-info'>{$modified}</td>
                    <td><a href=''><i class='fa fa-pencil'></i></a></td>
                    <td><a href=''><i class='fa fa-trash'></i></a></td>
                    <td><a href=''><i class='fa fa-link'></i></a></td>
                    <td></td>
                    </tr>";
                    

            }	
            elseif(!(in_array($file,$excludes)))
            {
                $count++;
                $files .="<tr>
                    <td>{$count}</td>
                    <td><span class='file file-{$filetype}'>{$file}</span></td>
                    <td>{$size}</td>
                    <td class='text-info'>{$modified}</td>
                    <td><a href=''><i class='fa fa-pencil'></i></a></td>
                    <td><a href='".URL."document-repository/delete/{$file}'><i class='fa fa-trash text-danger'></i></a></td>
                    <td><a href='".URL."document-repository/dl/{$file}'><i class='fa fa-link text-success'></i></a></td>
                    <td><a href='".URL."document-repository/view/{$file}'><i class='fa fa-link text-success'></i></a></td>
                    <td><input type='checkbox'/></td>
                    </tr>";
            }    
        }
    }
        if(strlen($files)==0)
        {
                $files = "<tr><td colspan='7'><div class='well text-center'>Oops, You Don't Have Any Files as Yet . <em><a href=\"document-repository/add-document\">Start Uploading Now !</a></em></div></td></tr>";
        }
        
		return $files;
	}


    public function humanTiming ($time)
    {
        $time = time() - $time; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($tokens as $unit => $text) {
            if ($time < $unit) continue;
            $numberOfUnits = floor($time / $unit);
            return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
        }

    }

    public function getFileType($extension)
    {   
        $audio  = array('mp3','wav','aac','wma'); 
        $images = array('jpg','gif','png','bmp','JPG');
        $office = array('doc','docx','odt');
        $text 	= array('txt','rtf','log');
        $apps 	= array('dll','exe','EXE','msi');
        $php = array('php');
        $scripts   = array('js','py','asp','css','xml','sql');
        $html   = array('html','htm');
        $pdf    = array('pdf');
        $ai    = array('ai');
        $nuke    = array('nk');
        $aep    = array('aep');
        $zip    = array('zip');
        $ppt   = array('ppt','pptx','pps');
        $accdb = array('accdb');
        $prproj = array('prproj');
        $psd  = array('psd');
        $video = array('VOB','avi','mpg','m2v','mov','mp4','flv');
        $rar = array('tar','tgz','deb','rmp','rar','gz');
        $excel = array('xlsx','xls');
        $flash =  array('fla');
        $swf   = array('swf');
        $iso   = array('iso','nrg','cue','mdx','b5t','isz','mds','b6t','cue','cdi','ccd','bwt','pdi','mdf');
        $publisher = array('pub');
      //
        if(in_array($extension, $php)) return "php";
        if(in_array($extension, $audio)) return "audio";
        if(in_array($extension, $images)) return "images";
        if(in_array($extension, $office)) return "office";
        if(in_array($extension, $text)) return "text";
        if(in_array($extension, $apps)) return "apps";
        if(in_array($extension, $scripts)) return "scripts";
        if(in_array($extension, $html)) return "html";
        if(in_array($extension, $pdf)) return "pdf";
        if(in_array($extension, $ai)) return "ai";
        if(in_array($extension, $nuke)) return "nuke";
        if(in_array($extension, $aep)) return "aep";
        if(in_array($extension, $zip)) return "zip";
        if(in_array($extension, $ppt)) return "ppt";
        if(in_array($extension, $accdb)) return "accdb";
        if(in_array($extension, $prproj)) return "prproj";
        if(in_array($extension, $psd)) return "psd";
        if(in_array($extension, $video)) return "video";
        if(in_array($extension, $rar)) return "rar";
        if(in_array($extension, $excel)) return "excel";
        if(in_array($extension, $flash)) return "flash";
        if(in_array($extension, $swf)) return "swf";
        if(in_array($extension, $iso)) return "iso";
        if(in_array($extension, $publisher)) return "publisher";
        else
        {
            return "unknown";
        }
    }


    public function formatBytes($bytes, $precision = 2) { 
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 
        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 
        $bytes /= pow(1024, $pow); 
        return round($bytes, $precision) . ' ' . $units[$pow]; 
    }

    public function dl($slug='')
    {
        $fmodel = $this->loadModel('docsModel');
        $docDetails = $fmodel->view($slug);
        $realfile = $docDetails['files'];
        $downloadHelper = $this->loadHelper('download_helper');
        $downloadHelper->force_download($realfile);
    }


    public function trash($slug)
    {
        $fmodel = $this->loadModel('docsModel');
        $docDetails = $fmodel->view($slug);
        $realfile = $docDetails['files'];
        if(file_exists(UPLOAD_DIR."/$realfile") && (is_file(UPLOAD_DIR."/$realfile")))
        {
            if(unlink(UPLOAD_DIR."/$realfile"))
               {
                    $delete = $fmodel->delete($slug);
                    if($delete)
                    {
                       $this->index();
                    }
               }
               else
               {
                   return "ERROR";
               }
        }
    }


    public function view($slug='')
    {
        $user_info = $this->bootstrap();
        $previewHelper = $this->loadHelper('doc_preview_helper');
        $fmodel = $this->loadModel('docsModel');
        $docDetails = $fmodel->view($slug);
        $realfile = $docDetails['files'];
        $docid = $realfile;
        $pageTitle = "Document Preview";
        $preview = $previewHelper->display_document($realfile);
        $head =  $this->loadView('common/header'); 
        $head->set('user_info',$user_info);
        $head->render();
        $content = $this->loadView('repository/preview');  
        $content->set('pageTitle',$pageTitle); 
        $content->set('preview',$preview);
        $content->set('docDetails',$docDetails);
        $content->render();
        $footer  = $this->loadView('common/footer');
        $footer->render();
       
    }


}