<?php 

/**
 * Jobs controller
 */
class job extends controller
{


      public function index(){

        $user_info = $this->bootstrap();   

        $accountsModel   = $this->loadModel('accountsModel');     

        $jobModel    = $this->loadModel('jobModel');

        $regModel = $this->loadModel('regModel');
        
        $all_clients = $regModel->get_bumaa_clients_with_options();

        $all_jobs = $jobModel->all_jobs();

        $fields   = $jobModel->all_fields();

        $head = $this->loadView('common/header'); 

        $head->set('user_info',$user_info);     
        
        $head->render();

        $content = $this->loadView('jobs/index');

        $content->set('pageTitle', 'Jobs - BUMSA IMS');

        $content->set('user_info',$user_info); 

        $content->set('fields',$fields);

        $content->set('accountsModel',$accountsModel);

        $content->set('all_clients',$all_clients);  

        $content->set('all_jobs', $all_jobs);

        $content->render();

        $footer  = $this->loadView('common/footer');
       
        $footer->render();
    }


//job category


    public function fields(){

       $user_info = $this->bootstrap();  

       $jobModel    = $this->loadModel('jobModel');

       $fields   = $jobModel->all_fields();

        $head = $this->loadView('common/header'); 

        $head->set('user_info',$user_info);     
        
        $head->render();

        $content = $this->loadView('jobs/fields');

        $content->set('pageTitle', 'Fields - BUMSA IMS');

        $content->set('fields', $fields);

        $content->set('user_info',$user_info); 

        $content->render();

        $footer  = $this->loadView('common/footer');
       
        $footer->render();
    



    }


//end field
    public function create_cat(){

         $user_info = $this->bootstrap();

         $jobModel    = $this->loadModel('jobModel');

          if(isset($_POST['field']))
            {
                $form_feedback = $jobModel->add_field($_POST["field"]);
            }
            $res_pass = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res_pass;




    }





//end of job 


    public function add(){

        $jobModel  = $this->loadModel('jobModel');
        $uploader = $this->loadHelper('uploader');
       
        $data = $uploader->upload($_FILES['photo'], array(
            'limit' => 100, //Maximum Limit of files. {null, Number}
            'maxSize' => 1024, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => UPLOAD_DIR, //Upload directory {String}
            'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => true, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));
        if($data['isComplete'])
        {
            $info      = $data['data'];           
            if(!empty($info['metas'][0]['name'])){
                $photo     = urlencode($info['metas'][0]['name']);
            }else{
                $photo  = "";
            }
            

            $form_feedback = $jobModel->addJob(               
                $photo,
                $_POST['company'],
                $_POST['location'],
                $_POST['title'],              
                $_POST['description'],
                $_POST['field'],
                $_POST['deadline'],
                $_POST['posted_by'], 
                $_POST['job_link']                        
               
            );
            
            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
        }

        if($data['hasErrors']){
            $errors = json_encode( $data['errors'] );
            header('Content-Type: application/json');
            echo $errors;
        }


        
    }



//edit cate

     public function edit($job_id="")
    {

        $jobModel     = $this->loadModel('jobModel'); 

        $accountsModel   = $this->loadModel('accountsModel');  

        $regModel = $this->loadModel('regModel');    

        $all_clients = $regModel->get_bumaa_clients_with_options();

        $user_info = $this->bootstrap();

        $fields   = $jobModel->all_fields();

        $job    = $jobModel->get_job_by_id(base64_decode($job_id));
        
        $head =     $this->loadView('common/header');  

        $head->set('user_info',$user_info);    
        
        $head->render();

        $content = $this->loadView('jobs/edit-job');

        $content->set('pageTitle', 'Edit Job - BUMSA IMS');

        $content->set('all_clients',$all_clients); 

        $content->set('accountsModel',$accountsModel);

        $content->set('fields',$fields);

        $content->set('user_info',$user_info);

        $content->set('job',$job);

        $content->render();

        $footer  = $this->loadView('common/footer');
       
        $footer->render();
    }






    //edit field

   public function edit_field($id="")
    {

        $jobModel     = $this->loadModel('jobModel');
      
        $user_info = $this->bootstrap();

        $jobF    = $jobModel->get_field_by_id(base64_decode($id));
        
        $head =     $this->loadView('common/header');  

        $head->set('user_info',$user_info);    
        
        $head->render();

        $content = $this->loadView('jobs/edit-field');

        $content->set('pageTitle', 'Edit Field - BUMSA IMS');

        $content->set('user_info',$user_info);

        $content->set('job',$jobF);

        $content->render();

        $footer  = $this->loadView('common/footer');
       
        $footer->render();
    }


//update field

    public function update_field(){

    	   $jobModel    = $this->loadModel('jobModel');

             if(isset($_POST['field']))
            {
                $form_feedback = $jobModel->editF($_POST["field"],$_POST["id"]);
            }
            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
    }



    //update 

    public function update(){

        $jobModel  = $this->loadModel('jobModel');
        $uploader = $this->loadHelper('uploader');
       
        $data = $uploader->upload($_FILES['photo'], array(
            'limit' => 100, //Maximum Limit of files. {null, Number}
            'maxSize' => 1024, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => UPLOAD_DIR."/jobs/", //Upload directory {String}
            'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => true, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));
        
        if($data['isComplete'])
        {
            $info      = $data['data'];           
            if(!empty($info['metas'][0]['name'])){
                $photo     = urlencode($info['metas'][0]['name']);
            }else{
                $photo  = "";
            }
            

            $form_feedback = $jobModel->editJob(
                $photo,
                $_POST['company'],                
                $_POST['title'],
                $_POST['location'], 
                $_POST['deadline'], 
                $_POST['field'],               
                $_POST['description'],                
                $_POST['posted_by'],
                $_POST['id'],
                $_POST['job_link']  
               
            );
            

            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
        }

        if($data['hasErrors']){
            $errors = json_encode( $data['errors'] );
            header('Content-Type: application/json');
            echo $errors;
        }


        
    }


    //delete


    public function delete_job($job_id)
    {
        $user_info  = $this->bootstrap();
        $jobModel  = $this->loadModel('jobModel');
        $job  = $jobModel->get_del_job(base64_decode($job_id));
        $delete  =  @($jobModel->del_job($job[0]->id));
        if($delete)
        {
          
            $this->redirect('job');
        }
         

    }


    //delete field

     public function delete_field($id)
    {
        $user_info  = $this->bootstrap();
        $jobModel  = $this->loadModel('jobModel');
        $field  = $jobModel->get_del_field(base64_decode($id));
        $delete  =  @($jobModel->del_field($field[0]->id));
        if($delete)
        {
          
            $this->redirect('job/fields');
        }
         

    }





   //end of methods




}









 ?>