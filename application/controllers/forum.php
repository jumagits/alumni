<?php
class forum extends Controller
{
    public function index($id = "")
    {
        $user_info = $this->bootstrap();

        $homeModel = $this->loadModel('homeModel');

        $head = $this->loadView('common/header');

        $head->set('user_info', $user_info);
        
        $forumModel = $this->loadModel('forumModel');

        $all_topics = $forumModel->fetchAllTopics();

        $all_categories = $forumModel->fetchCategories();

        $fetchAllCategories = $forumModel->fetchAllCategories();

        $catOptions = $forumModel->catOptions();

        $fetchTopic = $forumModel; //model
        
        $head->set('pageTitle', ' FORUM TOPICS');
        $head->render();

        //
        $content = $this->loadView('forum/forumTopics');

        $content->set('pageTitle', 'Events - BUMSA IMS');

        $content->set('all_topics', $all_topics);

        $content->set('fetchTopic', $fetchTopic);

        $content->set('catOptions', $catOptions);

        $content->set('all_categories', $all_categories);

        $content->set('fetchAllCategories', $fetchAllCategories);

        $content->render();
        //
        $footer = $this->loadView('common/footer');

        $footer->set('user_info', $user_info);

        $footer->set('catOptions', $catOptions);

        $footer->render();
    }

    public function topic($id)
    {
        $user_info = $this->bootstrap();
       
        $head = $this->loadView('common/header');

        $head->set('user_info', $user_info);
       
        $forumModel = $this->loadModel('forumModel');
        
        $fetchTopic = $forumModel->fetchSingleTopic(base64_decode($id));
       
       // $fetchAllCategories = $forumModel->fetchAllCategories();
       
        $fetchAllComments = $forumModel->fetchAllComments(base64_decode($id));
        $model = $forumModel; //model
     
        $head->set('pageTitle', 'TOPIC OPEN');
        $head->render();
        //
        $content = $this->loadView('forum/topic');

        $content->set('pageTitle', 'TOPIC OPEN');
      
        $content->set('forumModel', $model); //model

        $content->set('topic', $fetchTopic);

        $content->set('comments', $fetchAllComments);

        //$content->set('fetchAllCategories', $fetchAllCategories);

        $content->render();
        //
        $footer = $this->loadView('common/footer');

        $footer->set('user_info', $user_info);
        
        $footer->render();
    }

    public function createTopic()
    {

        $user_info = $this->bootstrap();
        $forumModel = $this->loadModel("forumModel");
        if (isset($_POST['post_title'])) {
            $result = $forumModel->addTopic($_POST["post_title"], $_POST["post_content"], $_POST['cat_id'], $_POST['created_by']);
            if ($result) {
                return $result;
            }
        }

    }

    //update topic

    public function updateTopic()
    {

        $user_info = $this->bootstrap();
        $forumModel = $this->loadModel("forumModel");
        if (isset($_POST['post_content'])) {
            $result = $forumModel->updateTopic($_POST["post_title"], $_POST["post_content"], $_POST['cat_id'], $_POST['created_by'],$_POST['post_id']);
            if ($result) {
                return $result;
            }
        }

    }

    //end update topic

    //update category

    public function updateCategory()
    {

        $user_info = $this->bootstrap();
        $forumModel = $this->loadModel("forumModel");
        if (isset($_POST['category_title'])) {
            $result = $forumModel->updateCategory($_POST["category_title"], $_POST['cat_id']);
            if ($result) {
                return $result;
            }
        }

    }

//end update

    public function comment()
    {

        $user_info = $this->bootstrap();
        $forumModel = $this->loadModel("forumModel");
        if (isset($_POST['comment'])) {
            $result = $forumModel->addComment($_POST["comment"], $_POST["user_id"], $_POST['post_id']);
            if ($result) {
                return $result;
            }
        }

    }

    public function addCategory()
    {

        $user_info = $this->bootstrap();
        $forumModel = $this->loadModel("forumModel");
        if (isset($_POST['category_title'])) {
            $result = $forumModel->addCategory($_POST["category_title"]);
            if ($result) {
                return $result;
            }
        }

    }

    //delete topic

    public function delete_topic($post_id)
    {
        $user_info = $this->bootstrap();
        $forumModel = $this->loadModel("forumModel");
        $result = $forumModel->delete_topic($post_id);
        if ($result) {
            $this->redirect('forum');
        }

    }

    //delete cat

    public function delete_cat($id)
    {
        $user_info = $this->bootstrap();
        $forumModel = $this->loadModel("forumModel");
        $result = $forumModel->delete_category($id);
        if ($result) {
            $this->redirect('forum');
        }

    }

    //end of methods
}
