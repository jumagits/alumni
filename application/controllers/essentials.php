<?php 
class essentials extends controller 
{

public function clinics()
{
$user_info = $this->bootstrap();
$administrativeModel  = $this->loadModel('administrativeModel');
$clinic = $administrativeModel->getClinics();
$head = $this->loadView('common/header');
$head->set('user_info',$user_info); 
$head->render();
$content = $this->loadView('administrative/clinics');
$content->set('pageTitle', 'CLINICS - BUMSA IMS');
$content->set('clinic',$clinic);
$content->set('user_info',$user_info);
$content->render();
$footer  = $this->loadView('common/footer');
$footer->render();
}


public function manage_clinic($clinic_id)
{
$user_info      = $this->bootstrap();
$logModel = $this->loadModel("systemLogModel");
$administrativeModel      = $this->loadModel('administrativeModel');

$regModel = $this->loadModel('regModel');

$clinic           = $administrativeModel->get_clinic(base64_decode($clinic_id));

$head   = $this->loadView('common/header');
$head->set('user_info',$user_info);
$head->set('pageTitle','Manage Clinic - BUMAA IMS');
$head->render();
if(isset($_POST['update']))
{
    $do_update_feedback = $administrativeModel->clinic_up($_POST["clinicName"], $_POST["cliniCode"],$_POST["cliniColor"],$_POST["clinicPhone"]);
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';
}
$content = $this->loadView('administrative/clinicMan');

$content->set('clinic',$clinic);
$content->set('pageTitle','Manage Clinic - BUMAA IMS');
$content->set('user_info',$user_info);
$content->render();

$footer = $this->loadView('common/footer');
$footer->render();
}

//delete clinic
public function delete_clinic($clinic_id)
{
$user_info  = $this->bootstrap();
$logModel = $this->loadModel("systemLogModel");
$administrativeModel  = $this->loadModel('administrativeModel');
$clinic  = $administrativeModel->get_del_clinic(base64_decode($clinic_id));
$delete  =  @($administrativeModel->del_clinic($clinic[0]->id));
if($delete)
{
    $this->redirect('essentials/clinics');
}
}

public function add_clinic()
{
$user_info  = $this->bootstrap();
$logModel = $this->loadModel("systemLogModel");
$administrativeModel  = $this->loadModel('administrativeModel');
$form_feedback = $administrativeModel->add_clinic($_POST["clinicName"],$_POST["cliniColor"],$_POST["clinicPhone"]);      
$res = json_encode($form_feedback);
header('Content-Type: application/json');
echo $res;
}

public function userLevel()
{
$user_info      = $this->bootstrap();
$administrativeModel  = $this->loadModel('administrativeModel');
$homeModel      = $this->loadModel('homeModel');
$regModel = $this->loadModel('regModel'); 
$uadministrative  = $administrativeModel->getCategories();
$head   = $this->loadView('common/header');
$head->set('user_info',$user_info);
$head->set('pageTitle','User Categories - BUMAA IMS');
$head->render();
$content = $this->loadView('administrative/userCat');        
$content->set('uadministrative',$uadministrative);
$content->set('user_info',$user_info);
$content->render();
$footer = $this->loadView('common/footer');
$footer->render();
}


public function transfer()
{
$user_info  = $this->bootstrap();
$logModel = $this->loadModel("systemLogModel");
$administrativeModel  = $this->loadModel('administrativeModel');   
$form_feedback = $administrativeModel->transferClient($_POST["new_clinic"],$user_info['clinic_id']);    
$clinicTransf = json_encode($form_feedback);
header('Content-Type: application/json');
echo $clinicTransf;

}

public function editUserCat($clinic_id)
{
$user_info      = $this->bootstrap();
$logModel = $this->loadModel("systemLogModel");
$administrativeModel      = $this->loadModel('administrativeModel');
$regModel = $this->loadModel('regModel'); 
$ucat  = $administrativeModel->get_cat(base64_decode($clinic_id));
$id = base64_decode($clinic_id);
$head   = $this->loadView('common/header');
$head->set('user_info',$user_info);
$head->set('pageTitle','Edit User Category - BUMAA IMS');
$head->render();

if(isset($_POST['updateCat']))
{
    $do_update_feedback = $administrativeModel->ucat_up($id,$_POST["catName"]);
    echo '<META HTTP-EQUIV="Refresh" Content="0; URL='.$location.'">';

}
$content = $this->loadView('administrative/editCat');
$content->set('pageTitle','Edit User Category - BUMAA IMS');
$content->set('ucat',$ucat);
$content->set('user_info',$user_info);
$content->render();

$footer = $this->loadView('common/footer');
$footer->render(); 
}


public function delete_ucat($clinic_id)
{
$user_info  = $this->bootstrap();
$logModel = $this->loadModel("systemLogModel");
$administrativeModel  = $this->loadModel('administrativeModel');
$ucat  = $administrativeModel->get_del_ucat(base64_decode($clinic_id));
$delete  =  @($administrativeModel->del_ucat($ucat[0]->id));
if($delete)
{
 $this->redirect('essentials/userLevel');
}
}

public function add_cat()
{
$user_info  = $this->bootstrap();
$logModel = $this->loadModel("systemLogModel");
$administrativeModel  = $this->loadModel('administrativeModel');        
$form_feedback = $administrativeModel->add_cat($_POST["catName"]);         
$res = json_encode($form_feedback);
header('Content-Type: application/json');
echo $res;
}


public function switchLogs()
{
$user_info      = $this->bootstrap();
$administrativeModel  = $this->loadModel('administrativeModel');
$head     = $this->loadView('common/header');
$regModel = $this->loadModel('regModel');
$myclinic = ($user_info['clinic_id']);
$head->set('user_info',$user_info);   
$head->set('pageTitle','Switch Logs - BUMAA IMS');
$head->render();
$content = $this->loadView('administrative/accessLogs');        
$content->set('administrativeModel',$administrativeModel);
$content->set('myclinic',$myclinic);
$content->set('user_info',$user_info);
$content->render();
$footer = $this->loadView('common/footer');
$footer->render();
}


}