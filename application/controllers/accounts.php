<?php
class accounts extends controller
{

//nav #3f4d66 === table == #f7f9f9 ==body == #f5f6fa

    public function index()
    {
        $this->login();
    }


 public function socialLogin($provider = null){

        $pageTitle = "Social Login"; 
        require_once APP_DIR . "views/accounts/socialLogin.php";
    }

    public function login()
    {        
        if (isset($_POST['login'])) {
              $accountsModel = $this->loadModel("accountsModel"); 
              $result = $accountsModel->doLogin($_POST["username"], $_POST["password"]);
             if ($result) {

                    $messageSubject = "Login Success";
                    $messageOnSuccess = "<div class='alert alert-block alert-success'><span class='glyphicon glyphicon-ok-sign'></span><strong> SUCCESS : {$email}</strong>Logged in Successfully </div>";

                $messageOnfail = "";               
                $ip = $this->GetIpAddress();
                $device = $this->GetDeviceType();
                $browser = $this->GetBrowser();
                $GetDeviceToken = $this->GetDeviceToken();
                 $accountDetails = $accountsModel->loadAccountDetailsByUsername($_POST['username']);
                $email = $accountDetails['email'];
                $fullName = $accountDetails['fullname'];
                $location = $this->isp();

                $data['names'] = $fullName." ".$email;
                $data['ip_address'] = $ip;
                $data['browser'] = $browser['name'];
                $data['token'] = $GetDeviceToken;
                $data['device'] = $device;
                $data['time'] = $activity_time = $activity_time = date('l, j<\s\u\p>S</\s\u\p> F Y (h:i:s A)');
                $data['country'] = $location['country'].'-'.$location['city'];
                $data['isp'] = $location['isp'];

                //var_dump($data);

                
                $mail = $this->loadHelper('email');
                $messageHtml = $mail->emailTemplate($data);
                $mail->sendMessage($email, $messageSubject, '', $messageHtml, $messageOnSuccess, $messageOnfail);

               $doLoginFeedback = "<div class='alert alert-info  bg-info'>
             <i class='fa fa-check'></i> Credentials Accepted, Sigining you in ...
             <script type='text/javascript'>setTimeout(function() { window.location.href = '" . URL . "application/index';}, 1000);</script>
              </div>";


              }else{

            $doLoginFeedback = "<div class='p-2 text-danger' >
            
            Either your Username or Password is Invalid <a class='badge badge-primary' href='".URL."accounts/forgot_password'>Reset It here</a>
            
             </div>";


             }


          }
        $pageTitle = "Login"; 
        require_once APP_DIR . "views/accounts/login.php";
    }
    //logout function

    public function logout()
    {
        session_destroy();
        $this->redirect('application/index');
    }

    public function forgot_password()
    {     

        if (isset($_POST['reset'])) {
            $email = $_POST['useremail'];
            $AccountDetails = $this->verify_account_existence($email);
            if ($AccountDetails) {
                $accountsModel = $this->loadModel("accountsModel");
                $token = mt_rand(100000, 999999);
                $userMobileNumber = $AccountDetails['phonenumber'];
                $userName = $AccountDetails['fullname'];
                $userEmail = $AccountDetails['email'];
                $result = $accountsModel->verify_and_pass_link($email, $token);
                if ($result) {


            $mail = $this->loadHelper('email');
            $messageSubject = "Pasword reset token";
            $messageOnSuccess = "<div class='alert alert-block alert-success'><span class='glyphicon glyphicon-ok-sign'></span><strong> SUCCESS : {$email}</strong>Password Successfully reset !</div>";
            $messageOnfail = "failed to send token";
            $data = [];
            $data['email'] = $userEmail;
            $data['token'] = $token;
            $messageHtml = $mail->send_reset_token($data); 
            $mail->sendMessage($email, $messageSubject, '', $messageHtml, $messageOnSuccess, $messageOnfail);
            $feedBack = "<div class='alert alert-success'>
                <i class='fa fa-check'></i>                
                 <p> We've sent a security token to {$userEmail}, Please wait as we redirect you ..............</p>
                <script type='text/javascript'>setTimeout(function() { window.location.href = '" . URL . "accounts/verify-token';}, 6000);</script>
                </div>";
             }
             
            } else {
                $feedBack = "<div class='alert alert-danger'>
                   <p> <strong><i class='fa fa-info-circle'></i> Oops:</strong> User with Email # $email ! not found.
                     </p>
                     </div>";
            }
        }

        require_once APP_DIR . "views/accounts/forgot.php";
    }

    //details

    public function details()
    {
        $administrativeModel = $this->loadModel('administrativeModel');
        $all_clinics = $administrativeModel->getClinics();
        require_once APP_DIR . "views/accounts/updateDetails.php";
    }

    //aluni register

    public function alumni_register()
    {

    $administrativeModel = $this->loadModel('administrativeModel');
    $all_clinics = $administrativeModel->getClinics();
    $getCourses = $administrativeModel->getCourses();
    require_once APP_DIR . "views/accounts/alumni_registration.php";

    }

    public function company_register()
    {

        require_once APP_DIR . "views/accounts/company_registration.php";

    }

    public function verify_account_existence($email)
    {
        $accountsModel = $this->loadModel("accountsModel");
        $accountDetails = $accountsModel->loadAccountDetails($email);
        return $accountDetails;
    }
    public function verify_token()
    {
        $accountsModel = $this->loadModel("accountsModel");

        if (isset($_POST['continue'])) {
            $token = $_POST['token'];
            $newPass1 = $_POST['pass1'];
            $newPass2 = $_POST['pass2'];
            if ($newPass1 == $newPass2) {
                $result = $accountsModel->resetPassword($token, $newPass1);
                if ($result) {
                    $feedBack = "<div class='alert alert-success text-center'>
                                <p> Success : You've reset your password successfully, Please wait as we login you ..............</p>
                            <script type='text/javascript'>setTimeout(function() { window.location.href = '" . URL . "';}, 6000);</script>
                            </div>";
                } else {
                    $feedBack = "<div class='alert alert-danger text-center'>                             
                                <p> Oops : Your password reset token may be invalid !</p>
                             </div>";
                }
            } else {
                $feedBack = " <div class='alert alert-danger text-center'><p> Slow down: Your Passwords don't Match</p></div>";
            }

        }
        require_once APP_DIR . "views/accounts/resetPass.php";
    }

    public function pass_reset()
    {
        $user_info = $this->bootstrap(); 
        $clinicId = $user_info['clinic_id'];
        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $head->set('pageTitle', 'Password Reset ');
        $head->render();
        $content = $this->loadView('accounts/passReset');
        $content->set('user_info', $user_info);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();
    }


    //Update Password

    public function update_pass()
    {
        $user_info = $this->bootstrap();
        $accountsModel = $this->loadModel('accountsModel');
        if(isset($_POST['password1']))
        {
         $form_feedback = $accountsModel->changePass($_POST["oldpassword"], $_POST["password1"], $_POST['password2'], $user_info['id']);
         }
        $res_pass = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res_pass;
    }


   

    public function system_logs()
    {
        $user_info = $this->bootstrap();
        $logModel  = $this->loadModel('systemLogModel');
        $regModel  = $this->loadModel('regModel');
        $head      = $this->loadView('common/header');
        $head->set('user_info',$user_info);
        $head->set('pageTitle','System Logs ');
        $head->render();
        $content  = $this->loadView('audits/systemlogs');
        $content->set('pageTitle','System Logs ');
        $content->set('logModel',$logModel);     
        $content->set('user_info',$user_info);
        $content->render();
        $footer  = $this->loadView('common/footer');
        $footer->render();
    }


 



public function GetIpAddress() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && $this->ValidateIpAddress($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if ($this->ValidateIpAddress($ip))
                    return $ip;
            }
        } else {
            if ($this->ValidateIpAddress($_SERVER['HTTP_X_FORWARDED_FOR']))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && $this->ValidateIpAddress($_SERVER['HTTP_X_FORWARDED']))
        return $_SERVER['HTTP_X_FORWARDED'];
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && $this->ValidateIpAddress($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && $this->ValidateIpAddress($_SERVER['HTTP_FORWARDED_FOR']))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    if (!empty($_SERVER['HTTP_FORWARDED']) && $this->ValidateIpAddress($_SERVER['HTTP_FORWARDED']))
        return $_SERVER['HTTP_FORWARDED'];
    return $_SERVER['REMOTE_ADDR'];
}


public function ValidateIpAddress($ip) {
    if (strtolower($ip) === 'unknown')
        return false;
    $ip = ip2long($ip);
    if ($ip !== false && $ip !== -1) {
        $ip = sprintf('%u', $ip);
        if ($ip >= 0 && $ip <= 50331647)
            return false;
        if ($ip >= 167772160 && $ip <= 184549375)
            return false;
        if ($ip >= 2130706432 && $ip <= 2147483647)
            return false;
        if ($ip >= 2851995648 && $ip <= 2852061183)
            return false;
        if ($ip >= 2886729728 && $ip <= 2887778303)
            return false;
        if ($ip >= 3221225984 && $ip <= 3221226239)
            return false;
        if ($ip >= 3232235520 && $ip <= 3232301055)
            return false;
        if ($ip >= 4294967040)
            return false;
    }
    return true;
}


public function isValidIpAddress($ip)
{
    if (filter_var($ip, FILTER_VALIDATE_IP,
        FILTER_FLAG_IPV4 |
        FILTER_FLAG_IPV6 |
        FILTER_FLAG_NO_PRIV_RANGE |
        FILTER_FLAG_NO_RES_RANGE) === false) {
        return false;
    }
    return true;
}

public function GetDeviceType() {
    $deviceName = '';
    $userAgent    = $_SERVER['HTTP_USER_AGENT'];
    $devicesTypes = array(
        'computer' => array(
            'msie 10',
            'msie 9',
            'msie 8',
            'windows.*firefox',
            'windows.*chrome',
            'x11.*chrome',
            'x11.*firefox',
            'macintosh.*chrome',
            'macintosh.*firefox',
            'opera'
        ),
        'tablet' => array(
            'tablet',
            'android',
            'ipad',
            'tablet.*firefox'
        ),
        'mobile' => array(
            'mobile ',
            'android.*mobile',
            'iphone',
            'ipod',
            'opera mobi',
            'opera mini'
        ),
        'bot' => array(
            'googlebot',
            'mediapartners-google',
            'adsbot-google',
            'duckduckbot',
            'msnbot',
            'bingbot',
            'ask',
            'facebook',
            'yahoo',
            'addthis'
        )
    );
    foreach ($devicesTypes as $deviceType => $devices) {
        foreach ($devices as $device) {
            if (preg_match('/' . $device . '/i', $userAgent)) {
                $deviceName = $deviceType;
            }
        }
    }
    return ucfirst($deviceName);
}

public function GetDeviceToken() {
    $finger_print               = array();
    $browser                    = $this->GetBrowser();
    $finger_print['ip']         = $this->GetIpAddress();
    $finger_print['browser']    = $browser['name'] . " " . $browser['version'];
    $finger_print['os']         = $browser['platform'];
    $finger_print['deviceType'] = $this->GetDeviceType();
    $device                     = serialize($finger_print);
    return $device;
}


public function GetBrowser() {
    $ub    = '';
    $u_agent  = $_SERVER['HTTP_USER_AGENT'];
    $bname    = 'Unknown';
    $platform = 'Unknown';
    $version  = '';
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub    = 'MSIE';
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub    = 'Firefox';
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub    = 'Chrome';
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub    = 'Safari';
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $bname = 'Opera';
        $ub    = 'Opera';
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub    = 'Netscape';
    }
    $known   = array(
        'Version',
        $ub,
        'other'
    );
    $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
    }
    $i = count($matches['browser']);
    if ($i != 1) {
        if (strripos($u_agent, 'Version') < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }
    if ($version == null || $version == "") {
        $version = '?';
    }
    return array(
        'userAgent' => $u_agent,
        'name' => $bname,
        'version' => $version,
        'platform' => $platform,
        'pattern' => $pattern
    );
}
  


// PHP code to extract IP
public function get_location_details(){
// PHP code to obtain country, city,
// continent, etc using IP Address

//$ip = $this->GetIpAddress();
    $ip = '197.157.0.32';

// Use JSON encoded string and converts
// it into a PHP variable
$ipdat = @json_decode(file_get_contents(
    "http://www.geoplugin.net/json.gp?ip=" . $ip)
);


$location = array(
    'country_name' => $ipdat->geoplugin_countryName , 
    'city_name' => $ipdat->geoplugin_city , 
    'Longitude' => $ipdat->geoplugin_longitude , 
    'Latitude' => $ipdat->geoplugin_latitude , 
    'Timezone' => $ipdat->geoplugin_timezone , 
    'currency_code' => $ipdat->geoplugin_currencyCode  , 
    'continent_name' => $ipdat->geoplugin_continentName , 
    'currency_symbol' => $ipdat->geoplugin_currencySymbol , 
              );

   return $location;

}

public function isp(){
//$ip = $this->GetIpAddress();  
 $ip = '197.239.4.163';
$url =  'http://extreme-ip-lookup.com/json/'.$ip;
$ipdat = @json_decode(file_get_contents($url));
 $details = array(
    'continent' => $ipdat->continent,
    'country' => $ipdat->country,
    'city' => $ipdat->city,
    'isp' => $ipdat->isp,
 );
 return $details;

}


    //functions end
}
