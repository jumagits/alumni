<?php

class e_share extends Controller
{


      public function generate()
    {
        $user_info = $this->bootstrap();
        $homeModel = $this->loadModel('homeModel');
        $locationModel = $this->loadModel('location');
        $regModel = $this->loadModel('regModel');

        $mpdf_creator = $this->loadHelper('generatepdf');
        $clientname = $_POST['clientname'];
        $fileno = $_POST['fileno'];
        $age = $_POST['age'];
        $claim = $_POST['claim'];
        $sex = $_POST['sex'];
        $referredto = $_POST['referredto'];
        $reasons = $_POST['reasons'];
        $referee = $_POST['referee'];
        $email = $_POST['email'];
        $designation = $_POST['designation'];
        $referdate = date_format(date_create($_POST['referdate']), ' l jS F Y');
        $facts = $_POST['facts'];

        $regModel->add_referral($fileno, $clientname, $sex, $age, $claim, $facts, $reasons, $_POST['referdate'], $user_info['clinic_id'], $user_info['id'], 'Done');

        $letterContents = "
            <!doctype html>
                <html>
                    <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' />

                        <title>Referral Letter</title>
                    </head>
                    <body>
                    <p style='text-align:center'><img width='100' height='100' src='" . URL . "static/images/logo.png' /></p>
                    <p>&nbsp;</p>
                    <h1 align='center'>LEGAL AID PROJECT OF UGANDA LAW SOCIETY</h1>
                    <h2 align='center'><u>REFERRAL LETTER</u></h2>
                    <p align='right'>Date: {$referdate}</p>
                    <p align='left'>File No : {$fileno}</p>
                    <p>Name of the Client:  {$clientname} </p>
                    <p>&nbsp;</p>
                    <h4 style='color:#722A8E; font-weight:bold;'>Subject: Referral of Client</h4>

                    <p>Dear colleague,</p>
                    <p>I hope this letter finds you well. I am hereby referring {$clientname} to your institution and kindly request you assist him/her to the best of your ability.
                    <br />
                    <b>{$clientname} related the following facts:</b></p>
                    <p style='text-align: justify;'>{$facts}</p>
                    <p><b>Reason For Referral</b></p>
                    <p>{$reasons}</p>
                    <p>&nbsp;</p>
                    <p>For further inquiries, please feel free to email me at {$email}.</p>

                    <p>Faithfully,</p>
                    <p>Centre Manager</p>
                    <p>Legal Aid Project Of Uganda Law Society</p>
            </body>
            </html>";

        //echo $letterContents;

        $mpdf_creator->makepdf($letterContents, 'Referral Letter', str_replace('/', '_', $fileno) . ".pdf");

    }


    public function index()
    {
        $user_info = $this->bootstrap();
        $homeModel      = $this->loadModel('homeModel');
        $locationModel  = $this->loadModel('location');
        $regModel       = $this->loadModel('regModel');
        $settingsModel  = $this->loadModel('settingsModel');
        $logModel       = $this->loadModel("systemLogModel");
        $sharemodel = $this->loadModel("eshareModel");
        $catModel = $this->loadModel('administrativeModel'); 
        $shared_doc = $sharemodel->getAllSharedDocuments($user_info['id']);        
        $user_options = $sharemodel->getAllUsersOptionValues();
        $head           = $this->loadView('common/header');
        $head->set('user_info',$user_info); 
        $head->set('pageTitle','E-Share/ Library - BUMAA IMS');
        $head->render();    
        $content = $this->loadView('settings/elibrary');
        $content->set('user_options', $user_options);
        $content->set('user_info',$user_info);   
        $content->set('shared_doc', $shared_doc);
        $content->set('sharemodel', $sharemodel);
        $content->render();

        $footer = $this->loadView('common/footer');
        $footer->render();
    }


    //all

       public function all()
    {
        $user_info = $this->bootstrap();
        $homeModel      = $this->loadModel('homeModel');
        $locationModel  = $this->loadModel('location');
        $regModel       = $this->loadModel('regModel');
        $settingsModel  = $this->loadModel('settingsModel');
        $logModel       = $this->loadModel("systemLogModel");
        $sharemodel = $this->loadModel("eshareModel");
        $catModel = $this->loadModel('administrativeModel');  
        $shared_doc = $sharemodel->AllSharedDocuments();        
        $user_options = $sharemodel->getAllUsersOptionValues();
        $head   = $this->loadView('common/header');
        $head->set('user_info',$user_info);        
        $head->set('notification',$notification);
        $head->set('mmt_noti',$mmt_noti);
        $head->set('myClininiCases',$myClininiCases);
        $head->set('pendingReviews',$pendingReviews);      
        $head->set('pageTitle','E-Share/ Library - BUMAA IMS');
        $head->render();    
        $content       = $this->loadView('elibrary');
        $content->set('user_options', $user_options);
        $content->set('user_info',$user_info);   
        $content->set('shared_doc', $shared_doc);
        $content->set('sharemodel', $sharemodel);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();
    }


    //upload shared document

    public function upload_shared_document()
    {
        $user_info = $this->bootstrap();
        $sharemodel = $this->loadModel("eshareModel");
        $email_helper = $this->loadHelper('email');
        $logModel       = $this->loadModel("systemLogModel");
        $uploader = $this->loadHelper('uploader');
        $data = $uploader->upload($_FILES['files'], array(
            'limit' => 100, //Maximum Limit of files. {null, Number}
            'maxSize' => 1024, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => true, //Minimum one file is required for upload {Boolean}
            'uploadDir' => UPLOAD_DIR, //Upload directory {String}
            'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => true, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));

        if($data['isComplete'])
        {
            $info = $data['data'];
            $y = date('y');
            $m = date('m');
            $d = date('d');
            $slug = $y . $m . $d . mt_rand(1000, 9999);
            $document_name = urlencode($info['metas'][0]['name']);
            $title = $_POST['doc_title'];
            $desc = $_POST['desc'];
            $shareWith = implode(',' ,$_POST['shareWith']);
            $user_ids = explode(",", $shareWith);
            if($user_ids[0] != 'ALL'){
                for ($i=0; $i < sizeof($user_ids) ; $i++) { 
                    $e = $sharemodel->getUser($user_ids[$i]);
                    $status_res = [];
                    $toMail = $e[0]['email'];
                    $recp = $e[0]['fullname'];
                    $shby = $sharemodel->getUser($user_info['id'])[0]['fullname'];

                    $body= "Hello {$recp}, 
                    <br>Please login into your BUMAA IMS system account to view documents shared by {$shby}.<br>
                    Document Title:<br><b> {$title}</b>.<br><br>
                    Document Description: <br><b>{$desc}</b><br>
                    <br>
                    Follow link: bumaa-ims.go.ug<br>
                    Thanks <br><br>Best Regards {$shby}";
                    $email_helper->sendMessage($toMail, 'BUMAA IMS E-SHARED DOCUMENTS NOTIFICATION', '',$body,  'SENT ', 'FAILED');
                }
            }

            if($user_ids[0] == 'ALL'){
                $e_array  = (array) $sharemodel->allUser();
                for ($i=0; $i < sizeof($e_array) ; $i++) { 
                    
                    $toMail = $e_array[$i]->email;
                    $recp = $e_array[$i]->fullname;
                    $shby = $sharemodel->getUser($user_info['id'])[0]['fullname'];

                    $body= "Hello {$recp}, <br>Please login into your BUMAA IMS system account to view documents shared by {$shby}.<br>Thanks <br><br>Best Regards {$shby}";
                    $email_helper->sendMessage($toMail, 'ULS IMS NOTIFICATION EMAIL FOR SHARED DOCUMENTS', '',$body,  'SENT ', 'FAILED');
                }
            }
            $form_feedback = $sharemodel->add_shared_file(
                $slug, $title, 
                $desc, $shareWith ,
                $document_name, 
                $user_info['id'],
                $user_info['clinic_id']
            );

            if($form_feedback['status'])
            {
                $log = $logModel->writeLog($form_feedback['msg']." from IP : ".IP,$user_info['id'],$user_info['clinic_id'],"SUCCESS");
            }else{
                $log = $logModel->writeLog($form_feedback['msg']." from IP : ".IP,$user_info['id'],$user_info['clinic_id'],"WARNING");
            }
            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;

        }

        if($data['hasErrors']){
            $errors = json_encode( $data['errors'] );
            header('Content-Type: application/json');
            echo $errors;
        }

    }


    // trash

    public function trash_shared_doc($id)
    {

    }
}
