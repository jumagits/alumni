<?php

/**
 *
 */
class application extends controller
{
    public function index()
    {      
        $postModel = $this->loadModel('postModel');
        $jobModel = $this->loadModel('jobModel');
        $eventsModel = $this->loadModel('eventsModel');
        $administrativeModel = $this->loadModel('administrativeModel');
        $all_posts = $postModel->only_three_posts();
        $bumaa_clients = $this->loadModel('regModel');
        $all_clients = $bumaa_clients->get_bumaa_clients();
        $tests = $bumaa_clients->get_testimonies();
        $current_clients = $bumaa_clients->get_bumaa_clients_current();
        $current_company = $bumaa_clients->get_bumaa_company_current();
        $members = $bumaa_clients->count_clients_all();
        $all_events = $eventsModel->getAllEvents();
        $top_four = $bumaa_clients->get_four_bumaa_clients();
        $all_jobs = $jobModel->only_three_jobs();   
        $get_four_bumaa_clients_randomly = $bumaa_clients->get_four_bumaa_clients_randomly();
        $eventsModel = $this->loadModel('eventsModel');
        $all_events = $eventsModel->get_only_four_events();
        $head = $this->loadView('front/header');   
        $head->set('page','home');
        $head->render();
        $content = $this->loadView('frontend/index');
        $content->set('pageTitle', 'Home ');
        $content->set('administrativeModel',$administrativeModel );
        $content->set('all_events', $all_events);
        $content->set('all_posts', $all_posts);
        $content->set('members',$members);
        $content->set('tests',$tests);
        $content->set('all_jobs',$all_jobs);
        $content->set('current_clients', $current_clients);
        $content->set('current_company', $current_company);
        $content->set('slides', $get_four_bumaa_clients_randomly);
        $content->set('top_four', $top_four);
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();

    }
   

    public function alumni()
    {
        $bumaa_clients = $this->loadModel('regModel');
        $all_clients = $bumaa_clients->get_bumaa_clients();
        $current_clients = $bumaa_clients->get_bumaa_clients_current();
        $current_company = $bumaa_clients->get_bumaa_company_current();
       $head = $this->loadView('front/header');   
        $head->render();
        $content = $this->loadView('frontend/alumni');
        $content->set('user_info',$user_info);
        $content->set('administrativeModel', $this->loadModel('administrativeModel'));
        $content->set('pageTitle', 'ALUMNI ');
        $content->set('all_clients', $all_clients);
        $content->set('current_clients', $current_clients);
        $content->set('current_company', $current_company);
        $content->render();//
        $footer = $this->loadView('front/footer');
        $footer->render();

    }

     public function alumni_details($id = null)
    {
       
        $segment = base64_decode($id);
        $bumaa_clients = $this->loadModel('regModel');
        $details = $bumaa_clients->get_details($segment);
        $head    = $this->loadView('front/header');       
        $head->render();
        $content    = $this->loadView('frontend/alumni_details');
        $content->set('pageTitle', 'ALUMNI DETAILS');
        $content->set('user_info',$user_info);
        $content->set('details',$details);
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();
    }

    public function Otherjobs($current_page = "")
    {
        $jobModel = $this->loadModel('jobModel');
        $accountsModel = $this->loadModel('accountsModel');        
        $all_jobs = $jobModel->all_jobs();
        $fields = $jobModel->all_fields();
        $recent_jobs = $jobModel->recent_jobs();
        $jobs_count = $jobModel->jobs_count();
        $head = $this->loadView('front/header');
        $head->set('pageTitle', 'JOBS ');      
        $head->render();
        $content = $this->loadView('frontend/jobs');       
        $content->set('all_jobs',$all_jobs);
        $content->set('fields', $fields);
        $content->set('count', $jobs_count);
        $content->set('recent_jobs', $recent_jobs);
        $content->set('jobModel', $jobModel);
        $content->set('accountsModel', $accountsModel);
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();

    }

    //search job

    public function search_job()
    {
        $jobModel = $this->loadModel('jobModel');
        $form_feedback = $jobModel->search_job($_POST['job']);
        $res = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res;
    }
    //end search

    public function job()
    {

        $jobModel = $this->loadModel('jobModel');
        $accountsModel = $this->loadModel('accountsModel');
        $all_jobs = $jobModel->all_jobs();     
        $fields = $jobModel->all_fields();
        $recent_jobs = $jobModel->recent_jobs();
        $jobs_count = $jobModel->jobs_count();
        $head = $this->loadView('front/header');
        $head->set('pageTitle', 'JOBS ');        
        $head->render();
        $content = $this->loadView('frontend/jobs');        
        $content->set('all_jobs',$all_jobs);
        $content->set('fields', $fields);
        $content->set('count', $jobs_count);
        $content->set('recent_jobs', $recent_jobs);
        $content->set('jobModel', $jobModel);
        $content->set('accountsModel', $accountsModel);
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();

    }

    public function job_details($id)
    {
        $jobModel = $this->loadModel('jobModel');
        $all_jobs = $jobModel->all_jobs();
        $original_id = base64_decode($id);
        $job_details = $jobModel->get_job_by_id($original_id);
        $recent_jobs = $jobModel->recent_jobs();
        $head = $this->loadView('front/header');
        $head->set('pageTitle', 'JOB DETAILS');       
        $head->render();
        $content = $this->loadView('frontend/job-details');        
        $content->set('job_details', $job_details);
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();

    }

    public function event_details($id)
    {

        $eventsModel = $this->loadModel('eventsModel');
        $user_info = $this->bootstrap();
        $original_id = base64_decode($id);
        $details = $eventsModel->get_event($original_id);
        $head = $this->loadView('front/header');
        $head->set('user_info',$user_info);
        $head->render();
        $content = $this->loadView('frontend/event-details');
        $content->set('pageTitle', 'Event Details');
        $content->set('model',$eventsModel);
        $content->set('user_info',$user_info);
        $content->set('details', $details);
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();

    }

    public function events()
    {

        $eventsModel = $this->loadModel('eventsModel');        ;
        $all_events = $eventsModel->getAllEvents();
        $head = $this->loadView('front/header');       
        $head->render();
        $content = $this->loadView('frontend/events');
        $content->set('pageTitle', 'Events');
        $content->set('all_events', $all_events);
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();

    }

    //about

    public function about()
    {      
        $head = $this->loadView('front/header');
        $head->set('pageTitle', 'About Us ');       
        $head->render();
        $content = $this->loadView('frontend/about');        
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();
    }

    public function studentLogin()
    {

        $head = $this->loadView('front/header');         
        $head->render();
        $content = $this->loadView('frontend/studentLogin');
        $content->set('pageTitle', 'Student Login ');
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();
    }

    public function contact()
    {
        $head = $this->loadView('front/header');
        $head->set('pageTitle', 'Contact Us ');
        $head->render();
        $content = $this->loadView('frontend/contact');        
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();
    }

    public function add_member()
    {
        $user_info = $this->bootstrap();
        $head = $this->loadView('front/header');
        $head->set('user_info',$user_info);
        $head->render();
        $content = $this->loadView('membership/add_member');
        $content->set('pageTitle', 'Add Member ');
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->render();
    }


    //profile


 public function profile()
    {

        $form_feedback ="";  
        $administrativeModel = $this->loadModel('administrativeModel');
        $all_clinics = $administrativeModel->getClinics();
        $getCourses = $administrativeModel->getCourses();
        $user_info = $this->bootstrap();
        $id = $user_info['id'];

        if(!empty($id)) {
 
        $head = $this->loadView('front/header');  
        $head->set('user_info', $user_info);
        $head->set('pageTitle', 'PROFILE ');
        $regModel = $this->loadModel('regModel');
        $get_user_testimonies = $regModel->get_user_testimonies($user_info['id']);
        $details = $regModel->profile($id);
        $head->render();
        $content = $this->loadView('frontend/profile');
        $content->set('pageTitle', 'PROFILE ');
        $content->set('user_info',$user_info);
        $content->set('details', $details);
        $content->set('all_clinics',$all_clinics);
        $content->set('getCourses',$getCourses);      
        $content->set('get_user_testimonies', $get_user_testimonies);
        $content->render();
        $footer = $this->loadView('front/footer');
        $footer->set('user_info',$user_info);
        $footer->render();

       }else{

         $this->index(); //redirect to home

       }

    }

    //testify

    public function testify(){       
        $regModel      = $this->loadModel('regModel');
        if(isset($_POST['testimony']))
        { $form_feedback = $regModel->addTestimony($_POST["testimony"],$_POST['user_id']);
        }
        $res_pass = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res_pass;
    }






    //methods end here
}
