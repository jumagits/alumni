<?php

/**
 * membership controller
 */
class adminEnd extends controller
{ 

  public function index(){   
   $accountsModel = $this->loadModel('accountsModel');
   $user_info = $this->bootstrap();    
        if(isset($_POST['adminCode'])){
            $coded = $accountsModel->getAdminCode($user_info['id']);
             $code = base64_decode($coded); 
            if($code === $_POST['adminCode']){
             $_SESSION['adminCode'] = $code;
                $feedback = "<div class='alert alert-success' >
                <strong class='text-success'>
                Code Accepted 
                </strong>
                Signing you in .........   
                <script>                 
                setTimeout(function() {
                window.location.href = '".URL."/adminEnd/admin';  
                }, 3000);             

                </script>                
                </div>";             

              }else{
                  echo "<script> alert('The code you have Provide is wrong!, Please Try again');               
              </script>";}

          }else{
             require_once APP_DIR . "views/adminLogin.php";
          }
          require_once APP_DIR . "views/adminLogin.php";
    }

    public function checkSessionOfAdmin(){
        if(!isset($_SESSION['adminCode']) AND empty($_SESSION['adminCode'])){
           require_once APP_DIR . "views/adminLogin.php";
           return false;
        }else{
            return true;
        }
    }
    public function admin()
    {   
        $check = $this->checkSessionOfAdmin();
        $user_info = $this->bootstrap();
        $accountsModel = $this->loadModel('accountsModel');
        $postModel = $this->loadModel('postModel');
        $regModel = $this->loadModel('regModel');
        $administrativeModel = $this->loadModel('administrativeModel');  
        $reportsModel = $this->loadModel('reportsModel');
        $eventsModel = $this->loadModel('eventsModel');
        $all_clinics = $administrativeModel->getClinicMembers();
        $count_all_events = $eventsModel->count_all_events();
        $all_labels = $reportsModel->labels();
        $clinic_colors = $reportsModel->campusColors(); 
        $comments_count = $postModel->comments_count();
        $jobModel = $this->loadModel('jobModel');
        $count_posts = $postModel->count_all_posts();
        $count_clients = $regModel->count_clients_all();
        $count_companies_all = $regModel->count_companies_all();
        $jobs_count = $jobModel->jobs_count();
        $count_all_categories = $postModel->count_all_categories();
        $online = $accountsModel->getOnlineNumber();
        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $head->set('online', $online);
        $head->render();

        $content = $this->loadView('template-dashboard');
        $content->set('user_info', $user_info);
        $content->set('count_clients', $count_clients);
        $content->set('administrativeModel', $administrativeModel);
        $content->set('count_posts', $count_posts);
        $content->set('jobs_count', $jobs_count);
        $content->set('all_clinics', $all_clinics);
        $content->set('labels',$all_labels);
        $content->set('count_all_categories',$count_all_categories);
        $content->set('count_events', $count_all_events);
        $content->set('comments_count', $comments_count);
        $content->set('count_companies_all',$count_companies_all);
        $content->set('pageTitle', 'DASHBOARD - BUMSA IMS');
        $content->render();
        $footer = $this->loadView('common/footer');
        $pieLables = json_encode(['ALUMNI','POSTS','JOBS','USERS','EVENTS','COMMENTS']);
        $pie = [];
        $pie[] = $count_clients;
        $pie[] = $count_posts;
        $pie[] = $jobs_count;
        //$pie[] = $users_count;
        $pie[] = $count_all_events;
        $pie[] = $comments_count;
        $dataPoints = array( 
        array("label"=>"ALUMNI", "symbol" => "ALUMNI","y"=>$count_clients),
        array("label"=>"POSTS", "symbol" => "POSTS","y"=>$count_posts),
        array("label"=>"JOBS", "symbol" => "JOBS","y"=>$jobs_count),
        array("label"=>"EVENTS", "symbol" => "EVENTS","y"=>$count_all_events),
        array("label"=>"COMMENTS", "symbol" => "COMMENTS","y"=>$comments_count),
       // array("label"=>"USERS", "symbol" => "USERS","y"=>$users_count)
         ); 

        $male = $reportsModel->getMaleData();
        $female = $reportsModel->getFemaleData();       
        $footer->set('labels',$all_labels);
        $footer->set('pieLables',$pieLables);
        $footer->set('pie',json_encode($pie));
        $footer->set('clinic_colors',$clinic_colors);
        $footer->set('male',$male);
        $footer->set('DataPointsForMembers',$administrativeModel->DataPointsForMembers());
        $footer->set('dataPoints',$dataPoints);
        $footer->set('female',$female);
        $footer->render();

    }

    public function site_settings()
    {
        $user_info = $this->bootstrap();
        $system_settings = $this->loadModel('settingsModel');
        $settings = $system_settings->get_settings();
        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $head->render();
        $content = $this->loadView('settings/site_settings');
        $content->set('pageTitle', 'Settings - BUMSA IMS');
        $content->set('user_info', $user_info);
        $content->set('system_settings', $settings);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();

    }


    public function labels(){
        $reportsModel = $this->loadModel('reportsModel');
        $all_labels = $reportsModel->campusColors();
        echo $all_labels;
    }

    //contacts

    public function contacts()
    {
        $user_info = $this->bootstrap();
        $system_settings = $this->loadModel('settingsModel');
        $get_contacts = $system_settings->get_contacts();
        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $head->render();
        $content = $this->loadView('settings/contacts');
        $content->set('pageTitle', 'CONTACTS - BUMAA IMS');
        $content->set('user_info', $user_info);
        $content->set('get_contacts', $get_contacts);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();

    }

    //newsletter

    public function newsletter()
    {
        $user_info = $this->bootstrap();
        $system_settings = $this->loadModel('settingsModel');
        $newsletters = $system_settings->get_newsletter();
        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $head->render();
        $content = $this->loadView('settings/newsletter');
        $content->set('pageTitle', 'Newsletter - BUMSA IMS');
        $content->set('user_info', $user_info);
        $content->set('newsletters', $newsletters);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();

    }

    public function reply_newsletter($id = null){
        $user_info = $this->bootstrap();
        $system_settings = $this->loadModel('settingsModel');
        $newsletters_person = $system_settings->get_newsletter_person(base64_decode($id));
        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $head->render();
        //
        $content = $this->loadView('settings/reply-newsletter');
        $content->set('pageTitle', 'Reply Newsletter- BUMSA IMS');
        $content->set('newsletters_person',$newsletters_person);       
        $content->set('user_info', $user_info);        
        $content->render();
        //
        $footer = $this->loadView('common/footer');
        $footer->render();
    }


    public function reply_newsletter_action(){
       $user_info = $this->bootstrap(); 
       $system_settings = $this->loadModel('settingsModel');       
        if(isset($_POST['email'])){

        $email = $_POST['email'];
        $new_id = $_POST['id'];
        $messageBody = $_POST['message']; 
        $data = array('email' =>$email,'message'=>$messageBody );
        $sender = $user_info['email'];    
        $messageSubject = "Official Message";
        $messageOnSuccess = "Successifully sent message";
        $messageOnfail = "Ooops failed to send message, try again";
     
        @$mail = $this->loadHelper('email');
         $messageHtml = $mail->send_newsletter_feedback($data);
        @$mail->sendMessage($email, $messageSubject, '', $messageHtml, $messageOnSuccess, $messageOnfail); 
             
        $result = $system_settings->sent_newsletter($messageBody,$sender,$email);
         if($result ){
        $resx = json_encode($result);
        header('Content-Type: application/json');
        echo $resx;            
          }
        } 

    } 


    private  function unique_code($limit){
        $code = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
        return $code;
    }


     public function send_admin_code(){
       $user_info = $this->bootstrap(); 
       $accountsModel = $this->loadModel('accountsModel');       
        if(isset($_POST['email'])){

        $email = $_POST['email'];
        $new_id = $_POST['id'];
        $code = $this->unique_code(4);
        $adminCode = base64_encode($code); 
        $data = array('email' =>$email,'code'=>$code );
        $sender = $user_info['email'];    
        $messageSubject = "Admin Secret Code";
        $messageOnSuccess = "Successifully sent Code";
        $messageOnfail = "Ooops failed to send code, try again";
     
        @$mail = $this->loadHelper('email');
         $messageHtml = $mail->send_admin_code_temp($data);
        @$mail->sendMessage($email, $messageSubject, '', $messageHtml, $messageOnSuccess, $messageOnfail);              
        $result = $accountsModel->sendAdminCode($new_id,$adminCode);
         if($result ){
        $resx = json_encode($result);
        header('Content-Type: application/json');
        echo $resx;            
          }
        } 

    }


       public function reply_contact($id){
        $user_info = $this->bootstrap();
        $system_settings = $this->loadModel('settingsModel');
        $contact_person = $system_settings->get_contact_person(base64_decode($id));
        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $head->render();
        //
        $content = $this->loadView('settings/reply-contact');
        $content->set('pageTitle', 'Reply Contact- BUMSA IMS');
        $content->set('contact_person',$contact_person);
        $content->set('user_info', $user_info);        
        $content->render();
        //
        $footer = $this->loadView('common/footer');
        $footer->render();


    }

    public function online()
    {
        $user_info = $this->bootstrap();
        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $head->render();
        $content = $this->loadView('settings/online');
        $content->set('pageTitle', 'ONLINE MEMBERS - BUMSA IMS');
        $content->set('user_info', $user_info);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();

    }

    public function profile($id)
    {
        $user_info = $this->bootstrap();
        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $regModel = $this->loadModel('regModel');
        $details = $regModel->profile(base64_decode($id));
        $head->render();
        $content = $this->loadView('settings/profile');
        $content->set('pageTitle', 'PROFILE - BUMSA IMS');
        $content->set('details', $details);
        $content->set('id', base64_decode($id));
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();

    }


    public function accessLogs()
    {
        $user_info = $this->bootstrap();
        $head = $this->loadView('common/header');
        $head->set('user_info', $user_info);
        $regModel = $this->loadModel('regModel');
        $administrativeModel = $this->loadModel('administrativeModel');
        $recent_switches = $administrativeModel->fetchRecentSwitch($user_info['clinic_id']);        
        $head->render();
        $content = $this->loadView('administrative/accessLogs');
        $content->set('pageTitle', 'Accesslogs - BUMSA IMS');
        $content->set('recent_switches',$recent_switches);
        $content->render();
        $footer = $this->loadView('common/footer');
        $footer->render();

    }

    //end of methods

}
