<?php
class starter_page extends Controller
{
    public function index()
    {
        $user_info = $this->bootstrap();
        $homeModel = $this->loadModel('homeModel');
        $head = $this->loadView('common/header');
        $head->set('user_info',$user_info);

        $head->set('pageTitle','Starter Page - ULS IMS');
        $head->render();
        //
        $content = $this->loadView('template-starter-page');
        $content->render();
        //
        $footer = $this->loadView('common/footer');
        $footer->render();
    }
}