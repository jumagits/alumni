<?php 

/**
 * 
 */
class frontend extends controller
{
    
    public function index(){}
     public function alumni_employer(){

       // $bumaa_clients = $this->loadModel('bumaa_clients');

        //$all_clients = $bumaa_clients->get_bumaa_clients();

        $head = $this->loadView('front/header');      
        
        $head->render();

        $content = $this->loadView('frontend/alumni_employer');

        $content->set('pageTitle', 'ALUMNI EMPLOYER ');

       // $content->set('all_clients',$all_clients);

        $content->render();

        $footer  = $this->loadView('front/footer');
       
        $footer->render();


    }


       public function faqs(){

       

        $head = $this->loadView('front/header');      
        
        $head->render();

        $content = $this->loadView('frontend/faq');

        $content->set('pageTitle', 'FAQS ');

       

        $content->render();

        $footer  = $this->loadView('front/footer');
       
        $footer->render();


    }


  


     public function alumni_employee(){

        $catsModel = $this->loadModel('catsModel');

        $all_clinics = $catsModel->getClinics();

        $head = $this->loadView('front/header');      
        
        $head->render();

        $content = $this->loadView('frontend/alumni_employee');

        $content->set('pageTitle', 'ALUMNI REGISTRATION ');

        $content->set('all_clinics',$all_clinics);

        $content->render();

        $footer  = $this->loadView('front/footer');
       
        $footer->render();


    }

      public function blog(){

        $postModel     = $this->loadModel('postModel'); 
        $pages = $this->loadHelper('paginator_helper');       
        $total_item = $postModel->count_all_posts();
        $url = URL.'frontend/blog/';
        $page_item = 2;
        $current_page = 0;        
        $pages->config($total_item, $page_item, $current_page, $url);
        $pagination = $pages->format();  
        $all_posts = $postModel->all_posts();
        $recent_posts = $postModel->recent_posts();
        $all_categories = $postModel->all_categories();
        $head = $this->loadView('front/header'); 
        $head->render();
        $content = $this->loadView('frontend/blog');
        $content->set('pageTitle', 'Blog ');
        $content->set('all_posts', $recent_posts);
        $content->set('all_categories', $all_categories);
        $content->set('recent_posts', $recent_posts);
        $content->set('pagination', $pagination);
        $content->render();
        $footer  = $this->loadView('front/footer');       
        $footer->render();


    }

      public function blog_details($id){
        $postModel     = $this->loadModel('postModel');
        $original_id = base64_decode($id);
        $all_posts = $postModel->all_posts();
        $recent_posts = $postModel->recent_posts();
        $all_comments = $postModel->all_comments_per_post($original_id);
        $post_details = $postModel->get_post_by_id($original_id);
        $all_categories = $postModel->all_categories();
        $pageTitle = 'POST DETAILS';
        $head = $this->loadView('front/header');              
        $head->render();
        $content = $this->loadView('frontend/blog_details');
        $content->set('recent_posts', $recent_posts);
        $content->set('all_posts', $all_posts);
        $content->set('post_details', $post_details);
        $content->set('all_categories',$all_categories);
        $content->set('all_comments',$all_comments);
        $content->set('pageTitle', $pageTitle);
        $content->render();
        $footer  = $this->loadView('front/footer');       
        $footer->render();


    }

    //posts per category

     public function category($id){
        $postModel     = $this->loadModel('postModel');
        $original_id = base64_decode($id);
        $all_posts = $postModel->all_posts_per_cat($original_id);
        $recent_posts = $postModel->recent_posts();
        $all_comments = $postModel->all_comments_per_post($original_id);        
        $all_categories = $postModel->all_categories();
        $pageTitle = 'POSTS PER CATEGORY';
        $head = $this->loadView('front/header');  
        $head->render();
        $content = $this->loadView('frontend/blog_cat');
        $content->set('recent_posts', $recent_posts);
        $content->set('all_posts', $all_posts);
        $content->set('all_categories',$all_categories);
        $content->set('all_comments',$all_comments);
        $content->set('pageTitle', $pageTitle);
        $content->render();
        $footer  = $this->loadView('front/footer');       
        $footer->render();
    }



    //subscribe

    public function subscribe(){

        $postModel    = $this->loadModel('validationModel');        
        $form_feedback = $postModel->subscribe(               
                $_POST['email']               
        );  

          if($form_feedback['status'])
            {               
                @$email = $_POST['email']; 
                $messageSubject = "Subscription ";
                $messageOnSuccess = "<div class='alert alert-block alert-success'><span class='glyphicon glyphicon-ok-sign'></span><strong> SUCCESS : </strong> User <strong>{$names}</strong> Successfully Subscribed !</div>";
                $messageOnfail = "";                
                $messageBody = "
                <p>&nbsp;</p>                
                <p>Thanks for subscribing to the  BUMSA MUSLIM STUDENTS ALUMNI</p>
                <p>&nbsp;</p>
                <p>We are glad to inform you that you will receive most of the News and Events Happening in BUMAA, InshaAllah</p>                
                <p>&nbsp;</p>
                <p>Thank you</p>
                <p><small>BUMAA MANAGEMENT</small></p>               
                <p>&nbsp;</p>";
                $mail = $this->loadHelper('email');
                $mail->sendMessage($email,$messageSubject,'',$messageBody,$messageOnSuccess,$messageOnfail);
            }


            //end sending

        $res = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res;


    }





     public function contact()
    {      
       
        $accountsModel = $this->loadModel('validationModel');
         $fullname = $_POST['fullname'];         
         $phone    = $_POST['phone'];
         $subject  = $_POST['subject'];
         $email    = $_POST['email'];
         $message  = $_POST['message'];       
        $form_feedback = $accountsModel->contact(
                $fullname,$phone,$subject,$email,$message,
               
            );
            if($form_feedback['status'])
            {
                @$names = $_POST['fullname'];
                @$email = $_POST['email'];
                $username = $_POST['username'];
                $messageSubject = "CONTACTING BUMAA ";
                $messageOnSuccess = "<div class='alert alert-block alert-success'><span class='glyphicon glyphicon-ok-sign'></span><strong> SUCCESS : </strong> User <strong>{$names}</strong> Successfully Subscribed !</div>";
                $messageOnfail = "";                
                $messageBody = "
                <p>&nbsp;</p>
                <p>Dear {$names},</p>
                <p>Thanks for contacting the  BUMSA MUSLIM STUDENTS ALUMNI</p>
                <p>&nbsp;</p>
                <p>We are glad to inform you that your response has been recorded and we shall be back to you shortly.</p>                
                <p>&nbsp;</p>
                <p>Thank you</p>
                <p><small>BUMAA MANAGEMENT</small></p>               
                <p>&nbsp;</p>";
                $mail = $this->loadHelper('email');
                $mail->sendMessage($email,$messageSubject,'',$messageBody,$messageOnSuccess,$messageOnfail);
            }


            //end sending
            

        $res = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res;
       
    }

    //generate password
      public function generatePassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if(strpos($available_sets, 'l') !== false)
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        if(strpos($available_sets, 'u') !== false)
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        if(strpos($available_sets, 'd') !== false)
            $sets[] = '23456789';
        if(strpos($available_sets, 's') !== false)
            $sets[] = '!@#$%&*?';
        $all = '';
        $password = '';
        foreach($sets as $set)
        {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for($i = 0; $i < $length - count($sets); $i++)
            $password .= $all[array_rand($all)];
        $password = str_shuffle($password);
        if(!$add_dashes)
            return $password;
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while(strlen($password) > $dash_len)
        {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    public function generatePIN($digits = 3)
    {
        $i = 0;
        $pin = "";
        while ($i <= $digits) {
            $pin .= mt_rand(0, 9);
            $i++;
        }
        return $pin;
    }

    public function generate_bumaa_client_refnumber()
    {
        # ULS/{$client_count}/{$current_year}                
        $client_count =  $this->generatePIN();
        $year         = date('Y');
        $no_ref       = $client_count;
        $ref_number   = "BUMAA/{$no_ref}/{$year}";
        return $ref_number;
    }

    //company Number


     public function generate_company_refnumber()
    {
        # ULS/{$client_count}/{$current_year}                
        $client_count =  $this->generatePIN();
        $year         = date('Y');
        $no_ref       = $client_count;
        $ref_number   = "COMP/BUMAA/{$no_ref}/{$year}";
        return $ref_number;
    }



    //alumni register



     public function alumni_register_in()
    {    
        $validationModel = $this->loadModel('validationModel');
         $ref_no = $this->generate_bumaa_client_refnumber();
         $student_number = $_POST['student_number'];         
         $transcript_serial    = $_POST['transcript_serial'];
         $email  = $_POST['email'];
         $firstname = $_POST['firstname'];
         $lastname  = $_POST['lastname'];
         $username    = $_POST['username'];
         $password  = $_POST['password'];
         $cpassword  = $_POST['cpassword'];
         $phone = $_POST['phone'];
         $clinic  = $_POST['clinic'];
         $sex = $_POST['sex'];
         $from_year = $_POST['from_year'];
         $upto_year = $_POST['upto_year'];
         $knowabout = $_POST['knowabout'];
         $maritalStatus = $_POST['maritalStatus'];
         $course = $_POST['course'];
            $form_feedback = $validationModel->registerA(
                $ref_no,$student_number,$transcript_serial,$email,$username,$password,$cpassword,$clinic,$firstname,$lastname,$sex,$knowabout,$phone,$maritalStatus,$course,$from_year,$upto_year);
            //sending mail
            if($form_feedback['status'])
            {
                @$names = $_POST['firstname']." ".$_POST['lastname'];
                @$email = $_POST['email'];
                $username = $_POST['username'];
                $messageSubject = "Login Credentials";
                $messageOnSuccess = "<div class='alert alert-block alert-success'><span class='glyphicon glyphicon-ok-sign'></span><strong> SUCCESS : </strong> User <strong>{$names}</strong> Successfully Added !</div>";
                $messageOnfail = "";  

                $data = array(
                    'username'=>$username,
                    'password'=>$password,
                    'email'=>$email,
                    'names'=>$names
                );     
                $mail = $this->loadHelper('email');
                $messageBody = $mail->send_credentials($data);                
                $mail->sendMessage($email,$messageSubject,'',$messageBody,$messageOnSuccess,$messageOnfail);
            }
            //end sending           

            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
       
    }

     public function alumni_register_in_update()
    {    
         $validationModel = $this->loadModel('validationModel');
         $student_number = $_POST['student_number'];         
         $transcript_serial  = $_POST['transcript_serial'];
         $email  = $_POST['email'];
         $user_id  = $_POST['user_id'];
         $firstname = $_POST['firstname'];
         $lastname  = $_POST['lastname'];        
         $phone = $_POST['phone'];
         $clinic  = $_POST['clinic'];
         $sex = $_POST['sex'];
         $from_year = $_POST['from_year'];
         $upto_year = $_POST['upto_year'];
         $knowabout = $_POST['knowabout'];
         $course = $_POST['course'];
         
         $form_feedback = $validationModel->alumni_update(
                $user_id,$student_number,$transcript_serial,$email,$firstname,$lastname,$clinic,$sex,$knowabout,$phone,$course,$from_year,$upto_year);
           
           if($form_feedback['status'])
            {
                @$names = $_POST['firstname']." ".$_POST['lastname'];
                @$email = $_POST['email'];              
                $messageSubject = "Profile Update";
                $messageOnSuccess = "<div class='alert alert-block alert-success'><span class='glyphicon glyphicon-ok-sign'></span><strong> SUCCESS : </strong> User <strong>{$names}</strong> Successfully updated profile !</div>";
                $messageOnfail = "";  
                $data = [];
                $data['names'] = $names;
                $mail = $this->loadHelper('email');
                $messageBody = $mail->send_profile_update($data);
                $mail->sendMessage($email,$messageSubject,'',$messageBody,$messageOnSuccess,$messageOnfail);
            }
            //end sending           

            $response = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $response;
       
    }

//company registration


     public function company_register_in()
    {
       
       
         $validationModel = $this->loadModel('validationModel');
         $ref_number = $this->generate_company_refnumber();
         $companyEmail  = $_POST['companyEmail'];
         $username    = $_POST['username'];
         $password  = $_POST['password'];
         $cpassword  = $_POST['cpassword'];
         $about_company  = $_POST['about_company'];
         $companyLocation  = $_POST['companyLocation'];
         $companyName    = $_POST['companyName'];

          $form_feedback = $validationModel->registerC($ref_number,$companyEmail,$username,$password,$cpassword,$about_company,$companyLocation,$companyName);


            //sending mail


            if($form_feedback['status'])
            {
                @$names = $_POST['companyName'];
                @$email = $_POST['companyEmail'];
                $username = $_POST['username'];

                $messageSubject = "Login Credentials";
                $messageOnSuccess = "<div class='alert alert-block alert-success'><span class='glyphicon glyphicon-ok-sign'></span><strong> SUCCESS : </strong> User <strong>{$names}</strong> Successfully Added !</div>";
                $messageOnfail = "";
                
                $messageBody = "
                <p>&nbsp;</p>
                <p>Dear {$names},</p>
                <p>Please, use the following credentials to access the BUMAA System</p>
                <p>Username : <strong>{$username}</strong></p>
                <p>Password : <strong>{$password}</strong></p>
                <p>It is recomended that you change the password after login.</p>
                <p>&nbsp;</p>
                <p><a href='".URL."accounts/login'>To login into your account, please follow this link. </a></p>
                <p>&nbsp;</p>
                <p>Thank you</p>
                <p><small>BUMAA MANAGEMENT</small></p>
               
                <p>&nbsp;</p>";
                $mail = $this->loadHelper('email');
                $mail->sendMessage($email,$messageSubject,'',$messageBody,$messageOnSuccess,$messageOnfail);
            }


            //end sending

            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
       
    }




   //methods end here
}








 ?>