<?php 

/**
 * Posts controller
 */
class post extends controller
{
      public function index(){
        $user_info = $this->bootstrap();  
        $postModel     = $this->loadModel('postModel');
        $all_posts = $postModel->all_posts();
        $head = $this->loadView('common/header');
        $head->set('user_info',$user_info); 
        $head->render();
        $content = $this->loadView('posts/index');
        $content->set('pageTitle', 'NEWS - ');
        $content->set('user_info',$user_info);
        $content->set('all_posts', $all_posts);
        $content->render();
        $footer  = $this->loadView('common/footer');       
        $footer->render();
    }


    public function show($id){
        $real_id = base64_decode($id);
        $user_info = $this->bootstrap();
        $categoryModel     = $this->loadModel('categoryModel');
        $postModel     = $this->loadModel('postModel');
        $post = $postModel->get_post_by_id($real_id);
        $all_cats = $categoryModel->all_cats();
        $head = $this->loadView('common/header');
        $head->set('user_info',$user_info);       
        $head->render();
        $content = $this->loadView('posts/edit-post');
        $content->set('pageTitle', 'Update Post - ');
        $content->set('user_info',$user_info); 
        $content->set('category',$categoryModel);
        $content->set('post',$post);
        $content->set('all_cats',$all_cats);                
        $content->render();
        $footer  = $this->loadView('common/footer');       
        $footer->render();
    }


//show 

      public function add_post(){
        $user_info = $this->bootstrap();
        $categoryModel     = $this->loadModel('categoryModel');
        $all_cats = $categoryModel->all_cats();
        $head = $this->loadView('common/header');
        $head->set('user_info',$user_info); 
        $head->render();
        $content = $this->loadView('posts/add_post');
        $content->set('pageTitle', 'Add Post - ');
        $content->set('user_info',$user_info); 
        $content->set('all_cats',$all_cats);                
        $content->render();
        $footer  = $this->loadView('common/footer');       
        $footer->render();
    }


     //create


      public function create(){

       $user_info = $this->bootstrap();
        $postModel  = $this->loadModel('postModel');
        $uploader = $this->loadHelper('uploader');
       
        $data = $uploader->upload($_FILES['photo'],$this->upload_array());
        if($data['isComplete'])
        {
            $info      = $data['data'];           
            if(!empty($info['metas'][0]['name'])){
                $photo     = urlencode($info['metas'][0]['name']);
            }else{
                $photo  = "";
            }


            

            $post_author = $_POST['post_author'];
            $post_status = 'Drafted';
            $post_date   = date('Y-m-d');
           
            $form_feedback = $postModel->addPost(               
               
                $_POST['post_title'],  
                $_POST['post_category_id'], 
                $post_author, 
                $photo, 
                $post_status,
                $_POST['post_content'], 
                $post_date,
                $_POST['post_tags']   
            );
            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
        }

        if($data['hasErrors']){
            $errors = json_encode( $data['errors'] );
            header('Content-Type: application/json');
            echo $errors;
        }
        
    }

    //update post



      public function update(){

       $user_info = $this->bootstrap();
        $postModel  = $this->loadModel('postModel');
        $uploader = $this->loadHelper('uploader');
       
        $data = $uploader->upload($_FILES['photo'],$this->upload_array());
        if($data['isComplete'])
        {
            $info      = $data['data'];           
            if(!empty($info['metas'][0]['name'])){
                $photo     = urlencode($info['metas'][0]['name']);
            }else{
                $photo  = "";
            }
            
            $post_status = 'Drafted';
            $post_date   = date('Y-m-d');           
            $form_feedback = $postModel->updatePost(               
                $_POST['id'], 
                $_POST['post_title'],  
                $_POST['post_category_id'], 
                $_POST['post_author'], 
                $photo, 
                $post_status,
                $_POST['post_content'], 
                $post_date,
                $_POST['post_tags']
               
            );            

            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;
        }

        if($data['hasErrors']){
            $errors = json_encode( $data['errors'] );
            header('Content-Type: application/json');
            echo $errors;
        }
        
    }

    //comment create

    public function create_comment(){

        $user_info = $this->bootstrap();
        $postModel  = $this->loadModel('postModel');
        $comment_date = date('Y-m-d');
        $comment_status = 'Unapproved';
        $form_feedback = $postModel->addComment(               
                $_POST['post_id'],
                $_POST['comment_email'],         
                $_POST['comment_content'],
                $comment_date,
                $_POST['comment_author'],
                $comment_status
        );           

        $res = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res;


    }

    //subscribers

        public function subscribers(){
        $user_info = $this->bootstrap();  
        $postModel     = $this->loadModel('postModel');
        $all_subscribers = $postModel->all_subscribers();
        $head = $this->loadView('common/header'); 
        $head->set('user_info',$user_info);   
        $head->render();
        $content = $this->loadView('posts/subscribers');
        $content->set('pageTitle', 'SUBSCRIBERS - ');
        $content->set('user_info',$user_info);   
        $content->set('all_subscribers', $all_subscribers);
        $content->render();
        $footer  = $this->loadView('common/footer');       
        $footer->render();
    }

//comments

        public function comments(){
        $user_info = $this->bootstrap();  
        $postModel     = $this->loadModel('postModel');
        $all_comments = $postModel->all_comments();
        $head = $this->loadView('common/header'); 
        $head->set('user_info',$user_info);  
        $head->render();
        $content = $this->loadView('posts/comments');
        $content->set('pageTitle', 'COMMENTS - ');
        $content->set('user_info',$user_info); 
        $content->set('all_comments', $all_comments);
        $content->render();
        $footer  = $this->loadView('common/footer');
        $footer->render();
    }

    //comments approve


     public function approve($id)
    {
        $user_info  = $this->bootstrap();
        $postModel     = $this->loadModel('postModel');
        $deactComment  = $postModel->get_unapproved_comment(base64_decode($id));
        $form_feedback  =  ($postModel->approveComment($deactComment[0]->comment_id));
        if($form_feedback)
        {
           $this->redirect('post/comments');

        }
}


//unapprove


     public function unapprove($id)
    {
    $user_info  = $this->bootstrap();
    $postModel     = $this->loadModel('postModel');
    $deactComment = $postModel->get_unapproved_comment(base64_decode($id));
    $form_feedback  =  ($postModel->unapproveComment($deactComment[0]->comment_id));
    if($form_feedback)
    {
       $this->redirect('post/comments');
    }
  }


//search

     public function search()
    {
        $postModel       = $this->loadModel('postModel');       
        $form_feedback  = $postModel->search_post($_POST['post_title']);
        $res = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res;
    }


  public function upload_array(){
    return  array(
            'limit' => 100, //Maximum Limit of files. {null, Number}
            'maxSize' => 1024, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => UPLOAD_DIR."/posts/", //Upload directory {String}
            'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => true, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        );
}

//end of methods


}

 ?>