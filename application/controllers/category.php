<?php 

/**
 * Jobs controller
 */
class category extends controller
{


      public function index(){
        $user_info = $this->bootstrap();
        $categoryModel     = $this->loadModel('categoryModel');
        $all_cats = $categoryModel->all_cats();
        $head = $this->loadView('common/header'); 
        $head->set('user_info',$user_info); 
        $head->render();
        $content = $this->loadView('categories/index');
        $content->set('pageTitle', 'POST CATEGORIES - BUMSA IMS');
        $content->set('user_info',$user_info);
        $content->set('all_cats', $all_cats);
        $content->render();
        $footer  = $this->loadView('common/footer');       
        $footer->render();
    }


    public function add_category(){

        $user_info      = $this->bootstrap();
        $categoryModel      = $this->loadModel('categoryModel');

        if(isset($_POST['cat_title']))
        {
            $form_feedback = $categoryModel->add_cat($_POST["cat_title"]);
        }
        $res_pass = json_encode($form_feedback);
        header('Content-Type: application/json');
        echo $res_pass;
    }


    //show 


      public function show($id){
        $user_info      = $this->bootstrap();
        $d_id = base64_decode($id);
        $categoryModel = $this->loadModel('categoryModel');
        $fetchCat = $categoryModel->fetchCat($d_id);            
        $head   =  $this->loadView('common/header'); 
        $head->set('user_info',$user_info); 
        $head->set('pageTitle','Edit Cat  -BUMAA');
        $head->render();
        $content     = $this->loadView('categories/edit-category');
        $content->set('user_info',$user_info);
        $content->set('category',$fetchCat);
        $content->render();
        $footer      = $this->loadView('common/footer');
        $footer->render();
    }


    //delete category


    public function delete($id)
    {
        $del_id = $_GET['id'];
        $user_info = $this->bootstrap();
        $categoryModel = $this->loadModel("categoryModel");
        $result = $categoryModel->delete_cate($del_id);        
        $res_pass = json_encode($result);
        header('Content-Type: application/json');
        echo $res_pass;
    }


    //update


    public function update(){

           $categoryModel    = $this->loadModel('categoryModel');
             if(isset($_POST['cat_title']))
            {
                $form_feedback = $categoryModel->editCate($_POST["cat_title"],$_POST["cat_id"]); 
            if($form_feedback['status']){
            $res = json_encode($form_feedback);
            header('Content-Type: application/json');
            echo $res;

            }

        }

    }

}









 ?>