<?php
class validate extends Controller
{
   

// checking_any_field_existance('field','customer_id','customers','customer_names');



public function check(){

    $validationModel = $this->loadModel('validationModel');
   //reg num
      if (isset($_POST['num']) AND !empty($_POST['num'])) {
          $form_feedback = $validationModel->regnum_check($_POST['num']);
       }

       //email
    if(isset($_POST['email']) AND !empty($_POST['email'])){

        $form_feedback = $validationModel->checking_any_field_existance($_POST['email'],'id','company_instance','companyEmail');

    }


    $res = json_encode($form_feedback);
    header('Content-Type: application/json');
    echo $res;

}






class Media extends Generic{
  protected $path, $file, $name, $size, $type, $crop, $cropHeight, $cropWidth, $allowed,$avatar = false;

    public function __construct() {

    }

  public function setFile($data = array()) {

    
    if (isset($data['file']) && !empty($data['file'])) {
          $this->file = $this->secure($data['file']);
      }

      if (isset($data['name']) && !empty($data['name'])) {
          $this->name = $this->secure($data['name']);
      }

      if (isset($data['size']) && !empty($data['size'])) {
          $this->size = $this->secure($data['size']);
      }

      if (isset($data['type']) && !empty($data['type'])) {
          $this->type = $this->secure($data['type']);
      }

      if (isset($data['allowed']) && !empty($data['allowed'])) {
          $this->allowed = $this->secure($data['allowed']);
      }

      if (isset($data['crop']) && !empty($data['crop'])) {
          $this->crop = $data['crop'];
      }

      if (isset($data['crop']['height']) && !empty($data['crop']['height'])) {
          $this->cropHeight = $this->secure($data['crop']['height']);
      }

      if (isset($data['crop']['width']) && !empty($data['crop']['width'])) {
          $this->cropWidth = $this->secure($data['crop']['width']);
      }
      if (isset($data['avatar']) && !empty($data['avatar'])) {
          $this->avatar = $this->secure($data['avatar']);
      }
  }

  // Compress image size
  public function compressImage($source_url, $destination_url, $quality) {
        $imgsize = getimagesize($source_url);
        $finfof  = $imgsize['mime'];
        $image_c = 'imagejpeg';
        if ($finfof == 'image/jpeg') {
            $image = @imagecreatefromjpeg($source_url);
        } else if ($finfof == 'image/gif') {
            $image = @imagecreatefromgif($source_url);
        } else if ($finfof == 'image/png') {
            $image = @imagecreatefrompng($source_url);
        } else {
            $image = @imagecreatefromjpeg($source_url);
        }
        $quality = 50;
        if (function_exists('exif_read_data')) {
            $exif = @exif_read_data($source_url);
            if (!empty($exif['Orientation'])) {
                switch ($exif['Orientation']) {
                    case 3:
                        $image = @imagerotate($image, 180, 0);
                        break;
                    case 6:
                        $image = @imagerotate($image, -90, 0);
                        break;
                    case 8:
                        $image = @imagerotate($image, 90, 0);
                        break;
                }
            }
        }
        @imagejpeg($image, $destination_url, $quality);
        return $destination_url;
    }

  // Crop image + decrease image quality
  public function cropImage($max_width, $max_height, $source_file, $dst_dir, $quality = 80) {
      $imgsize = @getimagesize($source_file);
      $width   = $imgsize[0];
      $height  = $imgsize[1];
      $mime    = $imgsize['mime'];
      switch ($mime) {
          case 'image/gif':
              $image_create = "imagecreatefromgif";
              $image        = "imagegif";
              break;
          case 'image/png':
              $image_create = "imagecreatefrompng";
              $image        = "imagepng";
              break;
          case 'image/jpeg':
              $image_create = "imagecreatefromjpeg";
              $image        = "imagejpeg";
              break;
          default:
              return false;
              break;
      }
      $dst_img    = @imagecreatetruecolor($max_width, $max_height);
      $src_img    = $image_create($source_file);
      $width_new  = $height * $max_width / $max_height;
      $height_new = $width * $max_height / $max_width;
      if ($width_new > $width) {
          $h_point = (($height - $height_new) / 2);
          @imagecopyresampled($dst_img, $src_img, 0, 0, 0, $h_point, $max_width, $max_height, $width, $height_new);
      } else {
          $w_point = (($width - $width_new) / 2);
          @imagecopyresampled($dst_img, $src_img, 0, 0, $w_point, 0, $max_width, $max_height, $width_new, $height);
      }
      @imagejpeg($dst_img, $dst_dir, $quality);
      if ($dst_img)
          @imagedestroy($dst_img);
      if ($src_img)
          @imagedestroy($src_img);
  }




  public function uploadToFtp($filename = '', $delete_file = true) {
    if (empty(self::$config['ftp_host']) || empty(self::$config['ftp_username']) || empty(self::$config['ftp_password']) || empty(self::$config['ftp_port'])) {
            return false;
    }

    $ftp = new \FtpClient\FtpClient();
        $ftp->connect(self::$config['ftp_host'], false, self::$config['ftp_port']);
        $login = $ftp->login(self::$config['ftp_username'], self::$config['ftp_password']);

    if ($login) {
            $file_path = substr($filename, 0, strrpos( $filename, '/'));
            $file_path_info = explode('/', $file_path);
            $path = '';
            if (!$ftp->isDir($file_path)) {
                foreach ($file_path_info as $key => $value) {
                    $path .= '/' . $value . '/' ;
                    if (!$ftp->isDir($path)) {
                        $mkdir = $ftp->mkdir($path);
                    }
                } 
            }
            $ftp->chdir($file_path);
            if ($ftp->putFromPath($filename)) {
              if ($delete_file == true) {
                  @unlink($filename);
              }
                return true;
            }
    }
    
  }

  public function uploadToS3($filename = '', $delete_file = true) {
    if (empty(self::$config['amazone_s3_key']) || empty(self::$config['amazone_s3_s_key']) || empty(self::$config['region']) || empty(self::$config['bucket_name'])) {
            return false;
    }
    $s3 = new S3Client([
            'version'     => 'latest',
            'region'      => self::$config['region'],
            'credentials' => [
                'key'    => self::$config['amazone_s3_key'],
                'secret' => self::$config['amazone_s3_s_key'],
            ]
        ]);

        $s3->putObject([
            'Bucket' => self::$config['bucket_name'],
            'Key'    => $filename,
            'Body'   => fopen($filename, 'r+'),
            'ACL'    => 'public-read',
            'CacheControl' => 'max-age=3153600',
    ]);
    
    if ($s3->doesObjectExist(self::$config['bucket_name'], $filename)) {
      if ($delete_file == true) {
        @unlink($filename);
      }
            return true;
        }
  }

  public function uploadToGoogleCloud($file_name = '', $delete_file = true) {
    if (self::$config['google_cloud_storage'] == 0 || empty(self::$config['google_cloud_storage_service_account']) || empty(self::$config['google_cloud_storage_bucket_name'])) {
      return false;
    }
    //set which bucket to work in
    $bucketName = self::$config['google_cloud_storage_bucket_name'];
    // get local file for upload testing
    $fileContent = file_get_contents($file_name);
    // NOTE: if 'folder' or 'tree' is not exist then it will be automatically created !
    $cloudPath = $file_name;
    $isSucceed = uploadFiletoGoogleCloud($fileContent, $cloudPath);
    if ($isSucceed == true) {     
      return true;
    } else {
      return false;
    }
  }

  public function deleteFromFTPorS3($filename) {

      if (self::$config['amazone_s3'] == 0 && self::$config['ftp_upload'] == 0) {
          return false;
      }

      if (self::$config['ftp_upload'] == 1) {
          $ftp = new \FtpClient\FtpClient();
          $ftp->connect(self::$config['ftp_host'], false, self::$config['ftp_port']);
          $login = $ftp->login(self::$config['ftp_username'], self::$config['ftp_password']);
          
          if ($login) {
              $file_path = substr($filename, 0, strrpos( $filename, '/'));
              $file_name = substr($filename, strrpos( $filename, '/') + 1);
              $file_path_info = explode('/', $file_path);
              $path = '';
              if (!$ftp->isDir($file_path)) {
                  return false;
              }
              $ftp->chdir($file_path);
              $ftp->pasv(true);
              if ($ftp->remove($file_name)) {
                  return true;
              }
          }
      } else {
          $s3Config = (
              empty(self::$config['amazone_s3_key']) || 
              empty(self::$config['amazone_s3_s_key']) || 
              empty(self::$config['region']) || 
              empty(self::$config['bucket_name'])
          ); 

          if ($s3Config){
              return false;
          }
          $s3 = new S3Client([
              'version'     => 'latest',
              'region'      => self::$config['region'],
              'credentials' => [
                  'key'    => self::$config['amazone_s3_key'],
                  'secret' => self::$config['amazone_s3_s_key'],
              ]
          ]);

          $s3->deleteObject([
              'Bucket' => self::$config['bucket_name'],
              'Key'    => $filename,
          ]);

          if (!$s3->doesObjectExist(self::$config['bucket_name'], $filename)) {
              return true;
          }
      }
      return true;
  }

 

  public function ImportImage($url = '', $type = 0,$type2 = '') {
    $this->initDir();
    $dir         = "media/upload";
    $generate = date('Y') . '/' . date('m') . '/' . $this->generateKey(50,50) . '_' . date('d') . '_' . md5(time());
    $file_path   = "photos/" .$generate. "_image.jpg";
    $filename    = $dir . '/' . $file_path;
    $put_file = file_put_contents($filename, $this->curlConnect($url));
    if ($type == 1) {
      $crop_image = $this->cropImage(150, 150, $filename, $filename, 100);
    }
    if (file_exists($filename)) {
      $this->uploadToS3($filename);
      $this->uploadToFtp($filename);
    }
    return $filename;
  }

  public function ImportImageAndCrop($url = '', $type = '') {
    $this->initDir();
    $dir         = "media/upload";
    $generate = date('Y') . '/' . date('m') . '/' . $this->generateKey(50,50) . '_' . date('d') . '_' . md5(time());
    if ($type == 'gif') {
      $file_path   = "photos/" .$generate. "_image.gif";
      $c_path   = "photos/" .$generate. "_image_c.gif";
    }
    else{
      $file_path   = "photos/" .$generate. "_image.jpg";
      $c_path   = "photos/" .$generate. "_image_c.jpg";
    }
    $filename    = $dir . '/' . $file_path;
    $c_path    = $dir . '/' . $c_path;
    $put_file = file_put_contents($filename, $this->curlConnect($url));
    if (!empty($c_path)) {
      $crop_image = $this->cropImage(300, 300, $filename, $c_path, 75);
    }
    if (self::$config['ftp_upload'] == 1) {
      $upload_     = $this->uploadToFtp($filename, true);
      $upload_     = $this->uploadToFtp($c_path, true);
    } else if (self::$config['amazone_s3'] == 1) {
      $upload_     = $this->uploadToS3($filename, true);
      $upload_     = $this->uploadToS3($c_path, true);
    }
    if ($type == 'gif') {
      @unlink($filename);
    }
    return array('filename' => $filename,
                   'extra'    => $c_path);
  }

  public function initDir($dir = 'photos'){
    if (!file_exists("media/upload/$dir/" . date('Y'))) {
            @mkdir("media/upload/$dir/" . date('Y'), 0777, true);
        }

      if (!file_exists("media/upload/$dir/" . date('Y') . '/' . date('m'))) {
          @mkdir("media/upload/$dir/" . date('Y') . '/' . date('m'), 0777, true);
      }
    
    return $this;
  }

  function watermark_image($target) {

     if (self::$config['watermark'] != 'on' || empty(self::$config['watermark_link'])) {
         return false;
     }

     try {
       $image = new \claviska\SimpleImage();

       $image
         ->fromFile($target)
         ->autoOrient()
         ->overlay(self::$config['watermark_link'], 'top left', 1, 30, 30)
         ->toFile($target, 'image/jpeg');

       return true;
     } catch(Exception $err) {

       return $err->getMessage();
     }
  }
}




class Blogs extends User{
    protected $article_id = 0;

    public function all(){
        global $context;
        $posts = self::$db->get(T_BLOG,$this->limit);
        $data  = array();
        foreach ($posts as $key => $post_data) {
            $post_data['category_name'] = $context['lang'][$post_data['category']];
            $post_data['full_thumbnail'] = media($post_data['thumbnail']);
            $data[]    = $post_data;
        }
        return $data;
    }
  public function getLikes($type = 'up'){
    if (empty($this->$article_id)) {
      return false;
    }

    else if(!in_array($type, array('up','down'))){
      return false;
    }

    $post_id = $this->article_id;
    self::$db->where('post_id',$post_id);
    self::$db->where('type',$type);
    $likes   = self::$db->getValue(T_POST_LIKES,'COUNT(*)');

    return $likes;
  }
    public function likeBlog(){
    if (empty($this->article_id) || empty(IS_LOGGED)) {
      return false;
    }

    $user_id = self::$me->user_id;
    $post_id = (int)$this->article_id;
        $code    = 0;
    if ($this->isLiked()) {
      self::$db->where('post_id',$post_id);
      self::$db->where('user_id',$user_id);
      self::$db->delete(T_BLOG_LIKES);
      //self::$db->where('user_id' , $user_id)->where('post_id' , $post_id)->where('type' ,'liked__post')->delete(T_ACTIVITIES);
      $code = -1;
    }
    else{
      $insert = self::$db->insert(T_BLOG_LIKES,array(
        'post_id' => $post_id,
        'user_id' => $user_id,
        'time'    => time()
      ));
      // self::$db->insert(T_ACTIVITIES,array('user_id' => $user_id,
          //                                  'post_id' => $post_id,
          //                                  'type'    => 'liked__post',
          //                                  'time'    => time()));

      if (is_numeric($insert)) {
        $code = 1;
      }
    }

    return $code;
    }
    public function setBlogId($post_id = 0){
    $this->article_id = self::secure($post_id);

    if (empty($this->article_id) || !is_numeric($this->article_id)) {
      $this->throwError("Invalid argument: Post id must be a positive integer");
    }

    return $this;
  }
    public function addPostComment($re_data = array()){
    $re_data['post_id'] = $this->article_id;
    $re_data['user_id'] = self::$me->user_id;

    if (!empty($re_data['text'])) {
      $this->upsertHtags($re_data['text']);
    }
    // self::$db->insert(T_ACTIVITIES,array('user_id' => $re_data['user_id'],
      //                                      'post_id' => $re_data['post_id'],
      //                                      'type'    => 'commented_on_post',
      //                                      'time'    => time()));

    return self::$db->insert(T_BLOG_COMMENTS,$re_data);
    }
    public function likifyMentions($text = ""){
    $text = preg_replace_callback('/(?:^|\s|,)\B@([a-zA-Z0-9_]{4,32})/is', function($m){
      $uname = $m[1];
      if ($this->userNameExists($uname)) {
        return self::createHtmlEl('a',array(
          'href' => sprintf("%s/%s",self::$site_url,$uname),
          'target' => '_blank',
          'class' => 'mention',
        ),"@$uname");
      }
      else{
        return "@$uname";
      }
    }, $text);

    return $text;
  }
  public function tagifyHTags($text = ""){
    if (!empty($text) && is_string($text)) {
      preg_match_all('/(#\[([0-9]+)\])/i', $text, $matches);
      $matches = (!empty($matches[2])) ? $matches[2] : array();

      if (!empty($matches)) {   
        $htags = self::$db->where('id',$matches,"IN")->get(T_HTAGS,null,array('id','tag'));
        if (!empty($htags)) {
          foreach ($htags as $htag) {
            $text = str_replace("#[{$htag->id}]", "#{$htag->tag}", $text);
          }
        }
      }
    }

      return $text;
  }
  public function linkifyDescription($text =""){
        if (!empty($text) && is_string($text)) {
            preg_match_all('/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/im', $text, $matches, PREG_SET_ORDER, 0);
            foreach ($matches as $match) {
                if( $match[0] !== 'http://' && $match[0] !== 'https://' ) {
                    if (preg_match("/http(|s)\:\/\//", $match[0])) {
                        $text = str_replace( $match[0] , '<a href="' . strip_tags($match[0]) . '" target="_blank" class="hash" rel="nofollow">' . $match[0] . '</a>', $text);
                    }
                }
            }
        }
        return $text;
    }
    public function postCommentData($id = 0){

    $t_users = T_USERS;
    $t_comms = T_BLOG_COMMENTS;

    self::$db->join("{$t_users} u","c.user_id = u.user_id ","INNER");
    self::$db->where("c.id",$id);
      $comment = self::$db->getOne("{$t_comms} c","c.id,c.user_id,c.post_id,c.text,c.time,u.username,u.avatar");
    if (!empty($comment)) {
      $comment->is_owner = $this->isCommentOwner($id);
      $comment->text     = $this->likifyMentions($comment->text);
      $comment->text     = $this->linkifyHTags($comment->text);
      $comment->text     = $this->link_Markup($comment->text);
      $comment->likes    = 0;//self::$db->where('comment_id',$id)->getValue(T_COMMENTS_LIKES,'COUNT(*)');
      $comment->is_liked = 0;
      // if (self::$db->where('comment_id',$id)->where('user_id',self::$me->user_id)->getValue(T_COMMENTS_LIKES,'COUNT(*)')) {
      //  $comment->is_liked = 1;
      // }
      //$comment->replies    = self::$db->where('comment_id',$id)->getValue(T_COMMENTS_REPLY,'COUNT(*)');
    }
    return (array)$comment;
    }
    public function isCommentOwner($comment_id = 0,$user_id = 0){

    if ((empty($user_id) || !is_numeric($user_id)) && IS_LOGGED) {
      $user_id = self::$me->user_id;
    }

    $comment = self::$db->where("id",$comment_id)->getOne(T_BLOG_COMMENTS);

    if ( IS_ADMIN || $comment->user_id == self::$me->user_id) {
      return true;
    }
    return false;
    }
    public function deletePostComment($comment_id = 0){
    $comment = self::$db->where("id",$comment_id)->getOne(T_BLOG_COMMENTS);
    //self::$db->where('comment_id',$comment_id)->delete(T_COMMENTS_LIKES);
    //$comment_object = new Comments();
    //$replies = $comment_object->get_comment_replies($comment_id);
    //foreach ($replies as $key => $reply) {
    //  self::$db->where('reply_id',$reply->id)->delete(T_COMMENTS_REPLY_LIKES);
    //}
        //self::$db->where('comment_id',$comment_id)->delete(T_COMMENTS_REPLY);
    //self::$db->where('user_id' , $comment->user_id)->where('post_id' , $comment->post_id)->where('type' ,'commented_on_post')->delete(T_ACTIVITIES);
    self::$db->where("id",$comment_id);
    return self::$db->delete(T_BLOG_COMMENTS);
    }
    public function isLiked(){

    if (empty($this->article_id) || empty(IS_LOGGED)) {
      return false;
    }

    $user_id = self::$me->user_id;
    $post_id = $this->article_id;

    self::$db->where('post_id',$post_id);
    self::$db->where('user_id',$user_id);
    $likes   = self::$db->getValue(T_BLOG_LIKES,"COUNT(*)");

    return ($likes > 0);
    }
    public function getBlogComments($offset = false){
    if (empty($this->article_id)) {
      return false;
    }

    if ($offset && is_numeric($offset)) {
      self::$db->where('id',$offset,'<');
    }

    self::$db->where('post_id',$this->article_id)->orderBy('id','DESC');

    $commset  = self::$db->get(T_BLOG_COMMENTS,20,array('id'));
    $comments = array();

    if (!empty($commset)) {
      foreach ($commset as $key => $comment) {
        $comments[] = $this->postCommentData($comment->id);
      }
    }

    return $comments;
  }
}





class Generic{
  public static $db,$site_url, $config,$theme,$mysqli,$user,$loggedin,$langs;

  public function __construct($data = array()){
    self::$db = $data['db'];
    self::$site_url = $data['site_url'];
    self::$config   = $data['config'];
    self::$theme    = self::$config['theme'];
    self::$mysqli   = $data['mysqli'];
    self::$user   = self::getLoggedInUser_();
    self::$loggedin   = self::isLogged_();
    self::$langs   = self::getLangs();
  }

  // PX_LoadPage
  public static function PX_LoadPage($page_url = '', $data = array(), $set_lang = true) {
    //global $pt, $lang_array, $config, $fl_currpage, $countries_name;
    global $context,$me,$pixelphoto,$post_data,$comment,$msg_data,$udata,$purchase_code,$site_url;
    $lang_array = $context['lang'];
    $config = self::$config;
      $page = dirname(dirname(__dir__)).DIRECTORY_SEPARATOR.'apps'.DIRECTORY_SEPARATOR . self::$theme . DIRECTORY_SEPARATOR . $page_url . '.html';
      
      if (!file_exists($page)) {
          die("File not Exists : $page");
      }

      $page_content = '';
      ob_start();
      require($page);
      $page_content = ob_get_contents();
      ob_end_clean();

      if ($set_lang == true) {
          $page_content = preg_replace_callback("/{{LANG (.*?)}}/", function($m) use ($lang_array) {
              return (isset($lang_array[$m[1]])) ? $lang_array[$m[1]] : '';
          }, $page_content);
      }
      if (!empty($data) && is_array($data)) {
          foreach ($data as $key => $replace) {
              if ($key == 'USER_DATA') {
                  $replace = ToArray($replace);
                  $page_content = preg_replace_callback("/{{USER (.*?)}}/", function($m) use ($replace) {
                      return (isset($replace[$m[1]])) ? $replace[$m[1]] : '';
                  }, $page_content);
              } else {
                  $object_to_replace = "{{" . $key . "}}";
                  $page_content      = str_replace($object_to_replace, $replace, $page_content);
              }
          }
      }

      if (self::$loggedin == true) {
          $replace = o2array(self::$user);
          $page_content = preg_replace_callback("/{{ME (.*?)}}/", function($m) use ($replace) {
              return (isset($replace[$m[1]])) ? $replace[$m[1]] : '';
          }, $page_content);
      }

      $page_content = preg_replace("/{{LINK (.*?)}}/", self::PX_Link("$1"), $page_content);
      $page_content = preg_replace_callback("/{{CONFIG (.*?)}}/", function($m) use ($config) {
          return (isset(self::$config[$m[1]])) ? self::$config[$m[1]] : '';
      }, $page_content);

      return $page_content;
  }
  
  public static function PX_Link($string) {
      global $site_url;
      return $site_url . '/' . $string;
  }


  // Add media string to site link
    public static function getMedia($media) {
      return sprintf('%s/%s',self::$site_url,$media);
    }

    // Geenrate random string
  public function generateKey($minlength = 20, $maxlength = 20, $uselower = true, $useupper = true, $usenumbers = true, $usespecial = false) {
      $charset = '';
      if ($uselower) {
          $charset .= "abcdefghijklmnopqrstuvwxyz";
      }
      if ($useupper) {
          $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      }
      if ($usenumbers) {
          $charset .= "123456789";
      }
      if ($usespecial) {
          $charset .= "~@#$%^*()_+-={}|][";
      }
      if ($minlength > $maxlength) {
          $length = mt_rand($maxlength, $minlength);
      } else {
          $length = mt_rand($minlength, $maxlength);
      }
      $key = '';
      for ($i = 0; $i < $length; $i++) {
          $key .= $charset[(mt_rand(0, strlen($charset) - 1))];
      }
      return $key;
  }

  // Raise Exceptions
  public function throwError($message) {
    throw new \Exception($message);
    return;
  }






  public function lastQuery(){
    return self::$db->getLastQuery();
  }

  public static function createHtmlEl($tag_name = "html",$attrs = array(),$cont = ""){
    $tag_attrs = "";

    if (!empty($attrs) && is_array($attrs)) {
      foreach ($attrs as $attr => $value) {
        $tag_attrs .= " $attr=\"$value\"";
      }
    }
    
    return "<$tag_name$tag_attrs>$cont</$tag_name>";
  }

  public function getLangs(){
    $t_langs = T_LANGS;
    $query   = self::$db->rawQuery("SHOW COLUMNS FROM `$t_langs`");
    $langs   = array();

    if (!empty($query)) {
      foreach ($query as $lang) {
        $langs[$lang->Field] = ucfirst($lang->Field);
      }

      try {
        unset($langs['id']);
                unset($langs['ref']);
        unset($langs['lang_key']);
      } catch (Exception $e) {
        
      }
    }

    return $langs;
  }

  public function fetchLanguage($lang_name = 'english'){
    try {
      $data = array();
      $lang = self::$db->get(T_LANGS,null,array('id','lang_key',$lang_name));
      if (!empty($lang) && is_array($lang)) {
        foreach ($lang as $val) {
          $data[$val->lang_key] = $val->{"$lang_name"};
        }
      }

      return $data;
    } 

    catch (Exception $e) {
      return array();
    }
  }

  static function decode($html = ''){
    return decode($html);
  }

  static function encode($html = ''){
    return encode($html); 
  }

  static function sendMail($data = array()) {
    if (empty($data)) {
      return false;
    }

    $mail            = new PHPMailer();
      $email_from      = $data['from_email'] = self::secure($data['from_email']);
      $to_email        = $data['to_email']   = self::secure($data['to_email']);
      $subject         = $data['subject'];


/*
        $headers = "From:" . $email_from;
     
        if(mail($to_email,$subject,$data['message_body'], $headers))
        {
            return true;
        } 
        else 
        {
            return false;
        }
*/

      $data['charSet'] = self::secure($data['charSet']);
      if (self::$config['smtp_or_mail'] == 'mail') {
          $mail->IsMail();
      } 

      else if (self::$config['smtp_or_mail'] == 'smtp') {
          $mail->isSMTP();
          $mail->Host        = self::$config['smtp_host'];
          $mail->SMTPAuth    = true;
          $mail->Username    = self::$config['smtp_username'];
          $mail->Password    = self::$config['smtp_password'];
          $mail->SMTPSecure  = self::$config['smtp_encryption'];
          $mail->Port        = self::$config['smtp_port'];
          $mail->SMTPOptions = array(
              'ssl' => array(
                  'verify_peer' => false,
                  'verify_peer_name' => false,
                  'allow_self_signed' => true
              )
          );
      } 

      else {
          return false;
      }

      $mail->IsHTML($data['is_html']);
      $mail->setFrom($data['from_email'], $data['from_name']);
      $mail->addAddress($data['to_email'], $data['to_name']);
      $mail->Subject = $data['subject'];
      $mail->CharSet = $data['charSet'];
      $mail->MsgHTML($data['message_body']);

      if ($mail->send()) {
          $mail->ClearAddresses();
          return true;
      }

  }


  static function getThemes() {
      $themes = glob('apps/*', GLOB_ONLYDIR);
      $data   = array();

      if (!empty($themes) && is_array($themes)) {
        foreach ($themes as $key => $theme) {

          $i = array(
            'folder' => str_replace('apps/', '', $theme),
            'author' => '',
            'name' => '',
            'version' => '',
            'email' => '',
            'cover' => ''
          );

          if (file_exists("$theme/info.php")) {
            require_once "$theme/info.php";
            $theme_folder = $i['folder'];
            $i['author']  = (isset($themeAuthor)) ? $themeAuthor : '';
            $i['name']    = (isset($themeName)) ? $themeName : '';
            $i['version'] = (isset($themeVersion)) ? $themeVersion : '';
            $i['cover']   = (isset($themeCover)) ? sprintf('%s/apps/%s/%s',self::$site_url,$theme_folder,$themeCover) : '';
            $i['email']   = (isset($themeEmail)) ? $themeEmail : '';
          }

          $data[] = $i;
        }
      }

      return $data;
  }

  public function createBackup() {
    $time    = time();
    $date    = date('d-m-Y');
    $mysqld  = new MySQLDump(self::$mysqli);

      if (!file_exists("script_backups/$date")) {
          @mkdir("script_backups/$date", 0777, true);
      }

      if (!file_exists("script_backups/$date/$time")) {
          mkdir("script_backups/$date/$time", 0777, true);
      }

      if (!file_exists("script_backups/$date/$time/index.html")) {
          $f = @fopen("script_backups/$date/$time/index.html", "a+");
          @fclose($f);
      }

      if (!file_exists('script_backups/.htaccess')) {
          $f = @fopen("script_backups/.htaccess", "a+");
          @fwrite($f, "deny from all\nOptions -Indexes");
          @fclose($f);
      }

      if (!file_exists("script_backups/$date/index.html")) {
          $f = @fopen("script_backups/$date/index.html", "a+");
          @fclose($f);
      }

      if (!file_exists('script_backups/index.html')) {
          $f = @fopen("script_backups/index.html", "a+");
          @fwrite($f, "");
          @fclose($f);
      }

      $folder_name = "script_backups/$date/$time";
      $put         = $mysqld->save("$folder_name/SQL-Backup-$time-$date.sql");

      try {
        $rootPath = ROOT;
          $zip      = new ZipArchive();
          $act      = (ZipArchive::CREATE | ZipArchive::OVERWRITE);
          $open     = $zip->open("$folder_name/Files-Backup-$time-$date.zip",$act);

          if ($open !== true) {
              return false;
          }

          $riiter = RecursiveIteratorIterator::LEAVES_ONLY;
          $rditer = new RecursiveDirectoryIterator($rootPath);
          $files  = new RecursiveIteratorIterator($rditer,$riiter);

          foreach ($files as $name => $file) {
              if (!preg_match('/\bscript_backups\b/', $file)) {
                  if (!$file->isDir()) {
                      $filePath     = $file->getRealPath();
                      $relativePath = substr($filePath, strlen($rootPath) + 1);
                      $zip->addFile($filePath, $relativePath);
                  }
              }
          }

          $zip->close();

          self::$db->where('name','last_backup')->update(T_CONFIG,array('value' => date('Y-m-d h:i:s')));
          return true;  
      } 
      catch (Exception $e) {
        return false;
      }
  }

  public function getPage($page_name = '') {
    $page_cont = "";
    if (!empty($page_name) && is_string($page_name)) {
      $page = self::$db->where('page_name',$page_name)->getValue(T_STAT_PAGES,'content');
      if (!empty($page)) {
        $page_cont = decode($page);
      }
    }
    
    return $page_cont;
  }

  public function savePage($page_name = '',$page_cont = '') {
    $page_save = false;
    if (!empty($page_name) && !empty($page_cont)) {
      $save  = self::$db->where('page_name',$page_name)->update(T_STAT_PAGES,array(
        'content' => $page_cont
      ));

      if (!empty($save)) {
        $page_save = true;
      }
    }

    return $page_save;
  }

 

  public static function is_valid_access_token() {
    $access_token = self::secure($_POST['access_token']);
      $id = self::$db->where('session_id', $access_token)->getValue(T_SESSIONS, 'user_id');
      return (is_numeric($id) && !empty($id)) ? true : false;
  }

  public function isEndPointRequest(){
     if ( strstr( $_SERVER['SCRIPT_NAME'], '/endpoints.php' ) !== '/endpoints.php' ) {
         return false;
     }else{
         return true;
     }
  }

  public function change_site_mode($up_data)
  {
    if (empty($up_data)) {
      return false;
    }

    foreach ($up_data as $name => $value) {
      try {
        self::$db->where('name', $name)->update(T_CONFIG,array('value' => $value));
      } 
      catch (Exception $e) {
        return false;
      }
    }

    return true;
  }
  public function getLoggedInUser_() {
    // ************ pixelphoto_API ******************
    if (!empty($_SESSION['user_id'])) {
      $session_id = $_SESSION['user_id'];
    }
    elseif (isset($_POST['access_token']) && !empty($_POST['access_token'])) {
      $session_id = Generic::secure($_POST['access_token']);
    }
    else{
      $session_id = !empty($_COOKIE['user_id']) ? $_COOKIE['user_id'] : '';
    }
    //$session_id = (!empty($_SESSION['user_id'])) ? $_SESSION['user_id'] : $_COOKIE['user_id'];
        $user_id  = self::$db->where('session_id', $session_id)->getValue(T_SESSIONS, 'user_id');
    return $this->fetchLoggedUser($user_id);
  }
  public function isLogged_() {
    $id = 0;
    // ************ pixelphoto_API ******************
    if (self::isEndPointRequest()) {
      if (isset($_POST['access_token']) && !empty($_POST['access_token'])) {
        $id = self::$db->where('session_id', Generic::secure($_POST['access_token']))->getValue(T_SESSIONS, 'user_id');

      }
    }
    else{
      if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
            $id = self::$db->where('session_id', $_SESSION['user_id'])->getValue(T_SESSIONS, 'user_id');
        } else if (!empty($_COOKIE['user_id']) && !empty($_COOKIE['user_id'])) {
            $id = self::$db->where('session_id', $_COOKIE['user_id'])->getValue(T_SESSIONS, 'user_id');
      }
    }
      return (is_numeric($id) && !empty($id)) ? true : false;
  }
  public function fetchLoggedUser($id) {
    $user_id = self::secure($id);
    
      $user_data = self::$db->where('user_id', $user_id)->getOne(T_USERS);
     

      if (empty($user_data)) {
        return false;
      }
      
    $user_data->name  = $user_data->username;


      if (!empty($user_data->fname) && !empty($user_data->lname)) {
        $user_data->name = sprintf('%s %s',$user_data->fname,$user_data->lname);
      }

      $user_data->avatar = media($user_data->avatar);
      $user_data->cover = media($user_data->cover);
      $user_data->uname = sprintf('%s',$user_data->username);
      $user_data->url = sprintf('%s/%s',self::$site_url,$user_data->username);
      return $user_data;
  }
}


class Embed extends Media{
  
  public $data = array(
      'youtube' => '',
      'vimeo' => '',
      'dailymotion' => '',
      'video_type' => '',
      'thumbnail' => '',
      'title' => '',
      'description' => '',
      'tags' => '',
      'tags_array' => '',
      'duration' => '',
    );

  public function fetchVideo($link=''){
    global $config;
    $re = str_replace("/", "\/", $config['playtube_url'].'/watch/');

    if (preg_match('#(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})#i', $link, $match)) {
        $this->data['youtube'] = self::secure($match[1]);
        $this->data['video_type'] = 'youtube';
    } 

    else if (preg_match("#https?://vimeo.com/([0-9]+)#i", $link, $match)) {
        $this->data['vimeo'] = self::secure($match[1]);
        $this->data['video_type'] = 'vimeo';
    } 

    else if (preg_match('#https://www.dailymotion.com/video/([A-Za-z0-9]+)#s', $link, $match)) {
        $this->data['dailymotion'] = self::secure($match[1]);
        $this->data['video_type'] = 'daily';
    }

    else if (preg_match("/({$re})([^\/]*)/", $link, $match)) {
        $this->data['playtube'] = self::secure($match[2]);
        $this->data['video_type'] = 'playtube';
    }
        
      if (!empty($this->data['youtube'])) {
        
        try {
          $youtube = new Madcoda\Youtube(array('key' => self::$config['yt_api']));
              $get_videos = $youtube->getVideoInfo($this->data['youtube']);

              if (!empty($get_videos)) {
            if (!empty($get_videos->snippet)) {
                  if (!empty($get_videos->snippet->thumbnails->medium->url)) {
                    $media = new Media();
                    $this->data['images'] = $media->ImportImageAndCrop($get_videos->snippet->thumbnails->medium->url);
                  } 
              $info = $get_videos->snippet;
              $this->data['title'] = $info->title;
              if (!empty(px_covtime($get_videos->contentDetails->duration))) {
                $this->data['duration'] = px_covtime($get_videos->contentDetails->duration);
              }
              $this->data['description'] = $info->description;
              $this->data['tags']        = '';
            }
          }
        } 
        catch (Exception $e) {
          //echo $e->getMessage();
        }
      } 

      else if (!empty($this->data['dailymotion'])) {

        $api_request = $this->curlConnect('https://api.dailymotion.com/video/' . $this->data['dailymotion'] . '?fields=thumbnail_large_url,title,duration,description,tags');

        if (!empty($api_request)) {

          if (!empty($api_request['title'])) {
            $this->data['title'] = $api_request['title'];
          }

          if (!empty($api_request['description'])) {
            $this->data['description'] = $api_request['description'];
          }

          if (!empty($api_request['thumbnail_large_url'])) {
            $media = new Media();
              $this->data['images'] = $media->ImportImageAndCrop($api_request['thumbnail_large_url']);
            //$this->data['thumbnail'] = $api_request['thumbnail_large_url'];
          }

          if (!empty($api_request['duration'])) {
            $this->data['duration'] = gmdate("i:s", $api_request['duration']);
          }

          if (is_array($api_request['tags'])) {
          $this->data['tags'] = implode(',', $api_request['tags']);
        }
        }
      } 

      else if (!empty($this->data['vimeo'])) {

        $api_request = $this->curlConnect('http://vimeo.com/api/v2/video/' . $this->data['vimeo'] . '.json');
        if (!empty($api_request)) {
          $api_request = end($api_request);
          if (!empty($api_request['title'])) {
            $this->data['title'] = $api_request['title'];
          }

          if (!empty($api_request['description'])) {
            $this->data['description'] = $api_request['description'];
          }

          if (!empty($api_request['thumbnail_large'])) {
            $media = new Media();
              $this->data['images'] = $media->ImportImageAndCrop($api_request['thumbnail_large']);
            //$this->data['thumbnail'] = $api_request['thumbnail_large'];
          }

          if (!empty($api_request['duration'])) {
            $this->data['duration'] = gmdate("i:s", $api_request['duration']);
          }

          if (!empty($api_request['tags'])) {
          $this->data['tags'] = $api_request['tags'];
        }
        }
      } 

      else if (!empty($this->data['playtube'])) {

        $api_link = $config['playtube_url']."/api/v1.0/?type=get_video_details&video_id=".$this->data['playtube'];
        $api_request = $this->curlConnect($api_link);

        
        if (!empty($api_request) && $api_request['api_status'] == 200) {
          
          if (!empty($api_request['data']['title'])) {
            $this->data['title'] = $api_request['data']['title'];
          }

          if (!empty($api_request['data']['description'])) {
            $this->data['description'] = $api_request['data']['description'];
          }

          if (!empty($api_request['data']['thumbnail'])) {
            // $media = new Media();
            // $img = $media->ImportImage($api_request['data']['thumbnail']);
            // $this->data['thumbnail'] = $img;
            $media = new Media();
              $this->data['images'] = $media->ImportImageAndCrop($api_request['data']['thumbnail']);
          }
        }
      } 
      
      return $this->data;
  }
  
  public function videoIdExists($type = ""){
    self::$db->where($type, $this->$data[$type]);
    return (self::$db->getValue(T_POSTS, 'count(*)') > 0);
  }
}



class User extends Generic{
  protected $user_id = 0;
  public $user_data = array();
  public $limit = 20;

  public static $user;
  protected static $me;

  public function __construct() {}

  public function getAllUsers(){
    $users = self::$db->get(T_USERS,$this->limit);
    $data  = array();
    foreach ($users as $key => $user_data) {
      $user_data = $this->userData($user_data);
      $data[]    = $user_data;
    }

    return $data;
  }

  public function offset($whereProp, $whereValue = 'DBNULL', $operator = '=', $cond = 'AND'){
    self::$db->where($whereProp, $whereValue, $operator);
    return $this;
  }

  public function orderBy($col = false,$type = false){
    self::$db->orderBy($col,$type);
    return $this;
  }

  // set user ID to use in the CLass.
  public function setUserById( $user_id = 0) {
    $this->user_id = self::secure($user_id);
    if (empty($this->user_id)) {
      $this->throwError("User doesn't exist");
    }
    return $this;
  }

  public function updateLastSeen(){
    if (empty(self::$me)) {
      return false;
    }
    
    self::$db->where('user_id',self::$me->user_id);
    return self::$db->update(T_USERS,array('last_seen' => time()));
  }

  // set the user in class by email
  public function setUserByEmail($email) {
    $this->user_id = self::$db->where('email', $this->secure($email))->getValue(T_USERS, 'user_id');
    if (empty($this->user_id)) {
      $this->throwError("User doesn't exist");
    }
    return $this;
  }

  // set the user in class by username
  public function setUserByName($username) {
    $this->user_id = self::$db->where('username', $this->secure($username))->getValue(T_USERS, 'user_id');
    if (empty($this->user_id)) {
      $this->throwError("User doesn't exist");
    }
    return $this->user_id;
  }

  // check if a user by username exists
  public static function userNameExists($username) {
    $user_id = self::$db->where('username', self::secure($username))->getValue(T_USERS, 'user_id');
    return (empty($user_id)) ? false : true;
  }

  // check if a user by email exits
  public static function userEmailExists($email) {
    $user_id = self::$db->where('email', self::secure($email))->getValue(T_USERS, 'user_id');
    return (empty($user_id)) ? false : true;
  }

    // return the user data (object)
  public function getUser() {
    return $this->fetchUser();
  }

  // export user data from class
  public static function getStaticUser($user_id = 0) {
    if (!empty($user_id)) {
      $user = new User;
      $user->setUserById($user_id)->getUser();
    }
    return self::$user;
  }

  // export logged in data from class
  public static function getStaticLoggedInUser() {
    return self::$me;
  }

  // update user stactily
  public function updateStatic( $id = 0, $data = array()) {
    return self::$db->where('user_id', $id)->update(T_USERS, $data);
  }

  // check for reset check
  public static function validateCode($code = '') {
    return self::$db->where('email_code', $code)->getValue(T_USERS, 'user_id');
  }

  // get user data from the database
  private function fetchUser() {
      $this->user_data = self::$db->where('user_id', $this->user_id)->getOne(T_USERS);
      

      if (empty($this->user_data)) {
        return false;
      }
      
    $this->user_data->name  = $this->user_data->username;


      if (!empty($this->user_data->fname) && !empty($this->user_data->lname)) {
        $this->user_data->name = sprintf('%s %s',$this->user_data->fname,$this->user_data->lname);
      }

      $this->user_data->avatar = media($this->user_data->avatar);
      $this->user_data->cover = media($this->user_data->cover);
      $this->user_data->uname = sprintf('%s',$this->user_data->username);
      $this->user_data->url = sprintf('%s/%s',self::$site_url,$this->user_data->username);

      self::$user = $this->user_data;
      return $this->user_data;
  }

  // get user data from the object
  public function userData($user_data = null) {
      $this->user_data = $user_data;

      if (empty($this->user_data)) {
        $this->throwError("Invalid argument: user_data must be a instance of " . T_USERS);
      }


      $this->user_data->name  = $this->user_data->username;
      if (!empty($this->user_data->fname) && !empty($this->user_data->lname)) {
        $this->user_data->name = sprintf('%s %s',$this->user_data->fname,$this->user_data->lname);
      }

      $this->user_data->avatar   = media($this->user_data->avatar);
      $this->user_data->uname    = sprintf('%s',$this->user_data->username);
      $this->user_data->url      = sprintf('%s/%s',self::$site_url,$this->user_data->username);
      $this->user_data->edit     = sprintf('%s/settings/general/%s',self::$site_url,$this->user_data->username);
      
      if (len($this->user_data->website)) {
        $this->user_data->website  = urldecode($this->user_data->website);
      } 
          
      if (len($this->user_data->facebook)) {
        $this->user_data->facebook  = urldecode($this->user_data->facebook);
      } 
              
      if (len($this->user_data->google)) {
        $this->user_data->google  = urldecode($this->user_data->google);
      }  
                 
      if (len($this->user_data->twitter)) {
        $this->user_data->twitter  = urldecode($this->user_data->twitter);
      }
      $this->user_data->followers    = $this->countFollowers();
    $this->user_data->following    = $this->countFollowing();
    $posts   = new Posts();
    $posts->setUserById($this->user_data->user_id);
    $this->user_data->favourites = $posts->countSavedPosts();
    $this->user_data->posts_count = $posts->countPosts();

    
      self::$user = $this->user_data;
      return $this->user_data;
  }

  public function getUserDataById($user_id = false){
    if (empty($user_id)) {
      return false;
    }

    self::$db->where('user_id',$user_id);
    $user_data = self::$db->getOne(T_USERS);

    if (!empty($user_data)) {
      return $this->userData($user_data);
    }

    return false;
  }

    // check if the user is logged in
  public function isLogged() {
    $id = 0;
    // ************ pixelphoto_API ******************
    if (self::isEndPointRequest()) {
      if (isset($_POST['access_token']) && !empty($_POST['access_token'])) {
        $id = self::$db->where('session_id', Generic::secure($_POST['access_token']))->getValue(T_SESSIONS, 'user_id');

      }
    }
    else{
      if (isset($_SESSION['user_id']) && !empty($_SESSION['user_id'])) {
            $id = self::$db->where('session_id', $_SESSION['user_id'])->getValue(T_SESSIONS, 'user_id');
        } else if (!empty($_COOKIE['user_id']) && !empty($_COOKIE['user_id'])) {
            $id = self::$db->where('session_id', $_COOKIE['user_id'])->getValue(T_SESSIONS, 'user_id');
      }
    }
      return (is_numeric($id) && !empty($id)) ? true : false;
  }
  
  // get logged in user data
  public function getLoggedInUser() {
    // ************ pixelphoto_API ******************
    if (!empty($_SESSION['user_id'])) {
      $session_id = $_SESSION['user_id'];
    }
    elseif (isset($_POST['access_token']) && !empty($_POST['access_token'])) {
      $session_id = Generic::secure($_POST['access_token']);
    }
    else{
      $session_id = $_COOKIE['user_id'];
    }
    //$session_id = (!empty($_SESSION['user_id'])) ? $_SESSION['user_id'] : $_COOKIE['user_id'];
        $user_id  = self::$db->where('session_id', $session_id)->getValue(T_SESSIONS, 'user_id');
    return self::$me = $this->setUserById($user_id)->getUser();
  }

  // check if user is admin
  public function isAdmin() {
    return ($this->user_data->admin == 1) ? true : false;
  }


  // check if user is authorized for an action
  public function isOwner( $user_id = 0) {
    return ($this->user_data->user_id == $user_id) ? true : false;
  }

  // register a new user
  public static function registerUser(){
    $gender = 'male';
    $active = (self::$config['email_validation'] == 'on') ? 0 : 1;
    $email_validation = (self::$config['email_validation'] == 'on') ? 0 : 1;
    $email_code = "";

    if ($_POST['gender'] == 'female') {
      $gender = 'female';
    }

    $insert_data = array(
            'username' => self::secure($_POST['username']),
            'password' => password_hash($_POST['password'], PASSWORD_DEFAULT),
            'email' => self::secure($_POST['email']),
            'ip_address' => '0.0.0.0',
            'gender' => $gender,
            'active' => $active,
            'last_seen' => time(),
            'registered' => date('Y') . '/' . intval(date('m'))
    );
    $user_object = new User();
    if (!empty($_SESSION['ref']) && self::$config['affiliate_type'] == 0 && self::$config['affiliate_system'] == 1) {
      
      $user_object->setUserByName($_SESSION['ref']);
      
      $user_data = $user_object->userData($user_object->getUser());
            $ref_user_id = $user_data->user_id;

            if (!empty($ref_user_id) && is_numeric($ref_user_id)) {
                $insert_data['referrer'] = self::secure($ref_user_id);
                $insert_data['src']      = self::secure('Referrer');
                self::$db->where('user_id',$ref_user_id)->update(T_USERS,array('balance' => self::$db->inc(self::$config['amount_ref'])));
                //unset($_SESSION['ref']);
            }
        }
    if (!empty($_POST['device_id'])) {
            $insert_data['device_id'] = self::secure($_POST['device_id']);
        }
        if (!empty($_SESSION['lang'])) {
            $insert_data['language'] = self::secure($_SESSION['lang']);
        }
    
    if( $email_validation == "on" ){
      $email_code = sha1(time() + rand(111,999));
      $insert_data['email_code'] = $email_code;
    }

        $user_id     = self::$db->insert(T_USERS, $insert_data);
        //$user_id     = 4;
        $signup      = false;

        if (!empty($user_id)) {
          $signup      = true;
      
      if ($email_validation == 'on') {
         $message_body = 'Hello {{NAME}},
        <br><br>
        Please confirm your email address by clicking the link below:
        <br>
        <a href="{{CONFIRM_LINK}}">Confirm email address</a>
        <br><br>
        {{SITE_NAME}} Team.
          ';
        $message_body = str_replace(
          array("{{NAME}}","{{SITE_NAME}}", "{{CONFIRM_LINK}}"),
          array(self::secure($_POST['username']), self::$config['site_name'], self::$config['site_url'] . '/co/' . $email_code),
          $message_body 
        );
        $send_email_data = array(
          'from_email' => self::$config['site_email'],
          'from_name' => self::$config['site_name'],
          'to_email' => self::secure($_POST['email']),
          'to_name' => self::secure($_POST['username']),
          'subject' => 'Confirm your account',
          'charSet' => 'UTF-8',
          'message_body' => $message_body,
          'is_html' => true
        );
        $send_message = Generic::sendMail($send_email_data);
        } else {
          
            $session_id  = sha1(rand(11111, 99999)) . time() . md5(microtime());
            $platform_details = $user_object->getUserBrowser();
            $insert_data = array(
               'user_id' => $user_id,
               'session_id' => $session_id,
               'time' => time(),
               'platform_details'  => json_encode($platform_details),
               'platform' => $platform_details['platform']
            );
        $insert              = self::$db->insert(T_SESSIONS, $insert_data);
        $_SESSION['user_id'] = $session_id;
              setcookie("user_id", $session_id, time() + (10 * 365 * 24 * 60 * 60), "/");
              // ************* pixelphoto_API ***********
              if ($user_object->isEndPointRequest()) {
          return array('user_id' => $insert_data['user_id'],
                         'access_token' => $session_id);
        }
        }
        }

        return $signup;
  }

  // register a new user
  public static function loginUser(){

    $username        = self::secure($_POST['username']);
        $password        = $_POST['password'];
        $getUserPassword = self::$db->where("(username = ? or email = ?)", array(
            $username,
            $username
        ))->getValue(T_USERS, 'password');
        //var_dump(); exit();
        $user_object = new User();
        $password_hashed = sha1($password);
        $password_hashed = self::secure($password_hashed);

        self::$db->where("(username = ? or email = ?)", array(
            $username,
            $username
        ));
        if (strlen($getUserPassword) == 40) {
          self::$db->where("password", $password_hashed);
          $login  = self::$db->getOne(T_USERS);
        } else if (strlen($getUserPassword) == 60) {
          $validate_password = password_verify($password, $getUserPassword);
          if ($validate_password) {
            $login = self::$db->where("(username = ? or email = ?)", array(
                $username,
                $username
            ))->getOne(T_USERS);
          }
        }

        $signin = false;
        if (!empty($login)) {
          if (strlen($getUserPassword) == 40) {
            $update_user_password = self::$db->where('user_id', $login->user_id)->update(T_USERS, ['password' => password_hash($password, PASSWORD_DEFAULT)]);
          }
          $signin      = true;
            $session_id  = sha1(rand(11111, 99999)) . time() . md5(microtime());
            $platform_details = $user_object->getUserBrowser();
            $insert_data = array(
                'user_id' => $login->user_id,
                'session_id' => $session_id,
                'time' => time(),
              'platform_details'  => json_encode($platform_details),
              'platform' => $platform_details['platform']
            );
            $insert              = self::$db->insert(T_SESSIONS, $insert_data);
            $_SESSION['user_id'] = $session_id;
            setcookie("user_id", $session_id, time() + (10 * 365 * 24 * 60 * 60), "/");
            $update_user_data = array();
            $update_user_data['ip_address'] = get_ip_address();
            if (!empty($_POST['device_id'])) {
                $update_user_data['device_id'] = self::secure($_POST['device_id']);
            }

            self::$db->where('user_id',$login->user_id)->update(T_USERS,$update_user_data);
            
            // ************* pixelphoto_API ***********
            if ($user_object->isEndPointRequest()) {
        return array('user_id' => $login->user_id,
                       'access_token' => $session_id);
      }

        }

        return $signin;
  }

  // logout a user 
  public static function signoutUser(){
    if (!empty($_SESSION['user_id'])) {
      self::$db->where('session_id', self::secure($_SESSION['user_id']));
      self::$db->delete(T_SESSIONS);
    }
  
    if (!empty($_COOKIE['user_id'])) {

      self::$db->where('session_id', self::secure($_COOKIE['user_id']));
      self::$db->delete(T_SESSIONS);
        unset($_COOKIE['user_id']);
        setcookie('user_id', null, -1);
    }

    @session_destroy();
  }

  public function delete() {
    self::$db->where('user_id' , $this->user_id)->delete(T_ACTIVITIES);
    $media  = self::$db->where('user_id',$this->user_id)->get(T_MEDIA,null,array('file','extra'));
    $story  = self::$db->where('user_id',$this->user_id)->get(T_STORY,null,array('media_file'));
    $media  = (!empty($media)) ? $media : array();
    $story  = (!empty($story)) ? $story : array();
      $del = new Media();
  
    foreach ($media as $file_obj) {
        $del->deleteFromFtpOrS3($file_obj->file);
        $del->deleteFromFtpOrS3($file_obj->extra);
        
      if (file_exists($file_obj->file)) {
        @unlink($file_obj->file);
      }

      if (file_exists($file_obj->extra)) {
        @unlink($file_obj->extra);
      }
    }

    foreach ($story as $file_obj) {
        $del->deleteFromFtpOrS3($file_obj->media_file);
        
      if (file_exists($file_obj->media_file)) {
        @unlink($file_obj->media_file);
      }
    }
    
    $delete = self::$db->where('user_id', $this->user_id)->delete(T_USERS);
    $delete = self::$db->where('user_id', $this->user_id)->delete(T_POST_COMMENTS);
    $delete = self::$db->where('user_id', $this->user_id)->delete(T_POST_LIKES);

    $delete = self::$db->where('user_id', $this->user_id)->delete(T_POSTS);
    $delete = self::$db->where('follower_id', $this->user_id)->delete(T_CONNECTIV);
    $delete = self::$db->where('following_id', $this->user_id)->delete(T_CONNECTIV);

    $delete = self::$db->where('to_id', $this->user_id)->delete(T_MESSAGES);
    $delete = self::$db->where('from_id', $this->user_id)->delete(T_MESSAGES);
    $delete = self::$db->where('user_id', $this->user_id)->delete(T_STORY);

    $delete = self::$db->where('user_id', $this->user_id)->delete(T_STORY);
    $delete = self::$db->where('user_id', $this->user_id)->delete(T_STORY_VIEWS);

    $delete = self::$db->where('notifier_id', $this->user_id)->delete(T_NOTIF);
    $delete = self::$db->where('recipient_id', $this->user_id)->delete(T_NOTIF);

    $delete = self::$db->where('from_id', $this->user_id)->delete(T_CHATS);
    $delete = self::$db->where('to_id', $this->user_id)->delete(T_CHATS);

    $delete = self::$db->where('user_id', $this->user_id)->delete(T_SAVED_POSTS);
    $delete = self::$db->where('user_id', $this->user_id)->delete(T_POST_REPORTS);

    $delete = self::$db->where('user_id', $this->user_id)->delete(T_POST_REPORTS);

    $delete = self::$db->where('profile_id', $this->user_id)->delete(T_USER_REPORTS);
    $delete = self::$db->where('user_id', $this->user_id)->delete(T_USER_REPORTS);

    $delete = self::$db->where('profile_id', $this->user_id)->delete(T_PROF_BLOCKS);
    $delete = self::$db->where('user_id', $this->user_id)->delete(T_PROF_BLOCKS);
    $delete = self::$db->where('user_id', $this->user_id)->delete(T_TRANSACTIONS);
    
    return $delete;
  }

  public function followSuggestions($limit = 10,$offset = false){
    if(empty(IS_LOGGED)){
      return false;
    }

    $data    = array();
    $user_id = self::$me->user_id;
    $sql     = pxp_sqltepmlate('users/get.suggested.users',array(
      't_users' => T_USERS,
      't_conn' => T_CONNECTIV,
      't_blocks' => T_PROF_BLOCKS,
      'user_id' => $user_id,
      'total_limit' => $limit,
      'offset' => $offset,
    ));

    try {
      $users = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $users = array();
    }
    
    if (!empty($users)) {
      foreach ($users as $user) {
        $data[] = $this->userData($user);
      }
    }
    
    return $data;
  }

  public function isFollowing($following_id = null,$rev = false) {
    if (empty($following_id) || !is_numeric($following_id)) {
      return false;
    }

    else if(empty(IS_LOGGED)){
      return false;
    }


    $res = false;

    if ($rev === true && ($following_id != self::$me->user_id)) {
      self::$db->where('follower_id',$following_id);
      self::$db->where('following_id',self::$me->user_id);
      self::$db->where('type',1);
      $res = (self::$db->getValue(T_CONNECTIV,'COUNT(*)') > 0);
      if ($res == 0) {
        self::$db->where('follower_id',$following_id);
        self::$db->where('following_id',self::$me->user_id);
        self::$db->where('type',2);
        $res2 = (self::$db->getValue(T_CONNECTIV,'COUNT(*)') > 0);
        if ($res2 > 0) {
          $res = 2;
        }
      }
    }
    elseif ($following_id != self::$me->user_id) {
      self::$db->where('follower_id',self::$me->user_id);
      self::$db->where('following_id',$following_id);
      self::$db->where('type',1);
      $res = (self::$db->getValue(T_CONNECTIV,'COUNT(*)') > 0);
      if ($res == 0) {
        self::$db->where('follower_id',self::$me->user_id);
        self::$db->where('following_id',$following_id);
        self::$db->where('type',2);
        $res2 = (self::$db->getValue(T_CONNECTIV,'COUNT(*)') > 0);
        if ($res2 > 0) {
          $res = 2;
        }
      }
    }
    
    return $res;
  }

  public function unFollow($following_id = null){
    if (empty($following_id) || !is_numeric($following_id)) {
      return false;
    }

    else if(empty(IS_LOGGED)){
      return false;
    }

    self::$db->where('follower_id',self::$me->user_id);
    self::$db->where('following_id',$following_id);
    $res = self::$db->delete(T_CONNECTIV);

    return boolval($res);

  }

  public function follow($following_id = null){
    if (empty($following_id) || !is_numeric($following_id)) {
      return false;
    }

    else if(empty(IS_LOGGED) || (self::$me->user_id == $following_id)) {
      return false;
    }

    $following_data = $this->getUserDataById($following_id);
    if (!empty($following_data)) {
      if ($following_data->p_privacy == 0 || $following_data->p_privacy == 1) {
        if ($this->isFollowing($following_id) === true) {
          self::$db->where('follower_id',self::$me->user_id);
          self::$db->where('following_id',$following_id);
          self::$db->delete(T_CONNECTIV);
          self::$db->where('user_id' , self::$me->user_id)->where('following_id' , $following_id)->where('type' , 'followed_user')->delete(T_ACTIVITIES);
          return -1;
        }
        elseif ($this->isFollowing($following_id) == 2) {
          self::$db->where('follower_id',self::$me->user_id);
          self::$db->where('following_id',$following_id);
          self::$db->delete(T_CONNECTIV);
          return -1;
        }
        else{
          $re_data = array(
            'follower_id' => self::$me->user_id,
            'following_id' => $following_id,
            'active' => 0,
            'type' => 2,
            'time' => time()
          );

          self::$db->insert(T_CONNECTIV,$re_data);
        }
      }
      else{
        if ($this->isFollowing($following_id) === true) {
          self::$db->where('follower_id',self::$me->user_id);
          self::$db->where('following_id',$following_id);
          self::$db->delete(T_CONNECTIV);
          self::$db->where('user_id' , self::$me->user_id)->where('following_id' , $following_id)->where('type' , 'followed_user')->delete(T_ACTIVITIES);
          return -1;
        }

        else{

          $re_data = array(
            'follower_id' => self::$me->user_id,
            'following_id' => $following_id,
            'active' => 1,
            'time' => time()
          );

          self::$db->insert(T_CONNECTIV,$re_data);
          self::$db->insert(T_ACTIVITIES,array('user_id' => self::$me->user_id,
                                               'following_id' => $following_id,
                                               'type'    => 'followed_user',
                                               'time'    => time()));
          return 1;
        }
      }
    }
    return false;
  }

  public function getFollowers($offset = false,$limit = null){
    if (empty($this->user_id) || !is_numeric($this->user_id)) {
      return false;
    }

    else if (!empty($limit) && !is_numeric($limit)) {
      return false;
    }

    $user_id = $this->user_id;
    $t_users = T_USERS;
    $t_conn  = T_CONNECTIV;

    self::$db->join("{$t_conn} c","c.follower_id = u.user_id AND c.type = 1","INNER");
    self::$db->where("c.following_id",$user_id);
    self::$db->orderBy("u.user_id","DESC");

    if (!empty($offset) && is_numeric($offset)) {
      self::$db->where("u.user_id",$offset,'<');
    }

    $users = self::$db->get("{$t_users} u",$limit);
    $data  = array();

    foreach ($users as $key => $user_data) {
      $user_data = $this->userData($user_data);
      $user_data->is_following = false;

      if (IS_LOGGED) {
        $this->user_id = self::$me->user_id;
        $user_data->is_following = $this->isFollowing($user_data->user_id);
      }

      $data[]    = $user_data;
    }
    
    return $data;
  }
  public function getUserRequests($offset = false,$limit = null){
    if (empty($this->user_id) || !is_numeric($this->user_id)) {
      return false;
    }

    else if (!empty($limit) && !is_numeric($limit)) {
      return false;
    }

    $user_id = $this->user_id;
    $t_users = T_USERS;
    $t_conn  = T_CONNECTIV;

    self::$db->join("{$t_conn} c","c.follower_id = u.user_id AND c.type = 2","INNER");
    self::$db->where("c.following_id",$user_id);
    self::$db->orderBy("c.id","DESC");

    if (!empty($offset) && is_numeric($offset)) {
      self::$db->where("u.user_id",$offset,'<');
    }

    $users = self::$db->get("{$t_users} u",$limit);
    $data  = array();

    foreach ($users as $key => $user_data) {
      $user_data = $this->userData($user_data);
      $user_data->is_following = false;

      $data[]    = $user_data;
    }
    
    return $data;
  }
  
  public function getFollowing($offset = false,$limit = null){
      if (empty($this->user_id) || !is_numeric($this->user_id)) {
        return false;
      }

      else if (!empty($limit) && !is_numeric($limit)) {
        return false;
      }

      $user_id = $this->user_id;
      $t_users = T_USERS;
      $t_conn  = T_CONNECTIV;

      self::$db->join("{$t_conn} c","c.following_id = u.user_id AND c.type = 1","LEFT");
      self::$db->where("c.follower_id",$user_id);
      self::$db->orderBy("u.user_id","DESC");

      if (!empty($offset) && is_numeric($offset)) {
        self::$db->where("u.user_id",$offset,'<');
      }


      $users = self::$db->get("{$t_users} u",$limit);
      $data  = array();
      foreach ($users as $key => $user_data) {
        $user_data = $this->userData($user_data);

        if (IS_LOGGED) {
          $this->user_id = self::$me->user_id;
          $user_data->is_following = $this->isFollowing($user_data->user_id);
        }

        $data[]    = $user_data;
      }
      
      return $data;
  }
  
  public function countFollowers(){
    if (empty($this->user_id) || !is_numeric($this->user_id)) {
      return false;
    }

    $user_id = $this->user_id;
    $t_conn  = T_CONNECTIV;
    self::$db->where('following_id',$user_id)->where('type',1);
    return self::$db->getValue($t_conn,"COUNT(`id`)");
  }

  public function countFollowing(){
    if (empty($this->user_id) || !is_numeric($this->user_id)) {
      return false;
    }

    $user_id = $this->user_id;
    $t_conn  = T_CONNECTIV;
    self::$db->where('follower_id',$user_id)->where('type',1);

    return self::$db->getValue($t_conn,"COUNT(`id`)");
  }

  public function getUserId( $username){
    if (empty($username) || !is_string($username)) {
      return false;
    }

    $user_id  = false;
    $username = self::secure($username);

    self::$db->where('username',$username);
    $query = self::$db->getValue(T_USERS,'user_id');
    if (!empty($query)) {
      $user_id = $query;
    }

    return $user_id;
  }

  public function explorePeople($offset = false){
    if (empty(IS_LOGGED)) {
      return false;
    }

    $data    = array();
    $user_id = self::$me->user_id;
    $sql     = pxp_sqltepmlate('users/explore.people',array(
      't_users' => T_USERS,
      't_conn' => T_CONNECTIV,
      't_posts' => T_POSTS,
      't_blocks' => T_PROF_BLOCKS,
      'total_limit' => $this->limit,
      'user_id' => $user_id,
      'offset' => $offset,
    ));

    try {
      $users = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $users = array();
    }
    
    if (!empty($users)) {
      $data = $users;
    }
    
    return $data;
  }

  public function profilePrivacy($user_id = false){
    if (empty($user_id) || !is_numeric($user_id) || empty(IS_LOGGED)) {
      return false;
    }

    self::$db->where('user_id',$user_id);
    $user_data = self::$db->getOne(T_USERS,'p_privacy');
    $privacy   = false;

    if (!empty($user_data)) {

      if ($user_id == self::$me->user_id) {
        $privacy = true;
      }
      
      else if ($user_data->p_privacy == '2') {
        $privacy = true;
      }

      elseif ($user_data->p_privacy == '1' && $this->isFollowing($user_id)) {
        $privacy = true;
      }
    }

    return $privacy;
  }

  public function chatPrivacy($user_id = false){
    if (empty($user_id) || !is_numeric($user_id) || empty(IS_LOGGED)) {
      return false;
    }

    self::$db->where('user_id',$user_id);
    $user_data = self::$db->getOne(T_USERS,'c_privacy');
    $privacy   = false;

    if (!empty($user_data)) {

      if ($user_data->c_privacy == '1' && self::$me->c_privacy == '1') {
        $privacy = true;
      }

      elseif ($user_data->c_privacy == '2' && self::$me->c_privacy == '1' && $this->isFollowing($user_id,true)) {
        $privacy = true;
      }

      elseif (self::$me->c_privacy == '2' && $user_data->c_privacy == '1' && $this->isFollowing($user_id)) {
        $privacy = true;
      }

      elseif (($user_data->c_privacy == '2' && $this->isFollowing($user_id,true)) &&  (self::$me->c_privacy == '2' && $this->isFollowing($user_id))) {
        $privacy = true;
      }
    }

    return $privacy;
  }

  public function isUserRepoted($user_id = false){
    if (empty(IS_LOGGED) || empty($user_id)) {
      return false;
    }

    self::$db->where('user_id',self::$me->user_id);
    self::$db->where('profile_id',$user_id);
    return (self::$db->getValue(T_USER_REPORTS,'COUNT(`id`)') > 0);
  }

  public function reportUser($user_id = false,$type = false){
    if (empty(IS_LOGGED) || empty($user_id) || empty($type)) {
      return false;
    }

    $code = null;

    if ($this->isUserRepoted($user_id) === true) {
      self::$db->where('user_id',self::$me->user_id);
      self::$db->where('profile_id',$user_id);
      self::$db->delete(T_USER_REPORTS);
      $code = -1;
    }
    else {
      self::$db->insert(T_USER_REPORTS,array(
        'user_id' => self::$me->user_id,
        'profile_id' => $user_id,
        'type' => $type,
        'time' => time()
      ));
      $code = 1;
    }
    return $code;
  }

  public function isBlocked($user_id = false,$rev = false){
    if (empty(IS_LOGGED) || empty($user_id)) {
      return false;
    }

    $blcoked = false;

    if ($rev === true && ($user_id != self::$me->user_id)) {
      self::$db->where('user_id',$user_id);
      self::$db->where('profile_id',self::$me->user_id);
      $blcoked = (self::$db->getValue(T_PROF_BLOCKS,'COUNT(`id`)') > 0);
    }
    else if($user_id != self::$me->user_id){
      self::$db->where('user_id',self::$me->user_id);
      self::$db->where('profile_id',$user_id);
      $blcoked = (self::$db->getValue(T_PROF_BLOCKS,'COUNT(`id`)') > 0);
    }

    return $blcoked;
  }

  public function unBlockUser($user_id = false){
    if (empty(IS_LOGGED) || empty($user_id)) {
      return false;
    }

    self::$db->where('user_id',self::$me->user_id);
    self::$db->where('profile_id',$user_id);
    return self::$db->delete(T_PROF_BLOCKS);
  }

  public function blockUser($user_id = false){
    if (empty(IS_LOGGED) || empty($user_id)) {
      return false;
    }

    self::$db->where('user_id',self::$me->user_id);
    self::$db->where('profile_id',$user_id);

    $code   = null;
    $bloked = self::$db->getValue(T_PROF_BLOCKS,'COUNT(`id`)');

    if (!empty($bloked)) {
      $this->unBlockUser($user_id);
      $code = -1;
    }
    else {
      self::$db->insert(T_PROF_BLOCKS,array(
        'user_id' => self::$me->user_id,
        'profile_id' => $user_id,
        'time' => time()
      ));

      $code = 1;
      $this->unFollow($user_id);

      self::$db->where('following_id',self::$me->user_id);
      self::$db->where('follower_id',$user_id);
      self::$db->delete(T_CONNECTIV);
    }

    return $code;
  }

  public function getBlockedUsers(){
    if (empty(IS_LOGGED)) {
      return false;
    }

    $data  = array();
    $sql   = pxp_sqltepmlate('users/get.blocked.users',array(
      't_users' => T_USERS,
      't_blocks' => T_PROF_BLOCKS,
      'user_id' => self::$me->user_id,
    )); 

    try {
      $users = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $users = array();
    }

    if (!empty($users)) {
      foreach ($users as $user) {
        $user->name = $user->username;
        if (!empty($user->fname) && !empty($user->lname)) {
          $user->name = sprintf('%s %s',$user->fname,$user->lname);
        }
        $data[] = $user;
      }
    }
    return $data;
  }

  public function seachUsers($keyword = '',$limit = 100,$offset = 0,$order = 'DESC'){
    if (empty($keyword)) {
      return false;
    }
    $offset_string = '';
    if (!empty($offset) && is_numeric($offset) && $offset > 0) {
      if ($order == 'DESC') {
        $offset_string = " AND  user_id < ".(int)$offset;
      }
      else{
        $offset_string = " AND  user_id > ".(int)$offset;
      }
    }
    $users = self::$db->rawQuery("SELECT * FROM ".T_USERS." WHERE (username LIKE '%$keyword%' OR fname LIKE '%$keyword%' OR lname LIKE '%$keyword%') ".$offset_string." ORDER BY user_id ".$order." LIMIT ".$limit);

    // //if (!empty($offset) && is_numeric($offset) && $offset > 0) {
    //  self::$db->where('user_id',$offset,'>');
    // //}
    // self::$db->where('username',"%$keyword%",'LIKE');
    // //self::$db->orWhere('fname',"%$keyword%",'LIKE');
    // //self::$db->orWhere('lname',"%$keyword%",'LIKE');
    
    // self::$db->orderBy("user_id","DESC");
    // $usres = self::$db->get(T_USERS,$limit,array('username','fname','lname','avatar','verified'));
    return $users;
  }

  public function sendVerificationRequest($data = array())
  {
    
    if (empty(IS_LOGGED) || empty($data)) {
      return false;
    }
    if (self::isVerificationRequested() > 0) {
      return false;
    }
    return self::$db->insert(T_VERIFY,$data);
  }

  public function isVerificationRequested()
  {
    global $me;
    if (empty(IS_LOGGED)) {
      return false;
    }
    self::$db->where('user_id',self::$me->user_id);
    return self::$db->getValue(T_VERIFY,'COUNT(*)');
  }

  public function is_verified($user_id)
  {
    if (empty(IS_LOGGED) || empty($user_id)) {
      return false;
    }
    $user_id = self::secure($user_id);
    self::$db->where('user_id',$user_id);
    $is_verified = self::$db->get(T_USERS,1,'verified');
    $is_verified = $is_verified[0]->verified;
    return $is_verified;
  }

  public function getUserSessions()
  {
    global $me;
    if (empty(IS_LOGGED)) {
      return false;
    }
    self::$db->where('user_id',self::$me->user_id);
    self::$db->orderBy('id','DESC');
    return self::$db->get(T_SESSIONS);
  }
  public function delete_session($id){
    if (!empty($id)) {
      self::$db->where('id', $id);
      self::$db->delete(T_SESSIONS);
    }
  }

  public function getUserBrowser() {
       $u_agent = $_SERVER['HTTP_USER_AGENT'];
       $bname = 'Unknown';
       $platform = 'Unknown';
       $version= "";
       // First get the platform?
       if (preg_match('/linux/i', $u_agent)) {
         $platform = 'linux';
       } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
         $platform = 'Mac';
       } elseif (preg_match('/windows|win32/i', $u_agent)) {
         $platform = 'windows';
       } elseif (preg_match('/iphone|IPhone/i', $u_agent)) {
         $platform = 'IPhone';
       } elseif (preg_match('/android|Android/i', $u_agent)) {
         $platform = 'Android';
       } else if (preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $u_agent)) {
         $platform = 'Mobile';
       }
       // Next get the name of the useragent yes seperately and for good reason
       if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
         $bname = 'Internet Explorer';
         $ub = "MSIE";
       } elseif(preg_match('/Firefox/i',$u_agent)) {
         $bname = 'Mozilla Firefox';
         $ub = "Firefox";
       } elseif(preg_match('/Chrome/i',$u_agent)) {
         $bname = 'Google Chrome';
         $ub = "Chrome";
       } elseif(preg_match('/Safari/i',$u_agent)) {
         $bname = 'Apple Safari';
         $ub = "Safari";
       } elseif(preg_match('/Opera/i',$u_agent)) {
         $bname = 'Opera';
         $ub = "Opera";
       } elseif(preg_match('/Netscape/i',$u_agent)) {
         $bname = 'Netscape';
         $ub = "Netscape";
       }
       // finally get the correct version number
       $known = array('Version', $ub, 'other');
       $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
       if (!preg_match_all($pattern, $u_agent, $matches)) {
         // we have no matching number just continue
       }
       // see how many we have
       $i = count($matches['browser']);
       if ($i != 1) {
         //we will have two since we are not using 'other' argument yet
         //see if version is before or after the name
         if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
           $version= $matches['version'][0];
         } else {
           $version= $matches['version'][1];
         }
       } else {
         $version= $matches['version'][0];
       }
       // check if we have a number
       if ($version==null || $version=="") {$version="?";}
       return array(
           'userAgent' => $u_agent,
           'name'      => $bname,
           'version'   => $version,
           'platform'  => $platform,
           'pattern'    => $pattern,
           'ip_address' => get_ip_address()
       );
  }

  public function isInBlackList($username = '',$email = '')
  {
    $ip = get_ip_address();
    self::$db->where('value',$ip);
    $count = self::$db->getValue(T_BLACKLIST,'COUNT(*)');
    if ($count > 0) {
      return array('count' => $count , 'type' => 'ip');
    }
    if (!empty($username) && empty($email)) {
      $username = self::secure($username);
      self::$db->where('value',$username);
      $count = self::$db->getValue(T_BLACKLIST,'COUNT(*)');
      return array('count' => $count , 'type' => 'username');
    }
    if (empty($username) && !empty($email)) {
      $email = self::secure($email);
      self::$db->where('value',$email);
      $count = self::$db->getValue(T_BLACKLIST,'COUNT(*)');
      return array('count' => $count , 'type' => 'email');
    }
    if (!empty($username) && !empty($email)) {
      $username = self::secure($username);
      $email = self::secure($email);
      self::$db->where('value',array($email,$username),'IN');
      $count = self::$db->getValue(T_BLACKLIST,'COUNT(*)');
      return array('count' => $count , 'type' => 'email_username');
    }
  }

  public function GetProUsers($limit = 6)
  {
    $data = array();
    $users = self::$db->where('is_pro' , 1)->orderBy('RAND()')->get(T_USERS,$limit);
    if (!empty($users)) {
      foreach ($users as $key => $user) {
        $data[] = self::getUserDataById($user->user_id);
      }
    }
    return $data;
  }

  public function GetUserAds()
  {
    $ads = self::$db->where('user_id' , self::$me->user_id)->orderBy('id','DESC')->get(T_ADS);
    $data = array();
    if (!empty($ads)) {
      foreach ($ads as $key => $ad) {
        $new_ad = $ad;
        $new_ad->edit_url = self::$config['site_url'].'/edit_ad/'.$ad->id;
        $data[] = $new_ad;
      }
    }
    return $data;
  }

  public function GetAdByID($id)
  {
    if (empty($id) || !is_numeric($id) || $id < 1) {
      return false;
    }
    $id = self::secure($id);
    $ad = self::$db->where('id' , $id)->getOne(T_ADS);
    return $ad;
  }

  public function GetRandomAd($type = 'post')
  {
    $ads_array = array();
    $type = self::secure($type);
    if (!empty($_SESSION['ads'])) {
      $ads_array = explode(',', $_SESSION['ads']);
      self::$db->where('id', $ads_array, 'NOT IN');
    }
    $sql   = "(`audience`     LIKE '%".self::$me->country_id."%')";
    $ad = self::$db->where('appears',$type)->where($sql)->orderBy('RAND()')->getOne(T_ADS);
    if (!empty($ad) && $ad->bidding == 'views' && !in_array($ad->id, $ads_array)) {
      self::$db->where('id', $ad->id)->update(T_ADS,array('views' => self::$db->inc(1)));
      self::$db->where('user_id', $ad->user_id)->update(T_USERS,array('wallet' => self::$db->dec(self::$config['ad_v_price'])));

      $ad_user = self::$db->where('user_id', $ad->user_id)->getOne(T_USERS);

      $user_wallet = $ad_user->wallet - self::$config['ad_v_price'];
      if ($user_wallet < self::$config['ad_v_price']) {
        self::$db->where('id', $ad->id)->delete(T_ADS);
      }
      $ads_array[] = $ad->id;
      $_SESSION['ads'] = implode(',', $ads_array);
    }
    return $ad;
  }

  public function GetFunding($limit = 6,$offset=0)
  {
    $data = array();
    if (!empty($offset) && $offset > 0) {
      self::$db->where('id',$offset,'<');
    }
    $funding = self::$db->orderBy('id','DESC')->get(T_FUNDING,$limit);
    if (!empty($funding)) {
      foreach ($funding as $key => $fund) {
        $new_data = $fund;
        $new_data->image = media($new_data->image);
        $new_data->raised = self::$db->where('funding_id',$new_data->id)->getValue(T_FUNDING_RAISE,"SUM(amount)");
        $new_data->bar = 0;
        if (empty($new_data->raised)) {
          $new_data->raised = 0;
        }
        elseif (!empty($new_data->raised) && $new_data->raised >= $new_data->amount) {
          $new_data->bar = 100;
        }
        elseif (!empty($new_data->raised) && $new_data->raised < $new_data->amount && $new_data->raised > 0) {
          $percent = ($new_data->raised * 100)/$new_data->amount;
          $new_data->bar = $percent;
        }
        $new_data->user_data = self::getUserDataById($fund->user_id);
        $data[] = $new_data;
      }
    }
    return $data;
  }
  public function GetFundingById($id,$type = 'id')
  {
    if (empty($id)) {
      return false;
    }
    $id = self::secure($id);
    $data = array();
    if ($type == 'hash') {
      $funding = self::$db->where('hashed_id',$id)->getOne(T_FUNDING);
    }
    else{
      $funding = self::$db->where('id',$id)->getOne(T_FUNDING);
    }
    
    if (!empty($funding)) {

      $funding->image = media($funding->image);
      $funding->raised = self::$db->where('funding_id',$funding->id)->getValue(T_FUNDING_RAISE,"SUM(amount)");
      $funding->all_donation = self::$db->where('funding_id',$funding->id)->getValue(T_FUNDING_RAISE,"COUNT(*)");
      $funding->bar = 0;
      if (empty($funding->raised)) {
        $funding->raised = 0;
      }
      elseif (!empty($funding->raised) && $funding->raised >= $funding->amount) {
        $funding->bar = 100;
      }
      elseif (!empty($funding->raised) && $funding->raised < $funding->amount && $funding->raised > 0) {
        $percent = ($funding->raised * 100)/$funding->amount;
        $funding->bar = $percent;
      }
      $funding->user_data = self::getUserDataById($funding->user_id);
      return $funding;
    }
    return false;
  }

  public function GetRecentRaise($id,$limit = 6,$offset=0)
  {
    if (empty($id)) {
      return false;
    }
    $id = self::secure($id);
    $data = array();
    if (!empty($offset) && $offset > 0) {
      self::$db->where('id',$offset,'<');
    }
    $funding = self::$db->where('funding_id',$id)->orderBy('id','DESC')->get(T_FUNDING_RAISE,$limit);
    if (!empty($funding)) {
      foreach ($funding as $key => $fund) {
        $new_data = $fund;
        $new_data->user_data = self::getUserDataById($fund->user_id);
        $data[] = $new_data;
      }
    }
    return $data;
  }

  public function GetFundingByUserId($user_id ,$limit = 6,$offset=0)
  {
    if (empty($user_id) || !is_numeric($user_id) || $user_id < 1) {
      return false;
    }
    $user_id = self::secure($user_id);
    $data = array();
    if (!empty($offset) && $offset > 0) {
      self::$db->where('id',$offset,'<');
    }
    $funding = self::$db->where('user_id',$user_id)->orderBy('id','DESC')->get(T_FUNDING,$limit);
    if (!empty($funding)) {
      foreach ($funding as $key => $fund) {
        $new_data = $fund;
        $new_data->image = media($new_data->image);
        $new_data->raised = self::$db->where('funding_id',$new_data->id)->getValue(T_FUNDING_RAISE,"SUM(amount)");
        $new_data->bar = 0;
        if (empty($new_data->raised)) {
          $new_data->raised = 0;
        }
        elseif (!empty($new_data->raised) && $new_data->raised >= $new_data->amount) {
          $new_data->bar = 100;
        }
        elseif (!empty($new_data->raised) && $new_data->raised < $new_data->amount && $new_data->raised > 0) {
          $percent = ($new_data->raised * 100)/$new_data->amount;
          $new_data->bar = $percent;
        }
        $new_data->user_data = self::getUserDataById($fund->user_id);
        $data[] = $new_data;
      }
    }
    return $data;
  }

  function getUserAffiliates()
  {
    $users = self::$db->where('referrer', self::$me->user_id)->get(T_USERS);
    $data = array();
    if (!empty($users)) {
      foreach ($users as $key => $user) {
        $data[] = $this->getUserDataById($user->user_id);
      }
    }
    return $data;
  }


}



class Posts extends User{
  
  public $hashtag    = '';
  public $tag_id     = '';
  public $comm_limit = null;
  protected $post_id = 0;

  public function all(){
    $posts = self::$db->get(T_POSTS,$this->limit);
    $data  = array();
    foreach ($posts as $key => $post_data) {
      $post_data = $this->postData($post_data);
      $data[]    = $post_data;
    }

    return $data;
  }
  public function explorePosts($offset = false){
    if (empty(IS_LOGGED)) {
      return false;
    }

    $data = array();
    $sql  = pxp_sqltepmlate('posts/explore.posts',array(
      't_posts' => T_POSTS,
      't_likes' => T_POST_LIKES,
      't_media' => T_MEDIA,
      't_comm' => T_POST_COMMENTS,
      't_blocks' => T_PROF_BLOCKS,
      't_users' => T_USERS,
      'total_limit' => $this->limit,
      'user_id' => self::$me->user_id,
      'offset' => $offset,
    ));
    try {
      $posts = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $posts = array();
    }
    
    foreach ($posts as $key => $post_data) {
      $post_data->thumb = '';
      $t = $post_data->type;
      if (in_array($t, array('youtube','gif','video','vimeo','dailymotion','playtube','mp4','fetched'))) {
        if(!empty($post_data->extra)){
          $post_data->thumb = $post_data->extra;
        }else{
          if($t == 'youtube'){
            $post_data->thumb = 'http://i3.ytimg.com/vi/'.$post_data->youtube.'/maxresdefault.jpg';
          }
        }
      }else{
          $post_data->thumb = $post_data->file;
      }
      $post_data->is_should_hide  = $this->is_should_hide($post_data->post_id);

      $data[] = $post_data;
    }

    return $data;
  }
  public function exploreBoostedPosts($limit = 1){
    if (empty(IS_LOGGED)) {
      return false;
    }

    $data = array();

    $post_data = self::$db->rawQuery("SELECT p.*,m.*,u.`username`,u.`user_id` owner_id,u.`avatar`,(SELECT COUNT(l.`id`) FROM `".T_POST_LIKES."` l WHERE l.`post_id` = p.`post_id` ) AS likes, (SELECT COUNT(c.`id`) FROM `".T_POST_COMMENTS."` c WHERE c.`post_id` = p.`post_id`) AS comments FROM `".T_POSTS."` p INNER JOIN `".T_MEDIA."` m ON m.`post_id` = p.`post_id` AND p.`boosted` = 1 INNER JOIN `".T_USERS."` u ON p.`user_id` = u.`user_id` WHERE u.`p_privacy` = '2' AND p.`user_id` NOT IN (SELECT b1.`profile_id` FROM `".T_PROF_BLOCKS."` b1 WHERE b1.`user_id` = '".self::$me->user_id."') AND p.`user_id` NOT IN (SELECT b2.`user_id` FROM `".T_PROF_BLOCKS."` b2 WHERE b2.`profile_id` = '".self::$me->user_id."') GROUP BY p.`post_id` ORDER BY RAND() DESC LIMIT 1");

    if (!empty($post_data)) {
      $post_data = $post_data[0];
      $post_data->thumb = '';
      $t = $post_data->type;
      if (in_array($t, array('youtube','gif','video','vimeo','dailymotion','playtube','mp4','fetched'))) {
        if(!empty($post_data->extra)){
          $post_data->thumb = $post_data->extra;
        }else{
          if($t == 'youtube'){
            $post_data->thumb = 'http://i3.ytimg.com/vi/'.$post_data->youtube.'/maxresdefault.jpg';
          }
        }
      }else{
          $post_data->thumb = $post_data->file;
      }
      $post_data->is_should_hide  = false;

      $data[] = $post_data;
    }

    return $data;
  }
  public function getHtagId($htag = ""){
    if (empty($htag) || !is_string($htag)) {
      return false;
    }

    $htag_id = 0;
    $query   = self::$db->where('tag',$htag)->getValue(T_HTAGS,'id');

    if (!empty($query)) {
      $htag_id = $query;
    }

    return $htag_id;
  }
  public function exploreTags($hashtag_id = '',$offset = false) {
    $data = array();
    $sql  = pxp_sqltepmlate('posts/explore.posts',array(
      't_posts' => T_POSTS,
      't_likes' => T_POST_LIKES,
      't_media' => T_MEDIA,
      't_users' => T_USERS,
      't_comm' => T_POST_COMMENTS,
      'total_limit' => $this->limit,
      'hashtag_id' => $hashtag_id,
      'offset' => $offset,
      'user_id' => ((empty(IS_LOGGED)) ? false : self::$me->user_id),
      't_blocks' => T_PROF_BLOCKS,
    ));

    try {
      $posts = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $posts =  array();
    }
  
    foreach ($posts as $key => $post_data) {
      $post_data->thumb = '';
      $t = $post_data->type;

      if (in_array($t, array('youtube','gif','image','vimeo','dailymotion'))) {
        $post_data->thumb = $post_data->file;
      }

      else if($t == 'video'){
        $post_data->thumb = $post_data->extra;
      }

      $data[] = $post_data;
    }

    return $data;
  }
  public function countPostsByTag($htag_id = ''){
    $htag_id = self::secure($htag_id);
    $posts   = self::$db->where('description',"%#[$htag_id]%",'LIKE')->getValue(T_POSTS,'COUNT(`post_id`)');
    return $posts;
  }
  public function getUserPosts($offset = false){

    if (empty($this->user_id) || !is_numeric($this->user_id)) {
      $this->throwError("Error: User id must be a positive integer");
    }

    $data    = array();
    $user_id = $this->user_id;
    $sql     = pxp_sqltepmlate('posts/get.user.posts',array(
      't_posts' => T_POSTS,
      't_likes' => T_POST_LIKES,
      't_comm' => T_POST_COMMENTS,
      't_media' => T_MEDIA,
      'user_id' => $user_id,
      'total_limit' => $this->limit,
      'offset' => $offset,
    ));
    
    try {
      $posts = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $posts = array();
    }

    foreach ($posts as $key => $post_data) {
      $post_data->thumb = '';
      $t = $post_data->type;

      if (in_array($t, array('youtube','gif','video','vimeo','dailymotion','playtube','mp4','fetched'))) {
        if(!empty($post_data->extra)){
          $post_data->thumb = $post_data->extra;
        }else{
          if($t == 'youtube'){
            $post_data->thumb = 'http://i3.ytimg.com/vi/'.$post_data->youtube.'/maxresdefault.jpg';
          }
        }
      }else{
          $post_data->thumb = $post_data->file;
      }
      $post_data->is_should_hide  = $this->is_should_hide($post_data->post_id);



      $data[] = $post_data;
    }
    
    return $data;
  }
  public function getSavedPosts($offset = false){
    if (empty(IS_LOGGED)) {
      return false;
    }

    $data    = array();
    $user_id = self::$me->user_id;
    $sql     = pxp_sqltepmlate('posts/get.saved.posts',array(
      't_posts' => T_POSTS,
      't_likes' => T_POST_LIKES,
      't_comm' => T_POST_COMMENTS,
      't_media' => T_MEDIA,
      't_saved' => T_SAVED_POSTS,
      't_users' => T_USERS,
      'user_id' => $user_id,
      'total_limit' => $this->limit,
      'offset' => $offset,
    ));

    try {
      $posts = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $posts = array();
    }
    
    foreach ($posts as $key => $post_data) {
      $post_data->thumb = '';
      $t = $post_data->type;

      if (in_array($t, array('youtube','gif','video','vimeo','dailymotion','playtube','mp4','fetched'))) {
        if(!empty($post_data->extra)){
          $post_data->thumb = $post_data->extra;
        }else{
          if($t == 'youtube'){
            $post_data->thumb = 'http://i3.ytimg.com/vi/'.$post_data->youtube.'/maxresdefault.jpg';
          }
        }
      }else{
          $post_data->thumb = $post_data->file;
      }
      $post_data->is_should_hide  = $this->is_should_hide($post_data->post_id);

      $data[] = $post_data;
    }

    return $data;
  }
  public function getTimelinePosts($offset = false,$limit = 5){
    if (empty(IS_LOGGED)) {
      return false;
    }

    $data    = array();
    $user_id = self::$me->user_id;
    $sql     = pxp_sqltepmlate('posts/get.timeline.posts',array(
      't_posts' => T_POSTS,
      't_conn' => T_CONNECTIV,
      't_likes' => T_POST_LIKES,
      't_comm' => T_POST_COMMENTS,
      't_blocks' => T_PROF_BLOCKS,
      't_users' => T_USERS,
      'user_id' => $user_id,
      'total_limit' => $limit,
      'offset' => $offset,
    ));

    try {
      $posts = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $posts = array();
    }

    foreach ($posts as $key => $post_data) {
      $post_data = $this->postData($post_data);
      $data[]    = $post_data;
    }

    $user = new User();
    $ad = $user->GetRandomAd();
    if (!empty($ad)) {
      $ad->type = 'ad';
      $ad->user_data = $user->getUserDataById($ad->user_id);
      $data[] = $ad;
    }
    
    return $data;
  }
  public function setPostId($post_id = 0){
    $this->post_id = self::secure($post_id);

    if (empty($this->post_id) || !is_numeric($this->post_id)) {
      $this->throwError("Invalid argument: Post id must be a positive integer");
    }

    return $this;
  }
  public function updatePost($re_data = array()){
    if (empty($this->post_id) || empty($re_data) || !is_array($re_data)) {
      return false;
    }

        if (!empty($re_data['description'])) {
            $re_data['description'] = $this->upsertHtags($re_data['description']);
            $re_data['description'] = strip_tags( RemoveXSS( px_StripSlashes( $re_data['description'] ) ) );
            $re_data['description'] = Generic::secure($re_data['description']);
        }

    return self::$db->where('post_id',$this->post_id)->update(T_POSTS,$re_data);
  }
  public function isPostReported(){
    if (empty(IS_LOGGED) || empty($this->post_id)) {
      return false;
    }

    self::$db->where('user_id',self::$me->user_id);
    self::$db->where('post_id',$this->post_id);

    return (self::$db->getValue(T_POST_REPORTS,'COUNT(*)') > 0);
  }
  public function reportPost(){
    if (empty(IS_LOGGED) || empty($this->post_id)) {
      return false;
    }

    $code = null;
    $user = self::$me->user_id;

    if ($this->isPostReported() == true) {
      self::$db->where('user_id',$user);
      self::$db->where('post_id',$this->post_id);
      self::$db->delete(T_POST_REPORTS);
      $code = -1;
    }
    else{
      self::$db->insert(T_POST_REPORTS,array(
        'user_id' => $user,
        'post_id' => $this->post_id,
        'time' => time()
      ));
      $code = 1;
    }

    return $code;
  }
  public function postData($post = null){
    if (empty($post)) {
      $t_users = T_USERS;
      $t_posts = T_POSTS;

      self::$db->join("`$t_users` u","u.`user_id` = p.`user_id`","INNER");
      self::$db->where('p.`post_id`',$this->post_id);

      $post = self::$db->getOne("`$t_posts` p","p.*,u.`avatar`,u.`username`");

      if (!empty($post)) {
        self::$db->where('post_id',$this->post_id);
        $post->likes = self::$db->getValue(T_POST_LIKES,"COUNT(`id`)");

        self::$db->where('post_id',$this->post_id);
        $post->votes = self::$db->getValue(T_POST_COMMENTS,"COUNT(`id`)");
      }

    }
    
    if (!empty($post)) {

      $this->setPostId($post->post_id);
      self::$db->where('post_id',$post->post_id);
      $media_set = self::$db->get(T_MEDIA);
      if (!empty($media_set)) {
        foreach ($media_set as $key => $file) {
          if ($post->type == 'gif') {
            $file->file  = urldecode($file->file);
            $file->extra = urldecode($file->extra);
          }     
          $media_set[$key] = $file;
        }
        $post->media_set = $media_set;
      }

      $post->comments     = $this->getPostComments();
      $post->is_owner     = false;
      $post->is_liked     = $this->isLiked();
      $post->is_saved     = $this->isSaved();
      $post->reported     = $this->isPostReported();

            $user = new User();
            $post->user_data = $user->getUserDataById($post->user_id);



            if(self::$config['clickable_url'] == 'on') {
                if((bool)$post->user_data->is_pro == true) {
                    $post->description = $this->linkifyDescription($post->description);
                }
            }

      $post->description  = $this->likifyMentions($post->description);
      $post->description  = $this->tagifyHTags($post->description);
      $post->description  = $this->linkifyHTags($post->description);
      $post->description  = $this->obsceneWords($post->description);
      $post->description  = htmlspecialchars_decode($post->description);


      $post->is_verified  = $this->is_verified($post->user_id);
      $post->is_should_hide  = $this->is_should_hide($post->post_id);



      if (IS_LOGGED) {
        $post->is_owner = (self::$me->user_id == $post->user_id || IS_ADMIN);
      }
    }
    return $post;
  }
  public function getPostComments($offset = false){
    if (empty($this->post_id)) {
      return false;
    }

    if ($offset && is_numeric($offset)) {
      self::$db->where('id',$offset,'<');
    }

    self::$db->where('post_id',$this->post_id)->orderBy('id','DESC');

    $commset  = self::$db->get(T_POST_COMMENTS,$this->comm_limit,array('id'));
    $comments = array();

    if (!empty($commset)) {
      foreach ($commset as $key => $comment) {
        $comments[] = $this->postCommentData($comment->id);
      }
    }

    return $comments;
  }
  public function likifyMentions($text = ""){
    $text = preg_replace_callback('/(?:^|\s|,)\B@([a-zA-Z0-9_]{4,32})/is', function($m){
      $uname = $m[1];
      if ($this->userNameExists($uname)) {
        return self::createHtmlEl('a',array(
          'href' => sprintf("%s/%s",self::$site_url,$uname),
          'target' => '_blank',
          'class' => 'mention',
        ),"@$uname");
      }
      else{
        return "@$uname";
      }
    }, $text);

    return $text;
  }
  public function tagifyHTags($text = ""){
    if (!empty($text) && is_string($text)) {
      preg_match_all('/(#\[([0-9]+)\])/i', $text, $matches);
      $matches = (!empty($matches[2])) ? $matches[2] : array();

      if (!empty($matches)) {   
        $htags = self::$db->where('id',$matches,"IN")->get(T_HTAGS,null,array('id','tag'));
        if (!empty($htags)) {
          foreach ($htags as $htag) {
            $text = str_replace("#[{$htag->id}]", "#{$htag->tag}", $text);
          }
        }
      }
    }

      return $text;
  }
  public function linkifyDescription($text =""){
        if (!empty($text) && is_string($text)) {
            preg_match_all('/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/im', $text, $matches, PREG_SET_ORDER, 0);
            foreach ($matches as $match) {
                if( $match[0] !== 'http://' && $match[0] !== 'https://' ) {
                    if (preg_match("/http(|s)\:\/\//", $match[0])) {
                        $text = str_replace( $match[0] , '<a href="' . strip_tags($match[0]) . '" target="_blank" class="hash" rel="nofollow">' . $match[0] . '</a>', $text);
                    }
                }
            }
        }
        return $text;
    }
  public function obsceneWords($text = ""){
    if (empty(self::$config['obscene'])) {
      return $text;
    }
      $obscene = preg_split('/[,]/s', self::$config['obscene']);
      if (!empty($obscene) && is_array($obscene)) {
          foreach ($obscene as $word) {
            $repl = self::createHtmlEl('s',null,str_repeat('?', len($word)));
              $text = preg_replace("/$word/is",$repl, $text);
          }
      }

      return $text;
  }
  public function insertPost($data = array()){
    if (empty(IS_LOGGED)) {
      return false;
    }

    if (!empty($data['description'])) {
      $data['description'] = $this->upsertHtags($data['description']);
            $data['description'] = strip_tags( RemoveXSS( px_StripSlashes( $data['description'] ) ) );
      $data['description'] = Generic::secure($data['description']);
    }
    $data['registered'] = sprintf('%s/%s',date('Y'),date('n'));
    return self::$db->insert(T_POSTS,$data);
  }
  public function insertMedia($data = array()){
    if (empty(IS_LOGGED) || (empty($data['post_id']) && empty($this->post_id))) {
      return false;
    }

    else if(empty($data['post_id']) && !empty($this->post_id)){
      $data['post_id'] = $this->post_id;
    }

    $data['user_id'] = self::$me->user_id;
    return self::$db->insert(T_MEDIA,$data);
  }
  public function isPostOwner($admin = true){
    if (empty(IS_LOGGED)) {
      return false;
    }

    // if ($admin && IS_ADMIN) {
    //  return true;
    // }

    $user_id = self::$me->user_id;
    $post_id = $this->post_id;

    if (empty($user_id) || empty($post_id)) {
      return false;
    }

    self::$db->where("user_id",$user_id);
    self::$db->where("post_id",$post_id);

    return (self::$db->getValue(T_POSTS,'COUNT(*)') > 0);
  }
  public function deletePost(){
    $post_id = $this->post_id;
    self::$db->where('post_id' , $post_id)->delete(T_ACTIVITIES);
    self::$db->where('post_id',$post_id);
    $media_set = self::$db->get(T_MEDIA);
    $del = new Media();
    $comments = $this->getPostComments();
    if (!empty($comments)) {
      foreach ($comments as $key => $comment) {
        $this->deletePostComment($comment->id);
      }
    }
    foreach ($media_set as $key => $file_data) {
        $del->deleteFromFTPorS3($file_data->file);
       // $del->deleteFromFTPorS3($file_data->extra);
        
      if (file_exists($file_data->file)) {
        try {
          unlink($file_data->file); 
        }
        catch (Exception $e) {
        }
      }

      if (file_exists($file_data->extra)) {
        try {
          unlink($file_data->extra);  
        }
        catch (Exception $e) {
        }
      }
    }
    self::$db->where("post_id",$post_id);
    return self::$db->delete(T_POSTS);
  }
  public function addPostComment($re_data = array()){
    $re_data['post_id'] = $this->post_id;
    $re_data['user_id'] = $this->user_id;

    if (!empty($re_data['text'])) {
      $this->upsertHtags($re_data['text']);
    }
    self::$db->insert(T_ACTIVITIES,array('user_id' => $re_data['user_id'],
                                           'post_id' => $re_data['post_id'],
                                           'type'    => 'commented_on_post',
                                           'time'    => time()));

    return self::$db->insert(T_POST_COMMENTS,$re_data);
  }
  public function postCommentData($id = 0){

    $t_users = T_USERS;
    $t_comms = T_POST_COMMENTS;

    self::$db->join("{$t_users} u","c.user_id = u.user_id ","INNER");
    self::$db->where("c.id",$id);
      $comment = self::$db->getOne("{$t_comms} c","c.id,c.user_id,c.post_id,c.text,c.time,u.username,u.avatar");
    if (!empty($comment)) {
      $comment->is_owner = $this->isCommentOwner($id);
      $comment->text     = $this->likifyMentions($comment->text);
      $comment->text     = $this->linkifyHTags($comment->text);
      $comment->text     = $this->link_Markup($comment->text);
      $comment->likes    = self::$db->where('comment_id',$id)->getValue(T_COMMENTS_LIKES,'COUNT(*)');
      $comment->is_liked = 0;
      if (self::$db->where('comment_id',$id)->where('user_id',self::$me->user_id)->getValue(T_COMMENTS_LIKES,'COUNT(*)')) {
        $comment->is_liked = 1;
      }
      $comment->replies    = self::$db->where('comment_id',$id)->getValue(T_COMMENTS_REPLY,'COUNT(*)');
    }
    return $comment;
  }
  public function isCommentOwner($comment_id = 0,$user_id = 0){

    if ((empty($user_id) || !is_numeric($user_id)) && IS_LOGGED) {
      $user_id = self::$me->user_id;
    }

    $comment = self::$db->where("id",$comment_id)->getOne(T_POST_COMMENTS);

      $post = self::$db->where("post_id",$comment->post_id)->getOne(T_POSTS);

    if ($post->user_id == self::$me->user_id || IS_ADMIN || $comment->user_id == self::$me->user_id) {
      return true;
    }
    return false;
  }
  public function deletePostComment($comment_id = 0){
    $comment = self::$db->where("id",$comment_id)->getOne(T_POST_COMMENTS);
    self::$db->where('comment_id',$comment_id)->delete(T_COMMENTS_LIKES);
    $comment_object = new Comments();
    $replies = $comment_object->get_comment_replies($comment_id);
    foreach ($replies as $key => $reply) {
      self::$db->where('reply_id',$reply->id)->delete(T_COMMENTS_REPLY_LIKES);
    }
        self::$db->where('comment_id',$comment_id)->delete(T_COMMENTS_REPLY);
    self::$db->where('user_id' , $comment->user_id)->where('post_id' , $comment->post_id)->where('type' ,'commented_on_post')->delete(T_ACTIVITIES);
    self::$db->where("id",$comment_id);
    return self::$db->delete(T_POST_COMMENTS);
  }
  public function countPosts(){
    if (empty($this->user_id)) {
      return false;
    }

    self::$db->where('user_id',$this->user_id);
    return self::$db->getValue(T_POSTS,'COUNT(*)');
  }
  public function countSavedPosts(){
    if (empty(IS_LOGGED)) {
      return false;
    }

    $user_id = self::$me->user_id;
    self::$db->where('user_id',$user_id);
    return self::$db->getValue(T_SAVED_POSTS,'COUNT(*)');
  }
  public function getPostOwnerData(){
    if (empty($this->post_id)) {
      return false;
    }

    $post_id = $this->post_id;
    $t_users = T_USERS;
    $t_posts = T_POSTS;
    $data    = null;

    self::$db->join("{$t_users} u","u.user_id = p.user_id ","RIGHT");
    self::$db->where('post_id',$post_id);
      $query   = self::$db->getOne("{$t_posts} p","u.*");

      if (!empty($query)) {
        $data = $query;
      }
      
      return $data;
  }
  public function isLiked(){

    if (empty($this->post_id) || empty(IS_LOGGED)) {
      return false;
    }

    $user_id = self::$me->user_id;
    $post_id = $this->post_id;
    

    self::$db->where('post_id',$post_id);
    self::$db->where('user_id',$user_id);
    $likes   = self::$db->getValue(T_POST_LIKES,"COUNT(*)");

    return ($likes > 0);
  }
  public function isSaved(){

    if (empty($this->post_id) || empty(IS_LOGGED)) {
      return false;
    }

    $user_id = self::$me->user_id;
    $post_id = $this->post_id;
    

    self::$db->where('post_id',$post_id);
    self::$db->where('user_id',$user_id);
    $likes   = self::$db->getValue(T_SAVED_POSTS,"COUNT(*)");

    return ($likes > 0);
  }
  public function likePost(){
    if (empty($this->post_id) || empty(IS_LOGGED)) {
      return false;
    }

    $user_id = self::$me->user_id;
    $post_id = $this->post_id;
    $code    = 0;

    if ($this->isLiked()) {
      self::$db->where('post_id',$post_id);
      self::$db->where('user_id',$user_id);
      self::$db->delete(T_POST_LIKES);
      self::$db->where('user_id' , $user_id)->where('post_id' , $post_id)->where('type' ,'liked__post')->delete(T_ACTIVITIES);
      $code = -1;
    }
    else{
      $insert = self::$db->insert(T_POST_LIKES,array(
        'post_id' => $post_id,
        'user_id' => $user_id,
        'time'    => time()
      ));
      self::$db->insert(T_ACTIVITIES,array('user_id' => $user_id,
                                           'post_id' => $post_id,
                                           'type'    => 'liked__post',
                                           'time'    => time()));

      if (is_numeric($insert)) {
        $code = 1;
      }
    }

    return $code;
  }
  public function savePost(){
    if (empty($this->post_id) || empty(IS_LOGGED)) {
      return false;
    }

    $user_id = self::$me->user_id;
    $post_id = $this->post_id;
    $code    = 0;

    if ($this->isSaved()) {
      self::$db->where('post_id',$post_id);
      self::$db->where('user_id',$user_id);
      self::$db->delete(T_SAVED_POSTS);
      $code = -1;
    }
    else{
      $insert = self::$db->insert(T_SAVED_POSTS,array(
        'post_id' => $post_id,
        'user_id' => $user_id
      ));

      if (is_numeric($insert)) {
        $code = 1;
      }
    }

    return $code;
  }
  public function getLikes($type = 'up'){
    if (empty($this->post_id)) {
      return false;
    }

    else if(!in_array($type, array('up','down'))){
      return false;
    }

    $post_id = $this->post_id;
    self::$db->where('post_id',$post_id);
    self::$db->where('type',$type);
    $likes   = self::$db->getValue(T_POST_LIKES,'COUNT(*)');

    return $likes;
  }
  public function getFeaturedPosts(){
    $data = array();
    $sql  = pxp_sqltepmlate('posts/get.featured.posts',array(
      't_posts' => T_POSTS,
      't_likes' => T_POST_LIKES,
      't_media' => T_MEDIA,
      't_blocks' => T_PROF_BLOCKS,
      't_users' => T_USERS,
      'total_limit' => $this->limit,
      'user_id' => ((!empty(IS_LOGGED)) ? self::$me->user_id : false),
      'time_date' => strtotime('-2 days')
    ));


    try {
      $posts = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $posts = array();
    }
    
    foreach ($posts as $key => $post_data) {
      $post_data->thumb = '';
      $t = $post_data->type;

      if (in_array($t, array('youtube','image','gif','video','vimeo','dailymotion','playtube','mp4','fetched'))) {
        $post_data->thumb = $post_data->file;
      }

      if (!empty($post_data->fname) && !empty($post_data->lname)) {
        $post_data->username = sprintf('%s %s',$post_data->fname,$post_data->lname);
      }

      $post_data->dsr = preg_replace('/(\#\[[0-9]+\])/is', '', $post_data->dsr);

      $data[] = $post_data;
    }

    return $data;
  }
  public function hasNext($page = false){
    if (empty($this->post_id)) {
      return false;
    }

    $next_id = 0;
    $table   = ($page == 'favourites') ? T_SAVED_POSTS : T_POSTS;
    $sql     = pxp_sqltepmlate('posts/get.next.post.id',array(
      'page' => $page,
      'post_id' => $this->post_id,
      'table' => $table,
      'tag_id' => $this->tag_id
    ));

    $query = self::$db->rawQuery($sql);

    if (!empty($query) && is_array($query)) {
      $query   = array_shift($query);
      $next_id = $query->post_id;
    }

    return $next_id;
  }
  public function hasPrev($page = false){
    if (empty($this->post_id)) {
      return false;
    }

    $next_id = 0;
    $table   = ($page == 'favourites') ? T_SAVED_POSTS : T_POSTS;
    $sql     = pxp_sqltepmlate('posts/get.prev.post.id',array(
      'page' => $page,
      'post_id' => $this->post_id,
      'table' => $table,
      'tag_id' => $this->tag_id
    ));

    $query = self::$db->rawQuery($sql);

    if (!empty($query) && is_array($query)) {
      $query   = array_shift($query);
      $next_id = $query->post_id;
    }
    
    return $next_id;
  }
  public function searchPosts($htag = "",$limit = 20,$offset = ''){
    $data  = array();
    if (!empty($offset)) {
      $offset = Generic::secure($offset);
      $offset = ' AND h.id > '.$offset.' ';
    }
    $sql   = pxp_sqltepmlate('posts/get.posts.bytag',array(
      't_htags' => T_HTAGS,
      't_posts' => T_POSTS,
            't_users' => T_USERS,
      'hashtag' => $htag,
      'limit' => $limit,
      'offset' => $offset
    ));

    try {
      $query = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $query = array();
    }

    if (!empty($query)) {
      $data = $query;
    }
    
    return $data;
  }
  public function getLikedUsers($offset  = '',$limit = '') {
    if (empty($this->post_id)) {
      return false;
    }
    if (!empty($limit)) {
      $limit = Generic::secure($limit);
      $limit = ' LIMIT '.$limit;
    }
    if (!empty($offset)) {
      $offset = Generic::secure($offset);
      $offset = ' AND `id` < '.$offset.' ';
    }

    $uid = (!empty(IS_LOGGED)) ? self::$me->user_id : false;
    $sql = pxp_sqltepmlate('posts/get.post.likes',array(
      't_users' => T_USERS,
      't_likes' => T_POST_LIKES,
      't_connv' => T_CONNECTIV,
      'user_id' => $uid,
      'post_id' => $this->post_id,
      'limit_'  => $limit,
      'offset_' => $offset
    ));

    try {
      $likes = self::$db->rawQuery($sql);
    } 
    catch (Exception $e) {
      $likes = array();
    }

    return (!empty($likes)) ? $likes : array();
  }
  public function add_view(){
    if (empty($this->post_id)) {
      return false;
    }
    $post_id = $this->post_id;
    $count    = 0;
    $hash = sha1($post_id);
    if (isset($_COOKIE[$hash])) {
      self::$db->where('post_id',$post_id);
      $count = self::$db->getValue(T_POSTS,'views');
    }
    else{
      setcookie($hash,$hash,time()+(60*60*2));
      self::$db->rawQuery("UPDATE ".T_POSTS." SET `views` = `views`+1 where `POST_id` = ?", Array ($post_id));
      self::$db->where('post_id',$post_id);
      $count = self::$db->getValue(T_POSTS,'views');
    }

    return $count;
  }
  public function is_should_hide($post_id){
    if (empty($post_id)) {
      return false;
    }
    $post_id = Generic::secure($post_id);

    self::$db->where('post_id',$post_id);

    $count  = self::$db->getValue(T_POST_REPORTS,'COUNT(*)');
    if ($count > 3) {
      return true;
    }
    return false;
  }
  public function getUsersActivities($offset = 0 , $limit = 5)
  {
    if (empty(IS_LOGGED)) {
      return false;
    }
    $limit = Generic::secure($limit);
    $subquery = " `id` > 0 ";
    if ($offset > 0) {
      $offset = Generic::secure($offset);
      $subquery = " `id` < ".$offset;
    }
    $user_id = self::$me->user_id;
    $query = "SELECT * FROM " . T_ACTIVITIES . " WHERE {$subquery}  AND `user_id` IN (SELECT `following_id` FROM " . T_CONNECTIV . " WHERE `follower_id` = {$user_id} AND `active` = '1') AND `user_id` NOT IN ($user_id) ORDER BY `id` DESC LIMIT {$limit} ";
    $activities = self::$db->rawQuery($query);
    foreach ($activities as $key => $value) {

      $users = new User();
      $users->setUserById($value->user_id);
      $value->user_data = $users->getUserDataById($value->user_id);
      $post_owner_id = self::$db->where('post_id',$value->post_id)->getOne(T_POSTS);
      if (!empty($value->user_data) && !empty($post_owner_id)) {

        $post_owner = $users->getUserDataById($post_owner_id->user_id);
        if (!empty($post_owner)) {
          if (!empty($value->post_id)) {
            $name = $post_owner->name;
            if (self::$me->user_id == $post_owner_id->user_id) {
              $name = lang('your');
            }
            if ($value->user_data->user_id == $post_owner_id->user_id) {
              $name = lang('his');
            }
            $value->activity_link = self::$config['site_url'].'/post/'.$value->post_id;
            if ($value->type == 'commented_on_post') {
              $value->text = str_replace("{post}", '<a href="'.$value->activity_link.'"  data-ajax="ajax_loading.php?app=posts&apph=view_post&pid='.$value->post_id.'">'.lang('post').'</a>', lang($value->type));
              $value->text = str_replace("{user}", '<a href="'.$post_owner->url.'"  data-ajax="ajax_loading.php?app=profile&apph=profile&uname='.$post_owner->username.'">'.$name.'</a>', $value->text);
              $value->text = '<a href="'.$value->user_data->url.'" data-ajax="ajax_loading.php?app=profile&apph=profile&uname='.$value->user_data->username.'"> '.$value->user_data->name.'</a>'.' '.$value->text;
            }
            elseif ($value->type == 'liked__post') {
              $value->text = str_replace("{post}", '<a href="'.$value->activity_link.'" data-ajax="ajax_loading.php?app=posts&apph=view_post&pid='.$value->post_id.'">'.lang('post').'</a>', lang($value->type));
              $value->text = str_replace("{user}", '<a href="'.$post_owner->url.'"   data-ajax="ajax_loading.php?app=profile&apph=profile&uname='.$post_owner->username.'">'.$name.'</a>', $value->text);
              $value->text = '<a href="'.$value->user_data->url.'" data-ajax="ajax_loading.php?app=profile&apph=profile&uname='.$value->user_data->username.'">'.$value->user_data->name.'</a>'.' '.$value->text;
            }elseif ($value->type == 'share_post') {
              $value->text = str_replace("{post}", '<a href="'.$value->activity_link.'" data-ajax="ajax_loading.php?app=posts&apph=view_post&pid='.$value->post_id.'">'.lang('post').'</a>', lang($value->type));
              $value->text = str_replace("{user}", '<a href="'.$post_owner->url.'"   data-ajax="ajax_loading.php?app=profile&apph=profile&uname='.$post_owner->username.'">'.$name.'</a>', $value->text);
              $value->text = '<a href="'.$value->user_data->url.'" data-ajax="ajax_loading.php?app=profile&apph=profile&uname='.$value->user_data->username.'">'.$value->user_data->name.'</a>'.' '.$value->text;
            }
          }
        }
      }
      
      if ($value->following_id > 0) {
        $users->setUserById($value->following_id);
        $following_data = $users->getUserDataById($value->following_id);
        if (!empty($following_data) && !empty($value->user_data)) {
          $value->activity_link = $following_data->url;
          $value->following_data = $following_data;
          $follow_name = $value->following_data->name;
          if (self::$me->user_id == $value->following_data->user_id) {
            $follow_name = lang('you') ;
          }
          $value->text = str_replace("{user}", '<a href="'.$value->activity_link.'"   data-ajax="ajax_loading.php?app=profile&apph=profile&uname='.$value->following_data->username.'">'.$follow_name.'</a>', lang($value->type));
          $value->text = '<a href="'.$value->user_data->url.'" data-ajax="ajax_loading.php?app=profile&apph=profile&uname='.$value->user_data->username.'">'.$value->user_data->name.'</a>'.' '.$value->text;
        }
      }
      

      
    }
    return $activities;
  }
  public function BoostPost($post_id){
    if (empty($post_id) || !is_numeric($post_id) || $post_id < 1) {
      return false;
    }
    $post_id = Generic::secure($post_id);
    $this->setPostId($post_id);
    $post_data = $this->postData(false);
    if ($post_data->user_data->is_pro && $post_data->is_owner) {
      if ($post_data->boosted) {
        self::$db->where('post_id',$post_id)->update(T_POSTS,array('boosted' => 0));
        return 2;
      }
      else{

        $boosted_posts_counts = self::$db->where('boosted',1)->where('user_id',$post_data->user_data->user_id)->getValue(T_POSTS,'COUNT(*)');
        if ($boosted_posts_counts < self::$config['boosted_posts']) {
          self::$db->where('post_id',$post_id)->update(T_POSTS,array('boosted' => 1));
          return 1;
        }
        else{
          return false;
        }
      }
    }
    return false;
  }
  public function getBoostedPosts($offset = false){
    if (empty(IS_LOGGED)) {
      return false;
    }

    $data    = array();
    $user_id = self::$me->user_id;

    try {
      $posts = self::$db->where('boosted',1)->where('user_id',$user_id)->get(T_POSTS);
    } 
    catch (Exception $e) {
      $posts = array();
    }

    if (!empty($posts)) {

      foreach ($posts as $key => $post) {
        $this->setPostId($post->post_id);
        $data[] = $this->postData(false);
      }
    }

    return $data;
  }
  public function countBoostedPosts(){
    if (empty($this->user_id)) {
      return false;
    }

    self::$db->where('user_id',$this->user_id)->where('boosted',1);
    return self::$db->getValue(T_POSTS,'COUNT(*)');
  }
  public function getBoostedPostsByUserID($user_id ,$limit = 0 ,$offset = 0){
    if (empty(IS_LOGGED) || empty($user_id)) {
      return false;
    }

    $data    = array();
    $user_id = Generic::secure($user_id);

    try {
      self::$db->where('boosted',1)->where('user_id',$user_id);
      if (!empty($offset) && $offset > 0) {
        self::$db->where('post_id',$offset,'>');
      }

      if (!empty($limit) && $limit > 0) {
        $posts = self::$db->get(T_POSTS,$limit);
      }
      else{
        $posts = self::$db->get(T_POSTS);
      }
    } 
    catch (Exception $e) {
      $posts = array();
    }

    if (!empty($posts)) {

      foreach ($posts as $key => $post) {
        $this->setPostId($post->post_id);
        $data[] = $this->postData(false);
      }
    }

    return $data;
  }
}













    //end of functions
}