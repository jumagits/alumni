<?php 

class location_ajax extends controller {

    public function task_json()
    {
        $locationModel = $this->loadModel("location");
        $url_seg = $this->loadHelper('url_helper');
        // $id = $url_seg->segment(3);
        // $type = $url_seg->segment(4);

        $id = $url_seg->segment(4);
        $type = $url_seg->segment(5);

        $res = $locationModel->location_json($id, $type);
        $resx = json_encode($res);
        header('Content-Type: application/json');
        echo $resx;
    }

}
