<?php 
class eventsModel extends Model {
    
     //count

       public function count_all_events()
    {
        $sql = "SELECT * FROM events";
        $result = $this->db->query($sql);
        return $result->num_rows;
    }

 

    public function getAllEvents() //$clinicId
    {
        $sql = "SELECT * FROM events ORDER BY id DESC";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

     public function get_only_four_events() //$clinicId
    {
        $sql = "SELECT * FROM events ORDER BY id DESC limit 4";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


    //get event 


    public function get_event($event_id)
    {
        $sql = "SELECT * FROM events WHERE id = '{$event_id}' LIMIT 1";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

    public function checkifAdttending($event_id,$user_id,$status){
        $sql = "SELECT * FROM event_attendance WHERE member_id = '{$user_id}'  LIMIT 1";
        $result = $this->db->query($sql);
         while($row = $result->fetch_array()){ 
            $status = $row['status'];
            if($status == '1'){
                return true;
            }else{
               return false;
            }
         }
    }


    public function register_for_event($event_id,$user_id,$status){

         $sql = "SELECT * FROM event_attendance WHERE member_id = '{$user_id}'  LIMIT 1";
        $result = $this->db->query($sql);

        while($row = $result->fetch_array()){
            $dbstatus = $row['status'];
        }
        if($result->num_rows > 0){           
           if($dbstatus === '1'){
            $SQL2 = "UPDATE event_attendance SET status = '0' WHERE member_id = '{$user_id}' ";
              if($this->db->query($SQL2))
                {
                    $msg = "Successfully Cancelled attendance for the event";
                    return array('msg'=>$msg, 'status'=> true);
                }else{
                    $msg = "Failed to register for the Event";
                    return array('msg'=>$msg, 'status'=> false);
                }
           }else{            
            $SQL2 = "UPDATE event_attendance SET status = '1' WHERE member_id = '{$user_id}' ";
              if($this->db->query($SQL2))
                {
                    $msg = "Successfully registered for the event";
                    return array('msg'=>$msg, 'status'=> true);
                }else{
                    $msg = "Failed to register for the Event";
                    return array('msg'=>$msg, 'status'=> false);
                }
           }         
       }else{
         $SQL2 = "INSERT INTO event_attendance (event_id,member_id,status) VALUES('$event_id','$user_id','$status')";
          if($this->db->query($SQL2))
            {
                $msg = "Successfully registered for the event";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = "Failed to register for the Event";
                return array('msg'=>$msg, 'status'=> false);
            }

       }

    }

     public function addEvent($category,$photo,$schedule,$title,$location,$description,$created_by)
    {       
            $SQL2 = "INSERT INTO events(category,title,content,schedule,banner,location,created_by
           )VALUES ('{$category}','{$title}','{$description}','{$schedule}','{$photo}','{$location}','{$created_by}')";
   
          if($this->db->query($SQL2))
            {
                $msg = "Successfully registered New Event # {$title}";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = "Failed to register Event # {$title}";
                return array('msg'=>$msg, 'status'=> false);
            }
    }


  public function editEvent($category,$photo,$schedule,$title,$location,$description,$created_by,$event_id)
    {     

    if(!empty($photo)){

         $SQL2 = "UPDATE events SET category = '{$category}', banner = '{$photo}', schedule = '{$schedule}', title = '{$title}', location = '{$location}', content = '{$description}', created_by = '{$created_by}' WHERE id = '{$event_id}' ";

    } else{

         $SQL2 = "UPDATE events SET category = '{$category}', schedule = '{$schedule}', title = '{$title}', location = '{$location}', content = '{$description}',created_by = '{$created_by}' WHERE id = '{$event_id}' ";

    } 
          if($this->db->query($SQL2))
            {
                $msg = "Successfully Updated  Event ";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = "Failed to Update Event ";
                return array('msg'=>$msg, 'status'=> false);
            }
    }


    //delete

     public function get_del_event($event_id)
   {
       $sql = "SELECT * FROM events WHERE id = '{$event_id}' LIMIT 1";
       $result = $this->db->query($sql);
       $data = [];
       while ($row = $result->fetch_object()) {
           $data[] = $row;
       }
       return @$data;
   }


   //delete codes

    public function del_event($id)
   {
       $sql = "DELETE FROM events WHERE id='{$id}'";
       $query = $this->db->query($sql)or die($this->db->error);

       if(!($this->db->query($sql)))
       {
           return die($this->db->error);
       }
       else
       {
           return true;
       }
   }


    //end of methods


    


}