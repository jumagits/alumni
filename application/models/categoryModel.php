<?php

class categoryModel extends model {  

public function all_cats()
{
    $sql = "SELECT * FROM categories";
    $result = $this->db->query($sql);        
    $data = [];
    while ($row = $result->fetch_object()) {
        $data[] = $row;
    }
    return @$data;
}


public function cat_by_name($id)
{
    $sql = "SELECT * FROM categories WHERE cat_id ='$id' LIMIT 1";
    $result = $this->db->query($sql);        
    $row = $result->fetch_array();           
    return @$row['cat_title'];
}


public function fetchCat($id)
{
    $sql = "SELECT * FROM categories WHERE cat_id ='$id' LIMIT 1";
    $result = $this->db->query($sql);
    $data = [];        
    while ($row = $result->fetch_object()) {
    $data[] = $row;
    }
    return @$data;      
        
}
    

public function editCate($cat_title,$cat_id){
    if(isset($cat_title)){
        $SQL2 = "UPDATE categories SET cat_title = '$cat_title' WHERE cat_id = '$cat_id'";  
        if($this->db->query($SQL2))
        {
            $msg = "Successfully Updated  Category ";
            return array('msg'=>$msg, 'status'=> true);
        }else{
            $msg =$this->db->error;
            return array('msg'=>$msg, 'status'=> false);
        }
    }
}

public function add_cat($cat_title){
    $XSQL = "INSERT INTO categories(cat_title)VALUES('{$cat_title}')";
    if($this->db->query($XSQL))
    {
        $msg = "Successfully registered New Category # {$$cat_title}";
        return array('msg'=>$msg, 'status'=> true);
    }else{
        $msg = "Failed to register category # {$cat_title}";
        return array('msg'=>$msg, 'status'=> false);
    }
}


 public function delete_cate($cat_id){
     $XSQL = "DELETE FROM categories WHERE cat_id = '$cat_id' ";
        if($this->db->query($XSQL))
        {
            $msg = "Successfully Deleted  Category ";
            return array('msg'=>$msg, 'status'=> true);
        }else{
            $msg = $this->db->error;
            return array('msg'=>$msg, 'status'=> false);
        }
}



//end of methods

}