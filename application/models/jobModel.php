<?php

class jobModel extends model {   
  
public function all_jobs()
    {
        $sql = "SELECT * FROM careers ";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


public function only_three_jobs()
    {
        $sql = "SELECT * FROM careers limit 3";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

public function all_jobs_m($m,$n)
    {
        $sql = "SELECT * FROM careers LIMIT $m,$n";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }



 public function jobs_count()
    {
        $sql = "SELECT * FROM careers";
        $result = $this->db->query($sql);
        return $result->num_rows;
    }



public function get_job_by_id($id)
    {
        $sql = "SELECT * FROM careers WHERE id = '{$id}'";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


public function get_field_by_id($id)
    {
        $sql = "SELECT * FROM fields WHERE id = '{$id}'";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }



 public function recent_jobs()
    {
        $sql = "SELECT * FROM careers ORDER BY id DESC LIMIT 2";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }
    

public function addJob($photo,$company,$location,$title,$description,$field,$deadline,$user_id,$link)
    {       
            $SQL2 = "INSERT INTO careers(photo,company,location,job_title,description,field,deadline,user_id,job_link)VALUES ('{$photo}','{$company}','{$location}','{$title}','{$description}','{$field}','{$deadline}','{$user_id}','{$link}')";
   
          if($this->db->query($SQL2))
            {
                $msg = "Successfully registered New Career # {$title}";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = "Failed to register Career # {$title}";
                return array('msg'=>$msg, 'status'=> false);
            }
    }


    //search code

public function search_job($search)
     {
        $sql = "SELECT * FROM careers WHERE  job_title LIKE '%$search%' ";
        $query = $this->db->query($sql);
        @$tr = "";
        if($query->num_rows > 0){
            while ($row = $query->fetch_array())
            {                
                $photo = $row['photo'];
                $job_title = $row['job_title'];
                $company = $row['company'];
                $location = $row['location']; 
                $tr .= '
                 <div class="action-box text-center mb-4" style="border:2px solid #d3d3d3;">                  
                  <div class="testimonial-content">
                        <h4>
                        <a href="#" style="color: teal !important; ">'. $job_title.' </a><br>
                         <small>
                         Company: <b>'.$company.'</b>,Location: <b>'.$location.'</b>
                         </small>
                       </h4>
                        <h5><a href="" class="badge badge-dark">More Info</a>
                        <a href="" class="badge badge-warning">Apply Now!</a></h5>

                    </div>
                </div>
                ';
            }
        }else{
            $tr = " 
                    <div class='text-center med ' style='color:red;'><p>Sorry <b>".strtoupper($search)."</b> doesnot Exist Yet</p> </div>
                  ";
        }
        return @$tr;
    }


public function get_del_job($job_id)
   {
       $sql = "SELECT * FROM careers WHERE id = '{$job_id}' LIMIT 1";
       $result = $this->db->query($sql);
       $data = [];
       while ($row = $result->fetch_object()) {
           $data[] = $row;
       }
       return @$data;
   }

public function del_job($id)
   {
       $sql = "DELETE FROM careers WHERE id='{$id}'";
       $query = $this->db->query($sql)or die($this->db->error);
       if(!($this->db->query($sql)))
       {
           return die($this->db->error);
       }
       else
       {
           return true;
       }
   }

public function del_field($id)
   {
       $sql = "DELETE FROM fields WHERE id='{$id}'";
       $query = $this->db->query($sql)or die($this->db->error);
       if(!($this->db->query($sql)))
       {
           return die($this->db->error);
       }
       else
       {
           return true;
       }
   }
   //update

public function editJob($photo,$company,$title,$location,$deadline,$field,$description,$posted_by,$job_id,$link)
    {     

    if(!empty($photo)){

         $SQL2 = "UPDATE careers SET 
            photo = '{$photo}',
            company = '{$company}',
            location = '{$location}',
            job_title = '{$title}'
            description = '{$description}',
            field = '{$field}',
            deadline ='{$deadline}',
            user_id = '{$posted_by}',
            job_link = '{$link}'  
            WHERE id = '{$job_id}' ";


    } else{

           $SQL2 = "UPDATE careers SET 
            company = '{$company}',
            location = '{$location}',
            job_title = '{$title}', 
            description = '{$description}',
            field = '{$field}',
            deadline='{$deadline}',
            user_id = '{$posted_by}',
            job_link = '{$link}'
            WHERE id = '{$job_id}' ";

    }          
   
      if($this->db->query($SQL2))
        {
            $msg = "Successfully Updated  Job ";
            return array('msg'=>$msg, 'status'=> true);
        }else{
            $msg = "Failed to Update Job ";
            return array('msg'=>$msg, 'status'=> false);
        }
    }


    //update field

public function editF($field,$id){
        if(isset($field)){
            $SQL2 = "UPDATE fields SET field = '$field' WHERE id = '$id'";   
            if($this->db->query($SQL2))
            {
                $msg = "Successfully Updated  field ";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = "Failed to Update field ";
                return array('msg'=>$msg, 'status'=> false);
            }
        }
    }


    //create field

public function add_field($field){
     $XSQL = "INSERT INTO fields(field)VALUES('{$field}')";
        if($this->db->query($XSQL))
        {
            $msg = "Successfully registered New Category # {$field}";
            return array('msg'=>$msg, 'status'=> true);
        }else{
            $msg = "Failed to register category # {$field}";
            return array('msg'=>$msg, 'status'=> false);
        }
    }


    //select field

public function get_del_field($id)
   {
       $sql = "SELECT * FROM fields WHERE id = '{$id}' LIMIT 1";
       $result = $this->db->query($sql);
       $data = [];
       while ($row = $result->fetch_assoc()) {
           $data[] = $row;
       }
       return @$data;
   }


public function all_fields()
    {
        $sql = "SELECT * FROM fields ";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }
    
}