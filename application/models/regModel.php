<?php

class regModel extends model
{
 
public function addUser($photo, $fullname, $user_title, $username, $password, $accesslevel, $clinic_id, $email, $phone)
    {
        $sql = "SELECT * FROM login WHERE username='{$username}' OR email='{$email}'";
        $query = $this->db->query($sql);
        $num_rows = $query->num_rows;
        if ($num_rows) {
            $msg = "The system failed to add this user !<br> username: <strong>{$username}</strong> Or email: <strong>{$email}</strong> <br>ALREADY EXIST";
            return array('msg' => $msg, 'status' => false);

        } else {

            $Upass = password_hash($password, PASSWORD_DEFAULT);
            $SQL = "INSERT INTO login(photo,username,user_title,password,pass,fullname,userlevel,clinic_id,phonenumber,email,status, default_clinic, default_role)VALUES('{$photo}','{$username}','{$user_title}','{$Upass}','{$password}','{$fullname}','{$accesslevel}','{$clinic_id}','{$phone}','{$email}','Active','$clinic_id','$accesslevel')";

            $XSQL = "INSERT INTO logins_roles(username,clinic_id,clinicrole)VALUES('{$username}','{$clinic_id}','{$accesslevel}')";

            if ($this->db->query($SQL)) {
                if ($this->db->query($XSQL)) {
                    $msg = "Successfully created user account with # {$username} as access username and an email has been sent to the provided email. Thanks";
                    return array('msg' => $msg, 'status' => true);
                }
            }
        }

    }



    public function get_bumaa_clients()
    {
        $sql = "SELECT * FROM  bumaa_clients WHERE comp = 0";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


     public function get_bumaa_clients_current()
    {
        $sql = "SELECT * FROM  bumaa_clients WHERE comp = 0  ORDER BY created_on DESC LIMIT 5";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


      public function get_bumaa_company_current()
    {

        $sql = "SELECT * FROM  bumaa_clients WHERE comp = 1 ORDER BY created_on DESC LIMIT 5";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


    //pending registration

    public function registeredCompanies()
    {
        $sql = "SELECT * FROM  bumaa_clients WHERE comp = 1 ";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


    public function profile($id)
    {
        $sql = "SELECT * FROM  bumaa_clients WHERE id = '$id' ";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

//end profile

    public function get_details($id)
    {
        $sql = "SELECT * FROM  bumaa_clients WHERE client_refnumber = '$id' OR id = '$id' ";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

    //options

    public function get_bumaa_clients_with_options()
    {
        $sql = "SELECT * FROM bumaa_clients";
        $option = "";
        $query = $this->db->query($sql);
        while ($row = $query->fetch_array()) {
            $name = $row['fname'] . " " . $row['lname'];
            $idx = $row['client_refnumber'];
            $option .= "<option value='{$idx}'";
            $option .= ">{$name}</option>";
        }
        return @$option;
    }

    public function get_four_bumaa_clients_randomly()
    {
        $sql = "SELECT * FROM bumaa_clients  ORDER BY RAND() LIMIT 4";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

    public function get_four_bumaa_clients()
    {
        $sql = "SELECT * FROM  bumaa_clients  order by id desc";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


    //add client

    public function addClient($client_refnumber, $photo,$fname,$lname,$contactphone, $sex,$tribe,$educationlevel,$maritalstatus,$nationality, $contactaddress, $resdistrict, $county, $subcounty,$parish,$village, $registered_by,$clinic_name,$email,$company
    ) {
        $SQL = "SELECT email FROM bumaa_clients WHERE client_refnumber = '{$client_refnumber}'";
        $QRY = $this->db->query($SQL);
        $NROWS = $QRY->num_rows;

        if ($NROWS > 0) {

            return array(

                'msg' => "ERROR : The Client Reference No. : [ $client_refnumber ], already exists",
                 'status' => false
             );

        } else {

            $SQL2 = "INSERT INTO bumaa_clients(
            client_refnumber,photo,fname,lname,            
            contactphone,sex,tribe,educationlevel,maritalstatus,nationality,contactaddress, resdistrict, county,subcounty, parish,
            village , registered_by,clinic_id,email,company)
            VALUES ('{$client_refnumber}', '{$photo}','{$fname}','{$lname}','{$contactphone}','{$sex}','{$tribe}','{$educationlevel}', '{$maritalstatus}', '{$nationality}',
        '{$contactaddress}','{$resdistrict}','{$county}','{$subcounty}','{$parish}','{$village}', '{$registered_by}', '{$clinic_name}','{$email}', '{$company}'
    )";

            if ($this->db->query($SQL2)) {
                $msg = "Successfully registered New Member # {$fname}";
                return array('msg' => $msg, 'status' => true);
            } else {

                return array('msg' => "ERROR" . $this->db->errno . " : " . $this->db->error . "", 'status' => false);
            }

        }

    }

    public function updatephoto($ref_no, $photo)
    {
        $sql = "UPDATE bumaa_clients SET photo = '{$photo}' WHERE client_refnumber='{$ref_no}'";
        if ($this->db->query($sql)) {
            return array('msg' => "updated client photo with refNumber #($ref_no) , Successfully change", 'status' => true);
        } else {
            return array('msg' => "ERROR" . $this->db->errno . " : " . $this->db->error . "", 'status' => false);
        }

    }

    //add get clinic

    public function get_clinic($clinic_id)
    {
        $sql = "SELECT clinic_code FROM clinics WHERE id = '{$clinic_id}' LIMIT 1";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


    //client by reference number

    public function get_clientByRefNo($refno)
    {
        //$sql = "SELECT * FROM bumaa_clients WHERE client_refnumber = '{$refno}' LIMIT 1";
        $sql = "SELECT * FROM bumaa_clients as uc INNER JOIN bumaa_clients as lg ON uc.registered_by = lg.id WHERE client_refnumber = '{$refno}' LIMIT 1";

        @$result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            @$data[] = $row;
        }
        return @$data;
    }

    public function count_clients_all()
    {
        $sql = "SELECT * FROM bumaa_clients WHERE comp = 0";
        $result = $this->db->query($sql);
        return $result->num_rows;
    }


      public function count_companies_all()
    {
        $sql = "SELECT * FROM bumaa_clients WHERE comp = 1";
        $result = $this->db->query($sql);
        return $result->num_rows;
    }

    public function count_clients()
    {
        $sql = "SELECT client_refnumber FROM bumaa_clients";
        $result = $this->db->query($sql);
        return @$result->num_rows;
    }

    public function getTribes($id = '')
    {
        $sql = "SELECT * FROM config_tribes";
        $option = "";
        $query = $this->db->query($sql);
        while ($row = $query->fetch_array()) {
            $name = $row['tribe'];
            $idx = $row['id'];

            $option .= "<option value='{$idx}'";if ($id == $idx) {$option .= 'selected';}$option .= ">{$name}</option>";
        }

        return @$option;
    }

    public function search_client($client_name)
    {
        $regxNames = explode(" ", $client_name);
        $fname = $regxNames[0];
        if (!empty($regxNames[1])) {
            $lname = $regxNames[1];
        } else {
            $lname = "";
        }

    $sql = "SELECT * FROM bumaa_clients WHERE (fname LIKE '%$fname%' OR lname LIKE '%$lname%') ";
        $query = $this->db->query($sql);
        @$tr = "";
        if ($query->num_rows > 0) {
            while ($row = $query->fetch_array()) {                          
                $photo = $row['photo'];
                $id = $row['id'];
                $client_name = $row['fname'] . " " . $row['lname'];
                $contactphone = $row['contactphone'];
                $regnof = date_create($row['created_on']);
                $regno = date_format($regnof, 'l, j<\s\u\p>S</\s\u\p> F Y (h:i:s A)');
                $tr .= '<tr class="text-info">
                   
                    <td>
                        <div> ' . $client_name . '</div>
                    </td>
                    <td>' . $contactphone . '</td>
                    <td>' . $regno . '</td>
                    <td><a href="' . URL . 'register_clients/member-details/' . base64_encode($id) . '" class="btn btn-danger">View</a>
                    </td>

                </tr>';
            }
        } else {
            $tr = "
                    <br/><p style=''><a class='btn btn-warning'> Register New Alumni</a> &nbsp; &nbsp;<a class='btn btn-info' href='" . URL . "register_clients/members'>View All Members</a></p>
                  ";
        }
        return @$tr;



    }



     public function user_information($user_id)
    {
        $sql = "SELECT * FROM bumaa_clients WHERE id = '{$user_id}' LIMIT 1";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_array()) {
            $id = $row['id'];
            $Photo = $row['photo'];
            $fullName = $row['fname'] . " " . $row['lname'];
            $userName = $row['username'];
            $Phone = $row['contactphone'];
            $mail = $row['email'];
            $level = $row['userLevel'];
            $title = $row['userLevel'];

        }
        @$clinicD = array("id" => $id, "photo" => $Photo, "fullname" => $fullName, "username" => $userName, "phonenumber" => $Phone, "email" => $mail, "userlevel" => $level, "user_title" => $title);
        return $clinicD;
    }






    //end 


       public function get_user_to_change($user_id)
    {
        $sql = "SELECT * FROM bumaa_clients WHERE id = '{$user_id}' LIMIT 1";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }





    //add testimony


      public function addTestimony($testimony, $user_id) {
        $SQL = "SELECT * FROM testimonies  WHERE testimony = '{$testimony}'";
        $QRY = $this->db->query($SQL);
        $NROWS = $QRY->num_rows;

        if ($NROWS > 0) {

            return array(

                'msg' => "ERROR : The Testimonial No. : [ $testimony ], already exists",
                 'status' => false
             );

        } else {

            $SQL2 = "INSERT INTO testimonies(user_id,testimony)VALUES ('{$user_id}','{$testimony}')";

            if ($this->db->query($SQL2)) {
                $msg = "Successfully registered New Testimonial # {$testimony}";
                return array('msg' => $msg, 'status' => true);
            } else {

                return array('msg' => "ERROR" . $this->db->errno . " : " . $this->db->error . "", 'status' => false);
            }

        }

    }


    //fetch testimonials

      public function get_testimonies()
    {
        $sql = "SELECT * FROM testimonies";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }



    //fetch testimonials per person

     public function get_user_testimonies($user_id)
    {
        $sql = "SELECT * FROM testimonies WHERE user_id = '{$user_id}' ";
        $result = $this->db->query($sql);
        $data = "";
        while ($row = $result->fetch_array()) {
            $data .= '<li class="list-group-item">'.$row['testimony'].'</li>';
        }
        return @$data;
    }


      //get user to delete
    public function get_del_user($user_id)
    {
        $sql = "SELECT * FROM bumaa_clients WHERE id = '{$user_id}' LIMIT 1";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

//delete user

    public function del_user($id)
    {
        $sql = "DELETE FROM bumaa_clients WHERE id='{$id}'";
        $query = $this->db->query($sql) or die($this->db->error);
        if (!($this->db->query($sql))) {
            return die($this->db->error);
        } else {
            return true;
        }
    }

//deactivation
    public function new_deact_user($id, $status, $username)
    {
        if (strtolower($status) == '1') {
            $sql = "UPDATE bumaa_clients SET status = '0' WHERE id= '{$id}'";
            $query = $this->db->query($sql) or die($this->db->error);
            if (!($this->db->query($sql))) {
                return die($this->db->error);
            } else {
                $msg = "Successfully Deactivated user # {$username}";
                return array('msg' => $msg, 'status' => true);
            }
        } else {
            $sql = "UPDATE bumaa_clients SET status = '1' WHERE id= '{$id}'";
            $query = $this->db->query($sql) or die($this->db->error);
            if (!($this->db->query($sql))) {
                return die($this->db->error);
            } else {
                $msg = "Successfully Activated user # {$username}";
                return array('msg' => $msg, 'status' => true);
            }
        }
    }

    

    public function getonline()
    {
        $sql = "SELECT COUNT(*) FROM bumaa_clients WHERE status = '1'";
        $result = $this->db->query($sql);
        while ($row = $result->fetch_assoc()) {
            $count = $row['COUNT(*)'];
        }
        return @$count;
    }



//adding access to user

    public function addAccess($username, $accesslevel, $clinics)
    {
        $sql = "SELECT * FROM logins_roles WHERE username='{$username}' AND clinic_id='{$clinics}' AND clinicrole='{$accesslevel}'";
        $query = $this->db->query($sql);
        $num_rows = $query->num_rows;
        if ($num_rows) {
            $msg = "You already granted user # {$username} this combination of access roles. i dont authorize this. sorry!";
            return array('msg' => $msg, 'status' => false);
        } else {
            $XSQL = "INSERT INTO logins_roles(username,clinic_id,clinicrole)VALUES('{$username}','{$clinics}','{$accesslevel}')";
            if ($this->db->query($XSQL)) {
                $msg = "Successfully granted user access role to # {$username}";
                return array('msg' => $msg, 'status' => true);
            } else {
                $msg = "Failed to granted user access role to # {$username}";
                return array('msg' => $msg, 'status' => false);
            }
        }
    }

//revoke access

    public function revokeAccess($id)
    {
        $sql = "SELECT * FROM logins_roles WHERE id='{$id}'";
        $query = $this->db->query($sql);
        $num_rows = $query->num_rows;
        if (!$num_rows) {
            $msg = "You already revoked this combination of access roles.";
            return array('msg' => $msg, 'status' => false);
        } else {
            $sql2 = "DELETE FROM logins_roles WHERE id ='{$id}'";
            if ($this->db->query($sql2)) {

                $msg = "Successfully revoked user access role ";
                return array('msg' => $msg, 'status' => true);
            } else {

                $msg = "Failed to revoke user access role from user";
                return array('msg' => $msg, 'status' => false);
            }
        }
    }

//switch clinic

    public function switchClinic($Id, $userLevel, $clinicId)
    {
        $sql = "SELECT * FROM bumaa_clients WHERE id = '{$Id}'";
        $query = $this->db->query($sql) or die($this->db->error);
        $num_rows = $query->num_rows;
        if (!$num_rows) {
            $msg = "Be sure of what you are attempting #";
            return array('msg' => $msg, 'status' => false);
        } else {

            $sql3 = "SELECT * FROM bumaa_clients WHERE id = '{$Id}' AND userLevel = '{$userLevel}' AND clinic_id = '{$clinicId}'";
            $query = $this->db->query($sql3) or die($this->db->error);
            $num_rows = $query->num_rows;
            if ($num_rows) {
                $msg = "You are already using this clinic #";
                return array('msg' => $msg, 'status' => false);
            } else {
                $sql2 = "UPDATE bumaa_clients SET userLevel = '{$userLevel}', clinic_id = '{$clinicId}', default_role ='{$userLevel}' WHERE id = '{$Id}'";

                if (!($this->db->query($sql2))) {
                    die($this->db->error);
                } else {
                    $msg = "You have successfully switched clinic #";
                    return array('msg' => $msg, 'status' => true);
                }
            }
        }
    }


 


//default role

    public function defaultRole($default_role, $id, $default_clinic, $default_role_name)
    {
        $sql = "UPDATE bumaa_clients SET default_role='{$default_role}', default_clinic ='{$default_clinic}' ,user_title ='{$default_role_name}' WHERE id='{$id}'";
        $result = $this->db->query($sql);
        if (!($this->db->query($sql))) {
            return array('msg' => $this->db->error, 'status' => false);
        } else {
            return array('msg' => $msg, 'status' => true);
        }
    }


    public function updateuser($Id, $photo, $fullName, $userName, $user_title, $phone, $email)
    {
        $sql = "SELECT * FROM bumaa_clients WHERE id = '{$Id}' AND username = '{$userName}'";
        $query = $this->db->query($sql) or die($this->db->error);
        $num_rows = $query->num_rows;
        if (!$num_rows) {
            $msg = "You can not perform this action #";
            return array('msg' => $msg, 'status' => false);
        } else {
            if (!empty($photo)) {
                $new_photo = trim(htmlentities($photo));
                $news_photo = $this->escapeString($new_photo);
                $sql2 = "UPDATE bumaa_clients SET photo = '{$news_photo}', fullname = '{$fullName}',user_title = '{$user_title}', phonenumber = '{$phone}',email = '{$email}' WHERE id = '{$Id}'";
                if (!($this->db->query($sql2))) {
                    return die($this->db->error);
                } else {
                    $msg = "You have successfully updated user details # {$userName}";
                    return array('msg' => $msg, 'status' => true);
                }
            } else {
                $sql2 = "UPDATE bumaa_clients SET  fullname = '{$fullName}',user_title = '{$user_title}', phonenumber = '{$phone}',email = '{$email}' WHERE id = '{$Id}'";

                if (!($this->db->query($sql2))) {
                    return die($this->db->error);
                } else {
                    $msg = "You have successfully updated user details # {$userName}";
                    return array('msg' => $msg, 'status' => true);
                }
            }
        }
    }



    public function getAllUsers() //$clinicId
    {
        $sql = "SELECT * FROM bumaa_clients ";
        $result = $this->db->query($sql);
         $option = "";
        $query = $this->db->query($sql);
        while ($row = $query->fetch_array()) {
            $name = $row['fname']." ".$row['lname'];
            $idx = $row['id'];

            $option .= "<option value='{$idx}'";if ($id == $idx) {$option .= 'selected';}$option .= ">{$name}</option>";
        }

        return @$option;
    }


    //others who did the outreach

     public function get_others($array)
    {
      $real_values = explode(',', $array);
      $data = [];
      foreach ($real_values as $key => $value) {
        $data[] = $this->get_usernames($value);
      }
      return json_encode($data);
    }

    //get usernames

    public function get_usernames($user_id)
    {
     $sql = "SELECT * FROM bumaa_clients WHERE id = '{$user_id}' LIMIT 1";        
        $result = $this->db->query($sql);        
        $row = $result->fetch_array();
        return @$row['fname']." ".$row['lname'];
    }


    public function get_user_roles($username)
    {
        $sql = "SELECT * FROM logins_roles WHERE username = '{$username}'";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

}
