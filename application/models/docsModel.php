<?php
class docsModel extends Model
{
    public function upload_doc($title,$description,$sharedWith,$files,$owner)
    {
        $check = "SELECT id FROM documents WHERE title='{$title}'";
        $result = $this->db->query($check);
        if($result->num_rows === 1)
        {
            return '<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">&times</a><strong>ERROR : </strong> A document with a similiar title already exists !</div>';
        }
        else
        {
            $date = date('l jS, F Y (h:i:s A)');
            if(preg_match('/ /',$title)){$slug = strtolower(str_replace(" ","-",$title));}else{$slug = strtolower($title);}
            $sql = "INSERT INTO documents(title,slug,description,sharedWith,files,owner,date_added)VALUES('{$title}','{$slug}','{$description}','{$sharedWith}','{$files}','{$owner}','{$date}')";
            if(!$this->db->query($sql))
            {
                return "<div class='alert alert-block alert-danger'><a href='#' data-dismiss='alert' class='close'>&times</a>".$this->db->error."</div>";
            }
            else
            {
                return "<div class='alert alert-block alert-success'><a href='#' data-dismiss='alert' class='close'>&times</a> <strong>SUCCESS : </strong> Upload Ok</div>";
            }
        }
    }
    public function getAlldocs()
    {
        $sql = "SELECT * FROM documents";
        $result = $this->db->query($sql);
        if($result->num_rows)
        {
            $images = array('jpg','gif','png','bmp','JPG');
            $count = 0;
            while($row = $result->fetch_array())
            {

                $count++;
               
                $title = $row['title'];

                $slug = $row['slug'];

                $date_added = $row['date_added'];

                $filesArray = $row['files'];

                $filename = $filesArray;

                $filetype = substr(strrchr($filesArray,"."),1);

                if(strlen($title) >= 20)
                {
                        
                        $displayTitle = $title;
                }else{

                    $displayTitle = $title;

                }

                if(in_array($filetype,$images))
                {

                    @$files .= "<tr>
                                <td>{$count}</td>                                  
                                <td>{$displayTitle}</td>
                                <td> {$date_added}</td>
                                <td><a href='".URL."repository/view/{$slug}/' class='btn btn-success'><i class='fa fa-eye text-success'></i> View</a></td>
                              </tr>";
                }
                else
                {
                    @$files .= "<tr>
                                <td>{$count}</td>
                                <td>  {$displayTitle} </td>
                                <td>{$date_added}</td>
                                <td><a href='".URL."repository/view/{$slug}/' class='btn btn-info'><i class='fa fa-eye text-info'></i> View</a></td></td>
                        </tr>";
                }
                
            }

            return "<table  class='table table-hover  table-striped table-bordered'id='datatable1'>

            <thead>
            <th>#</th><th>UPLOADED DOCUMENTS</th><th>Date</th><th>View</th>
            </thead>
            <tbody>{$files}</tbody>
            </table>";

        }

        else
        {
            
        }
    }


    //viewing Document

    public function view($document_slug)
    {
        $sql = "SELECT * FROM documents WHERE slug='{$document_slug}'";
        $result = $this->db->query($sql);
        if($result->num_rows)
        {
            while($row = $result->fetch_array())
            {
                return $row;
            }
        }
        else
        {
            
        }
    }
    public function delete($slug)
    {
        $SQL = "DELETE FROM documents WHERE slug='{$slug}'";
        $result = $this->db->query($SQL);
        if($result)
        {
            return true;
        }
        else
        {
            return false;
        }
        
    }
}
?>