<?php

class validationModel extends model {
         public function contact($fullname,$phone,$subject,$email,$message){
         $XSQL = "INSERT INTO contact(fullname,phone,subject,email,message)VALUES('{$fullname}','{$phone}','{$subject}','{$email}','{$message}')";
            if($this->db->query($XSQL))
            {
                $msg = "Your response has been recorded, we shall be back to you soon";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = $this->db->error;
                return array('msg'=>$msg, 'status'=> false);
            }

    }


    //alumni register


      public function registerA($ref_no,$student_number,$transcript_serial,$email,$username,$password,$cpassword,$clinic,$firstname,$lastname,$sex,$knowabout,$phone,$maritalStatus,$course,$from_year,$upto_year ){

          if (strcmp($password, $cpassword) !== 0)
               
            {
                    $msg = " Passwords used dont Match please!!";
                    return array('msg'=>$msg, 'status'=> false); 
            }
            else
            {
                $student_number = explode('-', $student_number);
                $student_number = implode('/', $student_number);
                $student_number = strtoupper($student_number);
                $transcript_serial   = strtoupper($transcript_serial);


           $hashedPassword = password_hash($password,PASSWORD_DEFAULT);

            $XSQL = "INSERT INTO bumaa_clients(client_refnumber,fname,lname,username,email,sex,password,cpassword,maritalstatus,knowabout,contactphone,transcript_no,student_no,clinic_id,notify,course,from_year,upto_year)VALUES('{$ref_no}','{$firstname}','{$lastname}','{$username}','{$email}','{$sex}','{$hashedPassword}','{$password}','{$maritalStatus}','{$knowabout}','{$phone}','{$transcript_serial}','{$student_number}','{$clinic}',1,'{$course}','{$from_year}','{$upto_year}')";
            
           
            if($this->db->query($XSQL))
            {

                $msg = "<p class='text-success'>Dear {$username},You have successfully registered with BUMAA,Please kindly wait for an email to complete registration</p>";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = $this->db->error;
                return array('msg'=>$msg, 'status'=> false);
            }

    }

}


  public function alumni_update($user_id,$student_number,$transcript_serial,$email,$firstname,$lastname,$clinic,$sex,$knowabout,$phone,$course,$from_year,$upto_year){

     $SQL2 = "UPDATE bumaa_clients SET fname = '$firstname', lname = '$lastname',email = '$email',sex = '$sex',transcript_no = '$transcript_no',student_no = '$student_number',contactphone = '$phone',clinic_id = '$clinic',transcript_no = '$student_number',upto_year = '$upto_year',from_year = '$from_year',course ='$course',knowabout ='$knowabout' WHERE id = '$user_id'";
       
        if($this->db->query($SQL2))
        {
            $msg = "Dear {$firstname}  {$lastname},You have successfully updated information.";
            return array('msg'=>$msg, 'status'=> true);
        }else{
            $msg = $this->db->error;
            return array('msg'=>$msg, 'status'=> false);
        }   

}

//company Register



      public function registerC($ref_no,$companyEmail,$username,$password,$cpassword,$about_company,$companyLocation,$companyName){

          if (strcmp($password, $cpassword) !== 0)
               
            {
                $msg = " Passwords used dont Match please!!";
                return array('msg'=>$msg, 'status'=> false); 
            }
            else
            {

           $hashedPassword = password_hash($password,PASSWORD_DEFAULT);
           $XSQL = "INSERT INTO bumaa_clients(client_refnumber,username,email,password,cpassword,companyName,companyLocation,companyAbout,notify,comp)VALUES('{$ref_no}','{$username}','{$companyEmail}','{$hashedPassword}','{$password}','{$companyName}','{$companyLocation}','{$about_company}',1,1)";
           
            if($this->db->query($XSQL))
            {
                $msg = " {$companyName} You have successfully registered with BUMAA,Please kindly wait for an email to complete registration";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = $this->db->error;
                return array('msg'=>$msg, 'status'=> false);
            }

    }

}




 //checking field existence

 public  function checking_any_field_existance($field,$id,$table,$column){

      if (isset($field)){

            $sql = "SELECT $id FROM $table WHERE $column = '".$field."' ";              
            $result = $this->db->query($sql);
            if (mysqli_num_rows($result) > 0)
             {
                $msg = "<small>{$field} Already Existing</small>";
                return array('msg'=>$msg, 'status'=> false);
            }
            else{
                $msg = "<small class='h6 text-success'>{$column} is Approved !!</small> ";
                 return array('msg'=>$msg, 'status'=> true);               
            }
    }
}

public function email_validation($email,$id,$table){
              if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                        $msg = "Please Enter a valid Email Address ";
                         return array('msg'=>$msg, 'status'=> false);
                     }else { 
                     $sql = "SELECT $id FROM $table WHERE email = '".$email."' ";
                    $result = $this->db->query($sql);
                    if (mysqli_num_rows($result) > 0)
                     {
                         $msg = `{$email} has been already taken`;
                         return array('msg'=>$msg, 'status'=> false);
                    }
                    else{
                        $msg = `{$email} has been Approved`;
                         return array('msg'=>$msg, 'status'=> true);
                    }
            } 
       }
  

public function regnum_check($num){
     $string = $num;
          $string = addslashes($string);
              $string = strtoupper($string);
                  $check = preg_match("/^[A-Z]{2}-[A-Z]{2}-[0-9]{4}-[0-9]{4}$/", $string);
                      $right = explode('-', $string);
                      $left = implode('/', $right);
              if($check){
                    $msg = "That is a correct Number ";
                    return array('msg'=>$msg, 'status'=> true);
              }else{
                 $msg = "That is a wrong Registration Number ";
                 return array('msg'=>$msg, 'status'=> false);        
      }

}

  public function subscribe($email){        
        $query = "INSERT INTO newsletter(email) VALUES('$email')";
        $result1 = $this->db->query($query);
        if( $result1)
            {
                $msg = "Successfully subscribed ";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = "Failed to Subscribe";
                return array('msg'=>$msg, 'status'=> false);
            }
    }

    //end of methods


}