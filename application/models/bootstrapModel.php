<?php
class bootstrapModel extends Model
{
    public function loadSettings()
    {
        $sql = "SELECT * FROM settings LIMIT 1";
        $query = $this->db->query($sql);
        while($row = $query->fetch_assoc())
        {
            $enableSMS = $row['sms_notification'];
            $enableEmail = $row['email_notification'];
        }

        $settings = array('smsNotifications'=>$enableSMS,'emailNotifications'=>$enableEmail);

        return $settings;
    }
}