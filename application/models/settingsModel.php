<?php

class settingsModel extends model {

     public function get_settings()
    {
        $sql = "SELECT * FROM system_settings";
        $query = $this->db->query($sql);
        $data = [];
        $row = $query->fetch_object();
        $data[] = $row;      
        return @$data;
    }


  public function get_contacts()
    {
      $sql = "SELECT * FROM contact ";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


    public function get_contact_person($id){
        $sql = "SELECT * FROM contact WHERE id = '$id'";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_array()) {
            $data[] = $row;
        }
        return @$data;
    }

    public function get_newsletter()
    {
        $sql = "SELECT * FROM newsletter ";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


    public function get_newsletter_person($id)
    {
        $sql = "SELECT * FROM newsletter WHERE id = '$id'";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

    public function sent_newsletter($message,$sender,$receiver){
        $sql = "INSERT INTO messages (sender,receiver,message)  VALUES ('$sender','$receiver','$message')";
        $result = $this->db->query($sql);
         if($result)
            {
                $msg = "Successfully Sent message to # {$receiver}";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = "Failed to Update Event ";
                return array('msg'=>$msg, 'status'=> false);
         }     
    }


public function interval_of_anything($table){
        $sql = "SELECT COUNT(*) FROM  {$table}  WHERE created_on BETWEEN 
                                DATE_SUB(LAST_DAY(NOW()),INTERVAL DAY(LAST_DAY(NOW()))- 1 DAY) AND LAST_DAY(NOW())";
        $result = $this->db->query($sql)->fetch_array(MYSQLI_ASSOC);
        $count  = intval($result['COUNT(*)']);
         return (int)$count;
    }

}