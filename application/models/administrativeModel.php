<?php
class administrativeModel extends model
{

    public function getCategories(){
        $sql = "SELECT * FROM user_categories";
        $query = $this->db->query($sql);
        $data = [];
        while ($row = $query->fetch_object()) {

        @$data[] = $row;
        }
        return @$data;
     }


     public function getClinics(){
        $sql = "SELECT * FROM clinics";
        $query = $this->db->query($sql);
        $data = [];
        while ($row = $query->fetch_object()) {

        @$data[] = $row;

        }
        return @$data;
     }





    public function getCourses(){
        $sql = "SELECT * FROM courses";
        $query = $this->db->query($sql);
        $data = [];
        while ($row = $query->fetch_object()) {
        @$data[] = $row;
        }
        return @$data;
     }

    public function get_members_per_clinic($clinic_id)
     {
         $sql = "SELECT COUNT(*) FROM bumaa_clients WHERE clinic_id = '{$clinic_id}'";
        $query = $this->db->query($sql)or die($this->db->error);
        while ($row = $query->fetch_array()) {
         $num = intval($row['COUNT(*)']);
        }
        return $num;        
     }
     //end

       public function num_per_clinic($clinic_id)
     {
         $sql = "SELECT * FROM bumaa_clients WHERE clinic_id = '{$clinic_id}'";
         $query = $this->db->query($sql)or die($this->db->error);       
        $num = $query->num_rows;   
        return $num;     
     }
     //end


     public function DataPointsForMembers(){
        $clinics = $this->getClinics();
        $data = '[';
        $x = 0;
        foreach ($clinics as $key => $clinic) {
            $x = $x+10;            
            $y = $this->get_members_per_clinic($clinic->id);
            $n = $clinic->clinic_name;
            $data .= '{ x: '.$x.', y: '.$y.', label:"'.$n.'" }'.',';           
        }
         $data .=']';

         return $data;

      
        }
       


    public function get_male_gender_per_clinic($clinic_id)
     {
        $sql = "SELECT COUNT(*) FROM bumaa_clients WHERE clinic_id = '{$clinic_id}' AND sex='Male' ";
        $query = $this->db->query($sql)or die($this->db->error);
        $num = "";
        if($query->num_rows > 0){
            while ($row = $query->fetch_array()) {
         $num = intval($row['COUNT(*)']);
         }

        }else{
            $num = 0;    
        }

        return $num;        
     }

    public function get_female_gender_per_clinic($clinic_id)
     {
         $sql = "SELECT COUNT(*) FROM bumaa_clients WHERE clinic_id = '{$clinic_id}' AND sex='Female' ";
        $query = $this->db->query($sql)or die($this->db->error);
        $num = "";

        if($query->num_rows > 0){

        while ($row = $query->fetch_array()) {
         $num = intval($row['COUNT(*)']);
        }


        }else{
            $num = 0;
        }
       
        return $num;        
     }

    

    public function getClinicMembers(){
        $sql = "SELECT * FROM clinics";
        $query = $this->db->query($sql);
        $data = [];
        while ($row = $query->fetch_object()) {
         @$data[] = $row;
        }
        return @$data;       
    }


    public function eachClinic(){
        $clinics = $this->getClinicMembers();
        foreach ($clinics as $key => $clinic) {
            $clinic_id = $clinic['id'];           
        }
    }


     public function get_clinic($clinic_id)
     {
        $sql = "SELECT * FROM clinics WHERE id = '{$clinic_id}'";
        $query = $this->db->query($sql);
        while ($row = $query->fetch_array()){

            $id = $row['id'];
            $clinicName = $row['clinic_name'];
            $clinicCode = $row['clinic_code'];
            $cliniColor = $row['clinic_color'];   
            $clinicPhone = $row['clinicPhone']; 
        } 
         $clinicD = array("id"=>$id,"clinic_name"=>$clinicName,"clinic_code"=>$clinicCode,"clinic_color"=>$cliniColor,"clinicPhone"=>$clinicPhone);
         return $clinicD;
     }

      public function get_clinic_name_today($clinic_id)
     {
        $sql = "SELECT * FROM clinics WHERE id = '{$clinic_id}'";
        $query = $this->db->query($sql);
        while ($row = $query->fetch_array()){
            $clinicName = $row['clinic_name'];
        }          
         return $clinicName;
     }


    public function get_username_today($id)
     {
        $sql = "SELECT * FROM bumaa_clients WHERE id = '{$id}'";
        $query = $this->db->query($sql);
        $name = "";
        while ($row = $query->fetch_array()){           
            $name = $row['fname']." ".$row['lname'];           
        }          
         return $name;
     }



     public function get_course_name_today($id)
     {
        $sql = "SELECT * FROM courses WHERE id = '{$id}'";
        $query = $this->db->query($sql);
        $name = "";
        while ($row = $query->fetch_array()){           
            $name = $row['course_name'];           
        }          
         return $name;
     }


     public function clinic_drop()
     {
        $sql = "SELECT * FROM clinics ";
        $query = $this->db->query($sql);
        $clinic_drops = '';
        while ($row = $query->fetch_array()){
            $id = $row['id'];
            $clinicName = $row['clinic_name'];

            $clinic_drops .= "<option value='{$id}'>{$clinicName}</option>";
        } 
        return $clinic_drops;
     }


      public function clinic_drop_two()
     {
        $sql = "SELECT * FROM clinics ";
        $query = $this->db->query($sql);
        $clinic_drops = '';
        while ($row = $query->fetch_array()){
            $id = $row['id'];
            $clinicName = $row['clinic_name'];

            $clinic_drops .= "<option value='{$clinicName}'>{$clinicName}</option>";
        } 
        return $clinic_drops;
     }


 
     public function get_del_clinic($clinic_id)
     {
         $sql = "SELECT * FROM clinics WHERE id = '{$clinic_id}' LIMIT 1";
         $result = $this->db->query($sql);
         $data = [];
         while ($row = $result->fetch_object()) {
             $data[] = $row;
         }
         return @$data;
     }
     
     public function clinic_up($clinicName,$cliniCode,$clinColor,$clinicPhone)
     {
        $clinicName = strtoupper($clinicName);
        $sql = "SELECT * FROM clinics WHERE clinic_code = '$cliniCode'";
        $query = $this->db->query($sql);
        $num_rows = $query->num_rows;
        if(!$num_rows)
        {
            return "<div class='alert alert-block alert-danger'>You Cant Edit The Clinic Code</div> ";
        }
        else
        {
            
            while($row = $query->fetch_array())
            {
                $id = $row['id'];
            }

         $sql2 = "UPDATE clinics SET clinic_name = '{$clinicName}', clinic_code = '{$cliniCode}', clinic_color = '{$clinColor}', clinicPhone = '{$clinicPhone}' WHERE id = '{$id}'";

         if(!($this->db->query($sql2)))
         {
             return '<div class="alert alert-danger alert-dismissible" role="alert"><a href="#" data-dismiss="alert" class="close">&times</a><strong>ERROR : </strong>'.$this->db->error.'</div>';
         }
         else
         {
             return '<div class="alert alert-success alert-dismissible fade show" role="alert"><a href="#" data-dismiss="alert" class="close">&times</a><strong>SUCCESS : </strong> Clinic Successfully Updated !</div>';
         }
        }
    }


    //delete clinic

     public function del_clinic($id)
     {
         $sql = "DELETE FROM clinics WHERE id='{$id}'";
         $query = $this->db->query($sql)or die($this->db->error);

         if(!($this->db->query($sql)))
         {
             return '<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">&times</a><strong>ERROR : </strong>'.$this->db->error.'</div>';
         }
         else
         {
             return true;
         }
     }
     


     //add clinic

     public function add_clinic($clinicName,$cliniColor,$clinicPhone)
     {
         $cliniCode = strtoupper(substr($clinicName,0,3));
         $clinicName = strtoupper($clinicName);

         $sql = "SELECT * FROM clinics WHERE clinic_name = '{$clinicName}'";
         $query = $this->db->query($sql)or die($this->db->error);
         $num_rows = $query->num_rows;
         if($num_rows)
         {
            $msg = "You already have # {$clinicName} clinic registered. sorry!";
            return array('msg'=>$msg, 'status'=> false);
         }
         else
         {
            $sql2 = "INSERT INTO clinics (clinic_name, clinic_code, clinic_color,clinicPhone) VALUES
           ('{$clinicName}', '{$cliniCode}', '{$cliniColor}','{$clinicPhone}')";
            
            if(!($this->db->query($sql2)))
            {
                die($this->db->error);
            }else
            {
                $msg = "Successfully added clinic # {$clinicName}";
                return array('msg'=>$msg, 'status'=> true);            
            }
         }


     }


     public function get_cat($clinic_id)
     {
        
            $sql = "SELECT * FROM user_categories WHERE id = '{$clinic_id}'";
            $query = $this->db->query($sql);
            while ($row = $query->fetch_array()){
    
                $id = $row['id'];
                $catName = $row['category_name'];
  
            } 
             $categoryD = array("id"=>$id,"category_name"=>$catName);
             return $categoryD;
           
     }


     //get User cat to update
     public function get_del_ucat($clinic_id)
     {
         $sql = "SELECT * FROM user_categories WHERE id = '{$clinic_id}' LIMIT 1";
         $result = $this->db->query($sql);
         $data = [];
         while ($row = $result->fetch_object()) {
             $data[] = $row;
         }
         return @$data;
     }



     //upddate cat
     public function ucat_up($catId,$catName)
     {
        $sql = "SELECT * FROM user_categories WHERE id = '$catId'";
        $query = $this->db->query($sql);
        $num_rows = $query->num_rows;
        if(!$num_rows)
        {
            return "<div class='alert alert-block alert-danger'>You Cant Edit The Clinic Code</div> ";
        }
        else
        {
            
            while($row = $query->fetch_array())
            {
                $id = $row['id'];
            }

         $sql2 = "UPDATE user_categories SET category_name = '{$catName}' WHERE id = '{$id}'";

         if(!($this->db->query($sql2)))
         {
             return '<div class="alert alert-danger alert-dismissible" role="alert"><a href="#" data-dismiss="alert" class="close">&times</a><strong>ERROR : </strong>'.$this->db->error.'</div>';
         }
         else
         {
             return '<div class="alert alert-success alert-dismissible fade show" role="alert"><a href="#" data-dismiss="alert" class="close">&times</a><strong>SUCCESS : </strong> Clinic Successfully Updated !</div>';
         }
        }
     }


     //delete User cat

     public function del_ucat($id)
     {
         $sql = "DELETE FROM user_categories WHERE id='{$id}'";
         $query = $this->db->query($sql)or die($this->db->error);

         if(!($this->db->query($sql)))
         {
             return '<div class="alert alert-danger" role="alert"><a href="#" data-dismiss="alert" class="close">&times</a><strong>ERROR : </strong>'.$this->db->error.'</div>';
         }
         else
         {
             return true;
         }
     }


     public function add_cat($catName)
     { 
         $sql = "SELECT * FROM user_categories WHERE category_name = '{$catName}'";
         $query = $this->db->query($sql)or die($this->db->error);
         $num_rows = $query->num_rows;
         if($num_rows)
         {
            $msg = "user category # {$catName} already exists";
            return array('msg'=>$msg, 'status'=> false);
         }
         else
         {
            $catName = ucfirst($catName);
            $sql2 = "INSERT INTO user_categories (category_name) VALUES
           ('{$catName}')";
            
            if(!($this->db->query($sql2)))
            {
                die($this->db->error);
            }else
            {
                $msg = "Successfully created user catetgory # {$catName}";
                return array('msg'=>$msg, 'status'=> true);
            }
         }


     }

    

public function fetchRecentSwitch($myclinic)
    {
        $SQL = "SELECT * FROM accesSwitch WHERE clinic_id = '{$myclinic}' ORDER BY activity_time DESC";
        $QRY = $this->db->query($SQL)or die($this->db->error);
        $tr = ""; $count=0;
        while($row = $QRY->fetch_array())
        {
            $user = $row['user_id'];
            $clinic = $row['clinic_id'];
            $clinic2 = $row['cur_clinic'];
            $activity = $row['activity'];
            $dayTime = $row['activity_time'];
            $type = $row['log_type'];
            $uRole = $row['role_id'];
            $uRole2 = $row['cur_role'];
            //$count++;
            $SQL2 = "SELECT * FROM bumaa_clients WHERE id ='{$user}'";
            $QRY2 = $this->db->query($SQL2)or die($this->db->error);
            while($row2 = $QRY2->fetch_array())
            {
                $uName = $row2['username'];
                @$fname = $row2['fname'].' '.$row2['lname'];
            }
            $SQL3 = "SELECT * FROM clinics WHERE id ='{$clinic}' AND id ='{$clinic}'";
            $QRY3 = $this->db->query($SQL3)or die($this->db->error);
            while($row3 = $QRY3->fetch_array())
            {
                @$cName = $row3['clinic_name'];
            }

            $SQL4 = "SELECT * FROM user_categories WHERE id ='{$uRole}'";
            $QRY4 = $this->db->query($SQL4)or die($this->db->error);
            while($row4 = $QRY4->fetch_array())
            {
                $rName = $row4['category_name'];
            }
                $tr .="<tr>
                    <td>{$fname}</td>
                    <td>{$uName}</td>
                    <td>{$activity} <strong>{$cName} As {$rName}</strong> From <strong>{$clinic2} AS {$uRole2}</strong></td>
                    <td>{$dayTime}</td>
                </tr>";
        }
        return $tr;
    }






}