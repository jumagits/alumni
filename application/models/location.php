
<?php 

class location extends model 
{

     public function get_countries()
    {
        $sql = "SELECT * FROM countries  ORDER BY country_name ASC";
        $qry = $this->db->query($sql);
        @$districtOptions .= "<option value='' disabled selected>Select a Nationality</option>";
        while($row = $qry->fetch_array())
        {
        
            $id = $row['id'];
            $country_name = $row['country_name'];
    
            @$districtOptions .= "<option value='{$id}'>{$country_name}</option>";
        }
        
        return $districtOptions;
    }
    public function get_districts()
    {
        $sql = "SELECT * FROM ug_districts  ORDER BY districtName ASC";
        $qry = $this->db->query($sql);
        @$districtOptions .= "<option value='' disabled selected>Select a District</option>";
        while($row = $qry->fetch_array())
        {
        
            $cid = $row['dID'];
            $districtName = $row['districtName'];
    
            @$districtOptions .= "<option value='{$cid}'>{$districtName}</option>";
        }
        
        return $districtOptions;
    }

    public function getDistrict($id)
    {
        $sql = "SELECT * FROM ug_districts WHERE  dID = '{$id}'";
        $query = $this->db->query($sql);
        $data = array();
        while($row = $query->fetch_object())
        {
            @$data[] = $row;
        }
        
        return $data;
    }

    public function getCounty($cid)
    {
        $sql = "SELECT * FROM ug_counties WHERE  cID = '{$cid}'";
        $query = $this->db->query($sql);
        $data = array();
        while($row = $query->fetch_object())
        {
            @$data[] = $row;
        }
        
        return $data;
    }

    public function getSubCounties($subID)
    {
        $sql = "SELECT * FROM ug_subcounties WHERE  subID = '{$subID}'";
        $query = $this->db->query($sql);
        $data = array();
        while($row = $query->fetch_object())
        {
            @$data[] = $row;
        }
        
        return $data;
    }

    public function getParish($parID)
    {
        $sql = "SELECT * FROM ug_parishes WHERE  parID = '{$parID}'";
        $query = $this->db->query($sql);
        $data = array();
        while($row = $query->fetch_object())
        {
            @$data[] = $row;
        }
        
        return $data;
    }

    public function getVillages($vID)
    {
        $sql = "SELECT * FROM ug_villages WHERE  vID = '{$vID}'";
        $query = $this->db->query($sql);
        $data = array();
        while($row = $query->fetch_object())
        {
            @$data[] = $row;
        }
        
        return $data;
    }

    public function location_json($id ,$type)
    {
        if($type=="counties")
        {
            $sql = "SELECT * FROM ug_counties WHERE dID='{$id}' ORDER BY countyName ASC";
            $qry = $this->db->query($sql);
            @$countyOptions .= "<option value='' disabled selected>Select a County</option>";
            while($row = $qry->fetch_array())
            {
            
                $cid = $row['cID'];
                $countyName = $row['countyName'];
        
                @$countyOptions .= "<option value='{$cid}'>{$countyName}</option>";
            }
            
            return  $countyOptions;
        }
        if($type =="subcounties")
        {
            $sql = "SELECT * FROM ug_subcounties WHERE cID='{$id}' ORDER BY subCountyName ASC";
            $qry = $this->db->query($sql);
            @$subCountyOptions .= "<option value='' disabled selected>Select a Sub County</option>";
            while($row = $qry->fetch_array())
            {
            
                $cid = $row['subID'];
                $subCountyName = $row['subCountyName'];
        
                @$subCountyOptions .= "<option value='{$cid}'>{$subCountyName}</option>";
            }
            return $subCountyOptions;
        }
        //
        if($type =="parishes")
        {
            $sql = "SELECT * FROM ug_parishes WHERE subID='{$id}' ORDER BY parishName ASC";
            $qry = $this->db->query($sql);
            @$Options .= "<option value='' disabled selected>Select a Parish</option>";
            while($row = $qry->fetch_array())
            {
            
                $_id = $row['parID'];
                $parishName = $row['parishName'];
                @$Options .= "<option value='{$_id}'>{$parishName}</option>";
            }
            return $Options;
        }
        //
        if($type =="villages")
        {
            $sql = "SELECT * FROM ug_villages WHERE parID='{$id}' ORDER BY villageName ASC";
            $qry = $this->db->query($sql);
            @$Options .= "<option value='' disabled selected>Select a Village</option>";
            while($row = $qry->fetch_array())
            {
            
                $_id = $row['vID'];
                $villageName = $row['villageName'];
        
                @$Options .= "<option value='{$_id}'>{$villageName}</option>";
            }
            return $Options;
        }
    }


    //names of locations

    


    //end of methods
}