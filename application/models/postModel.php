<?php

class postModel extends model {
    

    public function post_options()
    {
        $count=0;
        $sql = "SELECT * FROM posts";
        $query = $this->db->query($sql);
        $users = "";
        $option = "";
        while($row = $query->fetch_array())
        {
            $count++;

            $id = $row['post_id'];
            $title = $row['post_title'];
            
            $option .= "<option value='".$id."'>".$title."</option>";


        } return $option;
    }
    



    public function all_categories()
    {
        $sql = "SELECT * FROM categories ";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

    public function get_post_by_id($id)
    {
        $sql = "SELECT * FROM posts WHERE post_id = '{$id}'";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

    public function recent_posts()
    {
        $sql = "SELECT * FROM posts AS p INNER JOIN categories AS c ON   p.post_category_id = c.cat_id ORDER BY p.post_id DESC LIMIT 3";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

     public function all_posts()
    {
        $sql = "SELECT * FROM posts AS p INNER JOIN categories AS c ON   p.post_category_id = c.cat_id ORDER BY p.post_id DESC";

        //$sql = "SELECT * FROM posts ORDER BY post_id DESC";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


      public function only_three_posts()
    {
        $sql = "SELECT * FROM posts AS p INNER JOIN categories AS c ON   p.post_category_id = c.cat_id ORDER BY p.post_id DESC limit 3";

        //$sql = "SELECT * FROM posts ORDER BY post_id DESC";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

//posts per category

      public function all_posts_per_cat($id)
    {
        $sql = "SELECT * FROM posts AS p INNER JOIN categories AS c ON  p.post_category_id = c.cat_id WHERE p.post_category_id = '$id' ORDER BY p.post_id DESC";

        //$sql = "SELECT * FROM posts ORDER BY post_id DESC";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }



  //add post

     public function addPost($post_title,$post_category_id,$post_author,$post_image,$post_status,$post_content,$post_date,$post_tags)
    {       
           $query = "INSERT INTO posts (post_title,post_category_id,post_author,post_image,post_status,post_content,post_date,post_tags) VALUES('{$post_title}','{$post_category_id}','{$post_author}','{$post_image}','{$post_status}','{$post_content}','{$post_date}','{$post_tags}')";

        
   
          if($this->db->query($query))
            {
                $msg = "Successfully registered New Post # {$title}";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = $this->db->error;
                return array('msg'=>$msg, 'status'=> false);
            }
    }



//update Post


public function updatePost($id,$post_title,$post_category_id,$post_author,$post_image,$post_status,$post_content,$post_date,$post_tags){

        if (!empty($post_image)) {

             $query = "UPDATE  posts SET post_title= '$post_title',post_category_id = '$post_category_id',post_author ='$post_author',post_image = '$post_image',post_status = '$post_status',post_content = '$post_content',post_date = '$post_date',post_tags ='$post_tags' WHERE post_id = '$id' ";

             if($this->db->query($query))
            {
                $msg = "Successfully Updated Post # {$post_title}";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = $this->db->error;
                return array('msg'=>$msg, 'status'=> false);
            }


          
        }else{

              $query = "UPDATE  posts SET post_title= '$post_title',post_category_id = '$post_category_id',post_author ='$post_author',post_status = '$post_status',post_content = '$post_content',post_date = '$post_date',post_tags ='$post_tags' WHERE post_id = '$id' ";

              if($this->db->query($query))
            {
               $msg = "Successfully Updated Post # {$post_title}";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = $this->db->error;
                return array('msg'=>$msg, 'status'=> false);
            }
        }




    }


//add comment
    public function addComment($post_id,$comment_email,$comment_content,$comment_date,$comment_author,$comment_status){


        
        $query = "INSERT INTO comments(comment_post_id,comment_email,comment_content,comment_date,comment_author,comment_status)";

        $query .= " VALUES ($post_id,'$comment_email','$comment_content','$comment_date','$comment_author','$comment_status')";
        $result = $this->db->query($query);

        $query1 = "UPDATE posts SET post_comment_count = post_comment_count + 1 WHERE post_id = {$post_id}";
        $result1 = $this->db->query($query1);

        if($result AND $result1)
            {
                $msg = "Successfully commented ";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = "Failed to comment";
                return array('msg'=>$msg, 'status'=> false);
            }


    }


    
 public function all_subscribers()
      {
        $sql = "SELECT email,subscribed_at FROM  newsletter";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

//comments

      public function all_comments()
    {
        $sql = "SELECT * FROM comments AS c INNER JOIN posts AS p ON   c.comment_post_id = p.post_id ORDER BY c.comment_post_id DESC";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

//per post
      public function all_comments_per_post($id)
    {
        $sql = "SELECT * FROM comments AS c INNER JOIN posts AS p ON   c.comment_post_id = p.post_id WHERE p.post_id = '$id' AND c.comment_status = 'Approved' ORDER BY c.comment_post_id DESC";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


    //get comment


      public function get_unapproved_comment($id)
   {
       $sql = "SELECT * FROM comments WHERE comment_id = '{$id}' LIMIT 1";
       $result = $this->db->query($sql);
       $data = [];
       while ($row = $result->fetch_object()) {
           $data[] = $row;
       }
       return @$data;
   }





  //approve
    public function approveComment($id){


     $query = "UPDATE comments SET comment_status = 'Approved' WHERE comment_id = '$id' ";       
        $result1 = $this->db->query($query);

        if( $result1)
            {
                $msg = "Successfully Approved ";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = "Failed to Approve";
                return array('msg'=>$msg, 'status'=> false);
            }
    }


    //unapprove


     public function unapproveComment($id){


     $query = "UPDATE comments SET comment_status = 'Unapproved' WHERE comment_id = '$id' ";       
        $result1 = $this->db->query($query);

        if( $result1)
            {
                $msg = "Successfully Unapproved ";
                return array('msg'=>$msg, 'status'=> true);
            }else{
                $msg = "Failed to Approve";
                return array('msg'=>$msg, 'status'=> false);
            }
    }


    //search


     public function search_post($post_title)
    {
       
      
        $sql = "SELECT * FROM posts WHERE post_title LIKE '%$post_title%'  ";
        $query = $this->db->query($sql);
        @$tr = "";
        if($query->num_rows > 0){
            while ($row = $query->fetch_array())
            {
                $post_title = $row['post_title'];
                $post_date = date_create($row['post_date']);
                $date = date_format($regnof, ' l jS F Y'); 
                $tr .= "<p>".$post_title."" .$date."</p>";
            }

        }else{


             $tr = "<li>No such Post ...</li>";


          
        }
        return @$tr;
    }


    //count

    public function count_all_posts()
    {
        $sql = "SELECT * FROM posts";
        $result = $this->db->query($sql);
        return $result->num_rows;
    }

    public function count_all_categories()
    {
        $sql = "SELECT * FROM categories";
        $result = $this->db->query($sql);
        return $result->num_rows;
    }


  public function all_posts_limited($limit)
    {
        $sql = "SELECT * FROM posts AS p INNER JOIN categories AS c ON   p.post_category_id = c.cat_id ORDER BY p.post_id DESC LIMIT ".$limit;

        //$sql = "SELECT * FROM posts ORDER BY post_id DESC";
        $result = $this->db->query($sql);        
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


    //comments count

        public function comments_count()
    {
        $sql = "SELECT * FROM comments";
        $result = $this->db->query($sql);
        return $result->num_rows;
    }

//end of methods

}