<?php

class reportsModel extends Model
{
    public $campuses = ['ARAPAI','BUSITEMA','NAGONGERA','ARAPAI','SOROTI','MBALE','NAMASAGALI'];


     public function clinicLabels(){

        $sql = "SELECT * FROM clinics";
        $query = $this->db->query($sql);
        $data = [];
        while ($row = $query->fetch_array()) {

         @$data[] = substr($row['clinic_name'], 0,3);

        }

       return @$data;
       
    }

    //campus names 

     public function campusName($id){

        $sql = "SELECT * FROM clinics WHERE id = '{$id}' ";
        $query = $this->db->query($sql);       
        $row = $query->fetch_array();
        @$data = substr($row['clinic_name'], 0,3);
        return @$data;
       
    }



     public function clinicIDs(){

        $sql = "SELECT * FROM clinics";
        $query = $this->db->query($sql);
        $data = [];
        while ($row = $query->fetch_array()) {

         @$data[] = $row['id'];

        }

       return @$data;
       
    }

    public function labels()
    {
        $campuses = $this->clinicLabels();

        $labels = array();

        foreach ($campuses as $key => $campus) {

            $labels[] = $campus;
        }

        return json_encode($labels);

    }


    public function getTabularcampusData()
    {

        $campuses = $this->clinicIDs();

        $count = 0;
        foreach ($campuses as $key => $campus) {

            $SQLRecords = "SELECT * FROM client_instance WHERE clinic_id ='{$campus}' ";
            $queryRecords = $this->db->query($SQLRecords);
            $num_rows_records = $queryRecords->num_rows;
            $count++;
            $campus = $this->campusName($campus);

            @$forms .= "<tr>

                <td class='text-center'>{$campus}</td>
                <td>{$num_rows_records}</td>
             </tr>";

        }
        return $forms;

    }


    public function getFemaleData()
    {
        $campuses = $this->clinicIDs();
        $data = [];
        foreach ($campuses as $key => $campus) {

            $data[] = $this->get_female_per_clinic($campus);
           
        }
        return json_encode($data);

    }



//male


     public function getMaleData()
    {
        $campuses = $this->clinicIDs();
        $data = [];
        foreach ($campuses as $key => $campus) {

            $data[] = $this->get_male_per_clinic($campus);
           
        }
        return json_encode($data);

    }

    //colors

    public function campusColors()
    {
        $campuses = $this->clinicIDs();
        $data = [];
        foreach ($campuses as $key => $campus) {
         $sql = "SELECT * FROM clinics WHERE id = '{$campus}' ";
         $query = $this->db->query($sql);         
         $row  = $query->fetch_array(MYSQLI_ASSOC);
         @$data[] = $row['clinic_color'];
        }
        return json_encode($data);
    }



       public function get_male_per_clinic($clinic_id)
     {
         $sql = "SELECT COUNT(*) FROM bumaa_clients WHERE clinic_id = '{$clinic_id}' AND sex='Male' ";
        $query = $this->db->query($sql)or die($this->db->error);

        while ($row = $query->fetch_array()) {

         $num = intval($row['COUNT(*)']);

        }

        return $num;
        
     }


        public function get_female_per_clinic($clinic_id)
     {
         $sql = "SELECT COUNT(*) FROM bumaa_clients WHERE clinic_id = '{$clinic_id}' AND sex='Female' ";
        $query = $this->db->query($sql)or die($this->db->error);

        while ($row = $query->fetch_array()) {

         $num = intval($row['COUNT(*)']);

        }

        return $num;
        
     }



   

    public function total_members()
    {
        $sql = "SELECT * FROM client_instance  ";
        $result = $this->db->query($sql);
        $data = $result->num_rows;        
        return $data;

    }

   




//campus Data


    // end of methods

}
