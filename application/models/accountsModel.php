<?php
class accountsModel extends model
{
    public function restrict_access()
    {        
        if (!(@$_SESSION[session_name])) {
            header('location:' . URL . 'accounts/login');
        } else {
            $user = base64_decode($_SESSION[session_name]);
            $query = $this->db->query("SELECT * FROM bumaa_clients WHERE BINARY username= '{$user}'");
            while ($row = $query->fetch_array()) {
                @$user_id = $row['id'];
                @$photo = $row['photo'];
                @$fullname = $row['fname']." ".$row['lname'];
                @$userlevel = $row['userlevel'];
                @$clinic_id = $row['clinic_id'];
                @$email = $row['email'];
                @$default_clinic = $row['default_clinic'];
                @$default_role = $row['default_role'];               
                @$last_login_ip = $row['last_login_ip'];
                @$last_login_date = $row['last_login_date'];
            }

            $gravatar = $this->get_gravatar($user, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array());
            $clinic = $this->get_clinic($clinic_id);
            $default_role_name = $this->get_usercatergory($default_role);
            $default_clinic_name = $this->get_clinic($default_clinic);
            @$user_details = array(
                "gravatar" => $gravatar,
                "photo" => $photo,
                "names" => $fullname,
                "id" => $user_id,
                "email" => $email,
                "role" => $default_role,               
                "default_clinic" => $default_clinic,
                "default_role_name" => $default_role_name[0]->category_name,
                "default_clinic_name" => $default_clinic_name[0]->clinic_name,
                "clinic_id" => $clinic_id,
                "clinic_name" => $clinic[0]->clinic_name,
                "clinic_color" => $clinic[0]->clinic_color,
            );
            return @$user_details;
        }
    }

    public function doLogin($username, $password)
    {
        $sql = "SELECT * FROM bumaa_clients WHERE  username = '{$username}' AND status = '1'";
        $result = $this->db->query($sql);
        if ($result->num_rows === 1) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            if (password_verify($password, $row['password'])) {
                $username = $row['username'];
                $_SESSION[session_name] = base64_encode($username);
                return true;
            }
        } else {
            return false;
        }
    }


    public function sendAdminCode($id,$code){
     $sql = "UPDATE bumaa_clients SET adminCode ='$code', status = '1' WHERE  id = '{$id}'";
        $result = $this->db->query($sql);
        if($result){
            return true;
        }else{
        return $code;
      }
    }

    //get admin code


    public function getAdminCode($id){
     $sql = "SELECT adminCode FROM bumaa_clients WHERE  id = '{$id}' AND status = '1' LIMIT 1";
        $result = $this->db->query($sql);
         while ($row = $result->fetch_array()) {
            $code = $row['adminCode'];
        }
        return $code;
    }

//get gravator    

    public function get_gravatar($email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array())
    {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($atts as $key => $val) {
                $url .= ' ' . $key . '="' . $val . '"';
            }

            $url .= ' />';
        }
        return $url;
    }

    public function verify_and_pass_link($email, $token)
    {
        $sql = "SELECT * FROM bumaa_clients WHERE BINARY email = BINARY '$email'";
        $result = $this->db->query($sql);
        if ($result->num_rows === 1) {
            while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
                $userid = $row['id'];
                $SQL2 = "SELECT * FROM password_resets WHERE user_id='{$userid}'";
                $query = $this->db->query($SQL2);
                if ($query->num_rows === 1) {
                    $deleteKey3 = "DELETE FROM password_resets WHERE user_id='{$userid}'";
                    if (!($this->db->query($deleteKey3))) {
                        return false;
                    } else {
                        $SQL3 = "INSERT INTO password_resets(reset_key,user_id)VALUES('{$token}','{$userid}')";
                        if (!$this->db->query($SQL3)) {

                            return false;
                        } else {
                            return true;
                        }
                    }
                } else {
                    $SQL3 = "INSERT INTO password_resets(reset_key,user_id)VALUES('{$token}','{$userid}')";
                    if (!$this->db->query($SQL3)) {
                        die($this->db->error);
                    } else {
                        return true;
                    }
                }
            }
        }
    }

    public function verify_token($token)
    {
        $sql = "SELECT * FROM password_resets WHERE reset_key='{$token}'";
        $qry = $this->db->query($sql);
        if ($qry->num_rows === 1) {
            return true;
        } else {
            return false;
        }
    }

    public function resetPassword($token, $password)
    {
        $sql = "SELECT * FROM password_resets WHERE reset_key='{$token}'";
        $qry = $this->db->query($sql);
        if ($qry->num_rows === 1) {
            while ($row = $qry->fetch_array()) {
                $user_id = $row['user_id'];
                $hashedPassword = password_hash($password, PASSWORD_DEFAULT);
                $updateSQL = "UPDATE bumaa_clients SET password='{$hashedPassword}', cpassword = '{$password}' WHERE id='{$user_id}'";
                if (!$this->db->query($updateSQL)) {
                    return false;
                } else {
                    $updateSQL2 = "DELETE FROM password_resets WHERE reset_key='{$token}'";
                    if (!$this->db->query($updateSQL2)) {
                        return false;
                    } else {
                        $sql3 = "SELECT username FROM bumaa_clients WHERE id='{$user_id}'";
                        $qry3 = $this->db->query($sql3);
                        while ($row3 = $qry3->fetch_array()) {
                            $username = $row3['username'];
                        }
                        if (!(@$_SESSION[session_name])) {
                            $_SESSION[session_name] = base64_encode($username);
                        }
                        return true;
                    }
                }
            }
        } else {
            return false;
        }
    }

    public function loadAccountDetails($email)
    {
        if (isset($email)) {
            $query = $this->db->query("SELECT * FROM bumaa_clients WHERE BINARY email= BINARY '{$email}' LIMIT 1");
            $num_rows = ($query->num_rows);
            if ($num_rows > 0) {
                while ($row = $query->fetch_array()) {
                    @$AccountName = $row['fname'];
                    @$AccountPrimaryEmail = $row['email'];
                    @$AccountPrimaryMobile = $row['phonenumber'];
                }
                @$AccountDetails = array("fullname" => $AccountName, "email" => $AccountPrimaryEmail, "phonenumber" => $AccountPrimaryMobile);
                return $AccountDetails;
            } else {
                return false;
            }

        }
    }

      public function loadAccountDetailsByUsername($username)
    {
        if (isset($username)) {
            $query = $this->db->query("SELECT * FROM bumaa_clients WHERE  username =  '{$username}'");
            $num_rows = ($query->num_rows);
            if ($num_rows > 0) {
                while ($row = $query->fetch_array()) {
                    @$AccountName = $row['fname']." ".$row['lname'];
                    @$AccountPrimaryEmail = $row['email'];
                    @$AccountPrimaryMobile = $row['phonenumber'];
                }
                @$AccountDetails = array("fullname" => $AccountName, "email" => $AccountPrimaryEmail, "phonenumber" => $AccountPrimaryMobile);
                return $AccountDetails;

            } else {
                return false;
            }

        }
    }

    public function get_clinic($clinic_id)
    {
        $sql = "SELECT * FROM clinics WHERE id = '{$clinic_id}' LIMIT 1";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

    public function get_usercatergory($level_id)
    {
        $sql = "SELECT * FROM user_categories WHERE id = '{$level_id}' LIMIT 1";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }


    public function changePass($oldpassword, $password1, $password2, $userID)
    {
        $sql = "SELECT * FROM bumaa_clients WHERE id = '{$userID}'";
        $result = $this->db->query($sql) or die($this->db->error);
        if ($result->num_rows === 1) {
            $row = $result->fetch_array(MYSQLI_ASSOC);
            if (password_verify($oldpassword, $row['password'])) {
                //return true;
                if (strcmp($password1, $password2) !== 0) {
                    $msg = "New Passwords dont Match !!";
                    return array('msg' => $msg, 'status' => false);
                } else {
                    $hashedPassword = password_hash($password2, PASSWORD_DEFAULT);
                    $updateSQL = "UPDATE bumaa_clients SET password='{$hashedPassword}', cpassword = '{$password2}' WHERE id='{$userID}'";
                    if (!($this->db->query($updateSQL))) {
                        $msg = ".$this->db->error.";
                        return array('msg' => $msg, 'status' => false);
                    } else {
                        $msg = "You have successfully changed your password :-)";
                        return array('msg' => $msg, 'status' => true);
                    }
                }
            } else {
                $msg = "Invalid current Password :-)";
                return array('msg' => $msg, 'status' => false);
            }
        }
    }


    public function getAllClinics()
    {
        $count = 0;
        $sql = "SELECT * FROM clinics";
        $query = $this->db->query($sql);
        $users = "";
        $option = "<option value='#'>--Select all CLinics --</option>";
        while ($row = $query->fetch_array()) {
            $count++;
            $id = $row['id'];
            $clinic_name = $row['clinic_name'];
            $option .= "<option value='" . $id . "'>" . $clinic_name . "</option>";
        }
        return $option;
    }

    public function getAllClinicsData()
    {
        $count = 0;
        $sql = "SELECT * FROM clinics";
        $result = $this->db->query($sql);
        $data = [];
        while ($row = $result->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

    public function getAllAccessLevels()
    {
        $count = 0;
        $sql = "SELECT * FROM user_categories";
        $query = $this->db->query($sql);
        $users = "";
        $option = "<option value='#'>--Select all User Categories --</option>";
        while ($row = $query->fetch_array()) {
            $count++;
            $id = $row['id'];
            $category_name = $row['category_name'];
            $option .= "<option value='" . $id . "'>" . $category_name . "</option>";
        }
        return $option;
    }

    public function upTime($id)
    {
        if (isset($_SESSION)) {
            $sql = "UPDATE bumaa_clients SET last_activity = CURRENT_TIMESTAMP WHERE id = '{$id}' ";
            if (!($this->db->query($sql))) {
                return $this->db->error;
            } else { 
                return true;
            }
        }
    }

    public function getOnlineNumber()
    {
        $sql = "SELECT * FROM bumaa_clients  WHERE last_activity  > DATE_SUB(NOW(), INTERVAL 5 SECOND)";
        $query = $this->db->query($sql) or die($this->db->error);
        $num = $query->num_rows;
        return $num;
    }

    public function getOnline($id)
    {
        $count = 0;
        $sql = "SELECT * FROM bumaa_clients  WHERE last_activity  > DATE_SUB(NOW(), INTERVAL 5 SECOND)";
        $query = $this->db->query($sql) or die($this->db->error);
        $online = "";
        $online .= "
        <table class='table table-hover table-bordered table-striped mb-0 table-sm' >
        <tbody>
            <tr>
                <th>
                 USERS ONLINE
                </th>

            </tr>";

        while ($row = $query->fetch_array()) {

            $id = $row['id'];
            $fullname = $row['fullname'];
            $emoj = $row['photo'];
            $clinic = $this->get_clinic_details($row['clinic_id']);
            $clinic_name = $clinic['clinic_name'];

            if (empty($emoj)) {

                $emoji = $row['photo'];

            } else {

                $emoji = 'user.png';
            }

            $online .= "<tr>
                    <td>
                    <i class='fa fa-circle text-success'></i>
                    <img src='" . URL . "data/" . $emoji . "' height='40' alt='photo' class='rounded-circle'>
                    {$fullname}</strong><span class='badge badge-primary'>{$clinic_name}</span>

                    </td>
                </tr>";

        }

        $online .= "</tbody>
                    </table>";

        echo $online;
    }

    //clinic details

    public function get_client_details($client_refnumber)
    {
            $sql = "SELECT * FROM bumaa_clients WHERE client_refnumber = '{$client_refnumber}'";
            $query = $this->db->query($sql);
            $names = "";
            if($query){
            while ($row = $query->fetch_array()) {
                $names = $row['fname'] . " " . $row['lname'];
            }
            return $names;
        }else{
              $names = 'N/A'; return $names;
        }

    }

    public function get_clinic_details($clinic_id)
    {
        $sql = "SELECT * FROM clinics WHERE id = '{$clinic_id}'";
        $query = $this->db->query($sql);
        while ($row = $query->fetch_array()) {
            $id = $row['id'];
            $clinicName = $row['clinic_name'];
            $clinicCode = $row['clinic_code'];
            $cliniColor = $row['clinic_color'];
            $clinicPhone = $row['clinicPhone'];
        }
        $clinicD = array("id" => $id, "clinic_name" => $clinicName, "clinic_code" => $clinicCode, "clinic_color" => $cliniColor, "clinicPhone" => $clinicPhone);
        return $clinicD;
    }

}
