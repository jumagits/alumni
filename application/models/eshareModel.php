<?php 


class eshareModel extends model 
{
    public function add_shared_file($slug, $title, $descrption, $sharedwith, $files, $shared_by,$clinci_id)
    {
        $sql_check = "SELECT * FROM eshare_documents_ WHERE slug = '{$slug}'";
        $query        = $this->db->query($sql_check);
        $num_rows = $query->num_rows;
        if($num_rows > 0)
        {
           
        }else{
            $sql_insert = "INSERT INTO eshare_documents_(slug, title, description, sharedwith, files, owner,clinic_id )
                          VALUES('{$slug}', '{$title}', '{$descrption}', '{$sharedwith}', '{$files}', '{$shared_by}', '{$clinci_id}')";
            if($this->db->query($sql_insert))
            {
                $user_ids = explode(',', $sharedwith);
                return array('msg'=> "shared document bundle with title: [".$title."], Successfully shared document", 'status'=> true);
            }
            else
            {
                return array('msg'=> "ERROR".$this->db->errno." : ".$this->db->error."", 'status' => false);
            }              
        }
    }

    public function getUser($id)
    {
        $sql = "SELECT * FROM bumaa_clients WHERE id = '{$id}'  ORDER BY id DESC";
        $query = $this->db->query($sql);$count = 0;@$str = "";
        $data = [];
        while($row = $query->fetch_array())
        {
            $data[] = $row;
        }
        return $data;
    }



        public function AllSharedDocuments()
    {
        $sql = "SELECT * FROM eshare_documents_ ";
        $query = $this->db->query($sql);
        $count = 0;
        @$tr = "";
        $num_rows = $query->num_rows;
        if(!$num_rows)
        {
            return "<div class='alert alert-block alert-info'><strong><i class='fa fa-info-circle'></i> INFO :</strong> No shared files yet..... </div>";
        }else{
            $table = '';
            while($row = $query->fetch_array())
            {
                $file = $row['files'];
                $title = $row['title'];
                $desc = $row['description'];
                $shared_on = $row['date_added'];
                $s = explode(',', $row['sharedwith']);
                $sharedWith = '';
                $sharedby   = $row['owner'];

             
                $table = "<tr>
                            <td><a href='".URL."data/".$file."' data-toggle='tooltip' title='Download' class='openWindow'>
                            <img src='".URL."static/admin/images/icons/1.png'>
                            </a></td>
                            <td>{$title}</td>
                            <td>{$desc}</td>
                            <td>{$shared_on}</td>
                            <td>{$sharedWith}</td>
                            <td>".$this->getUser($sharedby)[0]['fname']."</td>
                        </tr>";
                    }
                    
                }
         
            return $table;
        }
    


    public function getAllSharedDocuments($user_id)
    {
        $sql = "SELECT * FROM eshare_documents_ ";
        $query = $this->db->query($sql);
        $count = 0;
        @$tr = "";
        $num_rows = $query->num_rows;
        if(!$num_rows)
        {
            return "<div class='alert alert-block alert-info'><strong><i class='fa fa-info-circle'></i> INFO :</strong> No shared files yet..... </div>";
        }else{
            $table = '';
            while($row = $query->fetch_array())
            {
                $file = $row['files'];
                $title = $row['title'];
                $desc = $row['description'];
                $shared_on = $row['date_added'];
                $s = explode(',', $row['sharedwith']);
                $sharedWith = '';
                $sharedby   = $row['owner'];

                if($s[0] != 'ALL'){
                    for ($i=0; $i < sizeof($s); $i++) { 
                        $sharedWith .= @$this->getUser($s[$i])[0]['fname'].", ";
                    }
                    if(in_array($user_id, $s))
                    {
                        $table .= "<tr>
                            <td><a href='".URL."data/".$file."' data-toggle='tooltip' title='Download' class='openWindow'>

                            <img src='".URL."static/admin/images/icons/1.png' width='40px'>
                            </a></td>
                            <td>{$title}</td>
                            <td>{$desc}</td>
                            <td>{$shared_on}</td>
                            <td>{$sharedWith}</td>
                            <td>".$this->getUser($sharedby)[0]['fname']."</td>
                        </tr>";
                    }
                    
                }else{
                    $sharedWith = 'Shared To All Staff';
                    $table .= "<tr>
                            <td><a href='".URL."data/".$file."' data-toggle='tooltip' title='Download' class='openWindow'>
                            <img src='".URL."static/admin/images/icons/1.png' width='40px'>
                            </a></td>
                            <td>{$title}</td>
                            <td>{$desc}</td>
                            <td>{$shared_on}</td>
                            <td>{$sharedWith}</td>
                            <td>".$this->getUser($sharedby)[0]['fullname']."</td>
                        </tr>";
                }
                


               
            }
            $table .= '';
            return $table;
        }
    }





    public function getAllUsersOptionValues()
    {
        $count=0;
        $sql = "SELECT * FROM bumaa_clients WHERE comp = 0";
        $query = $this->db->query($sql);
        $users = "";$option = "";
        while($row = $query->fetch_array())
        {
            $count++;

            $id = $row['id'];

            $names = $row['fname']." ".$row['lname'];

            $option .= "<option value='".$id."'>".$names."</option>";

        } 
        $option .= "<option value='ALL'>ALL</option>";
        return $option;
    }

    public function getUserId($id)
    {
        $sql = "SELECT * FROM users WHERE id = '{$id}'";
        $query = $this->db->query($sql);
        while ($row = $query->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

    public function allUser()
    {
        $sql = "SELECT * FROM login";
        $query = $this->db->query($sql);
        while ($row = $query->fetch_object()) {
            $data[] = $row;
        }
        return @$data;
    }

}