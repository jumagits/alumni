<?php
class forumModel extends Model
{


    //add comment

      public function addComment($comment,$user_id,$post_id){

      $datetime = date('Y-m-d H:i:s');

      $sql = "INSERT INTO forum_comments(comment, post_id, datetime, user_id)VALUES('{$comment}', '{$post_id}','{$datetime}', '{$user_id}')";
        if(!($this->db->query($sql)))
        {
            return json_encode("<div class='alert alert-block alert-danger'><strong>SQL [2] ERROR (" . $this->db->errno . ") : </strong>" . $this->db->error . "</div>");
            
            }else{

           return json_encode('New Comment  has been created Successfully ......');             
        }


  }


    //add topic

  public function addTopic($post_title,$post_content,$cat_id,$created_by){

    $datetime = date('Y-m-d H:i:s');

      $sql = "INSERT INTO forum_topics(post_title, post_content, datetime,cat_id, user_id)VALUES('{$post_title}', '{$post_content}','{$datetime}', '{$cat_id}','{$created_by}')";
        if(!($this->db->query($sql)))
        {
            return json_encode("<div class='alert alert-block alert-danger'><strong>SQL [2] ERROR (" . $this->db->errno . ") : </strong>" . $this->db->error . "</div>");
            
            }else{

           return json_encode('New Topic  has been created Successfully ......');             
        }

  }



   public function updateTopic($post_title,$post_content,$cat_id,$created_by,$post_id){

     $datetime = date('Y-m-d H:i:s');

      $sql = "UPDATE  forum_topics SET post_title = '$post_title', post_content = '$post_content',datetime = '$datetime',cat_id = '$cat_id', user_id = '$created_by' WHERE post_id = '$post_id'"; 
        if(!($this->db->query($sql)))
        {
            return json_encode("<div class='alert alert-block alert-danger'><strong>SQL [2] ERROR (" . $this->db->errno . ") : </strong>" . $this->db->error . "</div>");
            
            }else{

           return json_encode('New Topic  has been created Successfully ......');             
        }

  }


      public function fetchAllComments($id)
    {
        $sql = "SELECT * FROM forum_comments WHERE post_id = '$id' ORDER BY comment_id DESC ";
        $query = $this->db->query($sql)  or die($this->db->error);
        $data = [];  
        while($row = $query->fetch_object())
        {
            $data[] = $row;

        }

        return $data;

    }


     public function fetchAllCategories()
    {
        $sql = "SELECT * FROM forum_categories ORDER BY cat_id DESC ";
        $query = $this->db->query($sql)  or die($this->db->error);
        $data = [];  
        while($row = $query->fetch_object())
        {
            $data[] = $row;

        }

        return $data;

    }


    //delete topic


    public function delete_topic($post_id)
    {
        $deleteSQL = "DELETE FROM forum_topics WHERE post_id='{$post_id}'";
        if(!( $query = $this->db->query($deleteSQL)))
       {
            die($this->db->error);

       }else{

         return true;

        }
    }

    //end delete topic


   public function fetchTopic($id)
    {
        $sql = "SELECT * FROM forum_topics  WHERE cat_id = '$id' ";
        $query = $this->db->query($sql)  or die($this->db->error);
        $data = [];  
        while($row = $query->fetch_object())
        {
            $data[] = $row;

        }

        return $data;

    }

     public function fetchSingleTopic($id)
    {
        $sql = "SELECT * FROM forum_topics  WHERE post_id = '$id' LIMIT 1";
        $query = $this->db->query($sql)  or die($this->db->error);
        $data =  $query->fetch_object();
        return $data;

    }


     public function categoryName($id)
    {
        $sql = "SELECT * FROM forum_categories WHERE cat_id = '$id' ";
        $query = $this->db->query($sql)  or die($this->db->error);
      
        while($row = $query->fetch_array())
        {
            $name = $row['category_title'];

        }

        return $name;

    }


      public function created_by($id)
    {
        $sql = "SELECT fullname FROM login WHERE id = '$id' ";
        $query = $this->db->query($sql)  or die($this->db->error);
      
        while($row = $query->fetch_array())
        {
            $name = $row['fullname'];

        }

        return $name;

    }


   public function dateF($date)
    {
       return date_format(date_create($date), ' l jS F Y');

    }



   public function fetchCategories()
    {
        $sql = "SELECT * FROM forum_categories ";
        $query = $this->db->query($sql)  or die($this->db->error);
        $count = 0;@$forms = "";  
        while($row = $query->fetch_array())
        {
          $count++;
            $category_title = $row['category_title'];
            $cat_id = $row['cat_id'];
          
        
            @$forms .= "<tr>
                        <td>{$count}</td>
                        <td>{$category_title}</td>
                         <td><a data-toggle='modal' data-target='#exampleModal-{$cat_id}' ><i class='fa fa-edit '></i> Update </a></td>
           
                              <div class='modal fade' id='exampleModal-{$cat_id}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel1'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header '>

                                        EDIT CATEGORY
                                          
                                              </div>
                                        <div class='modal-body'>
                                            <form method='POST' action='#' id='categoryEditForm'>
                                                <div class='form-group'>
                                                   
                                                     <input type='hidden' class='form-control' value='{$cat_id}' name='cat_id'>
                                                   </div>

                                            <div class='form-group'>
                                                    <label for='category_title' class='control-label'>Category Name:</label>
                                                     <input type='text' class='form-control' value='{$category_title}' name='category_title'>
                                                   </div>
                                               
                                         
                                        </div>
                                        <div class='modal-footer'>
                                            
                                            <button type='submit' class='btn btn-primary'><i class='fa fa-edit'></i> Edit 
                                            </button>

                                            <button type='button' class='btn btn-danger' data-dismiss='modal'>Close</button>
                                        </div>
                                           </form>
                                    </div>
                                </div>
                            </div>  
                        <td><a  class='text-danger' href='".URL."forum/delete_cat/{$cat_id}'><i class='fas fa-trash'></i> Trash </a></td>
                        
                     </tr>";

        }

        return $forms;

    }

    public function fetchAllTopics()
    {
        $sql = "SELECT * FROM forum_topics ORDER BY post_id DESC";
        $query = $this->db->query($sql)  or die($this->db->error);
        $count = 0;@$forms = "";  
        while($row = $query->fetch_array())
        {
          $count++;
            $post_title = $row['post_title'];
            $post_content = $row['post_content'];
            $datetime = $row['datetime'];
            $post_id = $row['post_id'];
        
            @$forms .= "<tr>
                        <td>{$count}</td>
                        <td>{$post_title}</td>
                        <td>{$post_content}</td>
                        <td>{$datetime}</td>
                          <td><a data-toggle='modal' data-target='#exampleModal-{$post_id}' ><i class='fa fa-edit '></i> Update </a></td>
                            <div class='modal fade' id='exampleModal-{$post_id}' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel1'>
                                <div class='modal-dialog' role='document'>
                                    <div class='modal-content'>
                                        <div class='modal-header '>

                                        EDIT TOPIC
                                          
                                              </div>
                                        <div class='modal-body'>
                                            <form method='POST' action='#' id='topicEditForm'>
                                                <div class='form-group'>
                                                   
                                                 <input type='hidden' class='form-control' value='{$post_id}' name='post_id'>
                                                </div>

                                                 <div class='form-group'>
                                                    <label>Topic Title</label>
                                                    <input type='text' class='form-control' name='post_title' value='{$post_title}' required> 
                                                  </div>

                                                  <div class='form-group'>
                                                        <label>Category</label>
                                                        <select name='cat_id' class='form-control'>
                                                            <option selected='' disabled=''> -- Select Category --</option>
                                                            ".$this->catOptions()." </select>
                                                    </div>



                                                 <div class='form-group'>
                                                 <input type='hidden' name='created_by' value='".$user_info['id']."'>
                                                    <label>Content</label>
                                                    <textarea name='post_content' class='form-control'>
                                                    {$post_content}
                                                        </textarea>
                                                </div>


                                               
                                         
                                        </div>
                                        <div class='modal-footer'>
                                            
                                            <button type='submit' class='btn btn-primary'><i class='fa fa-edit'></i> Edit 
                                            </button>

                                            <button type='button' class='btn btn-danger' data-dismiss='modal'>Close</button>
                                        </div>
                                           </form>
                                    </div>
                                </div>
                            </div>  
           
                        <td><a class='text-danger' href='".URL."forum/delete_topic/{$post_id}'><i class='fas fa-trash'></i> Trash </a></td>
                        </tr>";

}

return $forms;

}



public function catOptions(){

$sql = "SELECT * FROM forum_categories ";
$query = $this->db->query($sql) or die($this->db->error);
@$option = "";
while($row = $query->fetch_array())
{

$category_title = $row['category_title'];
$cat_id = $row['cat_id'];
@$option .= "<option value='{$cat_id}'>
    {$category_title}
</option>";

}

return $option;


}



//add category


public function addCategory($category_title){

$sql = "INSERT INTO forum_categories(category_title)VALUES('{$category_title}')";
if(!($this->db->query($sql)))
{
return json_encode("<div class='alert alert-block alert-danger'><strong>SQL [2] ERROR (" . $this->db->errno . ") :
    </strong>" . $this->db->error . "</div>");

}else{

return json_encode('New Category has been created Successfully ......');
}
}

//delete cat


public function delete_category($id)
{
$deleteSQL = "DELETE FROM forum_categories WHERE cat_id='{$id}'";
if(!( $query = $this->db->query($deleteSQL)))
{
die($this->db->error);
}else{
$msg = "You have successfully Deleted a Category";
return array('msg'=>$msg, 'status'=> true);
}
}


//update category


public function updateCategory($category_title,$cat_id){

$sql = "UPDATE forum_categories SET category_title = '$category_title' WHERE cat_id = '$cat_id' ";
if(!($this->db->query($sql)))
{
return json_encode("<div class='alert alert-block alert-danger'><strong>SQL [2] ERROR (" . $this->db->errno . ") :
    </strong>" . $this->db->error . "</div>");

}else{

return json_encode('New Category has been Updated Successfully ......');
}
}

//update topic





//end of methods




}


?>