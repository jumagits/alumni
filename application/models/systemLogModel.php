<?php
class systemLogModel extends Model
{
    public function writeLog($activity,$user_id,$clinic_id, $log_type)
    {
        $month = date('F');$day = date('j');$year = date('Y');
        $activity_time = date('l, j<\s\u\p>S</\s\u\p> F Y (h:i:s A)');

        $SQL = "INSERT INTO system_logs(activity,day,month,year,user_id,clinic_id,activity_time,log_type)VALUES('{$activity}','{$day}','{$month}','{$year}','{$user_id}','{$clinic_id}','{$activity_time}','{$log_type}')";
        if(!$this->db->query($SQL))
        {
            die($this->db->error);
        }
    }
    public function fetchMostRecentLogs($user_id ,$clinic_id)
    {
        if(!empty($user_id)){
            
            $SQL = "SELECT * FROM system_logs WHERE user_id = '{$user_id}' ORDER BY id DESC LIMIT 11";
        
        }else{

            $SQL = "SELECT * FROM system_logs WHERE clinic_id = '{$clinic_id}' ORDER BY id DESC LIMIT 11";
        }
        
        $QRY = $this->db->query($SQL);
        $tr = ""; $count=0;
        while($row = $QRY->fetch_array())
        {
            $uid = $row['user_id'];
            $sqlx = "SELECT fullname FROM login WHERE id='{$uid}' LIMIT 1";
            $q = $this->db->query($sqlx);
            $data = [];
            while ($x = $q->fetch_object()) {
                $data[] = $x;
            }

            $activity_time = $row['activity_time'];
            $activity = $row['activity'];
            $log_type= strtolower($row['log_type']);
            $count++;
            //var_dump($data);
            if($uid == $user_id)
            {
                $by = "You";
            }else{
                $by = $data[0]->fullname;
            }
            
            $tr .="<tr>
                    <td>{$count}</td>
                    <td>{$activity_time}</td>
                    <td>".$by." ".$activity."</td>
                </tr>";
        }
        return $tr;
    }



    public function fetchAllLogs() //$id
    {
        $SQL = "SELECT * FROM system_logs"; // WHERE clinic_id='{$id}'
        $QRY = $this->db->query($SQL)or die($this->db->error);
        $tr = ""; $count=0;
        while($row = $QRY->fetch_array())
        {
            $activity = $row['activity'];
            $dayTime = $row['activity_time'];
            $user = $row['user_id'];
            $clinic = $row['clinic_id'];
            $type = $row['log_type'];
            $count++;
            $userName = "";
            $clinicName = "";
        
        $sql2 = "SELECT * FROM bumaa_clients WHERE id ='{$user}'";
        $query2 = $this->db->query($sql2)or die($this->db->error);
        while($row2 = $query2->fetch_array(MYSQLI_ASSOC))
        {
            $userName = $row2['username'];
            $fullName = $row2['fname'];
        }
        $sql3 = "SELECT * FROM clinics WHERE id ='{$clinic}'";
        $query3 = $this->db->query($sql3)or die($this->db->error);
        while($row3 = $query3->fetch_array(MYSQLI_ASSOC))
        {
            $clinicName = $row3['clinic_name'];
        }

            $tr .="<tr>
                    <td>{$count}</td>
                    <td>{$activity}</td>
                    <td>{$userName}</td>
                    <td>{$clinicName}</td>    
                    <td>{$dayTime}</td>
                    <td>{$type}</td>
                </tr>";
        }
        return $tr;
    }
    public function myLogs($myId)
    {
    $SQL = "SELECT * FROM system_logs WHERE user_id = '{$myId}'";
        $QRY = $this->db->query($SQL)or die($this->db->error);
        $tr = ""; $count=0;
        while($row = $QRY->fetch_array())
        {
            $activity = $row['activity'];
            $dayTime = $row['activity_time'];
            $user = $row['user_id'];
            $clinic = $row['clinic_id'];
            $type = $row['log_type'];
            $count++;
        
        $sql2 = "SELECT * FROM login WHERE id ='{$user}'";
        $query2 = $this->db->query($sql2)or die($this->db->error);
        while($row2 = $query2->fetch_array(MYSQLI_ASSOC))
        {
            $userName = $row2['username'];
            $fullName = $row2['fullname'];
        }
        $sql3 = "SELECT * FROM clinics WHERE id ='{$clinic}'";
        $query3 = $this->db->query($sql3)or die($this->db->error);
        while($row3 = $query3->fetch_array(MYSQLI_ASSOC))
        {
            $clinicName = $row3['clinic_name'];
        }

            $tr .="<tr>
                    <td>{$count}</td>
                    <td>{$activity}</td>
                    <td>{$userName}</td>
                    <td>{$clinicName}</td>    
                    <td>{$dayTime}</td>
                    <td>{$type}</td>
                </tr>";
        }
        return $tr;
    }
    public function fetchRecentLogins($id)
    {
        $SQL = "SELECT activity, activity_time, log_type FROM system_logs WHERE clinic_id='{$id}'";
        $QRY = $this->db->query($SQL)or die($this->db->error);
        $tr = ""; $count=0;
        while($row = $QRY->fetch_array())
        {
            $activity = $row['activity'];
            $dayTime = $row['activity_time'];
            $type = $row['log_type'];
            $count++;
        

            $tr .="<tr>
                    <td>{$count}</td>
                    <td>{$activity}</td>
                    <td>{$userName}</td>
                    <td>{$clinicName}</td>    
                    <td>{$dayTime}</td>
                    <td>{$type}</td>
                </tr>";
        }
        return $tr;
    }
    public function logSwitch($id,$activity,$clinic_id,$roles,$role_id,$curClinic,$curRole,$log_type)
    {
        $month = date('F');$day = date('j');$year = date('Y');
        $activity_time = date('l, j<\s\u\p>S</\s\u\p> F Y (h:i:s A)');

        $SQL = "INSERT INTO accesSwitch(user_id,activity,clinic_id,roles,role_id,cur_clinic,cur_role,activity_time,log_type)VALUES('{$id}','{$activity}','{$clinic_id}','{$roles}','{$role_id}','{$curClinic}','{$curRole}','{$activity_time}','{$log_type}')";
        if(!$this->db->query($SQL))
        {
            die($this->db->error);
        }
    }
    public function fetchRecentSwitch($myclinic)
    {
        $SQL = "SELECT * FROM accesSwitch WHERE clinic_id = '{$myclinic}' ORDER BY activity_time DESC LIMIT 5";
        $QRY = $this->db->query($SQL)or die($this->db->error);
        $tr = ""; $count=0;
        while($row = $QRY->fetch_array())
        {
            $user = $row['user_id'];
            $clinic = $row['clinic_id'];
            $clinic2 = $row['cur_clinic'];
            $activity = $row['activity'];
            $dayTime = $row['activity_time'];
            $type = $row['log_type'];
            $uRole = $row['role_id'];
            $uRole2 = $row['cur_role'];
            //$count++;
            $SQL2 = "SELECT * FROM login WHERE id ='{$user}'";
            $QRY2 = $this->db->query($SQL2)or die($this->db->error);
            while($row2 = $QRY2->fetch_array())
            {
                $uName = $row2['username'];
                @$fname = $row2['fullname'];
            }
            $SQL3 = "SELECT * FROM clinics WHERE id ='{$clinic}' AND id ='{$clinic}'";
            $QRY3 = $this->db->query($SQL3)or die($this->db->error);
            while($row3 = $QRY3->fetch_array())
            {
                @$cName = $row3['clinic_name'];
            }

            $SQL4 = "SELECT * FROM user_categories WHERE id ='{$uRole}'";
            $QRY4 = $this->db->query($SQL4)or die($this->db->error);
            while($row4 = $QRY4->fetch_array())
            {
                $rName = $row4['category_name'];
            }
            //$rName = 'test';
                $tr .="<div class='alert alert-dark p-3 inbox-item'>
                <div class='inbox-item-img float-left mr-3'></div>
                    <h6 class='inbox-item-author mt-0 mb-1'>{$uName}</h6>
                    <p class='inbox-item-text  mb-0' >{$activity} <strong>{$cName} As {$rName}</strong> From <strong>{$clinic2} AS {$uRole2}</strong></p>
                    <p class='inbox-item-date '>{$dayTime}</p>
                </div>";
        }
        return $tr;
    }

    

  
    
}