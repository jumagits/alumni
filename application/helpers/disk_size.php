<?php
class disk_size
{
    public function getStorageSpace($drive)
    {
        //$formatBytes = $this->loadHelper("format_bytes");

        $ds = disk_total_space($drive);
        $df = disk_free_space($drive);
        $du = $ds - $df;
        $duP = round($du / $ds * 100, 1);

        $usedDiskSpace = $this->format($du);
        $totalDiskSpace = $this->format(disk_total_space($drive));
        $freeDiskSpace = $this->format(disk_free_space($drive));


        $data["freeDiskSpace"] = $freeDiskSpace;
        $data['totalDiskSpace'] = $totalDiskSpace;
        $data['usedDiskSpace'] = $usedDiskSpace;
        $data['usedDiskPercentage'] = $duP;

        return $data;

    }

  public  function Wo_FolderSize($dir)
   {
    $count_size = 0;
    $count = 0;
    $dir_array = scandir($dir);
    foreach ($dir_array as $key => $filename) {
        if ($filename != ".." && $filename != "." && $filename != ".htaccess") {
            if (is_dir($dir . "/" . $filename)) {
                $new_foldersize = Wo_FolderSize($dir . "/" . $filename);
                $count_size = $count_size + $new_foldersize;
            } else if (is_file($dir . "/" . $filename)) {
                $count_size = $count_size + filesize($dir . "/" . $filename);
                $count++;
            }
        }
    }
    return $count_size;
}

public function  format($bytes, $precision = 1) {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    function size_format($bytes)
{
    $size = array('2000000' => '2MB',
        '6000000' => '6MB',
        '12000000' => '12MB',
        '24000000' => '24MB',
        '48000000' => '48MB',
        '96000000' => '96MB',
        '256000000' => '256MB',
        '512000000' => '512MB',
        '1000000000' => '1GB',
        '10000000000' => '10GB');
    return $size[$bytes];
}


 public   function copy_directory($src, $dst)
{
    $dir = opendir($src);
    @mkdir($dst);
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            if (is_dir($src . '/' . $file)) {
                copy_directory($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

function delete_directory($dirname)
{
    if (is_dir($dirname)) {
        $dir_handle = opendir($dirname);
    }

    if (!$dir_handle) {
        return false;
    }

    while ($file = readdir($dir_handle)) {
        if ($file != "." && $file != "..") {
            if (!is_dir($dirname . "/" . $file)) {
                unlink($dirname . "/" . $file);
            } else {
                delete_directory($dirname . '/' . $file);
            }

        }
    }
    closedir($dir_handle);
    rmdir($dirname);
    return true;
}

function unzip_file($file, $destination)
{
    // create object
    $zip = new ZipArchive();
    // open archive
    if ($zip->open($file) !== true) {
        return false;
    }
    // extract contents to destination directory
    $zip->extractTo($destination);
    // close archive
    $zip->close();
    return true;
}




    //
}