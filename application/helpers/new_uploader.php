<?php 


   function compress_image($source_url, $destination_url, $quality) {

       $info = getimagesize($source_url);

        if ($info['mime'] == 'image/jpeg')
              $image = imagecreatefromjpeg($source_url);

        elseif ($info['mime'] == 'image/gif')
              $image = imagecreatefromgif($source_url);

      elseif ($info['mime'] == 'image/png')
              $image = imagecreatefrompng($source_url);

        imagejpeg($image, $destination_url, $quality);
    return $destination_url;
    }


    //checking if images

     function isImage($file_path = ''){
    if (file_exists($file_path)) {
      $image      = getimagesize($file_path);
      $mime_types = array(IMAGETYPE_GIF,IMAGETYPE_JPEG,IMAGETYPE_PNG,IMAGETYPE_BMP);

          if (in_array($image[2], $mime_types)) {
              return true;
          }
    }

    return false;
  }

// upload files to sever
 function uploadFile($type = 0, $delete_from_stroage = true) {
    if( self::$user->is_pro == 0){
      if( !empty( self::$config['free_user_storage_limit'] ) && (int)self::$config['free_user_storage_limit'] > 1 ){
        if( (int)self::$user->uploads >= self::$config['free_user_storage_limit'] ){
          return array(
            'error' => lang('free_limit_storage')
          );
        }
      }
    }
    
    if (!file_exists('media/upload/photos/' . date('Y'))) {
      @mkdir('media/upload/photos/' . date('Y'), 0777, true);
    }
    if (!file_exists('media/upload/photos/' . date('Y') . '/' . date('m'))) {
      @mkdir('media/upload/photos/' . date('Y') . '/' . date('m'), 0777, true);
    }

    if (!file_exists('media/upload/videos/' . date('Y'))) {
      @mkdir('media/upload/videos/' . date('Y'), 0777, true);
    }
    if (!file_exists('media/upload/videos/' . date('Y') . '/' . date('m'))) {
      @mkdir('media/upload/videos/' . date('Y') . '/' . date('m'), 0777, true);
    }

    if (!file_exists('media/upload/files/' . date('Y'))) {
      @mkdir('media/upload/files/' . date('Y'), 0777, true);
    }
    if (!file_exists('media/upload/files/' . date('Y') . '/' . date('m'))) {
      @mkdir('media/upload/files/' . date('Y') . '/' . date('m'), 0777, true);
    }

    $new_string        = pathinfo($this->name, PATHINFO_FILENAME) . '.' . strtolower(pathinfo($this->name, PATHINFO_EXTENSION));
    $file_extension    = pathinfo($new_string, PATHINFO_EXTENSION);

    if (!empty($this->allowed)) {
      $extension_allowed = explode(',', $this->allowed);
      
      if (!in_array($file_extension, $extension_allowed)) {

        return array(
          'error' => 'File format not supported'
        );
      }
    }

    if ($file_extension == 'jpg' || $file_extension == 'jpeg' || $file_extension == 'png' || $file_extension == 'gif') {
      $folder   = 'photos';
      $fileType = 'image';
    } else if ($file_extension == 'mp4' || $file_extension == 'webm' || $file_extension == 'flv') {
      $folder   = 'videos';
      $fileType = 'video';
    } else {
      $folder   = 'files';
      $fileType = 'file';
    }

    if (empty($folder) || empty($fileType)) {
      return false;
    }

    $ar = array(
      'video/mp4',
      'video/mov',
      'video/mpeg',
      'video/flv',
      'video/avi',
      'video/webm',
      'audio/wav',
      'audio/mpeg',
      'video/quicktime',
      'audio/mp3',
      'image/png',
      'image/jpeg',
      'image/gif',
      'application/pdf',
      'application/msword',
      'application/zip',
      'application/x-rar-compressed',
      'text/pdf',
      'application/x-pointplus',
      'text/css',
      'text/plain',
      'application/x-zip-compressed'
    );

    if (!in_array($this->type, $ar)) {
      return array(
        'error' => 'File format not supported'
      );
    }

    $dir         = "media/upload";
    $generate    = date('Y') . '/' . date('m') . '/' . $this->generateKey(50,50) . '_' . date('d') . '_' . md5(time());
    $file_path   = "{$folder}/" . $generate . "_{$fileType}.{$file_extension}";
    $filename    = $dir . '/' . $file_path;
    $second_file = pathinfo($filename, PATHINFO_EXTENSION);
    if (move_uploaded_file($this->file, $filename)) {
      if ($second_file == 'jpg' || $second_file == 'jpeg' || $second_file == 'png' || $second_file == 'gif') {

        if ($second_file != 'gif') {
          if (!empty($this->crop)) {
            $crop_image = $this->cropImage($this->cropWidth, $this->cropHeight, $filename, $filename, 100);
          }
          $this->compressImage($filename, $filename, 90);
        }
        if ($this->avatar == false) {
          // Add WaterMark
          if (self::$config['watermark'] == 'on' && !empty(self::$config['watermark_link'])) {
            $this->watermark_image($filename);
            // $watermark = new Watermark($filename);
            // $watermark->setWatermarkImage(self::$config['watermark_link']);
            // $a = $watermark->save();
          }
            // Add WaterMark
        }

                if (!empty($this->crop)) {
                    $c_path = $dir . '/' . "{$folder}/" . $generate . "_{$fileType}_c.{$file_extension}";
                    self::cropImage(350, 350, $filename, $c_path, 90);
                }
      }

      $last_data             = array();
      $last_data['filename'] = $filename;
      $last_data['name']     = $this->name;
      if (!empty($c_path)) {
        $last_data['cname']     = $c_path;
      }
      
      if (self::$config['ftp_upload'] == 1 && $delete_from_stroage == true) {
        $upload_     = $this->uploadToFtp($filename, $delete_from_stroage);
        $upload_     = $this->uploadToFtp($c_path, $delete_from_stroage);
      } else if (self::$config['amazone_s3'] == 1 && $delete_from_stroage == true) {
        $upload_     = $this->uploadToS3($filename, $delete_from_stroage);
        $upload_     = $this->uploadToS3($c_path, $delete_from_stroage);
      } else if (self::$config['google_cloud_storage'] == 1 && $delete_from_stroage == true) {
        $upload_     = $this->uploadToGoogleCloud($filename, $delete_from_stroage);
        $upload_     = $this->uploadToGoogleCloud($c_path, $delete_from_stroage);
      } else if (self::$config['digital_ocean'] == 1) {
        $upload_     = $this->UploadToDigitalOcean($filename, $delete_from_stroage);
        $upload_     = $this->UploadToDigitalOcean($c_path, $delete_from_stroage);
      }
      if( self::$user->is_pro == 0 &&
        !empty( self::$config['free_user_storage_limit'] ) &&
        (int)self::$config['free_user_storage_limit'] > 1
      ){
        self::$db->where('user_id',self::$user->user_id);
        self::$db->update(T_USERS,array('uploads' => self::$db->inc( $this->size / 1000 )));
      }
      return $last_data;
    }
  }
  



 ?>