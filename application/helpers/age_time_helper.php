<?php

class age_time_helper
{
    function findage($date)
    {
        $dob = strtotime($date);
        $current_time = time();
        $age_years = date('Y',$current_time) - date('Y',$dob);
        $age_months = date('m',$current_time) - date('m',$dob);
        $age_days = date('d',$current_time) - date('d',$dob);

        if ($age_days<0) {
            $days_in_month = date('t',$current_time);
            $age_months--;
            $age_days= $days_in_month+$age_days;
        }

        if ($age_months<0) {
            $age_years--;
            $age_months = 12+$age_months;
        }

        return"{$age_years} years, {$age_months} months and {$age_days} days old.";  
    }
    function humanTiming ($human_time)
    {
        
        $providedtime = strtotime($human_time);
        $time = time() - $providedtime; // to get the time since that moment
        $time = ($time<1)? 1 : $time;
        $tokens = array (
        31536000 => 'year',
        2592000 => 'month',
        604800 => 'week',
        86400 => 'day',
        3600 => 'hour',
        60 => 'minute',
        1 => 'second'
    );

    foreach ($tokens as $unit => $text) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.$text.(($numberOfUnits>1)?'s':'');
    }
  }


  function get_time_ago_string($time_stamp, $divisor, $time_unit)
{
    $time_difference = strtotime("now") - $time_stamp;
    $time_units      = round(floor($time_difference / $divisor));
    settype($time_units, 'string');
    if( $time_difference < 45 ){
        return __('Just now');
    }else if( $time_difference < 90 ){
        return __('about a minute ago');
    }else if( $time_difference < 45*60 ){
        return str_replace('%d',$time_units, __('%d minutes ago'));
    }else if( $time_difference < 90*60 ){
        return __('about an hour ago');
    }else if( $time_difference < 24*60*60 ){
        return str_replace('%d',$time_units, __('%d hours ago'));
    }else if( $time_difference < 42*60*60 ){
        return __('a day ago');
    }else if( $time_difference < 30*24*60*60 ){
        return str_replace('%d',$time_units,__('%d days ago'));
    }else if( $time_difference < 45*24*60*60 ){
        return __('about a month ago');
    }else if( $time_difference < 365*24*60*60 ){
        return str_replace('%d',$time_units,__('%d months ago'));
    }else if( $time_difference < 1.5*365*24*60*60 ){
        return __('about a year ago');
    }else{
        return str_replace('%d',$time_units,__('%d years ago'));
    }
}


function get_time_ago($time_stamp)
{

    $strings = [
        'suffixAgo' => __("ago"),
        'suffixFromNow' => __("from now"),
        'inPast'=> __("any moment now"),
        'seconds'=> __("Just now"),
        'minute' => __("about a minute ago"),
        'minutes' => __("%d minutes ago"),
        'hour'=> __("about an hour ago"),
        'hours'=> __("%d hours ago"),
        'day'=> __("a day ago"),
        'days'=> __("%d days ago"),
        'month'=> __("about a month ago"),
        'months'=> __("%d months ago"),
        'year'=> __("about a year ago"),
        'years'=> __("%d years ago"),
    ];
    $time_difference = time() - $time_stamp;
    $seconds =  $time_difference ;
    $minutes = $seconds / 60;
    $hours = $minutes / 60;
    $days = $hours / 24;
    $years = $days / 365;

    if( $seconds < 45 ){
        return str_replace('%d',floor($seconds), $strings['seconds']);
    }
    if( $seconds < 90 ){
        return str_replace('%d',1, $strings['minute']);
    }
    if( $minutes < 45 ){
        return str_replace('%d',floor($minutes), $strings['minutes']);
    }
    if( $minutes < 90 ){
        return str_replace('%d',1, $strings['hour']);
    }
    if( $hours < 24 ){
        return str_replace('%d',floor($hours), $strings['hours']);
    }
    if( $hours < 42 ){
        return str_replace('%d',1, $strings['day']);
    }
    if( $days < 30 ){
        return str_replace('%d',floor($days), $strings['days']);
    }
    if( $days < 45 ){
        return str_replace('%d',1, $strings['month']);
    }
    if( $days < 365 ){
        return str_replace('%d',floor($days / 30), $strings['months']);
    }
    if( $years < 1.5 ){
        return str_replace('%d',1, $strings['year']);
    }else{
        return str_replace('%d',floor($years), $strings['years']);
    }
}


function GetDateScope($type = 'day', $interval = 1){
    $data        = array();
    $hours       = array('00' => 0 ,'01' => 0 ,'02' => 0 ,'03' => 0 ,'04' => 0 ,'05' => 0 ,'06' => 0 ,'07' => 0 ,'08' => 0 ,'09' => 0 ,'10' => 0 ,'11' => 0 ,'12' => 0 ,'13' => 0 ,'14' => 0 ,'15' => 0 ,'16' => 0 ,'17' => 0 ,'18' => 0 ,'19' => 0 ,'20' => 0 ,'21' => 0 ,'22' => 0 ,'23' => 0);
    $days        = array('Saturday' => 0 , 'Sunday' => 0 , 'Monday' => 0 , 'Tuesday' => 0 , 'Wednesday' => 0 , 'Thursday' => 0 , 'Friday' => 0);
    $month       = array_fill(1, cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')),0);
    if (cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) == 31) {
        $month = array('01' => 0 ,'02' => 0 ,'03' => 0 ,'04' => 0 ,'05' => 0 ,'06' => 0 ,'07' => 0 ,'08' => 0 ,'09' => 0 ,'10' => 0 ,'11' => 0 ,'12' => 0 ,'13' => 0 ,'14' => 0 ,'15' => 0 ,'16' => 0 ,'17' => 0 ,'18' => 0 ,'19' => 0 ,'20' => 0 ,'21' => 0 ,'22' => 0 ,'23' => 0,'24' => 0 ,'25' => 0 ,'26' => 0 ,'27' => 0 ,'28' => 0 ,'29' => 0 ,'30' => 0 ,'31' => 0);
    }elseif (cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) == 30) {
        $month = array('01' => 0 ,'02' => 0 ,'03' => 0 ,'04' => 0 ,'05' => 0 ,'06' => 0 ,'07' => 0 ,'08' => 0 ,'09' => 0 ,'10' => 0 ,'11' => 0 ,'12' => 0 ,'13' => 0 ,'14' => 0 ,'15' => 0 ,'16' => 0 ,'17' => 0 ,'18' => 0 ,'19' => 0 ,'20' => 0 ,'21' => 0 ,'22' => 0 ,'23' => 0,'24' => 0 ,'25' => 0 ,'26' => 0 ,'27' => 0 ,'28' => 0 ,'29' => 0 ,'30' => 0);
    }elseif (cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) == 29) {
        $month = array('01' => 0 ,'02' => 0 ,'03' => 0 ,'04' => 0 ,'05' => 0 ,'06' => 0 ,'07' => 0 ,'08' => 0 ,'09' => 0 ,'10' => 0 ,'11' => 0 ,'12' => 0 ,'13' => 0 ,'14' => 0 ,'15' => 0 ,'16' => 0 ,'17' => 0 ,'18' => 0 ,'19' => 0 ,'20' => 0 ,'21' => 0 ,'22' => 0 ,'23' => 0,'24' => 0 ,'25' => 0 ,'26' => 0 ,'27' => 0 ,'28' => 0 ,'29' => 0);
    }elseif (cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) == 28) {
        $month = array('01' => 0 ,'02' => 0 ,'03' => 0 ,'04' => 0 ,'05' => 0 ,'06' => 0 ,'07' => 0 ,'08' => 0 ,'09' => 0 ,'10' => 0 ,'11' => 0 ,'12' => 0 ,'13' => 0 ,'14' => 0 ,'15' => 0 ,'16' => 0 ,'17' => 0 ,'18' => 0 ,'19' => 0 ,'20' => 0 ,'21' => 0 ,'22' => 0 ,'23' => 0,'24' => 0 ,'25' => 0 ,'26' => 0 ,'27' => 0 ,'28' => 0);
    }

    $months = array('01' => 0 ,'02' => 0 ,'03' => 0 ,'04' => 0 ,'05' => 0 ,'06' => 0 ,'07' => 0 ,'08' => 0 ,'09' => 0 ,'10' => 0 ,'11' => 0 ,'12' => 0 );
    if( $type == 'day' ){
        $data  = $hours;
        $start = strtotime(date('M')." ".date('d').", ".date('Y')." 12:00am");
        $end   = strtotime(date('M')." ".date('d').", ".date('Y')." 11:59pm");
    } else if( $type == 'day_before' ){
        $start = strtotime('-' . (int)$interval . ' day', time());
        $end = time();
        if ($end>=$start) {
            while ($start<$end) {
                $start+=86400; // add 24 hours
                $data[$start] = date('d M',$start);
            }
        }
    } else if( $type == 'week' ){
        $data  = $days;
        $time = strtotime(date(' l').", ".date('M')." ".date('d').", ".date('Y'));
        if (date('l') == 'Saturday') {
            $start = strtotime(date('M')." ".date('d').", ".date('Y')." 12:00am");
        } else {
            $start = strtotime('last saturday, 12:00am', $time);
        }
        if (date('l') == 'Friday') {
            $end = strtotime(date('M')." ".date('d').", ".date('Y')." 11:59pm");
        } else {
            $end = strtotime('next Friday, 11:59pm', $time);
        }
    } else if( $type == 'month' ){
        $data  = $month;
        $start = strtotime("1 ".date('M')." ".date('Y')." 12:00am");
        $end = strtotime(cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'))." ".date('M')." ".date('Y')." 11:59pm");
    } else if( $type == 'year' ){
        $data  = $months;
        $start = strtotime("1 ".date('M')." ".date('Y')." 12:00am");
        $end = strtotime(cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'))." ".date('M')." ".date('Y')." 11:59pm");
    } else if( $type == 'week_before' ){
        $data  = $month;
        $start = strtotime('-' . (int)$interval . ' week', time());
        $end = time();
    } else if( $type == 'month_before' ){
        $data  = $month;
        $start = strtotime('-' . (int)$interval . ' month', time());
        $end = time();
    } else if( $type == 'year_before' ){
        $data  = $month;
        $start = strtotime('-' . (int)$interval . ' year', time());
        $end = time();
    }

    return array(
        'data'   => $data,
        'start'  => $start,
        'end'    => $end
    );
}

//





}