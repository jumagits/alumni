<?php 

/**
 * 
 */
class encrypt_helper
{

      // Decode links
  public function decodeMarkup($text, $link = true) {
      if ($link == true) {
          $link_search = '/\[a\](.*?)\[\/a\]/i';
          if (preg_match_all($link_search, $text, $matches)) {
              foreach ($matches[1] as $match) {
                  $match_decode     = urldecode($match);
                  $match_decode_url = $match_decode;
                  $count_url        = mb_strlen($match_decode);
                  $match_url        = $match_decode;
                  if (!preg_match("/http(|s)\:\/\//", $match_decode)) {
                      $match_url = 'http://' . $match_url;
                  }
                  $text = str_replace('[a]' . $match . '[/a]', $match_decode_url, $text);
              }
          }
      }
      return $text;
  }



  // Encode markup, and create links inside strings
  public function encodeMarkup($text, $link = true) {
      if ($link == true) {
          $link_search = '/\[a\](.*?)\[\/a\]/i';
          if (preg_match_all($link_search, $text, $matches)) {
              foreach ($matches[1] as $match) {
                  $match_decode     = urldecode($match);
                  $match_decode_url = $match_decode;
                  $count_url        = mb_strlen($match_decode);
                  if ($count_url > 50) {
                      $match_decode_url = mb_substr($match_decode_url, 0, 30) . '....' . mb_substr($match_decode_url, 30, 20);
                  }
                  $match_url = $match_decode;
                  if (!preg_match("/http(|s)\:\/\//", $match_decode)) {
                      $match_url = 'http://' . $match_url;
                  }
                  $text = str_replace('[a]' . $match . '[/a]', '<a href="' . strip_tags($match_url) . '" target="_blank" class="hash" rel="nofollow">' . $match_decode_url . '</a>', $text);
              }
          }
      }
      return $text;
  }

  // check if string is json
  public function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }


      private static $OPENSSL_CIPHER_NAME = "aes-128-cbc"; //Name of OpenSSL Cipher 
    private static $CIPHER_KEY_LEN = 16; //128 bits


    static function encrypt($key, $iv, $data) {
        if (strlen($key) < Encryption::$CIPHER_KEY_LEN) {
            $key = str_pad("$key", Encryption::$CIPHER_KEY_LEN, "0"); //0 pad to len 16
        } else if (strlen($key) > Encryption::$CIPHER_KEY_LEN) {
            $key = substr($data, 0, Encryption::$CIPHER_KEY_LEN); //truncate to 16 bytes
        }

        $encodedEncryptedData = base64_encode(openssl_encrypt($data, Encryption::$OPENSSL_CIPHER_NAME, $key, OPENSSL_RAW_DATA, $iv));
        $encodedIV = base64_encode($iv);
        $encryptedPayload = $encodedEncryptedData;

        return $encryptedPayload;
    }


    static function decrypt($key, $iv, $data) {
        if (strlen($key) < Encryption::$CIPHER_KEY_LEN) {
            $key = str_pad("$key", Encryption::$CIPHER_KEY_LEN, "0"); //0 pad to len 16
        } else if (strlen($key) > Encryption::$CIPHER_KEY_LEN) {
            $key = substr($data, 0, Encryption::$CIPHER_KEY_LEN); //truncate to 16 bytes
        }
        
        $decryptedData = openssl_decrypt(base64_decode($data), Encryption::$OPENSSL_CIPHER_NAME, $key, OPENSSL_RAW_DATA, $iv);
       
        return $decryptedData;
    }



 public   function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = $this->key;
        $secret_iv = $this->iv;

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }








  //
   
}







 ?>

