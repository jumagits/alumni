<?php 
/**
  * 
  */
 class clean_helper
 {

  function clear_cookies()
{
    foreach ($_COOKIE as $key => $value) {
        setcookie($key, $value, time() - 10000, "/");
    }
}

    function formatMoney($number, $fractional=false) {
                    if ($fractional) {
                        $number = sprintf('%.2f', $number);
                    }
                    while (true) {
                        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
                        if ($replaced != $number) {
                            $number = $replaced;
                        } else {
                            break;
                        }
                    }
                    return $number;
                }

    function sanitize_output($buffer)
    {

        $search = array(
            '/\>[^\S ]+/s', // strip whitespaces after tags, except space
            '/[^\S ]+\</s', // strip whitespaces before tags, except space
            '/(\s)+/s', // shorten multiple whitespace sequences
            '/<!--(.|\s)*?-->/', // Remove HTML comments
        );

        $replace = array(
            '>',
            '<',
            '\\1',
            '',
        );

        $buffer = preg_replace($search, $replace, $buffer);

        return $buffer;
    }


    function cleans($string, $censored_words = 1, $br = true)
{
    global $mysqli;
    $string = trim($string);
    $string = mysqli_real_escape_string($mysqli, $string);
    $string = htmlspecialchars($string, ENT_QUOTES);
    if ($br == true) {
        $string = str_replace('\r\n', " <br>", $string);
        $string = str_replace('\n\r', " <br>", $string);
        $string = str_replace('\r', " <br>", $string);
        $string = str_replace('\n', " <br>", $string);
    } else {
        $string = str_replace('\r\n', "", $string);
        $string = str_replace('\n\r', "", $string);
        $string = str_replace('\r', "", $string);
        $string = str_replace('\n', "", $string);
    }
    $string = stripslashes($string);
    $string = str_replace('&amp;#', '&#', $string);
    $string = preg_replace("/{{(.*?)}}/", '', $string);
    if ($censored_words == 1) {
        global $config;
        $censored_words = @explode(",", $config['censored_words']);
        foreach ($censored_words as $censored_word) {
            $censored_word = trim($censored_word);
            $string = str_replace($censored_word, '****', $string);
        }
    }
    return $string;
}


function formatMoney($number, $fractional = false)
{
    if ($fractional) {
        $number = sprintf('%.2f', $number);
    }
    while (true) {
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number);
        if ($replaced != $number) {
            $number = $replaced;
        } else {
            break;
        }
    }
    return $number;
}
     
    function Secure($string, $br = true, $strip = 0) {
    global $conn;
    if(is_array($string) || is_object($string)) return;
    $string = trim($string);
    $string = mysqli_real_escape_string($conn, $string);
    $string = htmlspecialchars($string, ENT_QUOTES);
    if ($br == true) {
        $string = str_replace('\r\n', ' <br>', $string);
        $string = str_replace('\n\r', ' <br>', $string);
        $string = str_replace('\r', ' <br>', $string);
        $string = str_replace('\n', ' <br>', $string);
    } else {
        $string = str_replace('\r\n', '', $string);
        $string = str_replace('\n\r', '', $string);
        $string = str_replace('\r', '', $string);
        $string = str_replace('\n', '', $string);
    }
    if ($strip == 1) {
        $string = stripslashes($string);
    }
    $string = str_replace('&amp;#', '&#', $string);
    return $string;
}

    // Convert object to array
  public static function toObject($array) {
      $object = new \stdClass();
      foreach ($array as $key => $value) {
          if (is_array($value)) {
              $value = self::toObject($value);
          }
          if (isset($value)) {
              $object->$key = $value;
          }
      }
      return $object;
  }

  // Convert object to array
  public static function toArray($obj) {
      if (is_object($obj))
          $obj = (array) $obj;
      if (is_array($obj)) {
          $new = array();
          foreach ($obj as $key => $val) {
              $new[$key] = self::toArray($val);
          }
      } else {
          $new = $obj;
      }
      return $new;
  }

  // Secure strings 
  public static function secure($string) {
      $string = trim($string);
      $string = mysqli_real_escape_string(self::$mysqli,$string);
      $string = htmlspecialchars($string, ENT_QUOTES);
      $string = str_replace('\r\n', " <br>", $string);
      $string = str_replace('\n\r', " <br>", $string);
      $string = str_replace('\r', " <br>", $string);
      $string = str_replace('\n', " <br>", $string);
      $string = str_replace('&amp;#', '&#', $string);
      $string = stripslashes($string);
      return $string;
  }



  // Check if url is valid
  public static function isUrl($url = "") {
      if (empty($url)) {
          return false;
      }
      if (filter_var($url, FILTER_VALIDATE_URL)) {
          return true;
      }
      return false;
  }




function ToArray($obj) {
    if (is_object($obj))
        $obj = (array) $obj;
    if (is_array($obj)) {
        $new = array();
        foreach ($obj as $key => $val) {
            $new[$key] = ToArray($val);
        }
    } else {
        $new = $obj;
    }
    return $new;
}



function SessionStart(){
    global $app;
    if (session_status() != PHP_SESSION_NONE) {
        return;
    }
    ini_set('session.hash_bits_per_character', 5);
    ini_set('session.serialize_handler', 'php_serialize');
    ini_set('session.use_only_cookies', 1);
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params(
        $cookieParams['lifetime'],
        $cookieParams['path'],
        $cookieParams['domain'],
        false,
        true
    );
    session_name(strtolower($app));
    session_start();
}
//SessionStart();
  // Crop strings
  public  function cropText($text = "", $len = 100) {
      if (empty($text) || !is_string($text) || !is_numeric($len) || $len < 1) {
          return "****";
      }
      if (strlen($text) > $len) {
          $text = mb_substr($text, 0, $len, "UTF-8") . "..";
      }
      return $text;
  }


  function thousandsCurrencyFormat($num)
{

    if ($num > 1000) {

        $x = round($num);
        $x_number_format = number_format($x);
        $x_array = explode(',', $x_number_format);
        $x_parts = array('K', 'M', 'B', 'T');
        $x_count_parts = count($x_array) - 1;
        $x_display = $x;
        $x_display = $x_array[0] . ((int) $x_array[1][0] !== 0 ? '.' . $x_array[1][0] : '');
        $x_display .= $x_parts[$x_count_parts - 1];

        return $x_display;

    }

    return $num;
}

function formatSeconds($str_time)
{
    sscanf($str_time, "%d:%d:%d", $hours, $minutes, $seconds);
    return isset($seconds) ? $hours * 3600 + $minutes * 60 + $seconds : $hours * 60 + $minutes;
}




    public static function escape($string) {
        return htmlspecialchars($string, ENT_QUOTES | ENT_HTML401, "UTF-8");
    }
    
    public static function arrayToObject(array $array, $className) {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(serialize($array), ':')
        ));
    }

    public static function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    
    
    public static function get_distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000){
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
    
    public static function objectToObject($instance, $className) {
        return unserialize(sprintf(
            'O:%d:"%s"%s',
            strlen($className),
            $className,
            strstr(strstr(serialize($instance), '"'), ':')
        ));
    }
    
    public static function redirect_to($url){
        header('Location: ' . $url);
    }

    public static function server_root(){
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
        return $protocol . $_SERVER['SERVER_NAME'];
    }

    public static function is_post(){
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }

    public static function is_get(){
        return $_SERVER['REQUEST_METHOD'] === 'GET';
    }

    public static function post_val($val){
        return (isset($_POST[$val]) && (!empty($_POST[$val]))) ? Helper::escape(trim($_POST[$val])) : null;
    }

    public static function file_val($val){
        return (isset($_FILES[$val]) && (!empty($_FILES[$val]))) ? $_FILES[$val] : null;
    }
    
    public static function get_val($val){
        return (isset($_GET[$val]) && (!empty($_GET[$val]))) ? Helper::escape(trim($_GET[$val])) : null;
    }

    public static function validateEmail($email) {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)) return "Invalid Email";
        else return null;
    }

    public static function invalid_length($key, $value, $length){
        if(strlen($value) < $length) return ucfirst($key) . ' must be at least ' . $length . ' char long.';
        else return null;
    }



    function encrypt_decrypt($action, $string) {
        $output = false;
        $encrypt_method = "AES-128-CBC";
        $secret_key = 'KEY';
        $secret_iv = 'IV';
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 8);
        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }
    

    public static function unique_numeric_code($limit){
        return substr(uniqid(mt_rand()), 0, $limit);
    }

    public static function format_address($address){
        $formatted_address = "";
        if(empty($address)) $formatted_address = "Unknown";
        else{
            $address->id = null;
            $address->user = null;
            foreach ($address as $key => $value){
                if(!empty($value)) {
                    $formatted_address .=  $value . ", ";
                }
            }
        }
        return rtrim($formatted_address, ", ");
    }

    public static function time_difference($date){
        $date1 = new DateTime($date);
        $date2 = new DateTime(date(DATE_FORMAT));

        return $date1->diff($date2);
    }


    public static function days_ago($date){
        $date1 = new DateTime($date);
        $date2 = new DateTime(date(DATE_FORMAT));

        $interval = $date1->diff($date2);
        
        if($interval->i <= 0) return $interval->s . " sec";
        else if($interval->h <= 0) return $interval->i . " min";
        else if($interval->d <= 0) return $interval->h . " hour";
        else if($interval->m <= 0) return $interval->d . " day";
        else if($interval->y <= 0) return $interval->m . " month";
        else if($interval->y > 0) return $interval->y . " year";
    }

    public static function format_only_date($date){
        $date1 = new DateTime($date);
        return date_format($date1, 'M j, Y');
    }

    public static function format_time($time){
        $date = date_create($time);
        return date_format($date, 'g:ia, M j, Y');
    }

    public static function format_date($time){
        $date = date_create($time);
        return date_format($date, 'Y-n-j');
    }
    
    public static function today(){
        $date = new DateTime();
        return date_format($date, 'Y-m-d');
    }
    

    public static function format_text($text, $char_count = 20){
        if(strlen($text) >$char_count) $text = substr($text, 0, $char_count) . "...";
        return $text;
    }










    public static function curl_mail_sender($current_page, $param){
        $url  = (isset($_SERVER['HTTPS'])) ? "https://" : "http://";
        $url .= $_SERVER['HTTP_HOST'];
        $url .= str_replace($current_page, "send-mail.php", $_SERVER['REQUEST_URI']);
        
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, Helper::param_str($param));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);
        return $server_output;
    }


    private static function param_str($param){
        $param_str = "";
        foreach ($param as $key => $value){
            $param_str .= $key . "=" .$value . "&";
        }
        return substr(trim($param_str), 0, -1);
    }
    
    
    public static function curl_post($url, $param){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, Helper::param_str($param));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);
        return $server_output;
    }

    
    
    public static function curl_get($url){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);
        return $server_output;
    }

    
    public static function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }


    public static function price($price){
        return number_format((float)$price, 2, '.', '');
    }



  



//


    
 }

  ?>