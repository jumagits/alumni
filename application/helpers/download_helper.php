<?php

class download_helper
{
	function force_download($file)
	{
        $err = "<div style='color: #a94442;background-color: #f2dede;border-color: #ebccd1;border: 1px solid transparent;font-family:monospace; width:60%; margin:auto;padding:15px 25px;text-align: center;'><strong>ERROR : </strong>Sorry, the file you are requesting is currently unavailable.</div>";

        
        if(isset($file))
        {
            $filename = urldecode($file);
        } 
        else 
        {
            $filename = NULL;
        } 
            
        if (!$filename) 
        {
            echo $err;
        }
        else
        {
            $path = UPLOAD_DIR."/$filename";
            if (file_exists($path) && is_readable($path)) 
            {
                $size = filesize($path);
                header('Content-Type: application/octet-stream');
                header('Content-Length: '.$size);
                header('Cache-Control: max-age=0');
                header('Content-Disposition: attachment; filename="'.$filename."");
                header('Content-Transfer-Encoding: binary');
                $file = @ fopen($path, 'rb');
                if ($file)
                {
                    fpassthru($file);
                    exit;
                } 
                else
                {
                    echo $err;
                }
            }
            else
            {
                echo $err;
            }
        }
	}
}