<?php
class generatepdf
{
    function makepdf($html, $page_title, $filename)
    {
        require_once APP_DIR.'thirdparty/mpdf/mpdf.php';
        $mpdf = new mPDF('c');
        $mpdf->mirrorMargins = 1;
        $mpdf->list_indent_first_level = 0;
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->showWatermarkImage = true;

        $mpdf->SetTitle($title);
        $stylesheet = file_get_contents('upgrade/css/bootstrap.min.css');
        $mpdf->WriteHTML($stylesheet,1);
        $mpdf->WriteHTML($html);
        $mpdf->Output($filename,"D");
    }
}