<?php 

/**
 * FETCH HELPER FOR MOST FUNCTIONS
 */
class fetch_helper
{
    
   function fetchDataFromURL($url = '') {
    if (empty($url)) {
        return false;
    }
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7');
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    return curl_exec($ch);
}


function Time_Elapsed_String($ptime) {
    $etime = time() - $ptime;
    if ($etime < 45) {
        return __('Just now');
    }
    if ($etime >= 45 && $etime < 90) {
        return __('about a minute ago');
    }
    $day = 24 * 60 * 60;
    if ($etime > $day * 30 && $etime < $day * 45) {
        return __('about a month ago');
    }
    $a        = array(
        365 * 24 * 60 * 60 => "year",
        30 * 24 * 60 * 60 => "month",
        24 * 60 * 60 => "day",
        60 * 60 => "hour",
        60 => "minute",
        1 => "second"
    );
    $a_plural = array(
        'year' => __("years"),
        'month' => __("months"),
        'day' => __("days"),
        'hour' => __("hours"),
        'minute' => __("minutes"),
        'second' => __("seconds")
    );
    foreach ($a as $secs => $str) {
        $d = $etime / $secs;
        if ($d >= 1) {
            $r        = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ' . __("ago");
        }
    }
}
function GetIpAddress() {
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && ValidateIpAddress($_SERVER['HTTP_CLIENT_IP'])) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (ValidateIpAddress($ip))
                    return $ip;
            }
        } else {
            if (ValidateIpAddress($_SERVER['HTTP_X_FORWARDED_FOR']))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && ValidateIpAddress($_SERVER['HTTP_X_FORWARDED']))
        return $_SERVER['HTTP_X_FORWARDED'];
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && ValidateIpAddress($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && ValidateIpAddress($_SERVER['HTTP_FORWARDED_FOR']))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    if (!empty($_SERVER['HTTP_FORWARDED']) && ValidateIpAddress($_SERVER['HTTP_FORWARDED']))
        return $_SERVER['HTTP_FORWARDED'];
    return $_SERVER['REMOTE_ADDR'];
}


function get_ip_address()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (filter_var($ip, FILTER_VALIDATE_IP)) {
                    return $ip;
                }

            }
        } else {
            if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            }

        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && filter_var($_SERVER['HTTP_X_FORWARDED'], FILTER_VALIDATE_IP)) {
        return $_SERVER['HTTP_X_FORWARDED'];
    }

    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && filter_var($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'], FILTER_VALIDATE_IP)) {
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    }

    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && filter_var($_SERVER['HTTP_FORWARDED_FOR'], FILTER_VALIDATE_IP)) {
        return $_SERVER['HTTP_FORWARDED_FOR'];
    }

    if (!empty($_SERVER['HTTP_FORWARDED']) && filter_var($_SERVER['HTTP_FORWARDED'], FILTER_VALIDATE_IP)) {
        return $_SERVER['HTTP_FORWARDED'];
    }

    return $_SERVER['REMOTE_ADDR'];
}


function ValidateIpAddress($ip) {
    if (strtolower($ip) === 'unknown')
        return false;
    $ip = ip2long($ip);
    if ($ip !== false && $ip !== -1) {
        $ip = sprintf('%u', $ip);
        if ($ip >= 0 && $ip <= 50331647)
            return false;
        if ($ip >= 167772160 && $ip <= 184549375)
            return false;
        if ($ip >= 2130706432 && $ip <= 2147483647)
            return false;
        if ($ip >= 2851995648 && $ip <= 2852061183)
            return false;
        if ($ip >= 2886729728 && $ip <= 2887778303)
            return false;
        if ($ip >= 3221225984 && $ip <= 3221226239)
            return false;
        if ($ip >= 3232235520 && $ip <= 3232301055)
            return false;
        if ($ip >= 4294967040)
            return false;
    }
    return true;
}


  public function getUserBrowser() {
       $u_agent = $_SERVER['HTTP_USER_AGENT'];
       $bname = 'Unknown';
       $platform = 'Unknown';
       $version= "";
       // First get the platform?
       if (preg_match('/linux/i', $u_agent)) {
         $platform = 'linux';
       } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
         $platform = 'Mac';
       } elseif (preg_match('/windows|win32/i', $u_agent)) {
         $platform = 'windows';
       } elseif (preg_match('/iphone|IPhone/i', $u_agent)) {
         $platform = 'IPhone';
       } elseif (preg_match('/android|Android/i', $u_agent)) {
         $platform = 'Android';
       } else if (preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $u_agent)) {
         $platform = 'Mobile';
       }
       // Next get the name of the useragent yes seperately and for good reason
       if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) {
         $bname = 'Internet Explorer';
         $ub = "MSIE";
       } elseif(preg_match('/Firefox/i',$u_agent)) {
         $bname = 'Mozilla Firefox';
         $ub = "Firefox";
       } elseif(preg_match('/Chrome/i',$u_agent)) {
         $bname = 'Google Chrome';
         $ub = "Chrome";
       } elseif(preg_match('/Safari/i',$u_agent)) {
         $bname = 'Apple Safari';
         $ub = "Safari";
       } elseif(preg_match('/Opera/i',$u_agent)) {
         $bname = 'Opera';
         $ub = "Opera";
       } elseif(preg_match('/Netscape/i',$u_agent)) {
         $bname = 'Netscape';
         $ub = "Netscape";
       }
       // finally get the correct version number
       $known = array('Version', $ub, 'other');
       $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
       if (!preg_match_all($pattern, $u_agent, $matches)) {
         // we have no matching number just continue
       }
       // see how many we have
       $i = count($matches['browser']);
       if ($i != 1) {
         //we will have two since we are not using 'other' argument yet
         //see if version is before or after the name
         if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
           $version= $matches['version'][0];
         } else {
           $version= $matches['version'][1];
         }
       } else {
         $version= $matches['version'][0];
       }
       // check if we have a number
       if ($version==null || $version=="") {$version="?";}
       return array(
           'userAgent' => $u_agent,
           'name'      => $bname,
           'version'   => $version,
           'platform'  => $platform,
           'pattern'    => $pattern,
           'ip_address' => get_ip_address()
       );
  }
function GetBrowser() {
    $ub    = '';
    $u_agent  = $_SERVER['HTTP_USER_AGENT'];
    $bname    = 'Unknown';
    $platform = 'Unknown';
    $version  = '';
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub    = 'MSIE';
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub    = 'Firefox';
    } elseif (preg_match('/Chrome/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub    = 'Chrome';
    } elseif (preg_match('/Safari/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub    = 'Safari';
    } elseif (preg_match('/Opera/i', $u_agent)) {
        $bname = 'Opera';
        $ub    = 'Opera';
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub    = 'Netscape';
    }
    $known   = array(
        'Version',
        $ub,
        'other'
    );
    $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
    }
    $i = count($matches['browser']);
    if ($i != 1) {
        if (strripos($u_agent, 'Version') < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }
    if ($version == null || $version == "") {
        $version = '?';
    }
    return array(
        'userAgent' => $u_agent,
        'name' => $bname,
        'version' => $version,
        'platform' => $platform,
        'pattern' => $pattern
    );
}
function GetDeviceType() {
    $deviceName = '';
    $userAgent    = $_SERVER['HTTP_USER_AGENT'];
    $devicesTypes = array(
        'computer' => array(
            'msie 10',
            'msie 9',
            'msie 8',
            'windows.*firefox',
            'windows.*chrome',
            'x11.*chrome',
            'x11.*firefox',
            'macintosh.*chrome',
            'macintosh.*firefox',
            'opera'
        ),
        'tablet' => array(
            'tablet',
            'android',
            'ipad',
            'tablet.*firefox'
        ),
        'mobile' => array(
            'mobile ',
            'android.*mobile',
            'iphone',
            'ipod',
            'opera mobi',
            'opera mini'
        ),
        'bot' => array(
            'googlebot',
            'mediapartners-google',
            'adsbot-google',
            'duckduckbot',
            'msnbot',
            'bingbot',
            'ask',
            'facebook',
            'yahoo',
            'addthis'
        )
    );
    foreach ($devicesTypes as $deviceType => $devices) {
        foreach ($devices as $device) {
            if (preg_match('/' . $device . '/i', $userAgent)) {
                $deviceName = $deviceType;
            }
        }
    }
    return ucfirst($deviceName);
}
function GetDeviceToken() {
    $finger_print               = array();
    $browser                    = GetBrowser();
    $finger_print['ip']         = GetIpAddress();
    $finger_print['browser']    = $browser['name'] . " " . $browser['version'];
    $finger_print['os']         = $browser['platform'];
    $finger_print['deviceType'] = GetDeviceType();
    $device                     = serialize($finger_print);
    return $device;
}


function execTime($id = '0', $round = 4, $reset = FALSE) {
    global $console_log;
    static $data = array();
    if (!isset($data[$id]) or $reset) {
        $data[$id] = microtime(true);
        return 0;
    } else {
        if (LOGTIME == true) {
            if (!isEndPointRequest()) {
                $console_log['log'][] = str_pad($id, 60, ".", STR_PAD_RIGHT) . " : \t" . number_format((microtime(true) - $data[$id]), $round) . " Sec";
            }
        } else {
            number_format((microtime(true) - $data[$id]), $round);
        }
    }
}



  // CURL connection, GET data
  public function curlConnect($url = '', $config = []) {
      if (empty($url)) {
          return false;
      }
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true );
      curl_setopt($curl, CURLOPT_HEADER, false );
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true );
      curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.7.12) Gecko/20050915 Firefox/1.0.7");
      if (!empty($config['POST'])) {
        curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $config['POST']);
      }
      if (!empty($config['bearer'])) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
          'Authorization: Bearer ' . $config['bearer']
        ));
      }

      $curl_response = curl_exec($curl);

      curl_close($curl);
      if ($this->isJson($curl_response)) {
        return json_decode($curl_response, true);
      }
      return $curl_response;
  }



   public function upsertHtags($text = ""){
    if (!empty($text)) {
        $reg = '/#([^`~!@$%^&*\#* ]+)/i';
        $reg_ = '/#([^`~!@$%^&*\#()\-+=\\|\/\.,<>?\'\":;{}\[\]* ]+)/i';
      preg_match_all($reg, $text, $htags);
      if (!empty($htags[1]) && is_array($htags[1])) {
        $htags = $htags[1];
        foreach ($htags as $key => $htag) {
            if($htag == 'http' || $htag == 'https'){
                $htag = $text;
                    }
          $htag_id   = 0;
          $htag_data = self::$db->where('tag',self::secure($htag))->getOne(T_HTAGS,array('id'));
          if (!empty($htag_data)) {
            $htag_id = $htag_data->id;
            self::$db->where('id',$htag_id)->update(T_HTAGS,array(
              'last_trend_time' => time(),
            ));
          }
          else{
            $htag_id = self::$db->insert(T_HTAGS,array(
              'hash' => md5($htag),
              'tag' => $htag,
              'last_trend_time' => time(),
            ));
          }

          $text = str_replace("#$htag", "#[$htag_id]", $text);
        }
      }
    }

    return $text;
  }

  public function linkifyHTags($text = ""){
    $surl = self::$site_url;
    $text = str_replace('&#039;', "'", $text);
    //$text = html_entity_decode($text, ENT_QUOTES);
        $reg = '/#([^`~!@$%^&*\#* ]+)/i';
        $reg_ = '/#([^`~!@$%^&*\#()\-+=\\|\/\.,<>?\'\":;{}\[\]* ]+)/i';
      $text = preg_replace_callback($reg, function($m) use($surl) {
          $tag = $m[1];
          return self::createHtmlEl('a',array(
              'href' => sprintf("%s/explore/tags/%s",$surl,$tag),
              'class' => 'hashtag',
          ),"#$tag");
      }, $text);

      return $text;
  }

  function link_Markup($text, $link = true) {
      if ($link == true) {
          $link_search = '/\[a\](.*?)\[\/a\]/i';
          if (preg_match_all($link_search, $text, $matches)) {
              foreach ($matches[1] as $match) {
                  $match_decode     = urldecode($match);
                  $match_decode_url = $match_decode;
                  $count_url        = mb_strlen($match_decode);
                  if ($count_url > 50) {
                      $match_decode_url = mb_substr($match_decode_url, 0, 30) . '....' . mb_substr($match_decode_url, 30, 20);
                  }
                  $match_url = $match_decode;
                  if (!preg_match("/http(|s)\:\/\//", $match_decode)) {
                      $match_url = 'http://' . $match_url;
                  }
                  $text = str_replace('[a]' . $match . '[/a]', '<a href="' . strip_tags($match_url) . '" target="_blank" class="hash" rel="nofollow">' . $match_decode_url . '</a>', $text);
              }
          }
      }
      return $text;
  }

    // convert array to json for API
  public static function json($response_data) {
      if (!empty($response_data)) {
        header("HTTP/1.1 ".$response_data['code']." ".$response_data['status']);
          header("Content-Type:application/json");
          if(!empty($response_data)){
             echo json_encode($response_data, JSON_UNESCAPED_UNICODE);
          }
      }
      exit();
  }


  function http_request_call($method, $url, $header, $data, $json)
{
    if ($method == 1) {
        $method_type = 1; // 1 = POST
    } else {
        $method_type = 0; // 0 = GET
    }

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_HEADER, 0);

    if ($header !== 0) {
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
    }

    curl_setopt($curl, CURLOPT_POST, $method_type);

    if ($data !== 0) {
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
    }

    $response = curl_exec($curl);

    if ($json == 0) {
        $json = $response;
    } else {
        $json = json_decode($response, true);
    }

    curl_close($curl);

    return $json;
}




//

}




 ?>