<?php
class email
{


public  function sendSMS($receiver, $message,$redirect) {
        $smsUN='#';
        $smsPW='#';
        $smsSA='MLM';
        $smsDA= $receiver;
        $smsMessage=$message;
        $smsRedirect = $redirect;


        return "http://mail.etextmail.com/smsapi/send.aspx?UN=" . $smsUN . "&P=" . $smsPW . "&SA=" . $smsSA . "&DA=" . $smsDA . "&M=" . $smsMessage . "&R=" . $smsRedirect;
         
    }

public function sendMessage($to,$subject,$cc,$body,$onSuccess,$onfail)
    {
	
        require_once APP_DIR."thirdparty/PHPMailer/PHPMailerAutoload.php";
        $mail = new PHPMailer;
        $mail->SMTPAuth = true;
        //* Abdi Joseph -  SMTP FIX !!!!
        $mail->SMTPOptions = array(
        'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );
        //End of Abdi Joseph
        $mail->isSMTP();
        $mail->SMTPDebug = false;
        $mail->do_debug = 0;
        $mail->Debugoutput = 'html';
        $mail->SMTPSecure = "tls";//tls/ssl
        $mail->Host = SMTPSERVER;
        $mail->Port = SMTP_PORT;
        $mail->Username = SYSTEM_EMAIL;
        $mail->Password = SYSTEM_EMAIL_PASSWORD;
        $mail->SetFrom(SYSTEM_EMAIL, 'BUMAA IMS');
        $mail->Subject = $subject;
        $mail->addAddress($to);
        if(!empty($cc))
        {
            foreach($cc as $email)
            {
            $mail->addCC($email);
            }
        }
        //$mail->addCC($AdminEmail);
        //$mail->addCC("alvinlove3@gmail.com");
        //Send HTML or Plain Text email
        $mail->isHTML(true);
        $mail->Body = $body;
        if(!$mail->send()) // Dont forget to add "!" condition
        {
            return $onfail;
        }
        else
        {
            return $onSuccess;
        }
    }



public function emailTemplate(array $data){

    return " ".$this->email_header()."
    <p>Dear Mr. ".$data['names'].",</p>
    <p>Your account was just logged in to from <strong>Uganda</strong>. You're getting this email to make sure it was you.</p>
    <table id='m_8249001960203836540alert' style='border-collapse:separate;text-align:left;line-height:40px;border-spacing:0;border:2px solid #3a3d3e;width:100%;margin:20px auto'>
         <thead>
              <tr>
                   <th colspan='2' style='padding:0 15px 0 20px;background:#3a3d3e;color:#fff;border:none'>Please review the details below</th>
              </tr>
         </thead>
         <tbody>
              <tr>
                   <td style='border-bottom:1px solid #ddd;text-align:left;padding-right:10px;padding:0 15px 0 20px'>Date</td>
                   <td style='border-bottom:1px solid #ddd;text-align:right;padding-right:10px;padding:0 15px 0 20px'>".$data['time']."</td>
              </tr>
              
              <tr>
                   <td style='border-bottom:1px solid #ddd;text-align:left;padding-right:10px;padding:0 15px 0 20px'>IP</td>
                   <td style='border-bottom:1px solid #ddd;text-align:right;padding-right:10px;padding:0 15px 0 20px'>".$data['ip_address']."</td>
              </tr>
              <tr>
                   <td style='border-bottom:1px solid #ddd;text-align:left;padding-right:10px;padding:0 15px 0 20px'>ISP</td>
                   <td style='border-bottom:1px solid #ddd;text-align:right;padding-right:10px;padding:0 15px 0 20px'>".$data['isp']."</td>
              </tr>
              <tr>
                   <td style='border-bottom:1px solid #ddd;text-align:left;padding-right:10px;padding:0 15px 0 20px'>Location</td>
                   <td style='border-bottom:1px solid #ddd;text-align:right;padding-right:10px;padding:0 15px 0 20px'>".$data['country']."</td>
              </tr>
              <tr>
                   <td style='border-bottom:1px solid #ddd;text-align:left;padding-right:10px;padding:0 15px 0 20px'>Browser</td>
                   <td style='border-bottom:1px solid #ddd;text-align:right;padding-right:10px;padding:0 15px 0 20px'>".$data['browser']."</td>
              </tr>
         </tbody>
    </table>
    <p>If this was you, you can ignore this alert. If you suspect any suspicious activity on your account, please change your password and enable two-factor authentication:</p>
    <p><a href='https://my.bumaa.com/my-account/account-settings' target='_blank' data-saferedirecturl='https://www.google.com/url?q=https://my.bumaa.com/my-account/account-settings&amp;source=gmail&amp;ust=1624607122186000&amp;usg=AFQjCNFwZoQIS0oMJWeHZhL43w-mEejYOQ'>https://my.bumaa.<wbr>com</a>
    </p>

    ".$this->email_footer()."
   ";

}

public function send_reset_token(array $data){

    return "
    ".$this->email_header()."
          
    <p>Hi, ".$data['email'].",</p>
    <p>You have just requested a reset token for your password <strong></strong>. You're getting this email to make sure  you are able to reset your password.</p>
    <table id='m_8249001960203836540alert' style='border-collapse:separate;text-align:left;line-height:40px;border-spacing:0;border:2px solid #3a3d3e;width:100%;margin:20px auto'>
         <thead>
              <tr>
                   <th colspan='2' style='padding:0 15px 0 20px;background:#3a3d3e;color:#fff;border:none'>Please use the details below</th>
              </tr>
         </thead>
         <tbody>
              <tr>
                   <td style='border-bottom:1px solid #ddd;text-align:left;padding-right:10px;padding:0 15px 0 20px'>Reset Token</td>
                   <td style='border-bottom:1px solid #ddd;text-align:right;padding-right:10px;padding:0 15px 0 20px'>
                    ".$data['token']."
                   </td>
              </tr>
         </tbody>
    </table>
  
      ".$this->email_footer()."

    ";
}

public function send_credentials(array $data){

    return "
    ".$this->email_header()."

     <p>Hi, ".$data['names'].",</p>
        <p>Please, use the following credentials to access the  System.</p>
        <table id='m_8249001960203836540alert' style='border-collapse:separate;text-align:left;line-height:40px;border-spacing:0;border:2px solid #3a3d3e;width:100%;margin:20px auto'>
         <thead>
              <tr>
                   <th colspan='2' style='padding:0 15px 0 20px;background:#3a3d3e;color:#fff;border:none'>Please use the details below</th>
              </tr>
         </thead>
         <tbody>
              <tr>
                   <td style='border-bottom:1px solid #ddd;text-align:left;padding-right:10px;padding:0 15px 0 20px'>username</td>
                   <td style='border-bottom:1px solid #ddd;text-align:right;padding-right:10px;padding:0 15px 0 20px'>
                    ".$data['username']."
                   </td>
              </tr>

               <tr>
                   <td style='border-bottom:1px solid #ddd;text-align:left;padding-right:10px;padding:0 15px 0 20px'>password</td>
                   <td style='border-bottom:1px solid #ddd;text-align:right;padding-right:10px;padding:0 15px 0 20px'>
                    ".$data['password']."
                   </td>
              </tr>
         </tbody>
        </table>
      
               
                <p><a href='".URL."accounts/login'>To login into your account, please follow this link. </a></p>

    ".$this->email_footer()."

    ";
}


public function send_admin_code_temp(array $data){

    return "
    ".$this->email_header()."
          
    <p>Hi, ".$data['email'].",</p>
    <p>You have been given an admin code by the Management <strong></strong>. You are getting this code in order to be able to access the Administration functionalities of the system.</p>
    <table id='m_8249001960203836540alert' style='border-collapse:separate;text-align:left;line-height:40px;border-spacing:0;border:2px solid #3a3d3e;width:100%;margin:20px auto'>
         <thead>
              <tr>
                   <th colspan='2' style='padding:0 15px 0 20px;background:#3a3d3e;color:#fff;border:none'>Please view the details below</th>
              </tr>
         </thead>
         <tbody>
              <tr>
                   <td style='border-bottom:1px solid #ddd;text-align:left;padding-right:10px;padding:0 15px 0 20px'>Admin Code</td>
                   <td style='border-bottom:1px solid #ddd;text-align:right;padding-right:10px;padding:0 15px 0 20px'>
                    ".$data['code']."
                   </td>
              </tr>
         </tbody>
    </table>
  
      ".$this->email_footer()."

    ";
}

public function send_profile_update(array $data){

    return "
    ".$this->email_header()."
          
    <p>Hi, ".$data['names'].",</p>
    <p>You have just updated your profile on the Bumaa system<strong></strong>. You're getting this email to make sure  it is you who updated your information.</p>
  
      ".$this->email_footer()."

    ";
}


public function send_newsletter_feedback(array $data){

    return "
    ".$this->email_header()."
          
    <p>Hi, ".$data['email'].",</p>
    <p>".(isset($data['message']) ? $data['message'] : "").",</p>
    <p>Thank you for subscribing to our newsletter, we shall send you the latest news feed and events</p>
  
      ".$this->email_footer()."

    ";
}


public function send_contact_feedback(array $data){

    return "
    ".$this->email_header()."
          
    <p>Hi, ".$data['names'].",</p>
    <p>Thank you for contacting the Bumaa management, we shall get back to you shortly</p>
  
      ".$this->email_footer()."

    ";
}



private function email_header(){
    return "
      <!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xmlns:v='urn:schemas-microsoft-com:vml' xmlns:o='urn:schemas-microsoft-com:office:office'>

<head>
<meta http-equiv='Content-type' content='text/html; charset=utf-8' />
<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1' />
<meta http-equiv='X-UA-Compatible' content='IE=edge' />
<meta name='format-detection' content='date=no' />
<meta name='format-detection' content='address=no' />
<meta name='format-detection' content='telephone=no' />
<meta name='x-apple-disable-message-reformatting' />
<!--[if !mso]><!-->
<link href='https://fonts.googleapis.com/css?family=Muli:400,400i,700,700i' rel='stylesheet' />
<!--<![endif]-->
<title>BUMAA IMS</title>
<style type='text/css' media='screen'>
/* Linked Styles */
body {
padding: 0 !important;
margin: 0 !important;
display: block !important;
min-width: 100% !important;
width: 100% !important;
background: #fff;
-webkit-text-size-adjust: none
}

a {
color: #66c7ff;
text-decoration: none
}
</style>

<body>
<div class=''>
<div class='aHl'></div>
<div id=':2y' tabindex='-1'></div>
<div id=':pc' class='ii gt'>
<div id=':pb' class='a3s aiL '>
<div class='adM'>
</div><u></u>
<div style='background:#edf0f2'>
<div style='background-color:#edf0f2'>
<div style='margin:0px auto;max-width:600px;background:#edf0f2'>
<table role='presentation' style='font-size:0px;width:100%;background:#edf0f2' cellspacing='0' cellpadding='0' border='0' align='center'>
<tbody>
<tr>
<td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px;padding:20px 0px;padding-top:30px'>

<div style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%'>
<table role='presentation' width='100%' cellspacing='0' cellpadding='0' border='0'>
<tbody>
<tr>
<td style='word-wrap:break-word;font-size:0px;padding:10px 25px' align='center'>
<table role='presentation' style='border-collapse:collapse;border-spacing:0px' cellspacing='0' cellpadding='0' border='0' align='center'>
<tbody>
<tr>
<td style='width:237px'>
<center><u></u>
</center>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</div>

</td>
</tr>
</tbody>
</table>
</div>


<div style='margin:0px auto;max-width:600px;background:#fff;border-top:3px solid #009900;'>
<table role='presentation' style='border:1px solid #dae1e9;font-size:0px;width:100%;background:#fff' cellspacing='0' cellpadding='0' border='0' align='center'>
<tbody>
<tr>
<td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px'>

<div style='margin:0px auto;max-width:600px'>
<table role='presentation' style='font-size:0px;width:100%' cellspacing='0' cellpadding='0' border='0' align='center'>
<tbody>
<tr>
<td style='text-align:center;vertical-align:top;direction:ltr;font-size:0px'>

<div style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%'>
<table role='presentation' width='100%' cellspacing='0' cellpadding='0' border='0'>
<tbody>

<tr>
<td style='word-wrap:break-word;font-size:0px;padding:20px 50px' align='left'>
<div style='color:#3a3d3e;font-family:Century Gothic,Lato,Arial,sans-serif;font-size:15px;font-weight:400;line-height:22px;text-align:left'>
    <img alt='' src='http://jobs.newsday.co.ug//static/assets2/images/logo.png' style='border:none;border-radius:0px;display:block;font-size:13px;outline:none;text-decoration:none;width:35%;height:auto' class='CToWUd' width='auto' height='auto'>
    <div style='border-bottom:1px solid #ddd;margin:20px 0px'></div>

    <p>&nbsp;</p>
    ";
}


private function email_footer(){

    return "
       <p>If you have any questions or concerns, don't hesitate to get in touch: support@bumaa.com </p>
    <p>
    </p>
    <p>Best,</p>
    <p><strong>Bumaa Management Team</strong></p>
    <p></p>
    <p></p>
    <p></p>
</div>
</td>
</tr>


</tbody>
</table>
</div>

</td>
</tr>
</tbody>
</table>
</div>

</td>
</tr>
</tbody>
</table>
</div>


<table role='presentation' style='font-size:0px;width:100%' cellspacing='0' cellpadding='0' border='0'>
<tbody>
<tr>
<td>
<div style='margin:0px auto;max-width:600px'>
<table role='presentation' style='font-size:0px;width:100%' cellspacing='0' cellpadding='0' border='0' align='center'>
<tbody>
<tr>
<td style='text-align:center;vertical-align:top;border-top:1px solid #edf0f2;direction:ltr;font-size:0px;padding:30px'>

<div style='vertical-align:top;display:inline-block;direction:ltr;font-size:13px;text-align:left;width:100%'>
<table role='presentation' width='100%' cellspacing='0' cellpadding='0' border='0'>
<tbody>

<tr>
<td style='word-wrap:break-word;font-size:0px;padding:10px 85px;padding-top:30px' align='center'>
<div style='color:#818585;font-family:Century Gothic,Lato,Arial,sans-serif;font-size:12px;font-weight:400;line-height:22px;text-align:center'>Port bell, Mukadde, Kampala - Uganda</div>
</td>
</tr>
</tbody>
</table>
</div>

</td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>

</div>
</div>

</div>

</body>

</html>
    ";
}






//end


}

?>
