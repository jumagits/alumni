 $(document).ready(function() {
     var Id, districtId, countyId, subCountyId, parishId, villageId;
     var districtId1, countyId1, subCountyId1, parishId1, villageId1;

     // $('#countySelect,#subCountySelect,#parishSelect,#villageSelect').prop('disabled',true);
     // DISTRICTS FILTER
     $('#districtSelect').on('change', function() {
         Id = $(this).val();
         $('#countySelect,#subCountySelect,#parishSelect,#villageSelect').html('');
         $('#countySelect,#subCountySelect,#parishSelect,#villageSelect').trigger("chosen:updated");
         rePopulateSelectedItem("counties", Id);
     });
     // COUNTIES FILTER
     $('#countySelect').on('change', function() {
         Id = $(this).val();
         console.log(Id);
         rePopulateSelectedItem("subcounties", Id);
     });
     // PARISH FILTER
     $('#subCountySelect').on('change', function() {
         Id = $(this).val();
         rePopulateSelectedItem("parishes", Id);
     });
     // VILLAGE FILTER
     $('#parishSelect').on('change', function() {
         Id = $(this).val();
         rePopulateSelectedItem("villages", Id);
     });
     //
     /*
      * function : RE-PopulateSelectedItem(@param1,@param2)
      */
     function rePopulateSelectedItem(fetchType, fetchId) {
         //console.log(fetchType+' - '+fetchId);
         if (fetchType == "counties") {
             $.get(APP_URL+"location-ajax/task-json/" + fetchId + "/counties", function(data, status) {
                 $('#countySelect').html(data.replace(/\\/g, ''));
                 console.log(data);
                //  $("#countySelect").trigger("chosen:updated");
             });
         } else if (fetchType == "subcounties") {
             $.get(APP_URL+"location-ajax/task-json/" + fetchId + "/subcounties", function(data, status) {
                 $('#subCountySelect').html(data.replace(/\\/g, ''));
                 console.log(data);
                //  $("#subCountySelect").trigger("chosen:updated");
             });
         } else if (fetchType == "parishes") {

             $.get(APP_URL+"location-ajax/task-json/" + fetchId + "/parishes", function(data, status) {
                 $('#parishSelect').html(data.replace(/\\/g, ''));
                 console.log(data);
                //  $("#parishSelect").trigger("chosen:updated");

             });
         } else if (fetchType == "villages") {

             $.get(APP_URL+"location-ajax/task-json/" + fetchId + "/villages", function(data, status) {
                 $('#villageSelect').html(data.replace(/\\/g, ''));
                 console.log(data);
                //  $("#villageSelect").trigger("chosen:updated");
             });
         }
     }


     // $('#countySelect,#subCountySelect,#parishSelect,#villageSelect').prop('disabled',true);
     // DISTRICTS FILTER
     $('#districtSelect1').on('change', function() {
        Id = $(this).val();
        $('#countySelect1,#subCountySelect1,#parishSelect1,#villageSelect1').html('');
        $('#countySelect1,#subCountySelect1,#parishSelect1,#villageSelect1').trigger("chosen:updated");
        rePopulateSelectedItem1("counties", Id);
    });
    // COUNTIES FILTER
    $('#countySelect1').on('change', function() {
        Id = $(this).val();
        console.log(Id);
        rePopulateSelectedItem1("subcounties", Id);
    });
    // PARISH FILTER
    $('#subCountySelect1').on('change', function() {
        Id = $(this).val();
        rePopulateSelectedItem1("parishes", Id);
    });
    // VILLAGE FILTER
    $('#parishSelect1').on('change', function() {
        Id = $(this).val();
        rePopulateSelectedItem1("villages", Id);
    });
    //
    /*
     * function : RE-PopulateSelectedItem(@param1,@param2)
     */
    function rePopulateSelectedItem1(fetchType, fetchId) {
        //console.log(fetchType+' - '+fetchId);
        if (fetchType == "counties") {
            $.get(APP_URL+"location-ajax/task-json/" + fetchId + "/counties", function(data, status) {
                $('#countySelect1').html(data.replace(/\\/g, ''));
                console.log(data);
               $("#countySelect1").trigger("chosen:updated");
            });
        } else if (fetchType == "subcounties") {
            $.get(APP_URL+"location-ajax/task-json/" + fetchId + "/subcounties", function(data, status) {
                $('#subCountySelect1').html(data.replace(/\\/g, ''));
               $("#subCountySelect1").trigger("chosen:updated");
            });
        } else if (fetchType == "parishes") {

            $.get(APP_URL+"location-ajax/task-json/" + fetchId + "/parishes", function(data, status) {
                $('#parishSelect1').html(data.replace(/\\/g, ''));
               $("#parishSelect1").trigger("chosen:updated");

            });
        } else if (fetchType == "villages") {

            $.get(APP_URL+"location-ajax/task-json/" + fetchId + "/villages", function(data, status) {
                $('#villageSelect1').html(data.replace(/\\/g, ''));
               $("#villageSelect1").trigger("chosen:updated");
            });
        }
    }

 });