const barChartData = {
    labels: <?php echo isset($labels) ?  $labels : ''; ?>,
    datasets: [{
        label: 'No of X',
        fillColor: "rgba(220,220,120,0.5)",
        strokeColor: "rgba(220,220,220,143)",
        data: [65, 59, 90, 81, 56, 55]
    }, {
        label: 'No of Y',
        fillColor: "rgba(151,187,205,1.0)",
        strokeColor: "rgba(151,187,205,1)",
        data: [28, 48, 40, 19, 96, 100]
    }]

};


const barOptions = {
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true
            }
        }]
    }
}

//bar chart init
const myChart = new Chart(document.getElementById('clients'), {
    type: 'bar',
    data: barChartData,
    options: barOptions
});