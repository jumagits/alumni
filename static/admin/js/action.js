     jQuery(document).ready(function($) {

    

              $('select').selectpicker({
                   dropupAuto: false,
                   liveSearch: true,
                   showTick: true,
                 //  header: true,
              });

         //      $('.count').each(function() {
         //           $(this).prop('Counter', 0).animate({
         //                Counter: $(this).text()
         //           }, {
         //                duration: 1000,
         //                easing: 'swing',
         //                step: function(now) {
         //                     $(this).text(Math.ceil(now));
         //                }
         //           });
         //      });

     });





     jQuery(document).ready(function($) {


   get_age =  function()
    {
        var dateString = $('input[name="dob"]').val();
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        //console.log( age );

        if(age >= 1 && age <= 17){
            $('#ag').val("1-17");
        }

        if(age >= 18 && age <= 35){
            $('#ag').val("18-35");
        }

        if(age >= 36 && age <= 50){
            $('#ag').val("36-50");
        }

        if(age > 50)
        {
            $('#ag').val("above 50");
        }


    }


         makeDefault = function($default_role, $default_clinic) {
             var default_clinic = $default_clinic;
             var default_role = $default_role;

             $.ajax({
                 url: APP_URL + 'user_accounts/default_role/',
                 data: {

                     default_role: default_role,
                     default_clinic: default_clinic
                 },
                 type: 'GET',
                 dataType: 'json',
                 success: function(res) {

                     if (res.status) {
                         swal({
                             title: "Good job!",
                             padding: 20,
                             text: "Default Role Changed Successfully!",
                             type: "success"
                         });

                         setTimeout(function() {
                             window.location.reload();
                         }, 1000);

                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     //console.log(jqXHR);
                 }
             });
         }


         $("form#makeJob").submit(function(e) {

             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#makeJob').attr('action');

             $.ajax({
                     url: action,
                     type: 'POST',
                     data: formData,
                     success: function(res) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         })

                         window.location.reload();
                     },
                     error: function(jqXHR, textStatus, errorThrown) {
                         console.log(jqXHR);
                     },
                     cache: false,
                     contentType: false,
                     processData: false
                 })
                 .done(function() {
                     console.log("success");
                 })
                 .fail(function() {
                     console.log("error");
                 })
                 .always(function() {
                     console.log("complete");
                 });

         });

     });

     //checkboxes
     $(document).ready(function() {

         $('#selectAllcheckboxes').click(function(event) {

             if (this.checked) {

                 $('.checkboxes').each(function() {

                     this.checked = true;

                 });

             } else {

                 $('.checkboxes').each(function() {

                     this.checked = false;

                 });
             }

         });

     });

     //
     jQuery(document).ready(function($) {

         $("form#add_post").submit(function(e) {

             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#add_post').attr('action');

             $.ajax({
                     url: action,
                     type: 'POST',
                     data: formData,
                     success: function(res) {

                         console.log(res);

                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         })

                         setTimeout(function() {

                             window.location.reload();

                         }, 5000)
                     },
                     error: function(jqXHR, textStatus, errorThrown) {
                         console.log(jqXHR);
                     },
                     cache: false,
                     contentType: false,
                     processData: false
                 })
                 .done(function() {
                     console.log("success");
                 })
                 .fail(function() {
                     console.log("error");
                 })
                 .always(function() {
                     console.log("complete");
                 });

         });

     });

     $(document).ready(function() {


         $('#selectAllcheckboxes').click(function(event) {

             if (this.checked) {

                 $('.checkboxes').each(function() {

                     this.checked = true;

                 });

             } else {

                 $('.checkboxes').each(function() {

                     this.checked = false;

                 });
             }

         });

         //adding categories using ajax program

         $("form#add-cat").submit(function(e) {

             e.preventDefault();

             var data = $('input[name="cat_title"]').val();

             var action = $('form#add-cat').attr('action');

             $.ajax({
                 url: action,
                 type: 'POST',
                 dataType: 'json',
                 data: {
                     cat_title: data
                 },
                 beforeSend: function() {
                     swal({
                         title: "Please wait adding your Activity!",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     })
                 },
                 success: function(res) {

                     console.log(res);

                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             //history.back();
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }

                 }
             });

         });

         //adding jobs

         //registering members

         $("form#registerclientform").submit(function(e) {

             e.preventDefault();

             var formData = new FormData(this);

             var uRO = APP_URL + "register-clients/med";
             console.log(formData);
             $.ajax({
                 url: uRO,
                 type: 'POST',
                 dataType: 'json',
                 cache: false,
                 contentType: false,
                 processData: false,
                 data: formData,
                 success: function(res) {

                     console.log(res);

                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             //history.back();
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }

                 }
             });

         });

    
         function fill(value) {
             $('#search-input').val(Value);
             $('#display').hide();
         }

   

         $("form#editclientform").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#editclientform').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait am updating information!",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             //history.back();
                             setTimeout(function() {

                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         //form for updating user information
         $("form#updateUserInfo").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#updateUserInfo').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait am updating User information!",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             //history.back();
                             setTimeout(function() {
                                 // window.location = APP_URL + 'user_accounts/users';
                                 window.location.reload();
                             }, 3000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         //form MMT for backlog <?php echo URL; ?>case-backlog/task_mmt_backlog

         $("form#mmt_backlog").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#mmt_backlog').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait preparing Backlog MMT information!",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     }).then(function() {

                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });




         $("form#adduser").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#adduser').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait preparing user information!",
                         text: "I will close in a few seconds.",
                         timer: 6e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     }).then(function() {

                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });
         ////////////////////////////////////////////////////////////////////////////
         ///////////////////////////////////////////////////////////////////////////

         $("form#addcat").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#addcat').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait adding new user category!",
                         text: "I will close in a few seconds.",
                         timer: 6e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     }).then(function() {

                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         $("form#addActs").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#addActs').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait adding your Activity!",
                         text: "I will close in a few seconds.",
                         timer: 6e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     }).then(function() {

                     });
                 },
                 success: function(resAct) {
                     console.log(resAct);
                     if (resAct.status) {
                         swal({
                             title: "Good job!",
                             text: resAct.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: resAct.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         $("form#addclinic").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#addclinic').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait adding new clinic!",
                         text: "I will close in a few seconds.",
                         timer: 6e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     }).then(function() {

                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         $("form#switchclinic").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#switchclinic').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait switching clinic!",
                         text: "I will close in a few seconds.",
                         timer: 6e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     }).then(function() {

                     });
                 },
                 success: function(reswitch) {
                     console.log(reswitch);
                     if (reswitch.status) {
                         swal({
                             title: "Good job!",
                             text: reswitch.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.href = ''
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: reswitch.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });
         $("form#updateUser").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#updateUser').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait updating user information!",
                         text: "I will close in a few seconds.",
                         timer: 6e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     }).then(function() {

                     });
                 },
                 success: function(resup) {
                     console.log(resup);
                     if (resup.status) {
                         swal({
                             title: "Good job!",
                             text: resup.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: resup.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         $("form#updatePass").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#updatePass').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait changing Password!",
                         text: "I will close in a few seconds.",
                         timer: 6e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     }).then(function() {

                     });
                 },
                 success: function(res_pass) {
                     console.log(res_pass);
                     if (res_pass.status) {
                         swal({
                             title: "Good job!",
                             text: res_pass.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res_pass.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         /////////////////////////////////////////////////////////////////////////////
         ////////////////////////deact and act users//////////////////////////////////
         $("a#sa-deact").click(function() {
             swal({
                 title: "Are you sure?",
                 text: "You want to Activate/Deactivate User !",
                 type: "warning",
                 showCancelButton: !0,
                 confirmButtonText: "Yeseeee !",
                 cancelButtonText: "No, cancel!",
                 confirmButtonClass: "btn btn-success",
                 cancelButtonClass: "btn btn-danger m-l-10",
                 buttonsStyling: !1
             }).then(function() {
                 swal("Deactivated!", "User Deactivated.", "success")
             }, function() {
                 "cancel" === t && swal("Cancelled", "User Not Deactivated :)", "error")
             })
         });
         ////////////////////////////////////////////////////////////////////////////

         $("form#addclinicaccessrole").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#addclinicaccessrole').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait preparing user access role information!",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     }).then(function() {

                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });


         $("form#assign_form").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#assign_form').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait preparing to assign case to the selected Legal Assistance / Advocate!",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             history.back();
                             // setTimeout(function() {

                             //     window.location.reload();
                             // }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         $("form#reg_form").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#reg_form').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait preparing to register case information!",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             history.back();
                             // setTimeout(function() {

                             //     //window.location.reload();
                             // }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         $("form#adr_update_form").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#adr_update_form').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait preparing to update case information!",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             //history.back();
                             setTimeout(function() {

                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });



         $("form#eshare_form").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#eshare_form').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait preparing to share document!",
                         text: "I will close in a few seconds.",
                         timer: 10e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             //history.back();
                             setTimeout(function() {

                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });






         $("form#doc_form").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#doc_form').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait preparing document for  upload!",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             //history.back();
                             setTimeout(function() {

                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         $("form#transferClient").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#transferClient').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait transfering Client",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             //history.back();
                             setTimeout(function() {

                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });

         $("form#photo_form").submit(function(e) {
             e.preventDefault();
             var formData = new FormData(this);
             var action = $('form#photo_form').attr('action');
             console.log(formData);
             $.ajax({
                 url: action,
                 type: 'POST',
                 data: formData,
                 beforeSend: function() {
                     swal({
                         title: "Please wait preparing change client photo!",
                         text: "I will close in a few seconds.",
                         timer: 2e3,
                         onOpen: () => {
                             swal.showLoading()
                         }
                     });
                 },
                 success: function(res) {
                     console.log(res);
                     if (res.status) {
                         swal({
                             title: "Good job!",
                             text: res.msg,
                             type: "success",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             //history.back();
                             setTimeout(function() {

                                 window.location.reload();
                             }, 1000);
                         });
                     } else {
                         swal({
                             title: "Oops something went wrong.!",
                             text: res.msg,
                             type: "warning",
                             showCancelButton: !0,
                             confirmButtonClass: "btn btn-success",
                             cancelButtonClass: "btn btn-danger m-l-10"
                         }).then(function() {
                             setTimeout(function() {
                                 window.location.reload();
                             }, 1000);
                         });
                     }
                 },
                 error: function(jqXHR, textStatus, errorThrown) {
                     console.log(jqXHR);
                 },
                 cache: false,
                 contentType: false,
                 processData: false
             });
         });









     });