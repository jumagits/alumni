jQuery(document).ready(function($) {


    $("form#addTopic").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        var action = APP_URL + "forum/createTopic";
        $.ajax({
            url: action,
            type: 'POST',
            data: formData,
            success: function(res) {
                alert(res)
                    setTimeout(function() {
                        window.location.reload();
                    }, 3000)
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            cache: false,
            contentType: false,
            processData: false
        })


    });


     $("form#addCategory").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        var action = APP_URL + "forum/addCategory";
        $.ajax({
            url: action,
            type: 'POST',
            data: formData,
            success: function(res) {
                //console.log(res);
               
                         alert(res)
                    setTimeout(function() {
                        window.location.reload();
                    }, 3000)
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            cache: false,
            contentType: false,
            processData: false
        })


    });


     //comment

      $("form#comment").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        var action = APP_URL + "forum/comment";
        $.ajax({
            url: action,
            type: 'POST',
            data: formData,
            success: function(res) {
                //console.log(res);
               
                      alert(res)
                    setTimeout(function() {
                        window.location.reload();
                    }, 3000)
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            cache: false,
            contentType: false,
            processData: false
        })


    });


     //update cat  


  $("form#categoryEditForm").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        var action = APP_URL + "forum/updateCategory";
        $.ajax({
            url: action,
            type: 'POST',
            data: formData,
            success: function(res) {
                //console.log(res);
               alert(res)
                    setTimeout(function() {
                        window.location.reload();
                    }, 3000)
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            cache: false,
            contentType: false,
            processData: false
        })


    });


  // updateTopic

    $("form#topicEditForm").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(this);
        var action = APP_URL + "forum/updateTopic";
        $.ajax({
            url: action,
            type: 'POST',
            data: formData,
            success: function(res) {
                //console.log(res);
               
                       alert(res)
                    setTimeout(function() {
                        window.location.reload();
                    }, 3000)
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
            },
            cache: false,
            contentType: false,
            processData: false
        })


    });

  
    //end of methods
});