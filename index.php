<?php
/**
 *  This file doesn't do anything, but loads app-init.php.
 *
 */
// Defines
define('ROOT_DIR', realpath(dirname(__FILE__)) .'/');
define('APP_DIR', ROOT_DIR .'application/');

/** Loads the core system  */
require( dirname( __FILE__ ) . '/system/app-init.php' );


// background-color: transparent;
// background-image: linear-gradient(90deg, #F01165 0%, #170A92 100%);