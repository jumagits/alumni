<?php

class init
{
    public function __construct()
    {
        global $config;
       
        global $app;

        // Set our defaults
        $controller = $config['default_controller'];
        $action = 'index';
        $url = '';

        // Get request url and script url
        $request_url = (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
        $script_url  = (isset($_SERVER['PHP_SELF'])) ? $_SERVER['PHP_SELF'] : '';

        // Get our url path and trim the / of the left and the right
        if($request_url != $script_url) $url = trim(preg_replace('/'. str_replace('/', '\/', str_replace('index.php', '', $script_url)) .'/', '', $request_url, 1), '/');

        // Split the url into segments
        $segments = explode('/', $url);

        // Do our default checks
        if(isset($segments[0]) && $segments[0] != '') $controller = $segments[0];
        if(isset($segments[1]) && $segments[1] != '') $action = $segments[1];


        // Date : 23th - 07 - 2016
        // Added NEW REWRITE RULE By Dennis Mariano
        if(isset($segments[0]) && $segments[0] != '')if(preg_match('/-/',$segments[0])){$controller = str_replace("-","_",$segments[0]);}
        if(isset($segments[1]) && $segments[1] != '')if(preg_match('/-/',$segments[1])){$action = str_replace("-","_",$segments[1]);}
        // END REWITE RULE

        //var_dump($route);


        // Get our controller file
        $path = APP_DIR . 'controllers/' . $controller . '.php';
        if(file_exists($path)){
            require_once($path);
        } else {
            $controller = $config['error_controller'];
            require_once(APP_DIR . 'controllers/' . $controller . '.php');
            // Create object and call method
        }

        // Check the action exists
        if(!method_exists($controller, $action)){
            //$controller = $config['error_controller']; // Disabling that enables the index controller to fallinto play
            require_once(APP_DIR . 'controllers/' . $controller . '.php');
            $action = 'index';

            // Create object and call method
            $obj = new $controller;
            die(call_user_func_array(array($obj, $action), array_slice($segments, 1)));
        }
        else
        {
            $obj = new $controller;
            die(call_user_func_array(array($obj, $action), array_slice($segments, 2)));
        }

        /*$obj = new $controller;
        die(call_user_func_array(array($obj, $action), array_slice($segments, 2)));
        */

    }
}
?>
