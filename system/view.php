<?php

class View {

	private $pageVars = array();
	private $template;

	public function __construct($template)
	{
        if(preg_match("/\\//",$template))
        {
            $template_src = explode("/",$template);
            $template_dir = $template_src[0];
            $template_file = $template_src[1];

           $this->template = APP_DIR ."views/{$template_dir}/{$template_file}.php";
           //require_once $this->template;
        }
        else
        {
            $this->template = APP_DIR ."views/{$template}.php";

            //require_once $this->template;
        }

	}
	public function set($var, $val)
	{
		$this->pageVars[$var] = $val;
	}

	public function render()
	{
		extract($this->pageVars);

		ob_start();
		require $this->template;
		echo ob_get_clean();
	}
    public function fullpath($p='')
    {
        $path = str_replace('\\', '/',$p);
        return $path;
    }
    
}

?>