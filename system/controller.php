<?php

class Controller {

    public function __construct()
    {
        global $config;


        if($config['licensing'])
        {
            $padl = new Padl\License(false,true,true,true);
            $server_array = $_SERVER;
            $padl->setServerVars($server_array);
            $initModel = $this->loadModel('initModel');
            $loadedKey = $initModel->loadLicense();
            $key_status = $padl->validate($loadedKey);
            var_dump($key_status);
            //echo $this->license_results($key_status);
        }

    }
    public function loadModel($name)
	{
		//require(APP_DIR .'models/'. strtolower($name) .'.php');
        require_once(APP_DIR ."models/{$name}.php");

		$model = new $name;
		return $model;
	}
	
	public function loadView($name)
	{
		$view = new View($name);
		return $view;

	}
	
	public function loadPlugin($name)
	{
		require(APP_DIR ."plugins/{$name}.php");
	}
	
	public function loadHelper($name)
	{
		require_once(APP_DIR ."helpers/{$name}.php");
		$helper = new $name;
		return $helper;
	}
	
	public function redirect($loc)
	{
		global $config;
		
		header('Location: '. $config['base_url'] . $loc);
	}
    public function bootstrap()
    {
        // Load User Model
        $a = $this->loadModel('accountsModel');
        $admin = $a->restrict_access();
        return $admin;  
    }
    public function doCurrencyConversion($amount,$from,$to)
    {
        $a = $this->loadModel('initModel');
        return $a->doCurrencyConversion($amount,$from,$to);

    }
    public function appSettings()
    {
        $a = $this->loadModel('bootstrapModel');
        $loadedSettings = $a->loadSettings();
        return $loadedSettings;
    }

    
}

?>