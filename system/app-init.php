<?php
ob_start();
//Start the Session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

// Includes
require(APP_DIR .'config/config.php');
require(ROOT_DIR .'system/model.php');
require(ROOT_DIR .'system/view.php');
require(ROOT_DIR .'system/controller.php');
require(ROOT_DIR .'system/license.php');
require(ROOT_DIR . 'system/init.php');

// Define base URL
global $config;
define('BASE_URL', $config['base_url']);

$app = new init();